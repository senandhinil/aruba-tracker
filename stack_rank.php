<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';

if($_POST['csatgreen_weightage'] || $_POST['closure_weightage'] || $_POST['softskill_weightage'] || $_POST['processquality_weightage'] || $_POST['escalation_weightage'] || $_POST['selecttype'] || $_POST['calendartype']){
	$csatgreen_weightage 	= 	$_POST['csatgreen_weightage'];
	$red_weightage 	= 	$_POST['red_weightage'];
	$closure_weightage 	= 	$_POST['closure_weightage'];
	$ttc_weightage 	= 	$_POST['ttc_weightage'];
	$softskill_weightage   	=	$_POST['softskill_weightage'];
	$processquality_weightage 	= 	$_POST['processquality_weightage'];
	$escalation_weightage	= 	$_POST['escalation_weightage'];
	$atten_weightage	= 	$_POST['atten_weightage'];
	$backlog_weightage	= 	$_POST['backlog_weightage'];
	$selecttype	    = 	$_POST['selecttype'];
	$calendartype = $_POST['calendartype']; 
	$selectrange    =	$_POST['period'];
	$ltype = $_POST['ltype'];
	$productgroup   	=	$_POST['productgroup'];
}else{
	$csatgreen_weightage    =	!empty($csatgreen_weightage)?$csatgreen_weightage:"25";
	$red_weightage    =	!empty($red_weightage)?$red_weightage:"0";
	$closure_weightage 	=	!empty($closure_weightage)?$closure_weightage:"35";
	$ttc_weightage 	= 	!empty($ttc_weightage)?$ttc_weightage:"0";
	$softskill_weightage 	=	!empty($softskill_weightage)?$softskill_weightage:"15";
	$processquality_weightage 	=	!empty($processquality_weightage)?$processquality_weightage:"25";
	$escalation_weightage 	=	!empty($escalation_weightage)?$escalation_weightage:"-10";
	$atten_weightage 	=	!empty($atten_weightage)?$atten_weightage:"-10";
	$backlog_weightage 	=	!empty($backlog_weightage)?$backlog_weightage:"0";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Monthly";
	$calendartype = !empty($calendartype)?$calendartype:"Normal";
	$currentmonth= $commonobj->getQry("SELECT distinct calendar_month from aruba_stackrank_engg_list order by calendar_month desc limit 0,1");
	$selectrange	=	!empty($selectrange)?$selectrange:$currentmonth[0]['calendar_month'];
	$ltype = !empty($ltype)?$ltype:"l1";
	$productgroup   	=	!empty($productgroup)?$productgroup:"Overall";
}
$total_weightage = (int)$csatgreen_weightage+(int)$closure_weightage+(int)$softskill_weightage+(int)$processquality_weightage+(int)$red_weightage+(int)$ttc_weightage;
/* if(isset($_POST['calendartype']))
{ */
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}

	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
/* } */
	$productName = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct product_group from aruba_stackrank_engg_list Where product_group != '' AND $type$selectQry='$selectrange'"),"","product_group");
	array_unshift($productName,"Overall");
	if(in_array($productgroup,$productName)){
		$productgroup = $productgroup;
	}else{
		$productgroup = "Overall";
	}
	
	$QryCondition = $productgroup!='Overall'?" and product_group='".$productgroup."'":'';
	
	//Engg. Name list
	$headerenggArr = $commonobj->arrayColumn($commonobj->getQry("select distinct engg_name from aruba_stackrank_engg_list where role = '$ltype' AND $type$selectQry='$selectrange' $QryCondition"),'engg_name','engg_name');
	
	//Employee Id
	$emplyId = $commonobj->arrayColumn($commonobj->getQry("select distinct emp_id,engg_name,team from aruba_stackrank_engg_list"),'engg_name','emp_id');
	
	//Target Declaration
	$closurecntArr = $commonobj->arrayColumn($commonobj->getQry('SELECT count(*) as closed_case_count,'.$type.$selectQry.' FROM `aruba_closure` where '.$type.$selectQry.'="'.$selectrange.'" AND case_owner in ("'.implode('","',$headerenggArr).'")'),$type.$selectQry,'closed_case_count');
	if($ltype == 'l1')
	{
		$green_target = empty($_POST['green_target'])?80:$_POST['green_target'];
		$red_target = empty($_POST['red_target'])?0:$_POST['red_target'];
		$closures_target = empty($_POST['closures_target'])?round($closurecntArr[$selectrange]/count($headerenggArr)):$_POST['closures_target'];
		$ttc_target = empty($_POST['ttc_target'])?80:$_POST['ttc_target'];
		$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
		$pq_target = empty($_POST['pq_target'])?90:$_POST['pq_target'];
		$esc_target = empty($_POST['esc_target'])?0:$_POST['esc_target'];
		$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
		$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
	}elseif($ltype == 'l2'){
		$green_target = empty($_POST['green_target'])?80:$_POST['green_target'];
		$red_target = empty($_POST['red_target'])?0:$_POST['red_target'];
		$closures_target = empty($_POST['closures_target'])?round($closurecntArr[$selectrange]/count($headerenggArr)):$_POST['closures_target'];
		$ttc_target = empty($_POST['ttc_target'])?80:$_POST['ttc_target'];
		$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
		$pq_target = empty($_POST['pq_target'])?90:$_POST['pq_target'];
		$esc_target = empty($_POST['esc_target'])?0:$_POST['esc_target'];
		$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
		$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
	}
	
	$sel_engg = $conn->prepare('select distinct engg_name, team, product_group from aruba_stackrank_engg_list where '.$type.$selectQry.'="'.$selectrange.'" order by engg_name asc');
	$sel_engg->execute();
	$sel_enggArr = $sel_engg->fetchAll(PDO::FETCH_ASSOC);
	foreach($sel_enggArr as $engg_name){
		//$headerenggArr[$engg_name['case_owner']]=$engg_name['case_owner'];
		$enggteamArr[$engg_name['engg_name']]=$engg_name['engg_name'];
		$enggprojetcArr[$engg_name['engg_name']]=$engg_name['team'];
		$enggproductArr[$engg_name['engg_name']]=$engg_name['product_group'];
	}
	
	$workingdaysArr = $commonobj->arrayColumn($commonobj->getQry('select working_days,'.$type.$selectQry.' from aruba_headcount where '.$type.$selectQry.' = "'.$selectrange.'" group by '.$type.$selectQry),$type.$selectQry,'working_days');
	$workingdays = $workingdaysArr[$selectrange];
	
	//tenure, bucket wise
	$engg_tenuArr = $commonobj->getQry('select emp_name,doj from aruba_tenure where emp_name in ("'.implode('","',$headerenggArr).'")');
	foreach($engg_tenuArr as $tenure){
		$date1 = date('Y-m-d H:i:s', strtotime($tenure['doj']));
		$date2 = date('Y-m-d H:i:s');
		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);
		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);
		
		if($tenure['doj'] != '-' || $tenure['doj'] != ''){
			$years = $year2 - $year1;
			$months = $month2 - $month1;
			$diff = (($years) * 12) + ($months);
			$tenureArr[$tenure['emp_name']] = $diff;
			$bucketArr[$tenure['emp_name']] = $years." Years,".$months." Months";
		}else{
			$tenureArr[$tenure['emp_name']] = "NA";
			$bucketArr[$tenure['emp_name']] = "NA";
		}
	}
	
	//CSAT Green, Red, Survey Count
	$csatsurveyArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as csat_count, case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','csat_count');
	
	$csatgreencntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as green, case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Green" group by case_owner'),'case_owner','green');
	
	$csatredcntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as red, case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Red" and client_exception !="Yes" group by case_owner'),'case_owner','red');
	
	foreach($csatsurveyArr as $engg_name => $survey_count){
		$csatgreenArr[$engg_name] = ($csatgreencntArr[$engg_name]/$survey_count)*100;
		$csatredArr[$engg_name] = ($csatredcntArr[$engg_name]/$survey_count)*100;
	}
	foreach($csatgreenArr as $engg_name => $csatgreen)
	{
		$plvarval = plcalculate($csatgreen);
		$greenplArr[$engg_name] = $plvarval;
	}
	
	
	//Productivity, closures
	$engg_pc = $conn->prepare('select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_pc->execute();
	$engg_pcArr = $engg_pc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_pcArr as $pc){
		$closuresArr[$pc['case_owner']] = $pc['tot_count'];
		$productivityArr[$pc['case_owner']] = $pc['tot_count']/$workingdays;
	}
	//TTC less than 6 days
	//echo 'select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "6" group by case_owner';
	$engg_ttc = $conn->prepare('select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "6" group by case_owner');
	$engg_ttc->execute();
	$engg_ttcArr = $engg_ttc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ttcArr as $ttc){
		$ttcArr[$ttc['case_owner']] = $ttc['tot_count'];
	}
	//print_r($ttcArr);
	//TTC Overall
	//echo 'select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner';
	$engg_ttc_overall = $conn->prepare('select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_ttc_overall->execute();
	$engg_ttc_overallArr = $engg_ttc_overall->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ttc_overallArr as $ttc_overall){
		$ttc_overallArr[$ttc_overall['case_owner']] = $ttc_overall['tot_count'];
	}
	
	foreach($ttcArr as $key => $ttc_val){
		$ttc_cal_val = round(($ttc_val/$ttc_overallArr[$key])*100);
		$ttc_percent[$key] = $ttc_cal_val;
		$ttc_percent_pl[$key] = plcalculate_ttc($ttc_cal_val);
	}
	//print_r($ttc_percent);
	//print_r($ttc_overallArr);
	//print_r($productivityArr);
	foreach($closuresArr as $engg_name => $closures){
		$closurepercen = (($closures/$closures_target)*100)> 121?121:($closures/$closures_target)*100;
		$closurepercentageArr[$engg_name] = $closurepercen;
	}
	foreach($closurepercentageArr as $engg_name => $closurepercent)
	{
		$plvarval = plcalculate($closurepercent);
		$closureplArr[$engg_name] = $plvarval;
	}
	//Open Cases
	$engg_oc = $conn->prepare('select count(*) as open_count,case_owner from aruba_open where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_oc->execute();
	$engg_ocArr = $engg_oc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ocArr as $oc){
		$opencasesArr[$oc['case_owner']] = $oc['open_count'];
	}
	//Soft Skill
	$softskillsArr = $commonobj->arrayColumn($commonobj->getQry('select avg(overall) as ss,case_owner from aruba_ssqa where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','ss');
	
	foreach($softskillsArr as $engg_name => $sspercent)
	{
		$ssplvarval = plcalculate($sspercent);
		$ssplArr[$engg_name] = $ssplvarval;
	}
	//Process Quality
	$processqualityArr = $commonobj->arrayColumn($commonobj->getQry('select avg(overall) as process_quality,case_owner from aruba_process_audit where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','process_quality');
	
	foreach($processqualityArr as $engg_name => $pcval)
	{
		$pqplvarval = plcalculate($pcval);
		$pqplArr[$engg_name] = $pqplvarval;
	}
	//Escalation
	//echo 'select count(*) as esc_count, case_owner from aruba_esc where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner order by case_owner asc';
	$escalationArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as esc_count, case_owner from aruba_esc where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" and client_exception !="Yes" group by case_owner order by case_owner asc'),'case_owner','esc_count');
	//print_r($escalationArr);
	
	//Attendance
	$overallout = $commonobj->arrayColumn($commonobj->getQry('select case_owner,count(*) as overalloutcnt from aruba_avaya_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" AND time_to_sec(total_time) >=  "18000" group by case_owner'),'case_owner','overalloutcnt');
	
	//Backlog
	if($selecttype == "Monthly" && $selectrange == "2017Month11") {
		$backlogArr = $commonobj->arrayColumn($commonobj->getQry('select (sum(age)/count(*)) as avg,case_owner from aruba_backlog_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
	}elseif($selecttype == "Quarterly" && $selectrange == "2017Q4"){
		$backlogArr1 = $commonobj->arrayColumn($commonobj->getQry('select case_owner,avg from aruba_backlog_new where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
		$backlogArr2 = $commonobj->arrayColumn($commonobj->getQry('select (sum(age)/count(*)) as avg,case_owner from aruba_backlog_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
		foreach($backlogArr1 as $case_owner => $avg1)
		{
			$avg = ($avg1 + $backlogArr2[$case_owner])/2;
			$backlogArr[$case_owner] = $avg;
		}
	}else{
		$backlogArr = $commonobj->arrayColumn($commonobj->getQry('select case_owner,avg from aruba_backlog_new where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
	}
	//echo 'select case_owner,avg from aruba_backlog_new where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner';
	
	//$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry('select case_owner,count(distinct case_owner) as outcnt from aruba_avaya_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" AND time_to_sec(total_time) >=  "18000" group by case_owner'),'case_owner','outcnt');
	
	//Backlog
	/* $overallbacklogcntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as overallbacklog from aruba_backlog where '.$type.$selectQry.' = "'.$selectrange.'" '),$type.$selectQry,'overallbacklog');
	$overallbacklogcnt = $overallbacklogcntArr[$selectrange];
	
	$overallbacklogArr = $commonobj->arrayColumn($commonobj->getQry('select case_owner,count(*) as backlogcnt from aruba_backlog where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','backlogcnt');
	
	foreach($overallbacklogArr as $engg_name => $backlog){
		$backlogArr[$engg_name] = round(($backlog/$overallbacklogcnt)*100,2);
	} */
	
	function plcalculate($plval){
		if($plval >= 0 && $plval < 76)
		{
			$plvar = "PL1";
		}elseif($plval >= 76 && $plval < 96)
		{
			$plvar = "PL2";
		}elseif($plval >= 96 && $plval < 106)
		{
			$plvar = "PL3";
		}elseif($plval >= 106 && $plval < 121)
		{
			$plvar = "PL4";
		}elseif($plval >= 121)
		{
			$plvar = "PL5";
		}
		return $plvar;
	}
	
	function plcalculate_ttc($plval){
		if($plval >= 0 && $plval <= 69)
		{
			$plvar = "PL1";
		}elseif($plval >= 70 && $plval <= 79)
		{
			$plvar = "PL2";
		}elseif($plval >= 80 && $plval <= 85)
		{
			$plvar = "PL3";
		}elseif($plval >= 86 && $plval <= 95)
		{
			$plvar = "PL4";
		}elseif($plval >= 96)
		{
			$plvar = "PL5";
		}
		return $plvar;
	}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	/*.form-control {
	    border: 0px solid #c2cad8 !important;
	}*/
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
th.thead{
	background-color:#F2784B !important;
	color:white;
	text-align:center;
}

td.thead{
	text-align:center;
	font-weight:bold;
}

@media (min-width: 992px){
	.col-md-1 {
		width: 9.33333%;
	}
}
.col-md-1 {
	padding-right: 0px;
}
.DTFC_LeftBodyLiner {
    overflow: hidden !important;
}
.table-scrollable {
	overflow-x:hidden;
}
.table-scrollable{
	margin:0px !important;
}
input.form-control.input-sm.input-small.input-inline {
    height: 25px;
}
</style>
<script>
function validate_weightage(id)
{
	var total = Number($("#text1").val())+Number($("#text2").val())+Number($("#text3").val())+Number($("#text4").val())+Number($("#text6").val())+Number($("#text9").val());
	//alert(total);
	$("#total_weightage").val(total);
	if(total > 100)
	{
		alert("Please Enter less value, Sum of weightage exceeded 100!");
		//$("#"+id).val("");
		$("#"+id).focus();
		return false;
	}else if(total < 100)
	{
		alert("Please Enter more value, Sum of weightage less then 100!");
		$("#"+id).focus();
		return false;
	}else{
		if(total != 100){
			alert("Please Enter Correct Weightage!");
			return false;
		}else if(total == 100){
			var total = Number(total);
			document.getElementById("frmsrch").action = 'stack_rank.php'; 
			document.getElementById("frmsrch").submit();
			return true;
		}
	}
}
function validate_weightage_submit(){
	var total = Number($("#text1").val())+Number($("#text2").val())+Number($("#text3").val())+Number($("#text4").val())+Number($("#text6").val())+Number($("#text9").val());
	//alert(total);
	$("#total_weightage").val(total);
	if(total > 100)
	{
		alert("Please Enter less value, Sum of weightage exceeded 100!");
		$("#text4").focus();
		return false;
	}else if(total < 100)
	{
		alert("Please Enter more value, Sum of weightage less then 100!");
		$("#text4").focus();
		return false;
	}else{
		if(total != 100){
			alert("Please Enter Correct Weightage!");
			return false;
		}else if(total == 100){
			var total = Number(total);
			document.getElementById("frmsrch").action = 'stack_rank.php'; 
			document.getElementById("frmsrch").submit();
			return true;
		}
	}
}
function validate_empty(id){
	if($("#"+id).val() == "")
	{
		alert("Enter value");
		$("#"+id).focus();
		return false;
	}else{
		document.getElementById("frmsrch").action = 'stack_rank.php'; 
		document.getElementById("frmsrch").submit();
		return true;
	}
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<form method="POST" id="frmsrch" onsubmit="return validate_weightage_submit();">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="col-sm-2">&nbsp;</div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="productgroup" onchange="reload()">
		                	<?php
								foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value.'">'.$value.'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
							/* $(document).ready(function(){
								var product_group = "<?php echo $productgroup ?>";
								$("#drop4 option").each(function(){
									if($(this).val()==product_group){
										$selected = "selected";
										$(this).attr("selected","selected");    
									}
									if(!$selected){
										$("#drop4").val("Overall");
									}
								});
							}); */
		                    jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
					<div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
					<div class="form-group col-md-3 col-sm-3 col-xs-6">
						<select class="form-control selectweek" id="drop6" name="selecttype">
							<option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
						</select>
						<script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
					<div class="form-group col-md-3 col-sm-3 col-xs-6">
						<select name="period" class="form-control submit" id="period">
							<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_stackrank_engg_list ORDER BY calendar_month desc");
								foreach ($drowpdownArr as $key => $value) {
									$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
									echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
						</select>
		            </div>
					<div class="form-group col-md-3 col-sm-3 col-xs-6">
						<select class="form-control submit" id="drop8" name="ltype">
							<option value="l1">L1</option>
		                    <option value="l2">L2</option>
						</select>
						<script>  
		                     jQuery("#drop8").val("<?php echo $ltype ?>");
		                </script>
						
		            </div>
				</div>
			</div>	
		</div>
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary
				</div>
			    <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
					<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			    </div>
		    </div>
		    <div class="portlet-body">
				<div class='col-md-12' style="padding-right:0px;">
					<div class="portlet">
						<div class="portlet-body">
							<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
								<label>Weightage</label>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								CSAT Green
								<input type="text" class="form-control textchange" required id="text1" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="csatgreen_weightage">
								<script>  
									 jQuery("#text1").val("<?php echo $csatgreen_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Red
								<input type="text" class="form-control textchange" required id="text6" value="<?php echo $red_weightage; ?>" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="red_weightage">
								<script>  
									 jQuery("#text6").val("<?php echo $red_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Closure
								<input type="text" class="form-control textchange" required id="text2" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="closure_weightage">
								<script> 
									 jQuery("#text2").val("<?php echo $closure_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								TTC
								<input type="text" class="form-control textchange" required id="text9" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="ttc_weightage">
								<script> 
									 jQuery("#text9").val("<?php echo $ttc_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Soft Skills
								<input type="text" class="form-control textchange" required id="text3" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="softskill_weightage">
								<script> 
									 jQuery("#text3").val("<?php echo $softskill_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Process Quality
								<input type="text" class="form-control textchange" required id="text4" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="processquality_weightage">
								<script> 
									 jQuery("#text4").val("<?php echo $processquality_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Escalation
								<input type="text" class="form-control textchange" required id="text5" name="escalation_weightage" onchange="validate_empty(this.id)">
								<script> 
									 jQuery("#text5").val("<?php echo $escalation_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								Attendance
								<input type="text" class="form-control" id="text7" required name="atten_weightage" value="<?php echo $atten_weightage; ?>" onchange="validate_empty(this.id)">
								<!--<script> 
									 jQuery("#text7").val("<?php echo $atten_weightage ?>");
								</script>-->
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
								Backlog 
								<input type="text" class="form-control" id="text8" required name="backlog_weightage" onchange="validate_empty(this.id)">
								<script> 
									 jQuery("#text8").val("<?php echo $backlog_weightage ?>");
								</script>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
								Total
								<input type="text" class="form-control" id="total_weightage" name="total_weightage" value="<?php echo $total_weightage;?>" readonly>
								<script> 
									 //jQuery("#text8").val("<?php echo $backlog_weightage ?>");
								</script>
							</div>
						</div>
						<div class="portlet-body">
							<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
								<label>Target</label>
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text11" onkeypress="return isNumber(event)" value="<?php echo $green_target; ?>" onchange="validate_empty(this.id)" name="green_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control textchange" required id="text61" onkeypress="return isNumber(event)" value="<?php echo $red_target; ?>" onchange="validate_empty(this.id)" name="red_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text21" onkeypress="return isNumber(event)" value="<?php echo $closures_target; ?>" onchange="validate_empty(this.id)" name="closures_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text91" onkeypress="return isNumber(event)" value="<?php echo $ttc_target; ?>" onchange="validate_empty(this.id)" name="ttc_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text31" onkeypress="return isNumber(event)" value="<?php echo $ss_target; ?>" onchange="validate_empty(this.id)" name="ss_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text41" onkeypress="return isNumber(event)" value="<?php echo $pq_target; ?>" onchange="validate_empty(this.id)" name="pq_target">
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text51" onchange="validate_empty(this.id)" name="esc_target" value="<?php echo $esc_target; ?>" >
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6">
								<input type="text" class="form-control" required id="text71" onchange="validate_empty(this.id)" name="atten_target" value="<?php echo $atten_target; ?>" >
							</div>
							<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
								<input type="text" class="form-control" required id="text81" onchange="validate_empty(this.id)" value="<?php echo $backlog_target; ?>" name="backlog_target" >
							</div>
						</div>
					</div>
				</div> 
			    <div class="table-scrollable">
			        <!--<table class="stripe table table-striped order-column text-center" width="100%" id="example" style='white-space: nowrap;' border="1">-->
					<table id="example" class="stripe row-border table-striped order-column text-center" width="100%" style='white-space: nowrap;' border="1">
	                      <thead class="header">
								<tr>
									<th class="thead" rowspan="2">Network Engineer</th>
									<th class="thead" rowspan="2" colspan="6">&nbsp;</th>
									<th class="thead">Target</th>
									<th class="thead"><?php echo $green_target; ?>%</th>
									<th class="thead" colspan="4" style="text-align:left;"><?php echo $red_target; ?>%</th>
									<th class="thead" colspan="3" style="text-align:left;"><?php echo $closures_target; ?>%</th>
									<th class="thead" colspan="3" style="text-align:left;"><?php echo $ttc_target; ?>%</th>
									<th class="thead" colspan="2" style="text-align:left;"><?php echo $ss_target; ?>%</th>
									<th class="thead" colspan="2" style="text-align:left;"><?php echo $pq_target; ?>%</th>
									<th class="thead"><?php echo $esc_target; ?>%</th>
									<th class="thead"><?php echo $atten_target; ?>%</th>
									<th class="thead"><?php echo $backlog_target; ?>%</th>
								</tr>
								<tr>
									<th class="thead">Weightage</th>
									<th class="thead"><?php echo $csatgreen_weightage.'%'; ?></th>
									<th class="thead" colspan="4" style="text-align:left;"><?php echo $red_weightage.'%'; ?></th>
									<th class="thead" colspan="3" style="text-align:left;"><?php echo $closure_weightage.'%'; ?></th>
									<th class="thead" colspan="3" style="text-align:left;"><?php echo $ttc_weightage.'%'; ?></th>
									<th class="thead" colspan="2" style="text-align:left;"><?php echo $softskill_weightage.'%'; ?></th>
									<th class="thead" colspan="2" style="text-align:left;"><?php echo $processquality_weightage.'%'; ?></th>
									<th class="thead"><?php echo $escalation_weightage.'%'; ?></th>
									<th class="thead"><?php echo $atten_weightage.'%'; ?></th>
									<th class="thead"><?php echo $backlog_weightage.'%'; ?></th>
								</tr>
	                            <tr>
									<th class="thead">Engg. Name</th>
									<th class="thead">Emp Id</th>
									<th class="thead">Team Lead</th>
									<th class="thead">Project</th>
									<th class="thead">Tenure</th>
									<th class="thead">Bucket Wise</th>
									<th class="thead">Overall</th>
									<th class="thead">Overall PL</th>	
									<th class="thead">CSAT Green</th>
									<th class="thead">Red</th>
									<th class="thead">Survey Count</th>
									<th class="thead">PL</th>
									<th class="thead">Productivity</th>
									<th class="thead">Closures</th>
									<th class="thead">Closure %</th>
									<th class="thead">PL</th>
									<th class="thead">TTC %</th>
									<th class="thead">PL</th>
									<th class="thead">Open Cases</th>
									<th class="thead">Soft Skills</th>
									<th class="thead">PL</th>
									<th class="thead">Process Quality</th>
									<th class="thead">PL</th>
									<th class="thead">Escalation</th>
									<th class="thead">Attendance</th>
									<th class="thead">Backlog Avg.</th>
	                            </tr>
	                        </thead>
							
	                        <tbody>
								<?php 
								if(!empty($headerenggArr)){
								foreach($headerenggArr as $engg_key => $engg_name){ 
									
									$attendance = round((empty($overallout[$engg_name])?0:$overallout[$engg_name]/$workingdays)*100); 
									$attendance_percent = ($attendance>=100)?'100%':$attendance.'%';
									
									$add1 = (float)$add11=((empty($csatgreenArr[$engg_name])?0:$csatgreenArr[$engg_name]/$green_target)*$csatgreen_weightage)/100;
									$add2 = (float)$add12=((1-$csatredArr[$engg_name])>0)?$red_weightage/100:0;
									$add3 = (float)$add13=(empty($closurepercentageArr[$engg_name])?0:$closurepercentageArr[$engg_name]/100)*($closure_weightage/100);
									$add9 = (float)$add19=(empty($ttc_percent[$engg_name])?0:$ttc_percent[$engg_name]/100)*($ttc_weightage/100);
									$add4 = (float)$add14=((empty($softskillsArr[$engg_name])?0:$softskillsArr[$engg_name]/$ss_target)*$softskill_weightage)/100;
									$add5 = (float)$add15=((empty($processqualityArr[$engg_name])?0:$processqualityArr[$engg_name]/$pq_target)*$processquality_weightage)/100;
									$add6 = (float)$add16=(empty($escalationArr[$engg_name])?0:$escalationArr[$engg_name]>0)?$escalation_weightage/100:0;
									$add7 = (float)$add17=(empty($attendance)?0:$attendance<95)?$atten_weightage/100:0;
									$add8 = (float)$add18=($backlogArr[$engg_name]>0)?$backlog_weightage/100:0;
									$overall = round(($add1+$add2+$add3+$add4+$add5+$add6+$add7+$add8+$add9)*100,2);
									
								?>
									<tr>
									<td class="thead"><?php echo $engg_name; ?></td>
									<td><?php echo $emplyId[$engg_name]; ?></td>	
									<td><?php echo empty($enggprojetcArr[$engg_name])?"NA":$enggprojetcArr[$engg_name]; ?></td>
									<td><?php echo empty($enggproductArr[$engg_name])?"NA":$enggproductArr[$engg_name]; ?></td>
									<td><?php echo empty($tenureArr[$engg_name])?"NA":number_format($tenureArr[$engg_name]); ?></td>
									<td><?php echo empty($bucketArr[$engg_name])?"NA":$bucketArr[$engg_name]; ?></td>
									<td><?php echo $overall.'%'; ?></td>
									<td><?php echo plcalculate($overall); ?></td>
									<?php 
										  if($csatgreenArr[$engg_name] !=''){
											  $surveygreen = number_format($csatgreenArr[$engg_name]).'%';
										  }else if($csatsurveyArr[$engg_name] =='' && $closuresArr[$engg_name] > 0){
												$csatteamsurveyArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as csat_count, team from aruba_csat where team = "'.$enggteamArr[$engg_name].'" AND '.$type.$selectQry.'="'.$selectrange.'"'),'team','csat_count');
												$csatteamgreencntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as green, team from aruba_csat where team = "'.$enggteamArr[$engg_name].'" AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Green"'),'team','green');
												$surveygreen = $csatgreenArr[$engg_name] = round(($csatteamgreencntArr[$enggteamArr[$engg_name]]/$csatteamsurveyArr[$enggteamArr[$engg_name]])*100,2).'%';
												//$surveygreen = 'NA';
										  }else if($csatgreenArr[$engg_name] =='' && $closuresArr[$engg_name] ==''){
											  $surveygreen = 'NA';
										  }
									?>
									<td><?php echo $surveygreen; ?></td>
									<?php 
										   if($csatredArr[$engg_name] !=''){
											  $surveyred = number_format($csatredArr[$engg_name]).'%';
										  }else if($csatsurveyArr[$engg_name] ==''){
											$surveyred = 'NA';
										  }else if($csatredArr[$engg_name] =='' && $csatsurveyArr[$engg_name] !=''){
											  $surveyred = '0%';
										  }
									?>
									<td><?php echo  $surveyred; ?></td>
									<td><?php echo empty($csatsurveyArr[$engg_name])?"NA":number_format($csatsurveyArr[$engg_name]); ?></td>
									<td><?php echo empty($greenplArr[$engg_name])?"NA":$greenplArr[$engg_name]; ?></td>
									<td><?php echo empty($productivityArr[$engg_name])?"NA":round($productivityArr[$engg_name],2); ?></td>
									<td><?php echo empty($closuresArr[$engg_name])?"NA":number_format($closuresArr[$engg_name]); ?></td>
									<td><?php echo empty($closurepercentageArr[$engg_name])?"NA":number_format($closurepercentageArr[$engg_name]).'%'; ?></td>
									<td><?php echo empty($closureplArr[$engg_name])?"NA":$closureplArr[$engg_name]; ?></td>
									<td><?php echo empty($ttc_percent[$engg_name])?"NA":number_format($ttc_percent[$engg_name]).'%'; ?></td>
									<td><?php echo empty($ttc_percent_pl[$engg_name])?"NA":$ttc_percent_pl[$engg_name]; ?></td>
									<td><?php echo empty($opencasesArr[$engg_name])?"NA":number_format($opencasesArr[$engg_name]); ?></td>
									<td><?php echo empty($softskillsArr[$engg_name])?"NA":number_format($softskillsArr[$engg_name]).'%'; ?></td>
									<td><?php echo empty($ssplArr[$engg_name])?"NA":$ssplArr[$engg_name]; ?></td>
									<td>
									<?php 
									if($processqualityArr[$engg_name] != ''){
										echo number_format($processqualityArr[$engg_name]).'%'; 
									}elseif($processqualityArr[$engg_name] == '' && $closuresArr[$engg_name] >= 10){
										echo '90%';
									}else{
										echo 'NA';
									}
									
									?></td>
									<td><?php echo empty($pqplArr[$engg_name])?"NA":$pqplArr[$engg_name]; ?></td>
									<td><?php echo empty($escalationArr[$engg_name])?"NA":number_format($escalationArr[$engg_name]); ?></td>
									<td><?php echo $attendance_percent; ?></td>
									<td><?php echo empty($backlogArr[$engg_name])?"NA":number_format($backlogArr[$engg_name]);?></td>
									</tr>
								<?php }
								}else{ ?>
									<tr><td colspan="23">No Data Found</td></tr>
								<?php } ?>
							</tbody>
	                    </table>
			    </div>
		    </div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax_stack.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#period").html("");
	        $("#period").html(obj);
	      }
	    }); 
	});
	// $("#drop1").change(function(){
	//     $("#drop2").val("Overall");
	//     $("#drop3").val("Overall");
	//     $("#drop4").val("Overall");
	// });
	
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		});
	});
	$('.submit').change(function(){
		document.getElementById("frmsrch").action = 'stack_rank.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	/* $('.textchange').change(function(){
		document.getElementById("frmsrch").action = 'stack_rank.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}); */
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'stack_export.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});

	$('#rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$("#drop6").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax_stack.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
			var obj = jQuery.parseJSON( output);
	        $("#period").html("");
	        $("#period").html(obj);
	      }
	    }); 
	});
$(document).ready(function() {
	    var table = $('#example').DataTable( {
			order: [[ 6, "desc" ]],
	        scrollY:        "300px",
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         false,
	        fixedColumns:   {
	            leftColumns: 1,
	            //rightColumns: 1
	        }
	    } );
	} );
	$(document).ready(function(){
		$(".dataTables_filter label").remove();
	});
	function reload(){
		document.getElementById("frmsrch").action = 'stack_rank.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}
</script>