<?php
// session_start();
// session_destroy();
if (strpos(strtoupper($_SESSION['username']), '@HPE') !== false) {
	session_start();
	session_destroy();
    header("Location:../aruba/login.php");
	exit;
}else{
	require_once("../sso/sso_config.php");
	header("Location:".LOGOUTURL);
	exit;
}
?>