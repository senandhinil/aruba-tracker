<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['reporttype'] || $_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange']){
	$reporttype 	= 	$_POST['reporttype'];
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:"Overall";
	$productwise 	=	!empty($productwise)?$productwise:"Overall";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
	$reporttype 	=	!empty($reporttype)?$reporttype:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition.=" and wlan_ns='".$projectwise."'";
		$hcount=strtolower($projectwise);
	}else{
		$hcount="wlan,ns";
	}
	if($productwise!='Overall' && $productwise!=''){
		$QryCondition.=" and que_new='".$productwise."'";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}
	if($reporttype!='Overall' && $reporttype!=''){
		$QryCondition.=" and region='".$reporttype."'";
	}
	
	//echo "select distinct ".$type.$selectQry." from aruba_open order by id asc";
	$headerQury = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	$headerQury->execute();
	$headQryArr = $headerQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	if($selecttype=='Quarterly'){	
		$tablehead=array_reverse(array_slice($arrayval, 0, 4, true));
	}else{
		$tablehead=array_reverse(array_slice($arrayval, 0, 12, true));
	}
	//select calendar_week,count(case_origin) as origin_count,case_origin,rma from aruba_open where calendar_week in ('2017Week27','2017Week28','2017Week29','2017Week30','2017Week31','2017Week32','2017Week33','2017Week34','2017Week35','2017Week36','2017Week37','2017Week38') group by case_origin, calendar_week order by id desc
	
	echo "select ".$type.$selectQry.",case_origin,rma from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc";
	$inflwQury = $conn->prepare("select ".$type.$selectQry.",case_origin,rma from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$inflwQury->execute();
	$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[]=$weeknamearr[$type.$selectQry];
		$caseorigin[$weeknamearr[$type.$selectQry]][]=$weeknamearr['case_origin'];
	}
	//print_r($caseorigin);
	$opencasecount=array_count_values($totopencase);
	$case_orgin=$commonobj->getarracount($caseorigin);
	print_r($case_orgin);
	//closure count
	echo "select ".$type.$selectQry.",ttc,rma,sdc,case_origin from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc";
	echo "<br>";
	$closureQury = $conn->prepare("select ".$type.$selectQry.",ttc,rma,sdc,case_origin from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$closureQury->execute();
	$closeQuryArr = $closureQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($closeQuryArr as $closeArrval){
		$closecount[]=$closeArrval[$type.$selectQry];
		if($closeArrval['ttc']<'9'){
			$ttccount[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
			$phonettc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
			$webttc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
			$sdcphone[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
			$sdcweb[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
		$sdctot[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		if($closeArrval['case_origin']=='Phone'){
			$phonecountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Web'){
			$webcountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
	}
	//print_r($sdcweb);
	$closecasecount=array_count_values($closecount);
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);
	//csat
	echo "select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc";
	echo "<br>";
	$csatunQury = $conn->prepare("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$csatunQury->execute();
	$csatunQuryArr = $csatunQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['rma']=='Yes'){
			$rmacaseoe[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		}
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//head count and working days
	echo "select ".$type.$selectQry.",working_days,$hcount from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."')";
	$holyQury = $conn->prepare("select ".$type.$selectQry.",working_days,$hcount from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."')");
	$holyQury->execute();
	$holyQuryArr = $holyQury->fetchAll(PDO::FETCH_ASSOC);
	foreach ($holyQuryArr as $holykey => $holyvalue) {
		$workingdays[$holyvalue[$type.$selectQry]]=$holyvalue['working_days'];
		$overallheadcount[$holyvalue[$type.$selectQry]]=$holyvalue['wlan']+$holyvalue['ns'];
	}
	$Qry=$type.$selectQry."_".$selectrange;
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
	/*/
</style>
<script type="text/javascript">

	</script>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop2"  name="projectwise">
	                    <?php
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script> 
		                     jQuery("#drop2").val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="productwise">
		                <?php
		                	foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="productgroup">
		                	<?php
		                		foreach ($productgroupwlan as $key => $value) {
		                			echo '<option value="'.$value.'">'.$value.'</option>';
		                		}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop1"  name="reporttype">
		                	<?php
		                	foreach($productgroupoverall as $drop1overallval){
								    echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop1").val("<?php echo $reporttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype">
		                	<!-- <option value="Daily">Daily</option> -->
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop7"  name="selectrange">
		                	<option value="">--- Select ---</option>
		                	<?php
								$selecttypeQry = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								$selecttypeQry->execute();
								$drowpdownArr = $selecttypeQry->fetchAll(PDO::FETCH_ASSOC);
								//print_r($drowpdownArr);
								foreach ($drowpdownArr as $key => $value) {
									$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
									echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
							jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        		<img src="img/exp.png" style="height: 25px;cursor: pointer;"  id='rawexport' title='Raw Download'>
			        		<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        		<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			        	
			        </div>
		    </div>
		    <div class="portlet-body">
			    <div class="table-scrollable">
			        <table class="table table-striped table-bordered table-hover text-center" id='tableId' data-height="300">
	                      <thead  class="header">
	                            <tr>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
	                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
	                        		<?php	}	?>
	                            </tr>
	                        </thead>
	                       <tfoot >
	                            <tr>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
	                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
	                        		<?php	}	?>
	                            </tr>
	                        </tfoot>
	                        <tbody class="scrollContent" style=" height: 120px;*display: block;">
	                        	<tr >
	                        		<td rowspan="3"style="vertical-align: inherit;">Volume</td>
		                        	<td>Overall</td>
		                        	<?php 
									echo $commonobj->volOverall($tablehead,$opencasecount);
									//foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<!--<td class="text-center"><?php echo empty($opencasecount[$rs])?0:$opencasecount[$rs]; ?></td>-->
		                        	<?php	//} ?>
	                        	</tr>
	                        	<tr>
		                        	<td>Phone</td>
		                        	<?php 
									echo $commonobj->volphnweb($tablehead,$case_orgin,"Phone");
									//foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<!--<td class="text-center"><?php echo empty($case_orgin[$rs]['Phone'])?"No":$case_orgin[$rs]['Phone']; ?></td>-->
		                        	<?php//}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Web</td>
		                        	<?php 
									echo $commonobj->volphnweb($tablehead,$case_orgin,"Web");
									//foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<!--<td class="text-center"><?php echo empty($case_orgin[$rs]['Web'])?"No":$case_orgin[$rs]['Web']; ?></td></td>-->
		                        	<?php//}?>
	                        	</tr>
	                        	
	                        	<tr>
		                        	<td rowspan=<?php echo $productwise=='GSC'?'11':'9';?> style="vertical-align: inherit;">Closed Cases</td>
		                        	<td>Closure</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td class="text-center"><?php echo empty($closecasecount[$rs])?0:$closecasecount[$rs]; ?></td>
		                        	<?php	} ?>
	                        	</tr>
	                        	<tr>
	                        		<td>RMA %</td>
	                        		<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($rmacount[$rs])?"0.00%":round($rmacount[$rs]['Yes']/array_sum($rmacount[$rs])*100).'%' ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA Cases</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($rmacount[$rs]['Yes'])?"0":$rmacount[$rs]['Yes']; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<!-- productivity -->
	                        	<?php if($productwise=='GSC'){?>
	                        	<tr>
		                        	<td>Productivity</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($closecasecount[$rs])?"0":number_format($closecasecount[$rs]/$workingdays[$rs]/$overallheadcount[$rs],2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Head Count</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($overallheadcount[$rs])?"NA":$overallheadcount[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<?php } ?>
	                        	<!-- end productivity -->
	                        	<tr>
		                        	<td>TTC &lt; 9 Days</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($ttccount[$rs])?"0":round(count($ttccount[$rs])/count($rma[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Phone - TTC &lt; 9 Days</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 

		                        		?>
		                        		<td class="text-center"><?php echo empty($phonettc[$rs])?"0":round(count($phonettc[$rs])/count($phonecountArr[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Web - TTC &lt; 9 Days</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($webttc[$rs])?"0":round(count($webttc[$rs])/count($webcountArr[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($sdctotper[$rs])?"0.00%":round($sdctotper[$rs]['1']/array_sum($sdctotper[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC – Phone (45%)</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($sdcphone[$rs])?"0.00%":round(count($sdcphone[$rs])/$closecasecount[$rs]*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC – Web (35%)</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($sdcweb[$rs])?"0.00%":round(count($sdcweb[$rs])/$closecasecount[$rs]*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
	                        		<td rowspan="10"style="vertical-align: inherit;">CSAT</td>
		                        	<td>Overall Experience</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td class="text-center"><?php echo empty($overallexp[$rs])?"NA":number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2); ?></td>
		                        	<?php	} ?>
	                        	</tr>
	                        	<tr>
		                        	<td>Green %</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
		                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Green'])?"NA":round($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Normal %</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Normal'])?"NA":round($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Red %</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Red'])?"NA":round($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100).'%'; ?></td>	
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Survey Count</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($alert_type[$rs])?"0":array_sum($alert_type[$rs]); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<?php 
	                        	foreach ($tablehead as $inflowkey=> $rs) { 
	                        		$greenper[$rs]=empty($netprompter[$rs]['Green'])?"0.00%":round($netprompter[$rs]['Green']/array_sum($netprompter[$rs])*100).'%'; 
	                        		$redperc[$rs]=empty($netprompter[$rs]['Red'])?"0.00%":round($netprompter[$rs]['Red']/array_sum($netprompter[$rs])*100).'%';
	                        	}
	                        	?>
	                        	<tr>
		                        	<td>Net Promoter Score</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
		                        		<td class="text-center"><?php echo empty($greenper[$rs])?"0":$greenper[$rs]-$redperc[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Loyalty Index</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($loyaltyindex[$rs])?"0":number_format(array_sum($loyaltyindex[$rs])/count($loyaltyindex[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA cases OE</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class=" text-center"><?php echo empty($rmacaseoe[$rs])?'NA':number_format(array_sum($rmacaseoe[$rs])/count($rmacaseoe[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA cases NPS</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class=" text-center"><?php echo empty($loyalty_index[$rs])?'NA':number_format(array_sum($loyalty_index[$rs])/count($loyalty_index[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>No Of Working days</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($workingdays[$rs])?"NA":$workingdays[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr><td></td></tr>
	                        	<!-- <tr>
		                        	<td>Red %</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class="text-center"><?php echo empty($redperc[$rs])?"0.00%":$redperc[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr> -->
	                        </tbody>
	                    </table>
			    </div>
		    </div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	// $("#drop1").change(function(){
	//     $("#drop2").val("Overall");
	//     $("#drop3").val("Overall");
	//     $("#drop4").val("Overall");
	// });
	
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('#drop1').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'export-summary.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop2').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop3').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop4').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop7').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});

	$('#rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
</script>
