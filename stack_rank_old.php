<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';

if($_POST['csat_weightage'] || $_POST['closure_weightage'] || $_POST['softskill_weightage'] || $_POST['processquality_weightage'] || $_POST['escalation_weightage'] || $_POST['selecttype'] || $_POST['calendartype']){
	$csat_weightage 	= 	$_POST['csat_weightage'];
	$closure_weightage 	= 	$_POST['closure_weightage'];
	$softskill_weightage   	=	$_POST['softskill_weightage'];
	$processquality_weightage 	= 	$_POST['processquality_weightage'];
	$escalation_weightage	= 	$_POST['escalation_weightage'];
	$selecttype	    = 	$_POST['selecttype'];
	$calendartype = $_POST['calendartype']; 
	$selectrange    =	$_POST['period'];
	$ltype = $_POST['ltype'];
}else{
	$csat_weightage    =	!empty($csat_weightage)?$csat_weightage:"25";
	$closure_weightage 	=	!empty($closure_weightage)?$closure_weightage:"35";
	$softskill_weightage 	=	!empty($softskill_weightage)?$softskill_weightage:"15";
	$processquality_weightage 	=	!empty($processquality_weightage)?$processquality_weightage:"25";
	$escalation_weightage 	=	!empty($escalation_weightage)?$escalation_weightage:"10";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Monthly";
	$calendartype = !empty($calendartype)?$calendartype:"Normal";
	$selectrange	=	!empty($selectrange)?$selectrange:current($currentmonth);
	$ltype = !empty($ltype)?$ltype:"l1";
}

/* if(isset($_POST['calendartype']))
{ */
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}

	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
/* } */
	echo "select distinct engg_name from aruba_stackrank_engg_list where role = '$ltype' AND $type$selectQry='$selectrange'";
	$headerenggArr = $commonobj->arrayColumn($commonobj->getQry("select distinct engg_name from aruba_stackrank_engg_list where role = '$ltype' AND $type$selectQry='$selectrange'"),'engg_name','engg_name');
	//$headerenggArr[$engg_name['case_owner']]=$engg_name['case_owner'];
	
	//echo "select distinct case_owner, team, product_group1 from aruba_open where $type$selectQry='$selectrange' order by case_owner asc";
	$sel_engg = $conn->prepare('select distinct case_owner, team, product_group1 from aruba_open where '.$type.$selectQry.'="'.$selectrange.'" order by case_owner asc');
	$sel_engg->execute();
	$sel_enggArr = $sel_engg->fetchAll(PDO::FETCH_ASSOC);
	foreach($sel_enggArr as $engg_name){
		//$headerenggArr[$engg_name['case_owner']]=$engg_name['case_owner'];
		$enggteamArr[$engg_name['case_owner']]=$engg_name['team'];
		$enggprojetcArr[$engg_name['case_owner']]=$engg_name['product_group1'];
	}
	//print_r($headerenggArr);
	//foreach($headerenggArr as $engg_key => $engg_name){
	//tenure, bucket wise
	$engg_tenu = $conn->prepare('select emp_name,doj from aruba_tenure where emp_name in ("'.implode('","',$headerenggArr).'")');
	$engg_tenu->execute();
	$engg_tenuArr = $engg_tenu->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_tenuArr as $tenure){
		$date1 = date('Y-m-d H:i:s', strtotime($tenure['doj']));
		$date2 = date('Y-m-d H:i:s');
		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);
		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);
		
		if($tenure['doj'] != '-' || $tenure['doj'] != ''){
			$years = $year2 - $year1;
			$months = $month2 - $month1;
			$diff = (($years) * 12) + ($months);
			$tenureArr[$tenure['emp_name']] = $diff;
			$bucketArr[$tenure['emp_name']] = $years." Years,".$months." Months";
		}else{
			$tenureArr[$tenure['emp_name']] = "NA";
			$bucketArr[$tenure['emp_name']] = "NA";
		}
	}
	//CSAT Green, Red, Survey Count
	$csatsurveyArr = $commonobj->arrayColumn($commonobj->getQry("select count(*) as csat_count, case_owner from aruba_csat where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner"),'case_owner','csat_count');
	
	$csatgreencntArr = $commonobj->arrayColumn($commonobj->getQry("select count(*) as green, case_owner from aruba_csat where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' AND alert_type = 'Green' group by case_owner"),'case_owner','green');
	
	$csatredcntArr = $commonobj->arrayColumn($commonobj->getQry("select count(*) as red, case_owner from aruba_csat where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' AND alert_type = 'Red' OR alert_type = 'Normal' group by case_owner"),'case_owner','red');
	
	foreach($csatsurveyArr as $engg_name => $survey_count){
		$csatgreenArr[$engg_name] = ($csatgreencntArr[$engg_name]/$survey_count)*100;
		$csatredArr[$engg_name] = ($csatredcntArr[$engg_name]/$survey_count)*100;
	}
	foreach($csatgreenArr as $engg_name => $csatgreen)
	{
		$plvarval = plcalculate($csatgreen);
		$greenplArr[$engg_name] = $plvarval;
	}
	//Productivity, closures
	$engg_pc = $conn->prepare("select count(*) as tot_count,case_owner from aruba_closure where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner");
	$engg_pc->execute();
	$engg_pcArr = $engg_pc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_pcArr as $pc){
		$closuresArr[$pc['case_owner']] = $pc['tot_count'];
		$productivityArr[$pc['case_owner']] = $pc['tot_count']/20;
	}
	foreach($closuresArr as $engg_name => $closures){
		$closurepercen = (($closures/50)*100)> 121?121:($closures/50)*100;
		$closurepercentageArr[$engg_name] = $closurepercen;
	}
	foreach($closurepercentageArr as $engg_name => $closurepercent)
	{
		$plvarval = plcalculate($closurepercent);
		$closureplArr[$engg_name] = $plvarval;
	}
	//Open Cases
	//echo "select count(*) as open_count,case_owner from aruba_open where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner";
	$engg_oc = $conn->prepare("select count(*) as open_count,case_owner from aruba_open where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner");
	$engg_oc->execute();
	$engg_ocArr = $engg_oc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ocArr as $oc){
		$opencasesArr[$oc['case_owner']] = $oc['open_count'];
	}
	//Soft Skill
	$softskillsArr = $commonobj->arrayColumn($commonobj->getQry("select avg(overall) as ss,case_owner from aruba_ssqa where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner"),'case_owner','ss');
	
	foreach($softskillsArr as $engg_name => $sspercent)
	{
		$ssplvarval = plcalculate($sspercent);
		$ssplArr[$engg_name] = $ssplvarval;
	}
	//Process Quality
	$processqualityArr = $commonobj->arrayColumn($commonobj->getQry("select avg(overall) as process_quality,case_owner from aruba_process_audit where case_owner in ('".implode("','",$headerenggArr)."') AND $type$selectQry='$selectrange' group by case_owner"),'case_owner','process_quality');
	
	foreach($processqualityArr as $engg_name => $pcval)
	{
		$pqplvarval = plcalculate($pcval);
		$pqplArr[$engg_name] = $pqplvarval;
	}
	//Escalation
	$escalationArr = $commonobj->arrayColumn($commonobj->getQry("select count(*) as esc_count, case_owner from aruba_esc where case_owner in ('".implode("','",$headerenggArr)."') AND ".$type.$selectQry." = '$selectrange' group by case_owner order by case_owner asc"),'case_owner','esc_count');
	
	$workingdays = $commonobj->arrayColumn($commonobj->getQry("select working_days,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." = '$selectrange' group by ".$type.$selectQry),$type.$selectQry,'working_days');
	
	$overallout = $commonobj->arrayColumn($commonobj->getQry("select case_owner,count(*) as overalloutcnt from aruba_avaya_raw where case_owner in ('".implode("','",$headerenggArr)."') AND ".$type.$selectQry." = '$selectrange' $QryCondition AND time_to_sec(total_time) >=  '18000' group by case_owner"),'case_owner','overalloutcnt');
	
	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select case_owner,count(distinct case_owner) as outcnt from aruba_avaya_raw where case_owner in ('".implode("','",$headerenggArr)."') AND ".$type.$selectQry." = '$selectrange' AND time_to_sec(total_time) >=  '18000' group by case_owner"),'case_owner','outcnt');
	
	//print_r($overalloutcnt);
	
	function plcalculate($plval){
		if($plval >= 0 && $plval < 76)
		{
			$plvar = "PL1";
		}elseif($plval >= 76 && $plval < 96)
		{
			$plvar = "PL2";
		}elseif($plval >= 96 && $plval < 106)
		{
			$plvar = "PL3";
		}elseif($plval >= 106 && $plval < 121)
		{
			$plvar = "PL4";
		}elseif($plval >= 121)
		{
			$plvar = "PL5";
		}
		return $plvar;
	}
	
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	/*.form-control {
	    border: 0px solid #c2cad8 !important;
	}*/
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
thead{
	background-color:#F2784B;
	color:white;
	text-align:center;
}
@media (min-width: 992px){
	.col-md-1 {
		width: 13.33333%;
	}
}
</style>
<script>
function validate_weightage(id)
{
	var total = Number($("#text1").val())+Number($("#text2").val())+Number($("#text3").val())+Number($("#text4").val())+Number($("#text5").val());
	//alert(total);
	if(total > 100)
	{
		var total = Number(total) - Number($("#text5").val());
		$("#text5").val(0);
		if(total > 100)
		{
			alert("Please Enter less value, Sum of weightage exceeded 100!");
			$("#"+id).val("");
			$("#"+id).focus();
		}
	}else{
		var total = Number(total);
	}
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:6%;">
						<label>Target</label>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						CSAT - 80%
					</div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						Closure - 38%
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						Softskill - 80%
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						Process Quality - 90%
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						Escalation - 0%
		            </div>
					
		        </div>
		    </div>
		</div>
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:6%;">
						<label>Weightage</label>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						<input type="text" class="form-control" id="text1" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="csat_weightage">
						<script>  
		                     jQuery("#text1").val("<?php echo $csat_weightage ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						<input type="text" class="form-control" id="text2" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="closure_weightage">
		                <script> 
		                     jQuery("#text2").val("<?php echo $closure_weightage ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						<input type="text" class="form-control" id="text3" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="softskill_weightage">
		                <script> 
		                     jQuery("#text3").val("<?php echo $softskill_weightage ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						<input type="text" class="form-control" id="text4" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="processquality_weightage">
		                <script> 
		                     jQuery("#text4").val("<?php echo $processquality_weightage ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-1 col-sm-2 col-xs-6">
						<input type="text" class="form-control" id="text5" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="escalation_weightage">
		                <script> 
		                     jQuery("#text5").val("<?php echo $escalation_weightage ?>");
		                </script>
		            </div>
				</div>
		    </div>
		</div>  
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:6%;">
						<label>&nbsp;</label>
		            </div>
					<div class="form-group col-md-3 col-sm-2 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
					<div class="form-group col-md-1 col-sm-2 col-xs-6">
						<select class="form-control selectweek" id="drop6" name="selecttype">
							<option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
						</select>
		            </div>
					<div class="form-group col-md-1 col-sm-2 col-xs-6">
						<select name="period" class="form-control submit" id="period">
							<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
									echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
						</select>
		            </div>
					<div class="form-group col-md-1 col-sm-2 col-xs-6">
						<select class="form-control submit" id="drop8" name="ltype">
							<option value="l1">L1</option>
		                    <option value="l2">L2</option>
						</select>
						<script>  
		                     jQuery("#drop8").val("<?php echo $ltype ?>");
		                </script>
						
		            </div>
				</div>
			</div>	
		</div>
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        		<img src="img/exp.png" style="height: 25px;cursor: pointer;"  id='rawexport' title='Raw Download'>
			        		<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        		<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			        	
			        </div>
		    </div>
		    <div class="portlet-body">
			    <div class="table-scrollable">
			        <table class="table table-striped table-bordered table-hover text-center" id='tableId' data-height="300">
	                      <thead  class="header">
								<tr>
									<th class="thead" rowspan="2" colspan="6">Network Engineer</th>
									<th class="thead">Target</th>
									<th class="thead">80%</th>
									<th class="thead" colspan="4">0%</th>
									<th class="thead" colspan="4">50%</th>
									<th class="thead" colspan="2">80%</th>
									<th class="thead" colspan="2">90%</th>
									<th class="thead">0%</th>
									<th class="thead">95%</th>
									<th class="thead">0%</th>
								</tr>
								<tr>
									<th class="thead">Weightage</th>
									<th class="thead">25%</th>
									<th class="thead" colspan="4">0%</th>
									<th class="thead" colspan="4">35%</th>
									<th class="thead" colspan="2">15%</th>
									<th class="thead" colspan="2">25%</th>
									<th class="thead">-10%</th>
									<th class="thead">-10%</th>
									<th class="thead">0%</th>
								</tr>
	                            <tr>
									<th class="thead">Engg. Name</th>
									<th class="thead">Team Lead</th>
									<th class="thead">Project</th>
									<th class="thead">Tenure</th>
									<th class="thead">Bucket Wise</th>
									<th class="thead">Overall</th>
									<th class="thead">Overall PL</th>
									<th class="thead">CSAT Green</th>
									<th class="thead">Red</th>
									<th class="thead">Survey Count</th>
									<th class="thead">PL</th>
									<th class="thead">Productivity</th>
									<th class="thead">Closures</th>
									<th class="thead">Closure %</th>
									<th class="thead">PL</th>
									<th class="thead">Open Cases</th>
									<th class="thead">Soft Skills</th>
									<th class="thead">PL</th>
									<th class="thead">Process Quality</th>
									<th class="thead">PL</th>
									<th class="thead">Escalation</th>
									<th class="thead">Attendance</th>
									<th class="thead">Backlog Avg.</th>
	                            </tr>
	                        </thead>
							<tfoot >
	                            <tr>
	                            	<th class="thead">Engg. Name</th>
									<th class="thead">Team Lead</th>
									<th class="thead">Project</th>
									<th class="thead">Tenure</th>
									<th class="thead">Bucket Wise</th>
									<th class="thead">Overall</th>
									<th class="thead">Overall PL</th>
									<th class="thead">CSAT Green</th>
									<th class="thead">Red</th>
									<th class="thead">Survey Count</th>
									<th class="thead">PL</th>
									<th class="thead">Productivity</th>
									<th class="thead">Closures</th>
									<th class="thead">Closure %</th>
									<th class="thead">PL</th>
									<th class="thead">Open Cases</th>
									<th class="thead">Soft Skills</th>
									<th class="thead">PL</th>
									<th class="thead">Process Quality</th>
									<th class="thead">PL</th>
									<th class="thead">Escalation</th>
									<th class="thead">Attendance</th>
									<th class="thead">Backlog Avg.</th>
	                            </tr>
	                        </tfoot>
	                        <tbody>
								<?php foreach($headerenggArr as $engg_key => $engg_name){ ?>
									<tr>
									<td class="thead"><?php echo $engg_name; ?></td>
									<td><?php echo empty($enggteamArr[$engg_name])?"NA":$enggteamArr[$engg_name]; ?></td>
									<td><?php echo empty($enggprojetcArr[$engg_name])?"NA":$enggprojetcArr[$engg_name]; ?></td>
									<td><?php echo empty($tenureArr[$engg_name])?"NA":$tenureArr[$engg_name]; ?></td>
									<td><?php echo empty($bucketArr[$engg_name])?"NA":$bucketArr[$engg_name]; ?></td>
									<td><?php echo $engg_name; ?></td>
									<td><?php echo $engg_name; ?></td>
									<td><?php echo empty($csatgreenArr[$engg_name])?"NA":$csatgreenArr[$engg_name]; ?></td>
									<td><?php echo empty($csatredArr[$engg_name])?"NA":$csatredArr[$engg_name]; ?></td>
									<td><?php echo empty($csatsurveyArr[$engg_name])?"NA":$csatsurveyArr[$engg_name]; ?></td>
									<td><?php echo empty($greenplArr[$engg_name])?"NA":$greenplArr[$engg_name]; ?></td>
									<td><?php echo empty($csatgreenArr[$engg_name])?"NA":$productivityArr[$engg_name]; ?></td>
									<td><?php echo empty($closuresArr[$engg_name])?"NA":$closuresArr[$engg_name]; ?></td>
									<td><?php echo empty($closurepercentageArr[$engg_name])?"NA":$closurepercentageArr[$engg_name]; ?></td>
									<td><?php echo empty($closureplArr[$engg_name])?"NA":$closureplArr[$engg_name]; ?></td>
									<td><?php echo empty($opencasesArr[$engg_name])?"NA":$opencasesArr[$engg_name]; ?></td>
									<td><?php echo empty($softskillsArr[$engg_name])?"NA":$softskillsArr[$engg_name]; ?></td>
									<td><?php echo empty($ssplArr[$engg_name])?"NA":$ssplArr[$engg_name]; ?></td>
									<td><?php echo empty($processqualityArr[$engg_name])?"NA":$processqualityArr[$engg_name]; ?></td>
									<td><?php echo empty($pqplArr[$engg_name])?"NA":$pqplArr[$engg_name]; ?></td>
									<td><?php echo empty($escalationArr[$engg_name])?"NA":$escalationArr[$engg_name]; ?></td>
									<td><?php echo $engg_name; ?></td>
									<td><?php echo $engg_name; ?></td>
									</tr>
								<?php } ?>
							</tbody>
	                    </table>
			    </div>
		    </div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#period").html("");
	        $("#period").html(obj);
	      }
	    }); 
	});
	// $("#drop1").change(function(){
	//     $("#drop2").val("Overall");
	//     $("#drop3").val("Overall");
	//     $("#drop4").val("Overall");
	// });
	
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('.submit').change(function(){
		document.getElementById("frmsrch").action = 'stack_rank.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'export-summary.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});

	$('#rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$("#drop6").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
			var obj = jQuery.parseJSON( output);
	        $("#period").html("");
	        $("#period").html(obj);
	      }
	    }); 
	});

</script>
