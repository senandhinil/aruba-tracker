<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange']){
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:"Overall";
	$productwise 	=	!empty($productwise)?$productwise:"Overall";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
//echo $productgroup;
if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	$QryCondition.= ($projectwise !='Overall' && $projectwise !='')?" and wlan_ns='".$projectwise."'":'';
	$QryCondition.= ($productwise !='Overall' && $productwise !='')?" and que_new='".$productwise."'":'';
	$QryCondition.= ($productgroup !='Overall' && $productgroup !='')?" and product_group='".$productgroup."'":'';
	
	//echo $QryCondition;

	$headQryArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));


	//echo "select ".$type.$selectQry.",overall_experience,case_origin,alert_type,region,product_easy_of_use,ease_of_access_score,technical_ability_score,non_technical_performance_score,kept_informed_score,solution_time_score,customer_country from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id Asc";

	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,case_origin,alert_type,region,product_easy_of_use,ease_of_access_score,technical_ability_score,non_technical_performance_score,kept_informed_score,solution_time_score,customer_country from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id Asc");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type'];
		$regionArr[$csatunArrval[$type.$selectQry]][$csatunArrval['region']][]=$csatunArrval['alert_type'];

		$customerCountry[$selectrange][$csatunArrval['region']][$csatunArrval['customer_country']][]=$csatunArrval['alert_type'];
		if($csatunArrval['customer_country'] !=''){
			$regionArrName[$csatunArrval['region']]=$csatunArrval['region'];
			$countryName[$selectrange][$csatunArrval['region']][$csatunArrval['customer_country']]=$csatunArrval['customer_country'];
		}
		$case_originArr[$csatunArrval[$type.$selectQry]][$csatunArrval['case_origin']][]=$csatunArrval['alert_type'];
		//Question & Easy of Use
		$product_easy_of_use[$csatunArrval[$type.$selectQry]][]=$csatunArrval['product_easy_of_use'];
		$ease_of_access_score[$csatunArrval[$type.$selectQry]][]=$csatunArrval['ease_of_access_score'];
		$technical_ability_score[$csatunArrval[$type.$selectQry]][]=$csatunArrval['technical_ability_score'];
		$non_tec_performance[$csatunArrval[$type.$selectQry]][]=$csatunArrval['non_technical_performance_score'];
		$kept_informed_score[$csatunArrval[$type.$selectQry]][]=$csatunArrval['kept_informed_score'];
		$solution_time_score[$csatunArrval[$type.$selectQry]][]=$csatunArrval['solution_time_score'];
	}
	
	$customercountry = $customerCountry[$selectrange];
	$customercountryName = $countryName[$selectrange];

	$csatsurvey = array('Green','Normal','Red','NA');
	$csatstock = array('Red','Normal','Green');
	$caseOriginName =array('Phone','Web');
	$alert_type=$commonobj->getarracount($alerttype);

	$product_easy_of_useArr=$commonobj->getarracount($product_easy_of_use);
	$ease_of_access_scoreArr=$commonobj->getarracount($ease_of_access_score);
	$technical_ability_scoreArr=$commonobj->getarracount($technical_ability_score);
	$non_tec_performanceArr=$commonobj->getarracount($non_tec_performance);
	$kept_informed_scoreArr=$commonobj->getarracount($kept_informed_score);
	$solution_time_scoreArr=$commonobj->getarracount($solution_time_score);
	

	foreach ($tablehead as $tblkey => $headvalue) {
		$regionwiseArr[$headvalue]=empty($regionArr[$headvalue])?0:$commonobj->getarracount($regionArr[$headvalue]);
		$caseoriginArr[$headvalue]= empty($case_originArr[$headvalue])?0:$commonobj->getarracount($case_originArr[$headvalue]);
	}
	//print_r($countryName);
	 foreach ($csatsurvey as $key => $csatvalue) {
	 	foreach ($tablehead as $tblkey => $rs) {
	       	$producteasytouse[$csatvalue][]=(float)$a=empty($product_easy_of_useArr[$rs][$csatvalue])?0:number_format($product_easy_of_useArr[$rs][$csatvalue]/array_sum($product_easy_of_useArr[$rs])*100,2);

	       	$tech_ablityscore[$csatvalue][]=(float)$a=empty($technical_ability_scoreArr[$rs][$csatvalue])?0:number_format($technical_ability_scoreArr[$rs][$csatvalue]/array_sum($technical_ability_scoreArr[$rs])*100,2);

	       	$easyofaccess[$csatvalue][]=(float)$a=empty($ease_of_access_scoreArr[$rs][$csatvalue])?0:number_format($ease_of_access_scoreArr[$rs][$csatvalue]/array_sum($ease_of_access_scoreArr[$rs])*100,2);

	       	$nontechpermancearr[$csatvalue][]=(float)$a=empty($non_tec_performanceArr[$rs][$csatvalue])?0:number_format($non_tec_performanceArr[$rs][$csatvalue]/array_sum($non_tec_performanceArr[$rs])*100,2);

	       	$keptinformscore[$csatvalue][]=(float)$a=empty($kept_informed_scoreArr[$rs][$csatvalue])?0:number_format($kept_informed_scoreArr[$rs][$csatvalue]/array_sum($kept_informed_scoreArr[$rs])*100,2);

	       	$solutiontimearr[$csatvalue][]=(float)$a=empty($solution_time_scoreArr[$rs][$csatvalue])?0:number_format($solution_time_scoreArr[$rs][$csatvalue]/array_sum($solution_time_scoreArr[$rs])*100,2);
	    }
	}
}
	foreach ($tablehead as $inflowkey=> $rs) {	
        $weekdayname[]	=$rs;	
        $surveycountArr[]=(int)$surveyval=empty($overallexp[$rs])?0:count($overallexp[$rs]);
        $csatgreen[]=(float)$alertgreen=empty($alert_type[$rs]['Green'])?0:number_format($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100,2);
        $csatnormal[]=(float)$alertgreen=empty($alert_type[$rs]['Normal'])?0:number_format($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100,2);
        $csatred[]=(float)$alertgreen=empty($alert_type[$rs]['Red'])?0:number_format($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100,2);
       
        $greentarget[] = (int)'80';
		$redtarget[] = (int)'3';
		

    	foreach ($regionArrName as $regkey => $regvalue) {
    		foreach ($csatstock as $csatkey => $csatvalues) {
    			$regionwiseArray[$regkey][$csatvalues][]=(float)$s=empty($regionwiseArr[$rs][$regvalue][$csatvalues])?0:number_format($regionwiseArr[$rs][$regvalue][$csatvalues]/array_sum($regionwiseArr[$rs][$regvalue])*100,2);
    		}
    		//$customercountry[$regvalue]
    	}
    	foreach ($caseOriginName as $casekey => $caseOriginVal) {
    		foreach ($csatstock as $csatkey => $csatvalues) {
    			$caseoriginArray[$caseOriginVal][$csatvalues][]=(float)$a=empty($caseoriginArr[$rs][$caseOriginVal][$csatvalues])?0:number_format($caseoriginArr[$rs][$caseOriginVal][$csatvalues]/array_sum($caseoriginArr[$rs][$caseOriginVal])*100,2);
    		}
    	}
	}
	//print_r($customerCountry);
	$Qry=$type.$selectQry."_".$selectrange;
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	     border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
			<div class='col-md-12' style='margin-top:12px'>
			    <div class="portlet">
			        <div class="portlet-body">
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop2"  name="projectwise" onchange="reload()">
		                    <?php
		                    	$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
		                    	echo '<option value="Overall">Overall Project</option>';
								foreach($drop1project as $drop1projectval){
								    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
								}
		                    ?>
		                    </select>
			                <script>  
			                     jQuery("#drop2").val("<?php echo $projectwise ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop3"  name="productwise"  onchange="reload()">
			                <?php
			                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
			                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
								echo '<option value="Overall">Overall Que</option>';
								foreach($drop1overall as $drop1overallval){
									echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
								}
			                ?>
			                </select>
			                <script> 
			                     jQuery("#drop3").val("<?php echo $productwise ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
						    <select class="form-control submit" id="drop4"  name="productgroup"  onchange="reload()">
			                	<?php
			                		
			                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                			$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
		                			$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery order by product_group ASC");
									echo '<option value="Overall">Overall Product</option>';
			                		foreach ($productName as $key => $value) {
			                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
			                		} 
			                	?>
			                </select>
			                <script> 
			                     jQuery("#drop4").val("<?php echo $productgroup ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control selectweek" id="drop5"  name="calendartype">
			                	<option value="Normal">Calendar</option>
			                	<option value="Fiscal">Fiscal</option>
			                </select>
			                <script> 
			                     jQuery("#drop5").val("<?php echo $calendartype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control selectweek" id="drop6"  name="selecttype">
			                	<option value="Weekly">Weekly</option>
			                    <option value="Monthly">Monthly</option>
			                    <option value="Quarterly">Quarterly</option>
			                </select>
			                <script> 
			                     jQuery("#drop6").val("<?php echo $selecttype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop7"  name="selectrange"  onchange="reload()">
			                	<option value="">--- Select ---</option>
			                	<?php
									$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
									foreach ($drowpdownArr as $key => $value) {
										$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
										echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
									}
		                		?> 
			                </select>
			                <script> 
								jQuery("#drop7").val("<?php echo $selectrange ?>");
			                </script>
			            </div>
			        </div>
			    </div>
			</div> 
		</div>   		
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>CSAT - Product Wise</div>
					<div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	<img src="img/exp.png" style="height: 25px;cursor: pointer;" id="csat-rawexport" title="Raw Download">
			        	<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        	<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			        </div>
		            
		    </div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container1"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container2"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container3"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">	
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container4"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container5"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">	
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container6"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container7"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">	
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container8"></div>
					</div>
				</div>
			</div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("#drop2").change(function(){
	    
	    if($("#drop2").val() == 'WC'){
	    	$("#drop3").val("WC");
	    	$("#drop4").val("WC");
	    }else{
	    	$("#drop3").val("Overall");
	    	$("#drop4").val("Overall");
	    }
	});
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	function reload(){
		document.getElementById("frmsrch").action = 'csat_product.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}

$(function() {
	Highcharts.setOptions({
	    lang: {
	        decimalPoint: '.',
	        thousandsSep: ''
	    },
	    animation: {
            duration: 3000,
            easing: 'easeOutBounce'
    	}
	});
	$('#container').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'CSAT - Rating %',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                format: '{value} %'
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:'{y}',
		                }
		            }
		        },
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Green %',
	            type:'column',	
	            data: <?php echo json_encode($csatgreen,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'Normal %',
	            type:'column',
	            data: <?php echo json_encode($csatnormal,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'Red %',
	            type:'column',
	            data: <?php echo json_encode($csatred,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        } ,{
	            name: 'Survey Count',
	            type:'spline',
	            yAxis:1,
	            data: <?php echo json_encode($surveycountArr,true);?>,

	        },{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
        });
		$('#container1').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Product Easy To Use',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($producteasytouse as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});
		$('#container2').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Technical Ablity Score',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($tech_ablityscore as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});
        $('#container3').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Easy Of Access',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($easyofaccess as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});
        $('#container4').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Non Technical Performance Score',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($nontechpermancearr as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});
		$('#container5').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Kept Informed Score',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($keptinformscore as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});
        $('#container6').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Questionnaire - Solution Time Score',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",
	                }
	            },
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }],
	        tooltip: {
	            shared: true
	        },

	        series: [
			<?php foreach($solutiontimearr as $csatkey => $regionArr){ ?>
			{
	            name: '<?php echo $csatkey; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
		});

		Highcharts.chart('container7', {
		    chart: {
		        type: 'column',
		        height: 300
		    },

		    title: {
		        text: 'Region wise - CSAT %'
		    },

		    xAxis: {
		        categories: <?php echo json_encode($weekdayname,true);?>,
		    },
		    yAxis: {
		        allowDecimals: true,
		        min: 0,
		        max:100,
		        labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	            },
		    },
		    credits: {
	            enabled: false
	        },

		    tooltip: {
		        formatter: function () {
		            return '<b>' + this.x + '</b><br/>' +
		                this.series.name + ': ' + this.y +'%'+'<br/>';
		        }
		    },
		    colors: ['#FF0000','#FF7F00','#1ED570'],
		    plotOptions: {
		        column: {
		            stacking: 'normal'
		        },
		        series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",

	                }
	            },
		    },

		    series: [
			<?php
			$i=0;
			foreach($regionwiseArray as $wkkey => $wkArr){
				foreach($wkArr as $key => $lastArr){
					?> 
					{
						name: '<?php echo $wkkey .'-'. $key; ?>',
						data: <?php echo json_encode($lastArr);?>,
						stack: '<?php echo $i; ?>'
					},
					<?php
				}
				$i++;
			}
			?>
			]
		});
		Highcharts.chart('container8', {
		    chart: {
		        type: 'column',
		        height: 300
		    },

		    title: {
		        text: 'Case Origin - CSAT %'
		    },

		    xAxis: {
		        categories: <?php echo json_encode($weekdayname,true);?>,
		    },
		    yAxis: {
		        allowDecimals: true,
		        min: 0,
		        max:100,
		        labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	            },
		    },
		    credits: {
	            enabled: false
	        },

		    tooltip: {
		        formatter: function () {
		            return '<b>' + this.x + '</b><br/>' +
		                this.series.name + ': ' + this.y +'%'+'<br/>';
		        }
		    },
		    colors: ['#FF0000','#FF7F00','#1ED570'],
		    plotOptions: {
		        column: {
		            stacking: 'normal'
		        },
		        series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",

	                }
	            },
		    },

		    series: [
			<?php
			$i=0;
			foreach($caseoriginArray as $wkkey => $wkArr){
				foreach($wkArr as $key => $lastArr){
					?> 
					{
						name: '<?php echo $wkkey .'-'. $key; ?>',
						data: <?php echo json_encode($lastArr);?>,
						stack: '<?php echo $i; ?>'
					},
					<?php
				}
				$i++;
			}
			?>
			]
		});
});


'use strict';
// import Highcharts from '../parts/Globals.js';
/* global document */
// Load the fonts
// Highcharts.createElement('link', {
//    href: 'https://fonts.googleapis.com/css?family=Signika:400,700',
//    rel: 'stylesheet',
//    type: 'text/css'
// }, null, document.getElementsByTagName('head')[0]);

// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(https://www.highcharts.com/samples/graphics/sand.png)';
});


Highcharts.theme = {
   colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
      '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: 'Signika, serif'
      }
   },
   title: {
      style: {
         color: 'black',
         fontSize: '16px',
         fontWeight: 'bold'
      }
   },
   subtitle: {
      style: {
         color: 'black'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   plotOptions: {
      series: {
         shadow: true
      },
      candlestick: {
         lineColor: '#404048'
      },
      map: {
         shadow: false
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#D0D0D8'
      }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#C0C0C8',
         'stroke-width': 1,
         states: {
            select: {
               fill: '#D0D0D8'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#C0C0C8'
   },

   // General
   background2: '#E0E0E8'

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
$('#csat-rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'csat_rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
</script>
</script>
