<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange']){
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$tlname   		=	$_POST['tlname'];
	$enggname   	=	$_POST['enggname'];
}else{
	$selectrange    =!   empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=!   empty($projectwise)?$projectwise:"Overall";
	$productwise 	=!   empty($productwise)?$productwise:"Overall";
	$productgroup 	=!   empty($productgroup)?$productgroup:"Overall";
	$calendartype 	=!   empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=!   empty($selecttype)?$selecttype:"Weekly";
	//echo "SELECT count(distinct case_owner) as counting,team from aruba_open where calendar_week='2017Week44' group by team order by counting ASC";
	$tlnamelist=$commonobj->arrayColumn($commonobj->getQry("SELECT distinct team from aruba_open where calendar_week='$selectrange' ORDER BY id ASC limit 0 , 1"),'','team');
	$tlname    		=!   empty($tlname) ? $tlname:$tlnamelist[0];
	$enggname[]    	=!   empty($enggname) ? $enggname:"Overall";
}
if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	$QryCondition.= $projectwise !='Overall' ?" and wlan_ns='".$projectwise."'":'';
	$QryCondition.= $productwise!='Overall'?" and que_new='".$productwise."'":'';
	$QryCondition.= $productgroup!='Overall'?" and product_group='".$productgroup."'":'';
	//$QryCondition.= $engname.= $tlname == 'Overall' ? '' : " and team = '$tlname'";
	//$QryCondition.= $enggname == 'Overall' ? '' : " and case_owner = '$enggname'";
	
	$engQry = in_array('Overall',$enggname) ? " and team = '$tlname'":" and case_owner in ('".implode("','",$enggname)."')";

	$headertotalArr = $commonobj->arrayColumn($commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id asc"),'',$type.$selectQry);
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));

	if(in_array('Overall',$enggname)){
		$caseEngArr = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct case_owner from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner ,".$type.$selectQry." order by case_owner asc"),'','case_owner');
	}
	
	$openQuryArr = $commonobj->getQry("select count(*) as open_count,".$type.$selectQry.",case_owner from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner ,".$type.$selectQry." order by case_owner asc");
	foreach($openQuryArr as $weeknamearr){
		$opencase[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['open_count'];
	}
	$totopencaseArr = $commonobj->multiArrayCheck($opencase,$tablehead);
	
	$closeQuryArr = $commonobj->getQry("select count(*) as close_count,".$type.$selectQry.",case_owner from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($closeQuryArr as $weeknamearr){
		$closecase[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['close_count'];
	}
	$closedcaseArr = $commonobj->multiArrayCheck($closecase,$tablehead);

	$closettcQuryArr = $commonobj->getQry("select sum(ttc) as ttc_sum,".$type.$selectQry.",case_owner from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($closettcQuryArr as $weeknamearr){
		$closecasettc[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['ttc_sum'];
	}
	foreach($closecasettc as $closettckey => $closettcvalArr){
		foreach($tablehead as $tblhd){
			$a = empty($closettcvalArr[$tblhd])?0:$closettcvalArr[$tblhd];
			$b = empty($closecase[$closettckey][$tblhd])?0:$closecase[$closettckey][$tblhd];
			if($b > 0){
				$closedttcArr[$closettckey][] = round($a/$b,2);
			}else{
				$closedttcArr[$closettckey][] = (int)0;
			}
		}
	}
	
	//Closed Case Vs SDC Calculation
	$closesdcQuryArr = $commonobj->getQry("select sum(sdc) as sdc_sum,".$type.$selectQry.",case_owner from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($closesdcQuryArr as $weeknamearr){
		$closecasesdc[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['sdc_sum'];
	}
	$closesdcoverallQuryArr = $commonobj->getQry("select count(sdc) as sdc_count,".$type.$selectQry.",case_owner from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($closesdcoverallQuryArr as $weeknamearr){
		$closecasesdcoverall[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['sdc_count'];
	}
	//print_r($closecasesdcoverall);
	foreach($closecasesdc as $closesdckey => $closesdcvalArr){
		foreach($tablehead as $tblhd){
			$x = empty($closesdcvalArr[$tblhd])?0:$closesdcvalArr[$tblhd];
			$y =empty($closecasesdcoverall[$closesdckey][$tblhd])?0:$closecasesdcoverall[$closesdckey][$tblhd];
			if($y > 0){
				$closedsdcArr[$closesdckey][] = round(($x/$y)*100,2);
			}else {
				$closedsdcArr[$closesdckey][] = (int)0;
			}
			
		}
	}

	//CSAT Summary
	$csatoverexpQuryArr = $commonobj->getQry("select avg(overall_experience) as overall_exp_avg,count(*) as survey_count,avg(loyalty_index) as loyelty_index_avg,".$type.$selectQry.",case_owner from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition $engQry  group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($csatoverexpQuryArr as $weeknamearr){
		$csatoverexp[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(float)$a=round($weeknamearr['overall_exp_avg'],2);
		$csatsurveycnt[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(float)$a=round($weeknamearr['survey_count'],2);
	}

	$csatoverexpArr = $commonobj->multiArrayCheck($csatoverexp,$tablehead);
	$csatsurveyArr = $commonobj->multiArrayCheck($csatsurveycnt,$tablehead);
	// CSAT Rating%
	// Green
	$csatratgreenQuryArr = $commonobj->getQry("select count(*) as green_count,alert_type,".$type.$selectQry.",case_owner from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Green' $QryCondition $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($csatratgreenQuryArr as $weeknamearr){
		$csatgreen[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['green_count'];
	}
	foreach($csatgreen as $csatgreenkey => $csatgreenval){
		foreach($tablehead as $tblhd){
			$csatgreenArr[$csatgreenkey][] = empty($csatgreenval[$tblhd])?0:round(($csatgreenval[$tblhd]/$csatsurveycnt[$csatgreenkey][$tblhd])*100,2);
		}
	}
	//Red
	$csatratredQuryArr = $commonobj->getQry("select count(*) as red_count,alert_type,".$type.$selectQry.",case_owner from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Red' $QryCondition  $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($csatratredQuryArr as $weeknamearr){
		$csatred[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['red_count'];
	}
	foreach($csatred as $csatredkey => $csatredval){
		foreach($tablehead as $tblhd){
			$y = empty($csatsurveycnt[$csatredkey][$tblhd])?0:$csatsurveycnt[$csatredkey][$tblhd];
			if($y > 0){
				$csatredArr[$csatredkey][] = empty($csatredval[$tblhd])?0:round(($csatredval[$tblhd]/$y)*100,2);
			}else{
				$csatredArr[$csatredkey][] = 0;
			}
			
		}
	}
	//Normal
	$csatratnorQuryArr = $commonobj->getQry("select count(*) as nor_count,alert_type,".$type.$selectQry.",case_owner from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Normal' $QryCondition  $engQry group by case_owner,".$type.$selectQry." order by case_owner asc");
	foreach($csatratnorQuryArr as $weeknamearr){
		$csatnor[ucwords($weeknamearr['case_owner'])][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['nor_count'];
	}
	//print_r($csatnor);
	foreach($csatnor as $csatnorkey => $csatnorval){
		foreach($tablehead as $tblhd){
			$x = empty($csatsurveycnt[$csatnorkey][$tblhd])?0:$csatsurveycnt[$csatnorkey][$tblhd];
			if($x > 0){
				$csatnorArr[$csatnorkey][] = empty($csatnorval[$tblhd])?0:round(($csatnorval[$tblhd]/$x)*100,2);
			}else{
				$csatnorArr[$csatnorkey][] = 0;
			}
			
		}
	}
	//print_r($csatnorArr);
}
include "includes/header.php";
?>
<link rel="stylesheet" href="../assets/sumoselect.min.css">
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-sm-2 {
	   		width: 16.667%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
    	margin-right: 0px;
	}
	.SumoSelect .select-all {
		height: 34px !important;
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="tlname"  name="tlname">
		                <?php
		               		$Qry.=$projectwise !='Overall' ?" and wlan_ns='".$projectwise."'":'';
							$Qry.= $productwise!='Overall'?" and que_new='".$productwise."'":'';
							$Qry.= $productgroup!='Overall'?" and product_group='".$productgroup."'":'';

							//echo "SELECT distinct team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by team asc";

							$tllist = $commonobj->getQry("SELECT distinct team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by team asc");
							foreach ($tllist as $key => $value) {
								echo'<option value="'.$value['team'].'">'.$value['team'].'</option>';
							}
	                	?> 
		                </select>
		                <script> 
		                     jQuery("#tlname").val("<?php echo $tlname ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control testSelAll" id="enggname"  name="enggname[]" multiple="multiple" onchange="console.log($(this).children(':selected').length)">
		                <?php
							$tllist = $commonobj->getQry("SELECT distinct case_owner from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry and team='$tlname' order by case_owner asc");
							foreach ($tllist as $key => $value) {
								if(count($caseEngArr)>0){
									$selected=in_array($value['case_owner'],$caseEngArr)?"selected":"";
								}else{
									$selected=in_array($value['case_owner'],$enggname)?"selected":"";
								}
								echo "<option value='".$value['case_owner']."' $selected >".$value['case_owner']."</option>";
							}
	                	?> 
		                </select>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop2"  name="projectwise" onchange="reload()">
	                    <?php
							$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
	                    	echo '<option value="Overall">Overall Project</option>';
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script> 
		                     jQuery("#drop2").val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="productwise" onchange="reload()">
		                <?php
		                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
		                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
							echo '<option value="Overall">Overall Que</option>';
							foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="productgroup" onchange="reload()">
		                	<?php
								
		                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                		$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
		                		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery");

								echo '<option value="Overall">Overall Product</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype" >
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop7"  name="selectrange"  onchange="reload()">
		                	<option value="">--- Select ---</option>';
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
	<?php 
	foreach ($tablehead as $inflowkey=> $rs) {	
            $weekdayname[]	=$rs;	
     }
	?>              		
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>Engineerwise -  Performance</div>
		            <div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<div id="container"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12">
						<div id="container1"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12">
						<div id="container2"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12">
						<div id="container4"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>CSAT</div>
		            <div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12">
						<div id="container5"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12">
						<div id="container6"></div>
					</div>
				</div>
			</div>
		</div>
		
</form>
<?php 
include("includes/footer.php");
?>
<script src="../assets/jquery.sumoselect.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
        window.testSelAll = $('.testSelAll').SumoSelect({okCancelInMulti:true, selectAll:true });
	});
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("#tlname").change(function(){
	   var tlname = $("#tlname").val();
	   var projectwise= $("#drop2").val();
	   var productwise= $("#drop3").val();
	   var productgroup= $("#drop4").val();
	   var calendartype=$("#drop5").val();
	   var selecttype=$("#drop6").val();
	   var selectrange=$("#drop7").val();
	    $.ajax({
	      url: 'ajax_engg_trend.php',
	      type: 'POST',
	      data: {'tlname':tlname,'projectwise':projectwise,'productwise':productwise,'productgroup':productgroup,'calendartype':calendartype,'selecttype':selecttype,'selectrange':selectrange},
	      success: function(output) {
			var obj = jQuery.parseJSON( output);
	        $("#enggname").html("");
	        $("#enggname").html(obj);
			$('.testSelAll')[0].sumo.reload();
	      }
	    }); 
	});
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});

	function reload(){
		document.getElementById("frmsrch").action = 'engg_trend.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}
$(function() {
	Highcharts.setOptions({
	    lang: {
	        decimalPoint: '.',
	        thousandsSep: ''
	    }
	});
	var chart = new Highcharts.Chart({
		chart: {
	            zoomType: 'xy',
	            height: 350,
	            renderTo: 'container'
		},
	    title: {
	    text: 'Open Cases',
	    style:{
	            color: 'black',
	            fontSize: '12px'
	        }       
	    },
	    legend: {
	        itemStyle: {
	            fontWeight: 'bold',
	            fontSize: '11px'
	        }
	    },
	    credits: {
	        enabled: false
	    }, 
		//colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	    xAxis: {
	        categories: <?php echo json_encode($weekdayname,true);?>,
	    },
    	plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                }	
            }
	    },

	    tooltip: {
	        //pointFormat: "{point.y}"
	        shared: true
	    },
     	series: [
		<?php foreach($totopencaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'spline',
	            data: <?php echo json_encode($regionArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:-10
                } 
			},
		 <?php } ?>]
	});

	var chart = new Highcharts.Chart({
		chart: {
	            zoomType: 'xy',
	            height: 350,
	            renderTo: 'container1'
		},
	    title: {
	    text: 'Closed Cases',
	    style:{
	            color: 'black',
	            fontSize: '12px'
	        }       
	    },
	    legend: {
	        itemStyle: {
	            fontWeight: 'bold',
	            fontSize: '11px'
	        }
	    },
	    credits: {
	        enabled: false
	    }, 
		//colors: ['#FE4469', '#263249', '#0000FF', '#55FF2A', '#FF0000'],
	    xAxis: {
	        categories: <?php echo json_encode($weekdayname,true);?>,
	    },
	    plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                     allowOverlap:true,
		                }	
		            }
		        },

	    tooltip: {
	        //pointFormat: "{point.y}"
	        shared: true
	    },
     series: [
		 <?php foreach($closedcaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'spline',
	            data: <?php echo json_encode($regionArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:-10
                } 
			},
		 <?php } ?>]
	});

	$('#container2').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 350
        },
        title: {
        text: 'Closed Cases Vs Avg. TTC ',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        //colors: ['#FF2AAA', '#55FF2A', '#FFAA55', '#55FF2A'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    allowOverlap:true,
                }
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,

	                    format:"{point.y:.0f}*"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}*"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        series: [
		<?php foreach($closedcaseArr as $region => $regionArr){ ?>
		{
            name: '<?php echo $region; ?>',
            type: 'column',
            data: <?php echo json_encode($regionArr,true);?>,
        },
		<?php } ?>
		<?php foreach($closedttcArr as $region => $regionArr){ ?>
		{
            name: '<?php echo $region; ?>-TTC',
            type: 'spline',
			yAxis:1,
            data: <?php echo json_encode($regionArr,true);?>,
        },
		<?php } ?>]
	});
    $('#container4').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 350
        },
        title: {
         text: 'Close Cases Vs SDC',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        //colors: ['#AA002B','#AA00FF', '#0000FF', '#55FF2A'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                }	
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
				format: '{value} %'
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}%",
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        
        series: [
		<?php foreach($closedcaseArr as $region => $regionArr){ ?>
		{
            name: '<?php echo $region; ?>',
            type: 'column',
            data: <?php echo json_encode($regionArr,true);?>,
        },
		<?php } ?>
		<?php foreach($closedsdcArr as $region => $regionArr){ ?>
		{
            name: '<?php echo $region; ?>-SDC',
            type: 'spline',
			yAxis:1,
            data: <?php echo json_encode($regionArr,true);?>,
			tooltip: {
                valueSuffix: '%'
            },
            dataLabels: {
                    format:"{y}%",
                }
        },
		<?php } ?>]
    });
    $('#container5').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 350
	        },
	        title: {
	        text: 'CSAT - Summary',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	       // colors: ['#218C8D', '#39C1FB', '#272822','#D50055','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },	        
	        series: [<?php foreach($csatsurveyArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?> - Survey Count',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($csatoverexpArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?> - Overall Experience',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			]
    });
    $('#container6').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 350
	        },
	        title: {
	        text: 'CSAT - Rating %',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                format: '{value} %'
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:'{y}',
		                }
		            }
		        },
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [
			<?php foreach($csatgreenArr as $region => $regionArr){ ?>
			{
	            name: "<?php echo $region; ?> - Green %",
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
				tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },
			<?php } ?>
			<?php foreach($csatnorArr as $region => $regionArr){ ?>
			{
	            name: "<?php echo $region; ?> - Normal %",
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
				tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },
			<?php } ?>
			<?php foreach($csatredArr as $region => $regionArr){ ?>
			{
	            name: "<?php echo $region; ?> - Red %",
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
				tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },
			<?php } ?>
			<?php foreach($csatsurveyArr as $region => $regionArr){ ?>
			{
	            name: "<?php echo $region; ?> - Survey Count",
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			]
    });
});
'use strict';
// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(https://www.highcharts.com/samples/graphics/sand.png)';
});


Highcharts.theme = {
   colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
      '#55BF3B', '#DF5353', '#7798BF', '#aaeeee','#FF7F55','#0080D5','#FF2A55','#00D580'],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: 'Signika, serif'
      }
   },
   title: {
      style: {
         color: 'black',
         fontSize: '16px',
         fontWeight: 'bold'
      }
   },
   subtitle: {
      style: {
         color: 'black'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   plotOptions: {
      series: {
         shadow: true
      },
      candlestick: {
         lineColor: '#404048'
      },
      map: {
         shadow: false
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#D0D0D8'
      }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#C0C0C8',
         'stroke-width': 1,
         states: {
            select: {
               fill: '#D0D0D8'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#C0C0C8'
   },

   // General
   background2: '#E0E0E8'

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
</script>