<?php 
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['dbselect']){
	$selectdata 	= 	$_POST['dbselect'];
	$selectype 		= 	$_POST['report_datatype'];
}else{
	$selectdata=!empty($selectdata)?$selectdata:"Wk 22 16";
	$selectype=!empty($selectype)?$selectype:"Weekly";
}
if($selectype!=""){
	if($selectype=='Quarterly'){
		$selectQry= 'cr_qtrly';
		$variables="qtrly";
		$teamprod=1040;
		$backlogval=169;
		$escalationday=39;
		$firstdayres=230;

	}else if($selectype=='Monthly') {
		$selectQry= 'cr_month';
		$variables="month";
		$teamprod=320;
		$backlogval=11;
		$escalationday=3;
		$firstdayres=22;
	}else if($selectype=='Weekly'){
		$selectQry= 'cr_week';
		$variables="week";
		$teamprod=80;
		$backlogval=0;
		$escalationday=0;
		$firstdayres=0;
	}else{
		$selectQry= 'cr_date';	
		$variables="date";
		$teamprod=16;
		$backlogval=14;
		$escalationday=0;
		$firstdayres=0;
	}
	$inflwQury = $conn->prepare("select * from tplink_master order by master_id desc");
	$inflwQury->execute();
	$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($inflwQuryArr as $weeknamearr){
		$arraycount[]=$weeknamearr[$selectQry];
		$tableheadArr[$weeknamearr[$selectQry]]=0;
		$closeddate[]=$weeknamearr["cl_".$variables];
		$kbcount[$weeknamearr["cl_".$variables]][]=$weeknamearr["kb"];
		$escount[$weeknamearr[$selectQry]][]=$weeknamearr["escalation"];
		$fdaycount[$weeknamearr["cl_".$variables]][]=$weeknamearr["first_day_resolution"];
	}
	unset($fdaycount['-']);
	$masterdateArr=array_reverse(array_count_values($arraycount));
	foreach ($masterdateArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($masterkey=="$selectdata"){break;}
	}
	$arrayval=array_reverse($totalArrval);
    $tablehead=array_reverse(array_slice($arrayval, 0, 10, true));
    $closeArr=array_count_values($closeddate);
    unset($closeArr['-']);
    $closeArray=array_reverse(array_merge($tableheadArr,$closeArr));
    foreach ($closeArray as $closekey => $closedvalue) {
		$closedvaluearr[$closekey]=$closedvalue;
	       if($closekey=="$selectdata"){break;}
	}
	$closedvaluearray=array_reverse($closedvaluearr);
    $closedate=array_reverse(array_slice($closedvaluearray, 0, 10, true));
    foreach ($kbcount as $kbcountkey => $kbcountvalue) {
    	$kbcountArr[$kbcountkey]=array_count_values($kbcountvalue);
    }
    unset($kbcountArr['-']);
    foreach ($kbcountArr as $kbcountkeys => $kbcountvalues) {
    	$kbvaluesArr[$kbcountkeys]=!empty($kbcountvalues['Yes'])?$kbcountvalues['Yes']:0;
    }
    $kbcounttotArr=array_reverse(array_merge($tableheadArr,$kbvaluesArr));
    foreach ($kbcounttotArr as $kbcounttotArrkey => $kbcounttotArrvalue) {
		$kbarrayvalue[$kbcounttotArrkey]=$kbcounttotArrvalue;
	       if($kbcounttotArrkey=="$selectdata"){break;}
	}
	$kbcountarray=array_reverse($kbarrayvalue);
	$totkbcount=array_reverse(array_slice($kbcountarray, 0, 10, true));
	//escalation start here
	foreach ($escount as $escountkey => $escountvalue) {
    	$escountArr[$escountkey]=array_count_values($escountvalue);
    }
    unset($escountArr['-']);
    foreach ($escountArr as $escountkeys => $escountvalues) {
    	$esvaluesArr[$escountkeys]=!empty($escountvalues['Yes'])?$escountvalues['Yes']:0;
    }
    $escounttotArr=array_reverse(array_merge($tableheadArr,$esvaluesArr));
    foreach ($escounttotArr as $escounttotArrkey => $escounttotArrvalue) {
		$esarrayvalue[$escounttotArrkey]=$escounttotArrvalue;
	       if($escounttotArrkey=="$selectdata"){break;}
	}
	$escountarray=array_reverse($esarrayvalue);
	$totescount=array_reverse(array_slice($escountarray, 0, 10, true));
	//print_r($totescount);
	foreach ($fdaycount as $fdkey => $fdayvalue) {
		$totcount=count($fdayvalue);
		$yescount[$fdkey]=array_count_values($fdayvalue);
	}
	foreach ($yescount as $fistdayesckey => $fistdayescvalue) {
		$totesclation[$fistdayesckey]=round($fistdayescvalue['Yes']/array_sum($fistdayescvalue)*100);
	}
	$fdayesccounttotArr=array_reverse(array_merge($tableheadArr,$totesclation));
    foreach ($fdayesccounttotArr as $fdesckey => $fdescvalue) {
		$fdayesarrayvalue[$fdesckey]=$fdescvalue;
	       if($fdesckey=="$selectdata"){break;}
	}
	$fdayesvalue=array_reverse($fdayesarrayvalue);
	$fdayestotval=array_reverse(array_slice($fdayesvalue, 0, 10, true));
	//baglog calc
	$backlogQry = $conn->prepare("select * from tplink_others where $selectQry!='' order by others_id desc");
	$backlogQry->execute();
	$backlogArr = $backlogQry->fetchAll(PDO::FETCH_ASSOC);
	foreach ($backlogArr as $backlogvalue) {
		$backlogarr[$backlogvalue[$selectQry]]=$backlogvalue['backlog'];
		$ssqalogarr[$backlogvalue[$selectQry]]=$backlogvalue['ssqa'];
		$incomearr[$backlogvalue[$selectQry]]=$backlogvalue['incoming_calls'];
		$incomecallarr[$backlogvalue[$selectQry]]=$backlogvalue['incoming_calls_home_support'];
		$abandoncallarr[$backlogvalue[$selectQry]]=$backlogvalue['abandon_calls'];
		$abandoncallperarr[$backlogvalue[$selectQry]]=$backlogvalue['abandon%'];
		$csativrarr[$backlogvalue[$selectQry]]=$backlogvalue['csat_ivr'];
		$ivrsentArr[$backlogvalue[$selectQry]]=$backlogvalue['ivr_sent'];
		$ivrexcelentArr[$backlogvalue[$selectQry]]=$backlogvalue['ivr_excellent'];
		$backlogpercentage[$backlogvalue[$selectQry]]=$backlogvalue['backlog%'];
	}
	
	$backlogarr=array_reverse(array_merge($tableheadArr,$backlogarr));
	foreach ($backlogarr as $backlogkey => $backlogvalues) {
		$backlogothersval[$backlogkey]=$backlogvalues;
	       if($backlogkey=="$selectdata"){break;}
	}
	$backlogothersvalue=array_reverse($backlogothersval);
	$backlogarrvalues=array_reverse(array_slice($backlogothersvalue, 0, 10, true));

	//
	$backlogpercentage=array_reverse(array_merge($tableheadArr,$backlogpercentage));
	foreach ($backlogpercentage as $backlogperkey => $backlogpervalues) {
		$backlogperothersval[$backlogperkey]=$backlogpervalues;
	       if($backlogperkey=="$selectdata"){break;}
	}
	$backlogperothersvalue=array_reverse($backlogperothersval);
	$backlogperarrvalues=array_reverse(array_slice($backlogperothersvalue, 0, 10, true));
	//ssqa percentage
	
	$ssqalogarr=array_reverse(array_merge($tableheadArr,$ssqalogarr));
	foreach ($ssqalogarr as $ssqakey => $ssqavalues) {
		$ssqaothersval[$ssqakey]=$ssqavalues;
	       if($ssqakey=="$selectdata"){break;}
	}
	$ssqaothersvalue=array_reverse($ssqaothersval);
	$ssqaarrvalues=array_reverse(array_slice($ssqaothersvalue, 0, 10, true));
	//income calls
	
	$incomearr=array_reverse(array_merge($tableheadArr,$incomearr));
	foreach ($incomearr as $incomekey => $incomevalues) {
		$incomeothersval[$incomekey]=$incomevalues;
	       if($incomekey=="$selectdata"){break;}
	}
	$incomeothersvalue=array_reverse($incomeothersval);
	$incomearrvalues=array_reverse(array_slice($incomeothersvalue, 0, 10, true));
	//income call home supports
	
	$incomecallarr=array_reverse(array_merge($tableheadArr,$incomecallarr));
	foreach ($incomecallarr as $incomecallkey => $incomecallvalues) {
		$incomecallothersval[$incomecallkey]=$incomecallvalues;
	       if($incomecallkey=="$selectdata"){break;}
	}
	$incomecallothersvalue=array_reverse($incomecallothersval);
	$incomecallarrvalues=array_reverse(array_slice($incomecallothersvalue, 0, 10, true));
	//Abandon calls
	
	$abandoncallarr=array_reverse(array_merge($tableheadArr,$abandoncallarr));
	foreach ($abandoncallarr as $abandoncallkey => $abandoncallvalues) {
		$abandoncallothersval[$abandoncallkey]=$abandoncallvalues;
	       if($abandoncallkey=="$selectdata"){break;}
	}
	$abandoncallothersvalue=array_reverse($abandoncallothersval);
	$abandoncallarrvalues=array_reverse(array_slice($abandoncallothersvalue, 0, 10, true));
	//abandon call %
	
	$abandoncallperarr=array_reverse(array_merge($tableheadArr,$abandoncallperarr));
	foreach ($abandoncallperarr as $abandoncallperkey => $abandoncallpervalues) {
		$abandoncallperothersval[$abandoncallperkey]=$abandoncallpervalues;
	       if($abandoncallperkey=="$selectdata"){break;}
	}
	$abandoncallperothersvalue=array_reverse($abandoncallperothersval);
	$abandoncallperarrvalues=array_reverse(array_slice($abandoncallperothersvalue, 0, 10, true));
	//csat 
	$csatQry = $conn->prepare("select * from tplink_csat where $selectQry!='' order by id desc");
	$csatQry->execute();
	$csatArr = $csatQry->fetchAll(PDO::FETCH_ASSOC);
	foreach ($csatArr as $csatkey => $csatvalue) {
		$csatscore[$csatvalue[$selectQry]][]=$csatvalue['score'];
		$overallcsat[$csatvalue[$selectQry]][]=$csatvalue['alert'];
		$commcsatarr[$csatvalue[$selectQry]][]=$csatvalue['communication'];
		$effectivearr[$csatvalue[$selectQry]][]=$csatvalue['solution_effectiveness'];
		$technicalarr[$csatvalue[$selectQry]][]=$csatvalue['technical_knowledge'];
		$responsivearr[$csatvalue[$selectQry]][]=$csatvalue['responsiveness'];
		if($csatvalue['score']>2){
			$emailcount[$csatvalue[$selectQry]][]=count($csatvalue['score']);
		}	
		if($csatvalue['alert']!='Needs Improvement'){
			$overallalert[$csatvalue[$selectQry]][]=count($csatvalue['alert']);
		}
	}
	foreach ($csatscore as $csatkeys => $csatvalues) {
		foreach ($emailcount as $great2key => $great2value) {
			 if($csatkeys==$great2key){
			 	$csatemail[$csatkeys]=round(count($great2value)/count($csatvalues)*100);
			 	$emailcount1[$csatkeys]=count($great2value);
			 	$csatcount2[$csatkeys]=count($csatvalues);

			 }
		}
	}
	$csatemailcount=array_reverse(array_merge($tableheadArr,$csatemail));
    foreach ($csatemailcount as $csatemailkey => $csatemailvalue) {
		$csatemailvalues[$csatemailkey]=$csatemailvalue;
	       if($csatemailkey=="$selectdata"){break;}
	}
	$csatemailvaluess=array_reverse($csatemailvalues);
	$csatemailvaluess=array_reverse(array_slice($csatemailvaluess, 0, 10, true));
	//csat ivr
	$csativrarr=array_reverse(array_merge($tableheadArr,$csativrarr));
    foreach ($csativrarr as $csativrkey => $csativrvalue) {
		$csativrvalues[$csativrkey]=$csativrvalue;
	       if($csativrkey=="$selectdata"){break;}
	}
	$csativrvaluess=array_reverse($csativrvalues);
	$csativrvaluess=array_reverse(array_slice($csativrvaluess, 0, 10, true));
	foreach ($overallcsat as $overallcsatkey => $overallcsatvalue) {
		$countvalue[$overallcsatkey]=count($overallcsatvalue);
	}
	foreach ($overallalert as $key => $overallvalue) {
		$overallperc[$key]=count($overallvalue);
	}
	//csat sent overall
	foreach($ivrsentArr as $key1=>$value1){
		foreach ($csatcount2 as $key2 => $value2) {
			if($key1==$key2){
				$overallcsatcount[$key2]=$value1+$value2;
			}
		}
	}
	foreach($ivrexcelentArr as $key3=>$value3){
		foreach ($emailcount1 as $key4 => $value4) {
			if($key3==$key4){
				$overallcsatoverall[$key3]=$value3+$value4;
			}
		}
	}
	foreach ($overallcsatoverall as $key5 => $value5) {
		foreach ($overallcsatcount as $key6 => $value6) {
			if($key5==$key6){
					$csatoverallArr[$key5]=round($value5/$value6*100);
			}
		}
	}
	$overallperce=array_reverse(array_merge($tableheadArr,$csatoverallArr));
 	foreach ($overallperce as $overallkey => $overallpercevalues) {
		$overallpercentagevalues[$overallkey]=$overallpercevalues;
	       if($overallkey=="$selectdata"){break;}
	}
	$overallpercentagevalue=array_reverse($overallpercentagevalues);
	$overallpercentagevalue=array_reverse(array_slice($overallpercentagevalue, 0, 10, true));

	$csatsentQry = $conn->prepare("select $selectQry,csatsent_id from tplink_csatsent where $selectQry!='' order by csatsent_id desc");
	$csatsentQry->execute();
	$csatsentArr = $csatsentQry->fetchAll(PDO::FETCH_ASSOC);
	foreach ($csatsentArr as $csatsentkey=> $csatsentvalue) {
		$csatsentArray[$csatsentvalue[$selectQry]][]=$csatsentvalue[$selectQry];
	}
	foreach ($csatsentArray as $key3 => $value3) {
		$csatsentemail=array_count_values($value3);
		foreach ($csatsentemail as $key4 => $value4) {
			$csatsentemailcount[$key3]=$value4;
		}
	}
	$csatsentemailcount=array_reverse(array_merge($tableheadArr,$csatsentemailcount));
    foreach ($csatsentemailcount as $csatsentemailkey => $csatsentemailvalues) {
		$csatsentemailvalue[$csatsentemailkey]=$csatsentemailvalues;
	       if($csatsentemailkey=="$selectdata"){break;}
	}
	$csatsentemailvalue=array_reverse($csatsentemailvalue);
	$csatsentemailvalue=array_reverse(array_slice($csatsentemailvalue, 0, 10, true));
	//ivr sent
	$ivrsentcount=array_reverse(array_merge($tableheadArr,$ivrsentArr));
    foreach ($ivrsentcount as $ivrsentkey => $ivrsentvalues) {
		$ivrsentvalue[$ivrsentkey]=$ivrsentvalues;
	       if($ivrsentkey=="$selectdata"){break;}
	}
	$ivrsentvaluess=array_reverse($ivrsentvalue);
	$ivrsentvaluess=array_reverse(array_slice($ivrsentvaluess, 0, 10, true));
	//email recv
	$emailreccount=array_reverse(array_merge($tableheadArr,$countvalue));
    foreach ($emailreccount as $emailreckey => $emailrecvalues) {
		$emailrecvalue[$emailreckey]=$emailrecvalues;
	       if($emailreckey=="$selectdata"){break;}
	}
	$emailrecvaluess=array_reverse($emailrecvalue);
	$emailrecvaluess=array_reverse(array_slice($emailrecvaluess, 0, 10, true));
	//csat communication value
	foreach ($commcsatarr as $commkey => $commvalue) {
		$communcount=count($commvalue)*5;
		$communicationcount[$commkey]=round(array_sum($commvalue)/$communcount*100);
	}
	$communication=array_reverse(array_merge($tableheadArr,$communicationcount));
    foreach ($communication as $commukey => $commvalues) {
		$communicationtotArr[$commukey]=$commvalues;
	       if($commukey=="$selectdata"){break;}
	}
	$communicationtotArr=array_reverse($communicationtotArr);
	$communicationtotArr=array_reverse(array_slice($communicationtotArr, 0, 10, true));
	//efective 
	foreach ($effectivearr as $effectivekey => $effectivevalue) {
		$effectiveuncount=count($effectivevalue)*5;
		$effectiveunicationcount[$effectivekey]=round(array_sum($effectivevalue)/$effectiveuncount*100);
	}
	$effectiveunication=array_reverse(array_merge($tableheadArr,$effectiveunicationcount));
    foreach ($effectiveunication as $effectiveukey => $effectivevalues) {
		$effectiveunicationtotArr[$effectiveukey]=$effectivevalues;
	       if($effectiveukey=="$selectdata"){break;}
	}
	$effectiveunicationtotArr=array_reverse($effectiveunicationtotArr);
	$effectiveunicationtotArr=array_reverse(array_slice($effectiveunicationtotArr, 0, 10, true));
	//technical technicalarr
	foreach ($technicalarr as $technicalkey => $technicalvalue) {
		$technicaluncount=count($technicalvalue)*5;
		$technicalunicationcount[$technicalkey]=round(array_sum($technicalvalue)/$technicaluncount*100);
	}
	$technicalunication=array_reverse(array_merge($tableheadArr,$technicalunicationcount));
    foreach ($technicalunication as $technicalukey => $technicalvalues) {
		$technicalunicationtotArr[$technicalukey]=$technicalvalues;
	       if($technicalukey=="$selectdata"){break;}
	}
	$technicalunicationtotArr=array_reverse($technicalunicationtotArr);
	$technicaltotArr=array_reverse(array_slice($technicalunicationtotArr, 0, 10, true));
	//responsiveness
	foreach ($responsivearr as $responsivekey => $responsivevalue) {
		$responsiveuncount=count($responsivevalue)*5;
		$responsiveunicationcount[$responsivekey]=round(array_sum($responsivevalue)/$responsiveuncount*100);
	}
	$responsiveunication=array_reverse(array_merge($tableheadArr,$responsiveunicationcount));
    foreach ($responsiveunication as $responsiveukey => $responsivevalues) {
		$responsiveunicationtotArr[$responsiveukey]=$responsivevalues;
	       if($responsiveukey=="$selectdata"){break;}
	}
	$responsiveunicationtotArr=array_reverse($responsiveunicationtotArr);
	$responsivetotArr=array_reverse(array_slice($responsiveunicationtotArr, 0, 10, true));
}
// header("Content-type: application/vnd.ms-excel");
// header("Content-Disposition: attachment; filename=Tplink.xls");
// header("Pragma: no-cache");
// header("Expires: 0");
header( "Content-Type: application/vnd.ms-excel" );
header( "Content-disposition: attachment; filename=Tplink.xls" );
header("Pragma: no-cache");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .font-size{
	    font-size: small !important;
		font-weight: 200 !important;
		border:1px solid;
    }
    .bold-font{
    	    font-weight: 900 !important;
    	    border:1px solid;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #F3F3F3;
	}
	.table-bordered, .table-bordered>tbody>tr>td{
	    border: 1px solid #d4e1ef;
	}
	tr,th,td{
		
	}
</style>
<!-- <form method="POST"> -->
	<!-- <div class="portlet box navbar navbar-default filter-postion"  style="width: 80.6%;background-color: #F3F3F3;">
        <div class="portlet-title" >
        <div class="caption"><i class="fa fa-cogs"></i>Filter</div>
        </div>
        <div class="portlet-body pull-right" style="background-color: #F3F3F3; padding-top: 11px;padding-top: 10px;
    margin-left: -11px;padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
            <div class="row portlet" style="margin-bottom: 0px;">
	            <div class="form-group col-md-5 col-sm-4">
	                <select class="form-control" id="reporttype"  name="report_datatype">
	                	<option value="Daily">Daily</option>
	                	<option value="Weekly">Weekly</option>
	                    <option value="Monthly">Monthly</option>
	                    <option value="Quarterly">Quarterly</option>
	                </select>
	                <script> 
	                     jQuery("#reporttype").val("<?php echo $selectype ?>");
	                </script>
	            </div>
	           <div class="form-group col-md-5 col-sm-4" style="margin-bottom:0px;">
	                <select class="form-control  dbselect" id="typedata" name="dbselect">
	                <?php
						$selecttypeQry = $conn->prepare("SELECT DISTINCT $selectQry FROM tplink_master where $selectQry!='' order by master_id desc");
						$selecttypeQry->execute();
						$drowpdownArr = $selecttypeQry->fetchAll(PDO::FETCH_ASSOC);
						foreach ($drowpdownArr as $key => $value) {
						$menulist[]=$value[$selectQry];
						}
						echo'<option value="">--- Select ---</option>';
	                    foreach($menulist as $weeknamearr) {
	                        echo'<option value="'.$weeknamearr.'" $selected>'.$weeknamearr.'</option>';
	                    }
	                ?> 
	                </select>
	                <script> 
	                    jQuery("#typedata").val("<?php echo $selectdata ?>");
	                </script>
	            </div>
	            <div class="form-group col-md-1 col-sm-4 " style="margin-top:18px;">
	                <img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;margin-top:-22px;" onclick="funexport();">
	            </div>
	        </div>
        </div>
    </div> -->
		<div class="portlet box blue" style='margin-top: 27px;'>
		    <div class="portlet-title">
		        <div class="caption">
		           <table>
                <tr rowspan=2>
                    <tr>
                        <td colspan=3 style="background-color:white;"></td>
                        <td colspan=<?php echo count($tablehead)+3?> align="center"style="border:1px solid;background-color:#3598DC;color:white;"><H3>
                            <i class="icon-calendar"></i>Summay <?php echo $selectdata; ?></div>
                        </H3>
                        </td>
                    </tr>
                </tr>
            <table>
		        <div class="tools"> </div>
		    </div>
		    <div class="portlet-body">
		       <!-- <div class="table-scrollable"> -->
		        	<table class="table table-striped table-bordered table-hover text-center">
                        <thead>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                                <th scope="col" style="width:250px !important;background-color:#3598dc;color:white;border:1px solid;" class="text-center">Metrics</th>
                                <th colspan="2" style="background-color:#3598dc;color:white;text-align: center;border:1px solid;">Targets</th>
                                <?php
                           		$bg=1;
	                            foreach ($tablehead as $inflowkey=> $rs) {
	                            $weekdayname[]=$inflowkey;
                        		?>
                            	<th class=" text-center blue" style="background-color:#3598dc;color:white;border:1px solid;"><?php echo $inflowkey; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                        </thead>
                        <tbody>

                            <tr style="background-color:#E8F1FA;">
                               <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Case Created </td>
                                <td style="border:1px solid"> </td><td style="border:1px solid"> </td>
                                <?php
	                            foreach ($tablehead as $keycount=> $totcountval) {
                        		?>
                            	<th class=" font-size text-center" style="border:1px solid"><?php echo $totcountval; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Cases Closed </td>
                                <td class="bold-font" style="border:1px solid"></td><td class="bold-font" style="border:1px solid"></td>
                                <?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center" style="border:1px solid"><?php echo $closeddate; ?></th>
                        		<?php
                            	}
                        		?>
                                
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Cases in Backlog </td>
                                <td class="bold-font" style="border:1px solid"></td><td class="bold-font" style="border:1px solid"></td>
                                <?php
	                            foreach ($backlogarrvalues as $keycount=> $backlogdata) {
                        		?>
                            	<th class=" font-size text-center" style="border:1px solid"><?php echo $backlogdata; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Backlog % of week </td>
                                <td class="bold-font" style="border:1px solid">< 20%</td>
                                <td class="bold-font"><?php echo $backlogval;?></td>
                                <?php
	                           foreach ($backlogperarrvalues as $keycount=> $closeddate12) {
                        		?>
                            	<th class=" font-size text-center" style="border:1px solid"><?php echo $closeddate12; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td></td>
	                            <td></td>
	                            <td></td>
                            	<?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center"></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> KB Count </td>
                                <td style="border:1px solid"></td>
	                            <td style="border:1px solid"></td>
                                <?php
	                            foreach ($totkbcount as $closurecountkey=> $closurecountvalues) {
                        		?>
                            	<th class="font-size text-center" style="border:1px solid"><?php echo $closurecountvalues; ?></th>
                        		<?php
                            	}
                        		?>
                        		
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Escalations Count </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
	                            foreach ($totescount as $escountkey=> $escountvalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escountvalues; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td  style="border:1px solid"> Escalations </td>
                                <td class="bold-font" style="border:1px solid"><?php echo '<5%';?> </td>
                                 <td class="bold-font"><?php echo $escalationday;?></td>
                                <?php
                                foreach ($tablehead as $keycount=> $totcountval) {
		                           foreach ($totescount as $escountkey=> $escountvalues) {
		                           	if($keycount==$escountkey){
		                        		?>
		                            	<th class=" font-size text-center" style="border:1px solid"><?php echo round(number_format($escountvalues/$totcountval*100,1))."%"; ?></th>
		                        		<?php
		                            	}
	                            	}
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> First day resolution% </td>
                                <td class="bold-font" style="border:1px solid"><?php echo '30%';?> </td>
                                 <td class="bold-font"><?php echo "$firstdayres";?></td>
                                <?php
	                            foreach ($fdayestotval as $fdayeskey=> $fdayesvalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $fdayesvalues."%"; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Soft skills score of the team </td>
                                <td class="bold-font" style="border:1px solid">85%</td>
                                <td class="bold-font" style="border:1px solid">85%</td>
                                <?php
	                            foreach ($ssqaarrvalues as $ssqaarraykey=> $ssqavalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $ssqavalues; ?></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Incoming Calls </td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <?php
	                            foreach ($incomearrvalues as $incomearraykey=> $overallvalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $overallvalues; ?></th>
                        		<?php
                            	}
                            	?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                            	<td style="border:1px solid"> Incoming Calls -Home Support  </td>
                            	<td class="bold-font" style="border:1px solid"></td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <?php
	                            foreach ($incomecallarrvalues as $incomecallarrkey=>$overallemailvalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $overallemailvalues; ?></th>
                        		<?php
                            	}
                            	?>
                            </tr>
                           <tr style="background-color:#E8F1FA;">
                           <td colspan=3 style="background-color:white;"></td>
                            	<td style="border:1px solid"> Abandon calls</td>
                            	<td class="bold-font" style="border:1px solid"></td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <?php
	                            foreach ($abandoncallarrvalues as $abandoncallarrkey=> $escvalues) {
                        		?>
                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escvalues; ?></th>
                        		<?php
                            	}
                            	?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Abandon % </td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <td class="bold-font" style="border:1px solid"></td>
                                <?php
                                 foreach ($abandoncallperarrvalues as $incomearraykey=> $abandoncallpervalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $abandoncallpervalue;?></th>
	                        		<?php 			
			                        }
                        		?>
                            </tr>
                           <tr style="background-color:#E8F1FA;">
                           <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Team Poroductivity </td>
                                <td class="bold-font" style="border:1px solid">80 /Wk</td>
                                <td class="bold-font"><?php echo $teamprod ?></td>
                                <?php
                                 foreach ($closedate as $keycount=> $closeddate) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo round($closeddate/$teamprod*100)."%";?></th>
	                        		<?php 			
			                        }
                        		?>
                            </tr>
                           <tr>
                           <td colspan=3 style="background-color:white;"></td>
                            	<td ></td>
	                            <td></td>
	                            <td></td>
                            	<?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center"></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td colspan="3" style="background-color:#3598dc;color:white;border:1px solid">CSAT Score</td>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Email </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($csatemailvaluess as $escavgkeys=> $escavgvalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue."%"?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> IVR </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($csativrvaluess as $escavgkeys=> $escavgvalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                           
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Overall </td>
                                <td class="bold-font" style="border:1px solid">85%</td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($overallpercentagevalue as $escavgkeys=> $escavgvalue4) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue4."%" ?></th>
	                        		<?php
	                            }
	                            ?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td></td>
	                            <td></td>
	                            <td></td>
                            	<?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center"</th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td colspan="3" style="background-color:#3598dc;color:white;border:1px solid">CSAT Sent</td>
                            </tr>
                           
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid" style="border:1px solid"> Email </td>
                                <td style="border:1px solid" style="border:1px solid"></td>
                                <td style="border:1px solid" style="border:1px solid"></td>
                                <?php
                                foreach ($csatsentemailvalue as $escavgkeys=> $escavgvalue1) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue1?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> IVR </td> <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($ivrsentvaluess as $escavgkeys=> $escavgvalue2) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue2 ?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                           
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Overall </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($csatsentemailvalue as $escavgkeys=> $escavgvalue1) {
                                	foreach ($ivrsentvaluess as $escavgkeys1=> $escavgvalue2) {
                                		if($escavgkeys1==$escavgkeys){
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue1+$escavgvalue2; ?></th>
	                        		<?php
	                        			}
	                        		}
	                            }
	                            ?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td></td>
	                            <td ></td>
	                            <td ></td>
                            	<?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center" ></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td colspan="3" style="background-color:#3598dc;border:1px solid;color:white">CSAT Received</td>
                            </tr>
                           
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Email </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($emailrecvaluess as $escavgkeys=> $escavgvalue5) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue5 ?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> IVR </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($ivrsentvaluess as $escavgkeys=> $escavgvalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                           
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                <td style="border:1px solid"> Overall </td>
                                <td style="border:1px solid"></td>
                                <td style="border:1px solid"></td>
                                <?php
                                foreach ($emailrecvaluess as $escavgkeys1=> $escavgvalue5) {
                                	foreach ($ivrsentvaluess as $escavgkeys2=> $escavgvalue6) {
                                		if($escavgkeys1==$escavgkeys2){
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue5+$escavgvalue6;?></th>
	                        		<?php
	                           			 }
	                           		}
	                           	}
	                            ?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	 <td ></td>
	                             <td></td>
	                             <td></td>
                            	<?php
	                            foreach ($closedate as $keycount=> $closeddate) {
                        		?>
                            	<th class=" font-size text-center"></th>
                        		<?php
                            	}
                        		?>
                            </tr>
                            <tr>
                            <td colspan=3 style="background-color:white;"></td>
                            	<td colspan="3" style="background-color:#3598dc;color:white;border:1px solid;">CSAT Questionwise</td>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                 <td style="border:1px solid"> Overall % </td>
                                 <td style="border:1px solid"></td>
                                 <td style="border:1px solid"></td>
                                <?php
                                foreach ($csatemailvaluess as $escavgkeys=> $escavgvalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $escavgvalue."%"?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                 <td style="border:1px solid"> Communication % </td>
                                 <td style="border:1px solid"></td>
                                 <td style="border:1px solid"></td>
                                <?php
                                foreach ($communicationtotArr as $communicationVal) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $communicationVal."%"?></th>
	                        		<?php
	                            }
                        		?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                 <td style="border:1px solid"> Solution Effectiveness % </td>
                                 <td style="border:1px solid"></td>
                                 <td style="border:1px solid"></td>
                                <?php
                                foreach ($effectiveunicationtotArr as $effectivevalue5) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $effectivevalue5."%";?></th>
	                        		<?php
	                           	}
	                            ?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                 <td style="border:1px solid"> Technical Knowledge % </td>
                                 <td style="border:1px solid"></td>
                                 <td style="border:1px solid"></td>
                                <?php
                                foreach ($technicaltotArr as $technicalarrvalue) {
	                        		?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $technicalarrvalue."%";?></th>
	                        		<?php
	                           	}
	                            ?>
                            </tr>
                            <tr style="background-color:#E8F1FA;">
                            <td colspan=3 style="background-color:white;"></td>
                                 <td style="border:1px solid"> Responsiveness % </td>
                                 <td style="border:1px solid"></td>
                                 <td style="border:1px solid"></td>
                                <?php
                                foreach ($responsivetotArr as $responsivevalue5) {
	                        	?>
	                            	 <th class="font-size text-center" style="border:1px solid"><?php echo $responsivevalue5."%";?></th>
	                        		<?php
	                           	}
	                            ?>
                            </tr>
                        </tbody>
                    </table>
		    </div>
		</div>
<!-- </form> -->
<?php 
// include("includes/footer.php");
?>