<?php
include "includes/config.php";
include 'includes/session_check.php';
if(isset($_POST['submit'])){
	$TableNameArr=array('avaya'=>'panzura_avaya','backlog'=>'panzura_backlog','closure'=>'panzura_closure','inflow'=>'panzura_inflow','work'=>'panzura_work');
    if(count($_FILES['upload']['name']) > 0){
        // print_r($_FILES['upload']);
        for($i=0; $i<count($_FILES['upload']['name']); $i++) {
            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];
            if($tmpFilePath != ""){
               $shortname = explode(".",$_FILES['upload']['name'][$i]);
                $filename=$shortname[0];
                $filePath = CSV_ROOT_PATH."csv/" . $_FILES['upload']['name'][$i];
                if(move_uploaded_file($tmpFilePath,$filePath)) {
                    //echo "not upload";
                    $files[] = $filename;
                    try {
						$conn = new PDO("mysql:host=".SERVER.";dbname=".DATABASE, DBUSER, DBPASS);
                        //echo "TRUNCATE TABLE `$TableNameArr[$filename]`";
                        $stmt=$conn->exec("TRUNCATE TABLE $TableNameArr[$filename]");
                        $conn->exec($stmt);

                        $conn->exec('LOAD DATA '.$localkeyword.' INFILE "'.$filePath.'" INTO TABLE  '. $TableNameArr[$filename]. ' FIELDS TERMINATED BY ","   OPTIONALLY ENCLOSED BY """" LINES TERMINATED BY "\n" IGNORE 1 LINES ');
                    }catch(PDOException $e){  
                        echo $e->getMessage(); 
                    }

                   unlink($filePath);
                }else{
                    echo $_FILES['upload']['name'][$i]." - not upload";
                }
            }
        }
    }
}


include "includes/header.php";
?>
<form method="POST" id="frmsrch" enctype="multipart/form-data">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<input type="hidden" value='submit' name='submit'>
	<div class="portlet box yellow-casablanca">
	    <div class="portlet-title">
	        <div class="caption">
            <i class="fa fa-send"></i>File Upload</div>
	        <div class="tools"> 
	        	<div class="dt-buttons">
	        	</div>
	        </div>
	    </div>
	    <div class="portlet-body" style="min-height: 200px">
            <div class="row">
                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label" style="font-size: x-large;">File Upload: </label>
                    <div class="col-md-3">
                       <input id='upload' name="upload[]" type="file" multiple="multiple" />
                        <p class="help-block">Upload Only CSV Format</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary btn-block">File Upload</button> 
            </div>
	   </div>
    </div>
</form>
<?php 
include("includes/footer.php");
?>