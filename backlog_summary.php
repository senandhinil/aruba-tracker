<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange'] || $_POST['regionwise'] || $_POST['manager'] || $_POST['tlname'] || $_POST['enggname']){
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$regionwise 	= 	$_POST['regionwise'];
	if($selecttype == 'Daily'){
		$selectrange    =	date('m/j/Y',strtotime($_POST['picked_date']));
	}else{
		$selectrange    =	$_POST['selectrange'];
	}
	$manager    	=	$_POST['manager'];
	$tlname   		=	$_POST['tlname'];
	$enggname   	=	$_POST['enggname'];
}else{
	$selectrange    =  !empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=  !empty($projectwise)?$projectwise:"Overall";
	$productwise 	=  !empty($productwise)?$productwise:"Overall";
	$productgroup 	=  !empty($productgroup)?$productgroup:"Overall";
	$regionwise 	=  !empty($regionwise)?$regionwise:"Overall";
	$calendartype 	=  !empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=  !empty($selecttype)?$selecttype:"Weekly";
	
	$manager    	=	empty($manager)?"Overall":$manager;
	$tlname    		=	empty($tlname)?"Overall":$tlname;
	$enggname    	=	empty($enggname)?"Overall":$enggname;
} 
/* if($selecttype == 'Daily' && $_POST['picked_date'] != ''){
	$selectrange    =	date('m/d/Y',strtotime($_POST['picked_date']));
}else{
	$selectrange    =  ($_POST['selectrange']=="")?$_POST['selectrange']:current($currentweek);
}
$projectwise 	=  !empty($_POST['projectwise'])?$_POST['projectwise']:"Overall";
$productwise 	=  !empty($_POST['productwise'])?$_POST['productwise']:"Overall";
$productgroup 	=  !empty($_POST['productgroup'])?$_POST['productgroup']:"Overall";
$regionwise 	=  !empty($_POST['regionwise'])?$_POST['regionwise']:"Overall";
$calendartype 	=  !empty($_POST['calendartype'])?$_POST['calendartype']:"Normal";
$selecttype  	=  !empty($_POST['selecttype'])?$_POST['selecttype']:"Weekly";
$manager    	=  !empty($_POST['manager'])?$_POST['manager']:"Overall";
$tlname    		=  !empty($_POST['tlname'])?$_POST['tlname']:"Overall";
$enggname    	=  !empty($_POST['enggname'])?$_POST['enggname']:"Overall"; 
 */
if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	
	/* if($_POST['tlname'] == ''){
		$tlnamelist=$commonobj->arrayColumn($commonobj->getQry("SELECT distinct team from aruba_backlog_raw where ".$type.$selectQry."='$selectrange' ORDER BY id ASC limit 0 , 1"),'','team');
		$tlname    		=  !empty($tlname) ? $tlname:$tlnamelist[0];
	} */
	
	$QryCondition1.= $projectwise !='Overall' ?" and wlan_ns='".$projectwise."'":'';
	$QryCondition1.= $productwise!='Overall'?" and que_new='".$productwise."'":'';
	$QryCondition1.= $productgroup!='Overall'?" and product_group='".$productgroup."'":'';
	$QryCondition1.= $regionwise!='Overall'?" and region='".$regionwise."'":'';	
	$QryCondition.= $manager!='Overall'?" and manager_name='".$manager."'":'';	
	$QryCondition2.= $tlname!='Overall'?" and team='".$tlname."'":'';	
	$QryCondition.= $enggname!='Overall'?" and case_owner='".$enggname."'":'';	
	
	$headertotalArr = $commonobj->arrayColumn($commonobj->getQry("select distinct ".$type.$selectQry." from aruba_backlog_raw order by id asc"),'',$type.$selectQry);
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 1, true));
	
	if($selecttype == "Daily"){
		$avg = "";
	}else{
		$avg = "(Avg.)";
	}
	
	$noofdaysArr = $commonobj->getQry("select count(distinct calendar_date) as working_days from aruba_backlog_raw where $type$selectQry='$selectrange'");
	$working_days = $noofdaysArr[0]['working_days'];
	
	//$backlog_summ = $commonobj->getQry("select * from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition");
	$backlogcount = $commonobj->arrayColumn($commonobj->getQry("SELECT sum(`count`) as count,product_group from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by product_group"),'product_group','count');
	$i=0;
	foreach ($backlogcount as $key => $value) {
		$spitupArr[$i]['name']=$key;
		if($selecttype == "Daily"){
			$spitupArr[$i]['y']=(float)$value;
		}else{
			$spitupArr[$i]['y']=(float)$value/$working_days;
		}
		$i++;
	}
	$backlog_manager_trend = $commonobj->arrayColumn($commonobj->getQry("SELECT sum(`count`) as count,manager_name from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by manager_name"),'manager_name','count');
	$i=0;
	foreach ($backlog_manager_trend as $key => $value) {
		$spitupmngrArr[$i]['name']=$key;
		if($selecttype == "Daily"){
			$spitupmngrArr[$i]['y']=(float)$value;
		}else{
			$spitupmngrArr[$i]['y']=(float)$value/$working_days;
		}
		$i++;
	}
	
	$backlog_summ_product = $commonobj->getQry("select case_owner,manager_name,team,product_group,que_new,sum(`count`) as avg_count,avg(`age`) as avg_age,sum(`0_to_5`) as avgfive,sum(`6_to_10`) as avgten,sum(`11_to_20`) as avgtwen,sum(`morethen20`) as avgmoretwen,sum(`untouchedmorethen3days`) as avguntouch,sum(`Untocuhedmorethan30days`) as avguntouchmt30,sum(`Untocuhedmorethan60days`) as avguntouchmt60 from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by product_group");
	$backlog_summ_product_open = $commonobj->arrayColumn($commonobj->getQry("select count(*) as open_count, product_group from aruba_open where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by product_group"),'product_group','open_count');
	$backlog_summ_product_close = $commonobj->arrayColumn($commonobj->getQry("select count(*) as close_count, product_group from aruba_closure where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by product_group"),'product_group','close_count');
	
	$backlog_summ_manager = $commonobj->getQry("select case_owner,manager_name,team,product_group,que_new,sum(`count`) as avg_count,avg(`age`) as avg_age,sum(`0_to_5`) as avgfive,sum(`6_to_10`) as avgten,sum(`11_to_20`) as avgtwen,sum(`morethen20`) as avgmoretwen,sum(`untouchedmorethen3days`) as avguntouch,sum(`Untocuhedmorethan30days`) as avguntouchmt30,sum(`Untocuhedmorethan60days`) as avguntouchmt60 from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 group by manager_name");
	$backlog_summ_manager_open = $commonobj->arrayColumn($commonobj->getQry("select count(*) as open_count, manager_name from aruba_open where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by manager_name"),'manager_name','open_count');
	$backlog_summ_manager_close = $commonobj->arrayColumn($commonobj->getQry("select count(*) as close_count, manager_name from aruba_closure where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by manager_name"),'manager_name','close_count');
	
	$backlog_summ_team = $commonobj->getQry("select case_owner,manager_name,team,product_group,que_new,sum(`count`) as avg_count,avg(`age`) as avg_age,sum(`0_to_5`) as avgfive,sum(`6_to_10`) as avgten,sum(`11_to_20`) as avgtwen,sum(`morethen20`) as avgmoretwen,sum(`untouchedmorethen3days`) as avguntouch,sum(`Untocuhedmorethan30days`) as avguntouchmt30,sum(`Untocuhedmorethan60days`) as avguntouchmt60 from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by team");
	$backlog_summ_team_open = $commonobj->arrayColumn($commonobj->getQry("select count(*) as open_count, team from aruba_open where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by team"),'team','open_count');
	$backlog_summ_team_close = $commonobj->arrayColumn($commonobj->getQry("select count(*) as close_count, team from aruba_closure where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by team"),'team','close_count');
	
	$backlog_summ_engg = $commonobj->getQry("select case_owner,manager_name,team,product_group,que_new,sum(`count`) as avg_count,avg(`age`) as avg_age,sum(`0_to_5`) as avgfive,sum(`6_to_10`) as avgten,sum(`11_to_20`) as avgtwen,sum(`morethen20`) as avgmoretwen,sum(`untouchedmorethen3days`) as avguntouch,sum(`Untocuhedmorethan30days`) as avguntouchmt30,sum(`Untocuhedmorethan60days`) as avguntouchmt60 from aruba_backlog_raw where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by case_owner");
	$backlog_summ_engg_open = $commonobj->arrayColumn($commonobj->getQry("select count(*) as open_count, case_owner from aruba_open where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by case_owner"),'case_owner','open_count');
	$backlog_summ_engg_close = $commonobj->arrayColumn($commonobj->getQry("select count(*) as close_count, case_owner from aruba_closure where $type$selectQry='$selectrange' $QryCondition $QryCondition1 $QryCondition2 group by case_owner"),'case_owner','close_count');
	
	
	
}
include "includes/header.php";
?>
<link href="assets/jquery-ui.css" rel="stylesheet">
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-sm-2 {
	   		width: 16.667%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
    	margin-right: 0px;
	}
	tr, th, td{
		text-align:center;
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="manager"  name="manager" onchange="reload()">
		                	<option value="Overall">Overall Manager</option>';
		                	<?php
								$Qry.= $projectwise == 'Overall' ? '':" and wlan_ns = '$projectwise'";
		                		$Qry.= $productwise == 'Overall' ? '':" and que_new = '$productwise'";
		                		$Qry.= $productgroup == 'Overall' ? '':" and product_group = '$productgroup'";
		                		$Qry.= $regionwise == 'Overall' ? '':" and region = '$regionwise'";
								//echo "SELECT distinct manager_name from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by manager_name asc";
		                		$managerList = $commonobj->getQry("SELECT distinct manager_name from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by manager_name asc");
								foreach ($managerList as $key => $value) {
									echo'<option value="'.$value['manager_name'].'">'.$value['manager_name'].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#manager").val("<?php echo $manager ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
					    <select class="form-control" id="tlname"  name="tlname" onchange="reload()">
							<option value="Overall">Overall TL</option>
							<?php
								$tlQry = $manager == 'Overall' ? '' : " and manager_name ='$manager'";
								$tllist = $commonobj->getQry("SELECT distinct team from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry $tlQry  order by team asc");
								foreach ($tllist as $key => $value) {
									echo'<option value="'.$value['team'].'">'.$value['team'].'</option>';
								}
							?> 
		                </select>
		                <script> 
		                     jQuery("#tlname").val("<?php echo $tlname ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="enggname"  name="enggname" onchange="reload()">
		                	<option value="Overall">Overall Engineer</option>
							<?php
								if ( $manager != 'Overall' && $tlname == 'Overall') {
		                			$engQry = " and manager_name = '$manager' ";
		                		} else if($manager != 'Overall' && $tlname != 'Overall') {
		                			$engQry = " and team = '$tlname' ";
		                		}else if($manager == 'Overall' && $tlname != 'Overall'){
		                			$engQry = " and team = '$tlname' ";
		                		}
								$tllist = $commonobj->getQry("SELECT distinct case_owner from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."')  $engQry $Qry order by case_owner asc");
								foreach ($tllist as $key => $value) {
									echo "<option value='".$value['case_owner']."'>".$value['case_owner']."</option>";
								}
							?> 
		                </select>
		                <script> 
		                     jQuery("#enggname").val("<?php echo $enggname ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop2"  name="projectwise" onchange="reload()">
	                    <?php
							$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
	                    	echo '<option value="Overall">Overall Project</option>';
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script> 
		                     jQuery("#drop2").val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="productwise" onchange="reload()">
		                <?php
		                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
		                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
							echo '<option value="Overall">Overall Que</option>';
							foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="productgroup" onchange="reload()">
		                	<?php
								
		                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                		$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
								$Qry.= $regionwise!='Overall'?" and region='".$regionwise."'":'';
		                		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_backlog_raw Where $Qery AND product_group != 'NA'");

								echo '<option value="Overall">Overall Product</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop8"  name="regionwise" onchange="reload()">
		                	<?php
								$productName = $commonobj->getQry("SELECT distinct region from aruba_backlog_raw Where $Qery");

								echo '<option value="Overall">Overall Region</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['region'].'">'.$value['region'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop8").val("<?php echo $regionwise; ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype" >
							<option value="Daily">Daily</option>
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
						<?php
						if($selecttype =='Daily'){ 
							$date_display = "display:inline-block;";
							$wmq_display = "display:none;";
						}else{ 
							$wmq_display = "display:inline-block;";
							$date_display = "display:none;";
						}?>
						<input type="text" name="picked_date" id="datepicker" placeholder="Select Date" value="<?php echo $_POST['picked_date']; ?>" class="form-control" onchange="reload()" style="<?php echo $date_display; ?>">
		                
		                <select class="form-control" id="drop7"  name="selectrange"  onchange="reload()" style="<?php echo $wmq_display; ?>">
		                	<option value="">--- Select ---</option>';
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_backlog_raw order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
						<script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
						
					 </div>
		        </div>
		    </div>
		</div>    
	</div>
	<?php 
	foreach ($tablehead as $inflowkey=> $rs) {	
        $weekdayname[]	=$rs;	
     }
	?>  
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Engineer wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-5 col-xm-5 col-xs-5">
					<span id="container"></span>
				</div>
				<div class="col-md-5 col-xm-5 col-xs-5">
					<span id="container1"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Product wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<!--<th scope="col">Engineer wise</th>
					<th scope="col">Team Lead</th>
					<th scope="col">Team Manager</th>-->
					<th scope="col">Product</th>
					<th scope="col">Que</th>
					<th scope="col">Opened Cases </th>
					<th scope="col">Closed Cases </th>
					<th scope="col">No. of Backlog <?php echo $avg; ?></th>
		            <th scope="col">Avg. Backlog Age <?php echo $avg; ?></th>
		            <th scope="col">0-5 Days <?php echo $avg; ?></th>
		            <th scope="col">6-10 Days <?php echo $avg; ?></th>
					<th scope="col">11-20 Days <?php echo $avg; ?></th>
		            <th scope="col">Great 20 Days <?php echo $avg; ?></th>
		            <th scope="col">Untouched(>3Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>30Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>60Days) <?php echo $avg; ?></th>
		          </tr>
		        </thead>
		        <tbody>
		          	<?php foreach($backlog_summ_product as $resArr){ ?>
		          	<tr>
						<!--<td><?php echo $resArr['case_owner'] ?></td>
						<td><?php echo $resArr['team'] ?></td>
						<td><?php echo $resArr['manager_name'] ?></td>-->
						<td><?php echo $resArr['product_group']?></td>
						<td><?php echo $resArr['que_new']?></td>
						<td><?php echo $backlog_summ_product_open[$resArr['product_group']]; ?></td>
						<td><?php echo $backlog_summ_product_close[$resArr['product_group']]; ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_count'],1):round($resArr['avg_count']/$working_days); ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_age'],1):round($resArr['avg_age'],1);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgfive'],1):round($resArr['avgfive']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgten'],1):round($resArr['avgten']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgtwen'],1):round($resArr['avgtwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgmoretwen'],1):round($resArr['avgmoretwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouch'],1):round($resArr['avguntouch']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt30'],1):round($resArr['avguntouchmt30']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt60'],1):round($resArr['avguntouchmt60']/$working_days);?></td>
		          	</tr>
		          	<?php } ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Manager Wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Team Manager</th>
					<!--<th scope="col">Team Lead</th>
					<th scope="col">Engineer wise</th>-->
					<th scope="col">Product</th>
					<th scope="col">Que</th>
					<th scope="col">Opened Cases </th>
					<th scope="col">Closed Cases </th> 
					<th scope="col">No. of Backlog <?php echo $avg; ?></th>
		            <th scope="col">Avg. Backlog Age <?php echo $avg; ?></th>
		            <th scope="col">0-5 Days <?php echo $avg; ?></th>
		            <th scope="col">6-10 Days <?php echo $avg; ?></th>
					<th scope="col">11-20 Days <?php echo $avg; ?></th>
		            <th scope="col">Great 20 Days <?php echo $avg; ?></th>
		            <th scope="col">Untouched(>3Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>30Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>60Days) <?php echo $avg; ?></th>
		          </tr>
		        </thead>
		        <tbody>
		          	<?php foreach($backlog_summ_manager as $resArr){ ?>
		          	<tr>
						<td><?php echo $resArr['manager_name'] ?></td>
						<!--<td><?php echo $resArr['team'] ?></td>
						<td><?php echo $resArr['case_owner'] ?></td>-->
						<td><?php echo $resArr['product_group']?></td>
						<td><?php echo $resArr['que_new']?></td>
						<td><?php echo $backlog_summ_manager_open[$resArr['manager_name']]; ?></td>
						<td><?php echo $backlog_summ_manager_close[$resArr['manager_name']]; ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_count'],1):round($resArr['avg_count']/$working_days); ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_age'],1):round($resArr['avg_age'],1);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgfive'],1):round($resArr['avgfive']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgten'],1):round($resArr['avgten']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgtwen'],1):round($resArr['avgtwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgmoretwen'],1):round($resArr['avgmoretwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouch'],1):round($resArr['avguntouch']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt30'],1):round($resArr['avguntouchmt30']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt60'],1):round($resArr['avguntouchmt60']/$working_days);?></td>
		          	</tr>
		          	<?php } ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Team Lead Wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Team Lead</th>
					<th scope="col">Team Manager</th>
					<!--<th scope="col">Engineer wise</th>-->
					<th scope="col">Product</th>
					<th scope="col">Que</th>
					<th scope="col">Opened Cases </th>
					<th scope="col">Closed Cases </th>
					<th scope="col">No. of Backlog <?php echo $avg; ?></th>
		            <th scope="col">Avg. Backlog Age <?php echo $avg; ?></th>
		            <th scope="col">0-5 Days <?php echo $avg; ?></th>
		            <th scope="col">6-10 Days <?php echo $avg; ?></th>
					<th scope="col">11-20 Days <?php echo $avg; ?></th>
		            <th scope="col">Great 20 Days <?php echo $avg; ?></th>
		            <th scope="col">Untouched(>3Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>30Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>60Days) <?php echo $avg; ?></th>
		          </tr>
		        </thead>
		        <tbody>
		          	<?php foreach($backlog_summ_team as $resArr){ ?>
		          	<tr>
						<td><?php echo $resArr['team'] ?></td>
						<td><?php echo $resArr['manager_name'] ?></td>
						<!--<td><?php echo $resArr['case_owner'] ?></td>-->
						<td><?php echo $resArr['product_group']?></td>
						<td><?php echo $resArr['que_new']?></td>
						<td><?php echo $backlog_summ_team_open[$resArr['team']]; ?></td>
						<td><?php echo $backlog_summ_team_close[$resArr['team']]; ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_count'],1):round($resArr['avg_count']/$working_days); ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_age'],1):round($resArr['avg_age'],1);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgfive'],1):round($resArr['avgfive']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgten'],1):round($resArr['avgten']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgtwen'],1):round($resArr['avgtwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgmoretwen'],1):round($resArr['avgmoretwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouch'],1):round($resArr['avguntouch']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt30'],1):round($resArr['avguntouchmt30']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt60'],1):round($resArr['avguntouchmt60']/$working_days);?></td>
		          	</tr>
		          	<?php } ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Engineer wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Engineer Name</th>
					<th scope="col">Team Lead</th>
					<th scope="col">Team Manager</th>
					<th scope="col">Product</th>
					<th scope="col">Que</th>
					<th scope="col">Opened Cases </th>
					<th scope="col">Closed Cases </th>
					<th scope="col">No. of Backlog <?php echo $avg; ?></th>
		            <th scope="col">Avg. Backlog Age <?php echo $avg; ?></th>
		            <th scope="col">0-5 Days <?php echo $avg; ?></th>
		            <th scope="col">6-10 Days <?php echo $avg; ?></th>
					<th scope="col">11-20 Days <?php echo $avg; ?></th>
		            <th scope="col">Great 20 Days <?php echo $avg; ?></th>
		            <th scope="col">Untouched(>3Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>30Days) <?php echo $avg; ?></th>
					<th scope="col">Untouched(>60Days) <?php echo $avg; ?></th>
		          </tr>
		        </thead>
		        <tbody>
		          	<?php foreach($backlog_summ_engg as $resArr){ ?>
		          	<tr>
						<td><?php echo $resArr['case_owner'] ?></td>
						<td><?php echo $resArr['team'] ?></td>
						<td><?php echo $resArr['manager_name'] ?></td>
						<td><?php echo $resArr['product_group']?></td>
						<td><?php echo $resArr['que_new']?></td>
						<td><?php echo $backlog_summ_engg_open[$resArr['case_owner']]; ?></td>
						<td><?php echo $backlog_summ_engg_close[$resArr['case_owner']]; ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_count'],1):round($resArr['avg_count']/$working_days); ?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avg_age'],1):round($resArr['avg_age'],1);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgfive'],1):round($resArr['avgfive']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgten'],1):round($resArr['avgten']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgtwen'],1):round($resArr['avgtwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avgmoretwen'],1):round($resArr['avgmoretwen']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouch'],1):round($resArr['avguntouch']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt30'],1):round($resArr['avguntouchmt30']/$working_days);?></td>
						<td><?php echo ($selecttype == "Daily")?round($resArr['avguntouchmt60'],1):round($resArr['avguntouchmt60']/$working_days);?></td>
		          	</tr>
		          	<?php } ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	
</form>
<?php 
include("includes/footer.php");
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 

<script>
$("#manager").change(function(){
	jQuery("#tlname").val("Overall");
	jQuery("#enggname").val("Overall");
});
/* $("#tlname").change(function(){
	   var tlname = $("#tlname").val();
	   var projectwise= $("#drop2").val();
	   var productwise= $("#drop3").val();
	   var productgroup= $("#drop4").val();
	   var calendartype=$("#drop5").val();
	   var selecttype=$("#drop6").val();
	   var selectrange=$("#drop7").val();
	    $.ajax({
	      url: 'ajax_backlog_summary.php',
	      type: 'POST',
	      data: {'tlname':tlname,'projectwise':projectwise,'productwise':productwise,'productgroup':productgroup,'calendartype':calendartype,'selecttype':selecttype,'selectrange':selectrange},
	      success: function(output) {
			var obj = jQuery.parseJSON( output);
	        $("#enggname").html("");
	        $("#enggname").html(obj);
		  }
	    }); 
	}); */
	function reload(){
		document.getElementById("frmsrch").action = 'backlog_summary.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}
	$(".selectweek").change(function(){
	    var selectdate= $("#drop6").val();
		if(selectdate == 'Daily'){
			$("#drop7").css("display","none");
		    $("#datepicker").css("display","inline-block");
	    }else{
			$("#drop7").css("display","inline-block");
			$("#datepicker").css("display","none");
			var calendertype= $("#drop5").val();
			$.ajax({
			  url: 'ajax_backlog.php',
			  type: 'POST',
			  data: {'reporttype':selectdate,'calendertype':calendertype},
			  success: function(output) {
				var obj = jQuery.parseJSON( output);
				$("#drop7").html("");
				$("#drop7").html(obj);
			  }
			}); 
		}
	});
	$( "#datepicker" ).datepicker();
	
	// Make monochrome colors and set them as default for all pies
	var pieColors = (function () {
	    var colors = [],
	        base = Highcharts.getOptions().colors[0],
	        i;

	    for (i = 0; i < 10; i += 1) {
	        // Start out with a darkened base color (negative brighten), and end
	        // up with a much brighter color
	        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
	    }
	    return colors;
	}());
	Highcharts.chart('container', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie',
	        height: 300,
	    },
	    title: {
	        text: 'Backlog split-up'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
	    },
	    credits: {
		            enabled: false
		        },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                format: '<b>{point.name}</b>:({point.y:.0f})-{point.percentage:.1f}% ',
	                style: {
	                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                }
	            }
	        }
	    },
	    series: [{
	        colorByPoint: true,
	        data: <?php echo json_encode($spitupArr,true);?>
	    }]
	});
	Highcharts.chart('container1', {
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: null,
	        plotShadow: false,
	        type: 'pie',
	        height: 300,
	    },
	    title: {
	        text: 'Backlog split-up'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
	    },
	    credits: {
		            enabled: false
		        },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                format: '<b>{point.name}</b>:({point.y:.0f})-{point.percentage:.1f}% ',
	                style: {
	                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                }
	            }
	        }
	    },
	    series: [{
	        colorByPoint: true,
	        data: <?php echo json_encode($spitupmngrArr,true);?>
	    }]
	});
	Highcharts.theme = {
   colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066','#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: {
         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
         stops: [
            [0, '#2a2a2b'],
            [1, '#3e3e40']
         ]
      },
      style: {
         fontFamily: '\'Unica One\', sans-serif'
      },
      plotBorderColor: '#606063'
   },
   title: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase',
         fontSize: '20px'
      }
   },
   subtitle: {
      style: {
         color: '#E0E0E3',
         textTransform: 'uppercase'
      }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);

</script>