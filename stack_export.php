<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';

if($_POST['selecttype'] || $_POST['calendartype']){
	$selecttype	    = 	$_POST['selecttype'];
	$calendartype = $_POST['calendartype']; 
	$selectrange    =	$_POST['period'];
	$ltype = $_POST['ltype'];
	$productgroup   	=	$_POST['productgroup'];
}else{
	$selecttype  	=	!empty($selecttype)?$selecttype:"Monthly";
	$calendartype = !empty($calendartype)?$calendartype:"Normal";
	$currentmonth= $commonobj->getQry("SELECT distinct calendar_month from aruba_stackrank_engg_list order by calendar_month desc limit 0,1");
	$selectrange	=	!empty($selectrange)?$selectrange:$currentmonth[0]['calendar_month'];
	$ltype = !empty($ltype)?$ltype:"l1";
	$productgroup   	=	!empty($productgroup)?$productgroup:"Overall";
}

/* if(isset($_POST['calendartype']))
{ */
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}

	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
/* } */
	$productName = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct product_group from aruba_stackrank_engg_list Where product_group != '' AND $type$selectQry='$selectrange' AND role = '$ltype'"),"","product_group");
	array_unshift($productName,"Overall");
	if(in_array($productgroup,$productName)){
		$productgroup = $productgroup;
	}else{
		$productgroup = "Overall";
	}
	
	$QryCondition = $productgroup!='Overall'?" and product_group='".$productgroup."'":'';
	
	//Engg. Name list
	$headerenggArr = $commonobj->arrayColumn($commonobj->getQry("select distinct LOWER(engg_name) as engg_name from aruba_stackrank_engg_list where role = '$ltype' AND $type$selectQry='$selectrange' $QryCondition"),'engg_name','engg_name');
	
	//Employee Id
	$emplyId = $commonobj->arrayColumn($commonobj->getQry("select distinct emp_id,LOWER(engg_name) as engg_name,team from aruba_stackrank_engg_list"),'engg_name','emp_id');
	
	//Target Declaration
	$closurecntArr = $commonobj->arrayColumn($commonobj->getQry('SELECT count(*) as closed_case_count,'.$type.$selectQry.' FROM `aruba_closure` where '.$type.$selectQry.'="'.$selectrange.'" AND case_owner in ("'.implode('","',$headerenggArr).'")'),$type.$selectQry,'closed_case_count');
	$productgroupArr1 = $productName;//array("Overall","Airwave","Aruba OS","Clearpass","Instant OS");
	if($ltype == 'l1')
	{
		if(in_array($productgroup,$productgroupArr1)){
			$csatgreen_weightage = 	$loocsatgreen_weightage = empty($_POST['loocsatgreen_weightage'])?"25":$_POST['loocsatgreen_weightage'];
			$red_weightage 	= 	$loored_weightage = empty($_POST['loored_weightage'])?"0":$_POST['loored_weightage'];
			$closure_weightage 	= 	$looclosure_weightage = empty($_POST['looclosure_weightage'])?"35":$_POST['looclosure_weightage'];
			$ttc_weightage 	= 	$loottc_weightage = empty($_POST['loottc_weightage'])?"20":$_POST['loottc_weightage'];
			//$softskill_weightage   	=	empty($_POST['softskill_weightage'])?"15":$_POST['softskill_weightage'];
			$processquality_weightage 	= 	$looprocessquality_weightage = empty($_POST['looprocessquality_weightage'])?"20":$_POST['looprocessquality_weightage'];
			$escalation_weightage	= 	$looescalation_weightage = empty($_POST['looescalation_weightage'])?"-10":$_POST['looescalation_weightage'];
			//$atten_weightage	= 	empty($_POST['atten_weightage'])?"-10":$_POST['atten_weightage'];
			//$backlog_weightage	= 	empty($_POST['backlog_weightage'])?"0":$_POST['backlog_weightage'];
			
			$green_target = $loogreen_target = empty($_POST['loogreen_target'])?80:$_POST['loogreen_target'];
			$red_target = $loored_target = empty($_POST['loored_target'])?0:$_POST['loored_target'];
			$closures_target = $looclosures_target = empty($_POST['looclosures_target'])?50:$_POST['looclosures_target'];
			$ttc_target = $loottc_target = empty($_POST['loottc_target'])?80:$_POST['loottc_target'];
			//$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
			$pq_target = $loopq_target = empty($_POST['loopq_target'])?90:$_POST['loopq_target'];
			$esc_target = $looesc_target = empty($_POST['looesc_target'])?0:$_POST['looesc_target'];
			//$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
			//$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
		}elseif($productgroup == "THD"){
			
			$green_target = $lotgreen_target = empty($_POST['lotgreen_target'])?80:$_POST['lotgreen_target'];
			$red_target = $lotred_target = empty($_POST['lotred_target'])?0:$_POST['lotred_target'];
			$closures_target = $lotclosures_target = empty($_POST['lotclosures_target'])?50:$_POST['lotclosures_target'];
			$ttc_target = $lotttc_target = empty($_POST['lotttc_target'])?80:$_POST['lotttc_target'];
			//$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
			$pq_target = $lotpq_target = empty($_POST['lotpq_target'])?90:$_POST['lotpq_target'];
			$esc_target = $lotesc_target = empty($_POST['lotesc_target'])?0:$_POST['lotesc_target'];
			//$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
			//$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
			
			$csatgreen_weightage 	= 	$lotcsatgreen_weightage = empty($_POST['lotcsatgreen_weightage'])?"25":$_POST['lotcsatgreen_weightage'];
			$red_weightage 	= 	$lotred_weightage = empty($_POST['lotred_weightage'])?"0":$_POST['lotred_weightage'];
			$closure_weightage 	= 	$lotclosure_weightage = empty($_POST['lotclosure_weightage'])?"35":$_POST['lotclosure_weightage'];
			$ttc_weightage 	= 	$lotttc_weightage = empty($_POST['lotttc_weightage'])?"20":$_POST['lotttc_weightage'];
			//$softskill_weightage   	=	empty($_POST['softskill_weightage'])?"15":$_POST['softskill_weightage'];
			$processquality_weightage 	= 	$lotprocessquality_weightage = empty($_POST['lotprocessquality_weightage'])?"20":$_POST['lotprocessquality_weightage'];
			$escalation_weightage	= 	$lotescalation_weightage = empty($_POST['lotescalation_weightage'])?"-10":$_POST['lotescalation_weightage'];
			//$atten_weightage	= 	empty($_POST['atten_weightage'])?"-10":$_POST['atten_weightage'];
			//$backlog_weightage	= 	empty($_POST['backlog_weightage'])?"0":$_POST['backlog_weightage'];
		}
	}elseif($ltype == 'l2'){
		if(in_array($productgroup,$productgroupArr1)){
			$green_target = $ltogreen_target = empty($_POST['ltogreen_target'])?80:$_POST['ltogreen_target'];
			$red_target = $ltored_target = empty($_POST['ltored_target'])?0:$_POST['ltored_target'];
			$closures_target = $ltoclosures_target = empty($_POST['ltoclosures_target'])?40:$_POST['ltoclosures_target'];
			//$gscclosures_target = empty($_POST['gscclosures_target'])?10:$_POST['gscclosures_target'];
			$ttc_target = $ltottc_target = empty($_POST['ltottc_target'])?80:$_POST['ltottc_target'];
			//$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
			$pq_target = $ltopq_target = empty($_POST['ltopq_target'])?90:$_POST['ltopq_target'];
			$esc_target = $ltoesc_target = empty($_POST['ltoesc_target'])?0:$_POST['ltoesc_target'];
			//$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
			//$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
			
			$csatgreen_weightage 	= 	$ltocsatgreen_weightage= empty($_POST['ltocsatgreen_weightage'])?"25":$_POST['ltocsatgreen_weightage'];
			$red_weightage 	= 	$ltored_weightage = empty($_POST['ltored_weightage'])?"0":$_POST['ltored_weightage'];
			$closure_weightage 	= 	$ltoclosure_weightage = empty($_POST['ltoclosure_weightage'])?"35":$_POST['ltoclosure_weightage'];
			//$gscclosures_weightage = empty($_POST['gscclosures_weightage'])?"20":$_POST['gscclosures_weightage'];
			$ttc_weightage 	= 	$ltottc_weightage = empty($_POST['ltottc_weightage'])?"20":$_POST['ltottc_weightage'];
			//$softskill_weightage   	=	empty($_POST['softskill_weightage'])?"15":$_POST['softskill_weightage'];
			$processquality_weightage 	= 	$ltoprocessquality_weightage = empty($_POST['ltoprocessquality_weightage'])?"20":$_POST['ltoprocessquality_weightage'];
			$escalation_weightage	= 	$ltoescalation_weightage = empty($_POST['ltoescalation_weightage'])?"-10":$_POST['ltoescalation_weightage'];
			//$atten_weightage	= 	empty($_POST['atten_weightage'])?"-10":$_POST['atten_weightage'];
			//$backlog_weightage	= 	empty($_POST['backlog_weightage'])?"0":$_POST['backlog_weightage'];
			
		 }elseif($productgroup == "THD"){
			$green_target = $lttgreen_target = empty($_POST['lttgreen_target'])?80:$_POST['lttgreen_target'];
			$red_target = $lttred_target = empty($_POST['lttred_target'])?0:$_POST['lttred_target'];
			$closures_target = $lttclosures_target = empty($_POST['lttclosures_target'])?40:$_POST['lttclosures_target'];
			//$gscclosures_target = empty($_POST['gscclosures_target'])?10:$_POST['gscclosures_target'];
			$ttc_target = $lttttc_target = empty($_POST['lttttc_target'])?80:$_POST['lttttc_target'];
			//$ss_target = empty($_POST['ss_target'])?80:$_POST['ss_target'];
			$pq_target = $lttpq_target = empty($_POST['lttpq_target'])?90:$_POST['lttpq_target'];
			$esc_target = $lttesc_target = empty($_POST['lttesc_target'])?0:$_POST['lttesc_target'];
			//$atten_target = empty($_POST['atten_target'])?95:$_POST['atten_target'];
			//$backlog_target = empty($_POST['backlog_target'])?0:$_POST['backlog_target'];
			
			$csatgreen_weightage 	= 	$lttcsatgreen_weightage = empty($_POST['lttcsatgreen_weightage'])?"25":$_POST['lttcsatgreen_weightage'];
			$red_weightage 	= 	$lttred_weightage = empty($_POST['lttred_weightage'])?"0":$_POST['lttred_weightage'];
			$closure_weightage 	= 	$lttclosure_weightage = empty($_POST['lttclosure_weightage'])?"35":$_POST['lttclosure_weightage'];
			//$gscclosures_weightage = empty($_POST['gscclosures_weightage'])?"20":$_POST['gscclosures_weightage'];
			$ttc_weightage 	= 	$lttttc_weightage = empty($_POST['lttttc_weightage'])?"20":$_POST['lttttc_weightage'];
			//$softskill_weightage   	=	empty($_POST['softskill_weightage'])?"15":$_POST['softskill_weightage'];
			$processquality_weightage 	= 	$lttprocessquality_weightage = empty($_POST['lttprocessquality_weightage'])?"20":$_POST['lttprocessquality_weightage'];
			$escalation_weightage	= 	$lttescalation_weightage = empty($_POST['lttescalation_weightage'])?"-10":$_POST['lttescalation_weightage'];
			//$atten_weightage	= 	empty($_POST['atten_weightage'])?"-10":$_POST['atten_weightage'];
			//$backlog_weightage	= 	empty($_POST['backlog_weightage'])?"0":$_POST['backlog_weightage'];
		}
		
	}
	$total_weightage = (int)$csatgreen_weightage+(int)$closure_weightage+(int)$softskill_weightage+(int)$processquality_weightage+(int)$red_weightage+(int)$ttc_weightage;
	
	//$headerenggArr = $commonobj->arrayColumn($commonobj->getQry("select distinct engg_name from aruba_stackrank_engg_list where role = '$ltype' AND $type$selectQry='$selectrange'"),'engg_name','engg_name');
	
	$sel_engg = $conn->prepare('select distinct LOWER(engg_name) as engg_name, team, product_group from aruba_stackrank_engg_list where '.$type.$selectQry.'="'.$selectrange.'" order by engg_name asc');
	$sel_engg->execute();
	$sel_enggArr = $sel_engg->fetchAll(PDO::FETCH_ASSOC);
	foreach($sel_enggArr as $engg_name){
		$enggteamArr[$engg_name['engg_name']]=$engg_name['team'];
		$enggprojetcArr[$engg_name['engg_name']]=$engg_name['product_group'];
	}
	
	$workingdaysArr = $commonobj->arrayColumn($commonobj->getQry('select working_days,'.$type.$selectQry.' from aruba_headcount where '.$type.$selectQry.' = "'.$selectrange.'" group by '.$type.$selectQry),$type.$selectQry,'working_days');
	$workingdays = $workingdaysArr[$selectrange];
	
	//tenure, bucket wise
	$engg_tenu = $conn->prepare('select LOWER(emp_name) as emp_name,doj from aruba_tenure where emp_name in ("'.implode('","',$headerenggArr).'")');
	$engg_tenu->execute();
	$engg_tenuArr = $engg_tenu->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_tenuArr as $tenure){
		$date1 = date('Y-m-d H:i:s', strtotime($tenure['doj']));
		$date2 = date('Y-m-d H:i:s');
		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y', $ts1);
		$year2 = date('Y', $ts2);
		$month1 = date('m', $ts1);
		$month2 = date('m', $ts2);
		
		$yearinmonth = '12';
		
		if($tenure['doj'] != '-' || $tenure['doj'] != ''){
			$years = $year2 - $year1;
			$months = $month2 - $month1;
			$diff = (($years) * 12) + ($months);
			
			$yearsf = floor($val = $diff/$yearinmonth);
			$monthsf = fmod($diff,$yearinmonth);
			
			
			$tenureArr[$tenure['emp_name']] = $diff;
			$bucketArr[$tenure['emp_name']] = $yearsf." Years,".abs($monthsf)." Months";
		}else{
			$tenureArr[$tenure['emp_name']] = "NA";
			$bucketArr[$tenure['emp_name']] = "NA";
		}
	}
	//CSAT Green, Red, Survey Count
	$csatsurveyArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as csat_count, LOWER(case_owner) as case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','csat_count');
	
	$csatgreencntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as green, LOWER(case_owner) as case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Green" group by case_owner'),'case_owner','green');
	
	$csatredcntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as red, LOWER(case_owner) as case_owner from aruba_csat where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Red" and client_excetion !="Yes" group by case_owner'),'case_owner','red');
	
	
	foreach($csatsurveyArr as $engg_name => $survey_count){
		$csatgreenArr[$engg_name] = ($csatgreencntArr[$engg_name]/$survey_count)*100;
		$csatredArr[$engg_name] = ($csatredcntArr[$engg_name]/$survey_count)*100;
	}
	foreach($csatgreenArr as $engg_name => $csatgreen)
	{
		$plvarval = plcalculate($csatgreen);
		$greenplArr[$engg_name] = $plvarval;
	}
	//Productivity, closures
	$engg_pc = $conn->prepare('select count(*) as tot_count,LOWER(case_owner) as case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_pc->execute();
	$engg_pcArr = $engg_pc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_pcArr as $pc){
		$closuresArr[$pc['case_owner']] = $pc['tot_count'];
		$productivityArr[$pc['case_owner']] = $pc['tot_count']/20;
	}
	//TTC less than 6 days
	//echo 'select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "6" group by case_owner';
	//$engg_ttc = $conn->prepare('select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "6" group by case_owner');
	if($productgroup == "THD"){
		$engg_ttc = $conn->prepare('select count(*) as tot_count,LOWER(case_owner) as case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "6" group by case_owner');
	}else{
		$engg_ttc = $conn->prepare('select count(*) as tot_count,LOWER(case_owner) as case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" AND round(ttc) <= "7" group by case_owner');
	}
	$engg_ttc->execute();
	$engg_ttcArr = $engg_ttc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ttcArr as $ttc){
		$ttcArr[$ttc['case_owner']] = $ttc['tot_count'];
	}
	//print_r($ttcArr);
	//TTC Overall
	//echo 'select count(*) as tot_count,case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner';
	$engg_ttc_overall = $conn->prepare('select count(*) as tot_count,LOWER(case_owner) as case_owner from aruba_closure where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_ttc_overall->execute();
	$engg_ttc_overallArr = $engg_ttc_overall->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ttc_overallArr as $ttc_overall){
		$ttc_overallArr[$ttc_overall['case_owner']] = $ttc_overall['tot_count'];
	}
	
	foreach($ttcArr as $key => $ttc_val){
		$ttc_cal_val = round(($ttc_val/$ttc_overallArr[$key])*100);
		$ttc_percent[$key] = $ttc_cal_val;
		$ttc_percent_pl[$key] = plcalculate_ttc($ttc_cal_val);
	}
	foreach($closuresArr as $engg_name => $closures){
		$closurepercen = (($closures/50)*100)> 121?121:($closures/$closures_target)*100;
		$closurepercentageArr[$engg_name] = $closurepercen;
	}
	foreach($closurepercentageArr as $engg_name => $closurepercent)
	{
		$plvarval = plcalculate($closurepercent);
		$closureplArr[$engg_name] = $plvarval;
	}
	//Open Cases
	$engg_oc = $conn->prepare('select count(*) as open_count,LOWER(case_owner) as case_owner from aruba_open where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner');
	$engg_oc->execute();
	$engg_ocArr = $engg_oc->fetchAll(PDO::FETCH_ASSOC);
	foreach($engg_ocArr as $oc){
		$opencasesArr[$oc['case_owner']] = $oc['open_count'];
	}
	//Soft Skill
	$softskillsArr = $commonobj->arrayColumn($commonobj->getQry('select avg(overall) as ss,LOWER(case_owner) as case_owner from aruba_ssqa where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','ss');
	
	foreach($softskillsArr as $engg_name => $sspercent)
	{
		$ssplvarval = plcalculate($sspercent);
		$ssplArr[$engg_name] = $ssplvarval;
	}
	//Process Quality
	$processqualityArr = $commonobj->arrayColumn($commonobj->getQry('select avg(overall) as process_quality,LOWER(case_owner) as case_owner from aruba_process_audit where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.'="'.$selectrange.'" group by case_owner'),'case_owner','process_quality');
	
	foreach($processqualityArr as $engg_name => $pcval)
	{
		$pqplvarval = plcalculate($pcval);
		$pqplArr[$engg_name] = $pqplvarval;
	}
	//Escalation
	$escalationArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as esc_count, LOWER(case_owner) as case_owner from aruba_esc where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" and client_exception !="Yes" group by case_owner order by case_owner asc'),'case_owner','esc_count');
	
	$workingdaysArr = $commonobj->arrayColumn($commonobj->getQry('select working_days,'.$type.$selectQry.' from aruba_headcount where '.$type.$selectQry.' = "'.$selectrange.'" group by '.$type.$selectQry),$type.$selectQry,'working_days');
	$workingdays = $workingdaysArr[$selectrange];
	
	//Attendance
	$overallout = $commonobj->arrayColumn($commonobj->getQry('select LOWER(case_owner) as case_owner,count(*) as overalloutcnt from aruba_avaya_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" AND time_to_sec(total_time) >=  "18000" group by case_owner'),'case_owner','overalloutcnt');
	
	//Backlog
	if($selecttype == "Monthly" && $selectrange == "2017Month11") {
		$backlogArr = $commonobj->arrayColumn($commonobj->getQry('select (sum(age)/count(*)) as avg,LOWER(case_owner) as case_owner from aruba_backlog_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
	}elseif($selecttype == "Quarterly" && $selectrange == "2017Q4"){
		$backlogArr1 = $commonobj->arrayColumn($commonobj->getQry('select LOWER(case_owner) as case_owner,avg from aruba_backlog_new where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
		$backlogArr2 = $commonobj->arrayColumn($commonobj->getQry('select (sum(age)/count(*)) as avg,LOWER(case_owner) as case_owner from aruba_backlog_raw where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
		foreach($backlogArr1 as $case_owner => $avg1)
		{
			$avg = ($avg1 + $backlogArr2[$case_owner])/2;
			$backlogArr[$case_owner] = $avg;
		}
	}else{
		$backlogArr = $commonobj->arrayColumn($commonobj->getQry('select LOWER(case_owner) as case_owner,avg from aruba_backlog_new where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','avg');
	}
	
	//Backlog
	/* $overallbacklogcntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as overallbacklog from aruba_backlog where '.$type.$selectQry.' = "'.$selectrange.'" '),$type.$selectQry,'overallbacklog');
	$overallbacklogcnt = $overallbacklogcntArr[$selectrange];
	
	$overallbacklogArr = $commonobj->arrayColumn($commonobj->getQry('select case_owner,count(*) as backlogcnt from aruba_backlog where case_owner in ("'.implode('","',$headerenggArr).'") AND '.$type.$selectQry.' = "'.$selectrange.'" group by case_owner'),'case_owner','backlogcnt');
	
	foreach($overallbacklogArr as $engg_name => $backlog){
		$backlogArr[$engg_name] = round(($backlog/$overallbacklogcnt)*100,2);
	} */
	
	function plcalculate($plval){
		if($plval >= 0 && $plval < 76)
		{
			$plvar = "PL1";
		}elseif($plval >= 76 && $plval < 96)
		{
			$plvar = "PL2";
		}elseif($plval >= 96 && $plval < 106)
		{
			$plvar = "PL3";
		}elseif($plval >= 106 && $plval < 121)
		{
			$plvar = "PL4";
		}elseif($plval >= 121)
		{
			$plvar = "PL5";
		}
		return $plvar;
	}
	
	function plcalculate_ttc($plval){
		if($plval >= 0 && $plval <= 69)
		{
			$plvar = "PL1";
		}elseif($plval >= 70 && $plval <= 79)
		{
			$plvar = "PL2";
		}elseif($plval >= 80 && $plval <= 85)
		{
			$plvar = "PL3";
		}elseif($plval >= 86 && $plval <= 95)
		{
			$plvar = "PL4";
		}elseif($plval >= 96)
		{
			$plvar = "PL5";
		}
		return $plvar;
	}
	
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Stank Rank Summary.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	/*.form-control {
	    border: 0px solid #c2cad8 !important;
	}*/
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	tbody { 
	  height: 150px; 
	  overflow-y: auto;
	  overflow-x: hidden;
	}
	th.thead{
		background-color:#F2784B !important;
		color:white;
		text-align:center;
	}

	td.thead{
		text-align:center;
		font-weight:bold;
	}

	@media (min-width: 992px){
		.col-md-1 {
			width: 11.33333%;
		}
	}
	.DTFC_LeftBodyLiner {
	    overflow: hidden !important;
	}
	.table-scrollable {
		overflow-x:hidden;
	}
	table th, tr, td{
		text-align:center;
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-body">
				<div class="table-scrollable">
		        <!--<table class="stripe table table-striped order-column text-center" width="100%" id="example" style='white-space: nowrap;' border="1">-->
				<table id="example" class="stripe row-border table-striped order-column text-center" width="100%" style='white-space: nowrap;' border="1">
                     <thead class="header">
						<tr>
							<th style="background-color:#F2784B;color:white;text-align:center;" rowspan="2">Network Engineer</th>
							<th style="background-color:#F2784B;color:white;text-align:center;" rowspan="2" colspan="6">&nbsp;</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Target</th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $green_target; ?>%</th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="4"><?php echo $red_target; ?>%</th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="3"><?php echo $closures_target; ?>%</th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="3"><?php echo $ttc_target; ?>%</th>
							<!--<th style="background-color:#F2784B;color:white;text-align:left;" colspan="2"><?php echo $ss_target; ?>%</th>-->
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="2"><?php echo $pq_target; ?>%</th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $esc_target; ?>%</th>
							<!--<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $atten_target; ?>%</th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $backlog_target; ?>%</th>-->
						</tr>
						<tr>
							<th style="background-color:#F2784B;color:white;text-align:center;">Weightage</th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $csatgreen_weightage.'%'; ?></th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="4"><?php echo $red_weightage.'%'; ?></th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="3"><?php echo $closure_weightage.'%'; ?></th>
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="3"><?php echo $ttc_weightage.'%'; ?></th>
							<!--<th style="background-color:#F2784B;color:white;text-align:left;" colspan="2"><?php echo $softskill_weightage.'%'; ?></th>-->
							<th style="background-color:#F2784B;color:white;text-align:left;" colspan="2"><?php echo $processquality_weightage.'%'; ?></th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $escalation_weightage.'%'; ?></th>
							<!--<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $atten_weightage.'%'; ?></th>
							<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $backlog_weightage.'%'; ?></th>-->
						</tr>
                        <tr>
							<th style="background-color:#F2784B;color:white;text-align:center;">Engg. Name</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Emp Id</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Team Lead</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Project</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Tenure</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Bucket Wise</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Overall</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Overall PL</th>	
							<th style="background-color:#F2784B;color:white;text-align:center;">CSAT Green</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Red</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Survey Count</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">PL</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Productivity</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Closures</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Closure %</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">PL</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">TTC % <?php if($productgroup == "THD"){ echo "(Less than 6 Days)"; }else{ echo "(Less than 7 Days)"; } ?></th>
							<th style="background-color:#F2784B;color:white;text-align:center;">PL</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Open Cases</th>
							<!--<th style="background-color:#F2784B;color:white;text-align:center;">Soft Skills</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">PL</th>-->
							<th style="background-color:#F2784B;color:white;text-align:center;">Process Quality</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">PL</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Escalation</th>
							<!--<th style="background-color:#F2784B;color:white;text-align:center;">Attendance</th>
							<th style="background-color:#F2784B;color:white;text-align:center;">Backlog Avg.</th>-->
                        </tr>
                    </thead>
                    <tbody>
						<?php 
						if(!empty($headerenggArr)){
							foreach($headerenggArr as $engg_key => $engg_name){ 
							
							$attendance = round((empty($overallout[$engg_name])?0:$overallout[$engg_name]/$workingdays)*100); 
							$attendance_percent = ($attendance>=100)?'100%':$attendance.'%';
							
							$add1 = (float)$add11=((empty($csatgreenArr[$engg_name])?0:$csatgreenArr[$engg_name]/$green_target)*$csatgreen_weightage)/100;
							$add2 = (float)$add12=((1-$csatredArr[$engg_name])>0)?$red_weightage:0;
							$add3 = (float)$add13=(empty($closurepercentageArr[$engg_name])?0:$closurepercentageArr[$engg_name]/100)*($closure_weightage/100);
							$add9 = (float)$add19=(empty($ttc_percent[$engg_name])?0:$ttc_percent[$engg_name]/100)*($ttc_weightage/100);
							//$add4 = (float)$add14=((empty($softskillsArr[$engg_name])?0:$softskillsArr[$engg_name]/$ss_target)*$softskill_weightage)/100;
							$add5 = (float)$add15=((empty($processqualityArr[$engg_name])?0:$processqualityArr[$engg_name]/$pq_target)*$processquality_weightage)/100;
							$add6 = (float)$add16=(empty($escalationArr[$engg_name])?0:$escalationArr[$engg_name]>0)?$escalation_weightage/100:0;
							//$add7 = (float)$add17=(empty($attendance)?0:$attendance<95)?$atten_weightage/100:0;
							//$add8 = (float)$add18=($backlogArr[$engg_name]>0)?$backlog_weightage/100:0;
							$overallArr[$engg_name] = round(($add1+$add2+$add3+$add5+$add6+$add9)*100,2);
							}
							//print_r($overallArr);
							arsort($overallArr);
							//print_r($overallArr);
						foreach($overallArr as $engg_name => $overall){ 
							
							/* $attendance = round((empty($overallout[$engg_name])?0:$overallout[$engg_name]/$workingdays)*100); 
							$attendance_percent = ($attendance>=100)?'100%':$attendance.'%';
							
							$add1 = (float)$add11=((empty($csatgreenArr[$engg_name])?0:$csatgreenArr[$engg_name]/$green_target)*$csatgreen_weightage)/100;
							$add2 = (float)$add12=((1-$csatredArr[$engg_name])>0)?$red_weightage:0;
							$add3 = (float)$add13=(empty($closurepercentageArr[$engg_name])?0:$closurepercentageArr[$engg_name]/100)*($closure_weightage/100);
							$add9 = (float)$add19=(empty($ttc_percent[$engg_name])?0:$ttc_percent[$engg_name]/100)*($ttc_weightage/100);
							$add4 = (float)$add14=((empty($softskillsArr[$engg_name])?0:$softskillsArr[$engg_name]/$ss_target)*$softskill_weightage)/100;
							$add5 = (float)$add15=((empty($processqualityArr[$engg_name])?0:$processqualityArr[$engg_name]/$pq_target)*$processquality_weightage)/100;
							$add6 = (float)$add16=(empty($escalationArr[$engg_name])?0:$escalationArr[$engg_name]>0)?$escalation_weightage/100:0;
							$add7 = (float)$add17=(empty($attendance)?0:$attendance<95)?$atten_weightage/100:0;
							//$add8 = ($backlogArr<10)?$backlog_weightage:0;
							$add8 = (float)$add18=($backlogArr[$engg_name]>0)?$backlog_weightage/100:0;
							$overall = round(($add1+$add2+$add3+$add4+$add5+$add6+$add7)*100,2); */
						?>
							<tr>
							<td class="thead" style="text-align:center;"><?php echo UCWORDS($engg_name); ?></td>
							<td class="thead" style="text-align:center;"><?php echo $emplyId[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($enggteamArr[$engg_name])?"NA":$enggteamArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($enggprojetcArr[$engg_name])?"NA":$enggprojetcArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($tenureArr[$engg_name])?"NA":number_format($tenureArr[$engg_name]); ?></td>
							<td style="text-align:center;"><?php echo empty($bucketArr[$engg_name])?"NA":$bucketArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo $overall.'%'; ?></td>
							<td style="text-align:center;"><?php echo plcalculate($overall); ?></td>
							<?php 
								if($csatgreenArr[$engg_name] !=''){
									$surveygreen = number_format($csatgreenArr[$engg_name]).'%';
								}else if($csatsurveyArr[$engg_name] =='' && $closuresArr[$engg_name] > 0){
									$csatteamsurveyArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as csat_count, team from aruba_csat where team = "'.$enggteamArr[$engg_name].'" AND '.$type.$selectQry.'="'.$selectrange.'"'),'team','csat_count');
									$csatteamgreencntArr = $commonobj->arrayColumn($commonobj->getQry('select count(*) as green, team from aruba_csat where team = "'.$enggteamArr[$engg_name].'" AND '.$type.$selectQry.'="'.$selectrange.'" AND alert_type = "Green"'),'team','green');
									$surveygreen = $csatgreenArr[$engg_name] = round(($csatteamgreencntArr[$enggteamArr[$engg_name]]/$csatteamsurveyArr[$enggteamArr[$engg_name]])*100,2).'%';
									//$surveygreen = 'NA';
								}else if($csatgreenArr[$engg_name] =='' && $closuresArr[$engg_name] ==''){
									$surveygreen = 'NA';
								}
							?>
							<td style="text-align:center;"><?php echo $surveygreen; ?></td>
							<?php 
								if($csatredArr[$engg_name] !=''){
									$surveyred = number_format($csatredArr[$engg_name]).'%';
								}else if($csatsurveyArr[$engg_name] ==''){
									$surveyred = 'NA';
								}else if($csatredArr[$engg_name] =='' && $csatsurveyArr[$engg_name] !=''){
									$surveyred = '0%';
								}
							?>
							<td style="text-align:center;"><?php echo  $surveyred; ?></td>

							<td style="text-align:center;"><?php echo empty($csatsurveyArr[$engg_name])?"NA":number_format($csatsurveyArr[$engg_name]); ?></td>
							<td style="text-align:center;"><?php echo empty($greenplArr[$engg_name])?"NA":$greenplArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($productivityArr[$engg_name])?"NA":round($productivityArr[$engg_name],2); ?></td>
							<td style="text-align:center;"><?php echo empty($closuresArr[$engg_name])?"NA":number_format($closuresArr[$engg_name]); ?></td>
							<td style="text-align:center;"><?php echo empty($closurepercentageArr[$engg_name])?"NA":number_format($closurepercentageArr[$engg_name]).'%'; ?></td>
							<td style="text-align:center;"><?php echo empty($closureplArr[$engg_name])?"NA":$closureplArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($ttc_percent[$engg_name])?"NA":number_format($ttc_percent[$engg_name]).'%'; ?></td>
							<td style="text-align:center;"><?php echo empty($ttc_percent_pl[$engg_name])?"NA":$ttc_percent_pl[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($opencasesArr[$engg_name])?"NA":number_format($opencasesArr[$engg_name]); ?></td>
							<!--<td style="text-align:center;"><?php echo empty($softskillsArr[$engg_name])?"NA":number_format($softskillsArr[$engg_name]).'%'; ?></td>
							<td style="text-align:center;"><?php echo empty($ssplArr[$engg_name])?"NA":$ssplArr[$engg_name]; ?></td>-->
							<td style="text-align:center;"><?php 
								if($processqualityArr[$engg_name] != ''){
									echo number_format($processqualityArr[$engg_name]).'%'; 
								}elseif($processqualityArr[$engg_name] == '' && $closuresArr[$engg_name] >= 10){
									echo '90%';
								}else{
									echo 'NA';
								}
							?></td>
							<td style="text-align:center;"><?php echo empty($pqplArr[$engg_name])?"NA":$pqplArr[$engg_name]; ?></td>
							<td style="text-align:center;"><?php echo empty($escalationArr[$engg_name])?"NA":number_format($escalationArr[$engg_name]); ?></td>
							<!--<td style="text-align:center;"><?php echo $attendance_percent; ?></td>
							<td style="text-align:center;"><?php echo empty($backlogArr[$engg_name])?"NA":number_format($backlogArr[$engg_name]);?></td>-->
							<!-- <td style="text-align:center;"><?php //echo empty($overallArr[$engg_name]/$overallbacklogcnt)?"NA":$overallArr[$engg_name]/$overallbacklogcnt; ?></td> -->
							</tr>
						<?php }
						}else{ ?>
							<tr><td colspan="23">No Data Found</td></tr>
						<?php } ?>
					</tbody>
                </table>
		    </div>
	    </div>
	</div>
</form>
