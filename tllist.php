<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['tlname'] || $_POST['selectrange']){
	$tlname 	= 	$_POST['tlname'];
	$queuetype	= 	$_POST['queue'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$tlname 		=	!empty($tlname)?$tlname:"Overall";
	$queuetype 		=	!empty($queuetype)?$queuetype:"GSC";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($tlname!=""){
	$type="calendar_";
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	if($tlname!='Overall' && $tlname!=''){
		$QryCondition.=" and team='".$tlname."'";
		$QryCondition1.=" and team='".$tlname."'";
	}
	if($queuetype!='Overall' && $queuetype!=''){
		$QryCondition.=" and que_new='".$queuetype."'";

		$getQue.=" and que_new='".$queuetype."'";
	}
	$headerQury = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	$headerQury->execute();
	$headQryArr = $headerQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	if($selecttype=='Quarterly'){	
		$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	}else{
		$tablehead=array_reverse(array_slice($arrayval, 0, 12, true));
	}
	$inflwQury = $conn->prepare("select ".$type.$selectQry.",team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$inflwQury->execute();
	$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[$weeknamearr[$type.$selectQry]][]=$weeknamearr[$type.$selectQry];
	}
	// //closure count
	$closureQury = $conn->prepare("select ".$type.$selectQry.",rma,team from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$closureQury->execute();
	$closeQuryArr = $closureQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($closeQuryArr as $closeArrval){
		$closecount[$closeArrval[$type.$selectQry]][]=$closeArrval[$type.$selectQry];
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
	}
	$rmacount=$commonobj->getarracount($rma);
	// //csat
	$csatunQury = $conn->prepare("select ".$type.$selectQry.",overall_experience,rma,alert_type from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$csatunQury->execute();
	$csatunQuryArr = $csatunQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type']=='RED'?'Red':$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//backlog
	$backlogQury = $conn->prepare("select ".$type.$selectQry." from aruba_backlog where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition1 order by id desc");
	$backlogQury->execute();
	$backlogArr = $backlogQury->fetchAll(PDO::FETCH_ASSOC);
	foreach ($backlogArr as $bkey => $backlogvalue) {
		$backlogweekcount[$backlogvalue[$type.$selectQry]][]=$backlogvalue[$type.$selectQry];
	}
	//Avaya
	$ayaQury = $conn->prepare("select ".$type.$selectQry.",`no_of_working`,`no_of_days` from aruba_avaya where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition1 order by id desc");
	$ayaQury->execute();
	$ayaQuryArr = $ayaQury->fetchAll(PDO::FETCH_ASSOC);
	foreach ($ayaQuryArr as $ayakey => $ayavalue) {
		$nuofworkingdays[$ayavalue[$type.$selectQry]][]=$ayavalue['no_of_working'];
		$nuofdays[$ayavalue[$type.$selectQry]][]=$ayavalue['no_of_days'];
	}
	//print_r($nuofworkingdays);
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
.clr{
	background-color:#F2784B;color:white;
}

</style>
<script type="text/javascript">

	</script>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		            <div class="form-group col-md-3"></div>
		            <div class="form-group col-md-3 "></div>
		            <div class="form-group col-md-3"></div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		            	<select class="form-control selectweek" id="que"  name="queue">
		                	<option value="GSC">GSC</option>
		                	<option value="WC">WC</option>
		                </select>
		                <script> 
		                     jQuery("#que").val("<?php echo $queuetype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop1"  name="tlname">
		                	<option value='Overall'>Overall</option>
		                	<?php
		                	$closureQry = $conn->prepare("select distinct team from aruba_closure where que_new!='GEC' $getQue order by team asc");
		                	$closureQry->execute();
							$tllist = $closureQry->fetchAll(PDO::FETCH_ASSOC);
		                	foreach($tllist as $name){
								echo '<option value="'.$name['team'].'">'.$name['team'].'</option>'; 
							}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop1").val("<?php echo $tlname ?>");
		                </script>
		            </div>
		           <!--  <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div> -->
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype">
		                	<!-- <option value="Daily">Daily</option> -->
		                	<option value="Weekly">Weekly</option>
		                    <!-- <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option> -->
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop7"  name="selectrange">
		                	<option value="">--- Select ---</option>';
		                	<?php
								$selecttypeQry = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								$selecttypeQry->execute();
								$drowpdownArr = $selecttypeQry->fetchAll(PDO::FETCH_ASSOC);
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	<div class="dt-buttons">
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        	</div>
			        </div>
		    </div>
		    <div class="portlet-body">
		    	<div class="table-scrollable">
		        	<table class="table table-fixed-header table-striped table-bordered table-hover text-center scroll" data-height="300">
                      <thead  class="header">
                            <tr>
                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
                        		<?php	}	?>
                            </tr>
                        </thead>
                       <tfoot >
                            <tr>
                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
                        		<?php	}	?>
                            </tr>
                        </tfoot>
                        <tbody class="scrollContent" style=" height: 120px;*display: block;">
                        	<tr >
                        		<td rowspan="1"style="vertical-align: inherit;">Volume</td>
	                        	<td>Open</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td class="text-center"><?php echo empty($totopencase[$rs])?0:count($totopencase[$rs]); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td rowspan="2"style="vertical-align: inherit;">Closed Cases</td>
	                        	<td>Closure</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td class="text-center"><?php echo empty($closecount[$rs])?0:count($closecount[$rs]); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
                        		<td>RMA Case</td>
                        		<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td class="text-center"><?php echo empty($rmacount[$rs]['Yes'])?"0":$rmacount[$rs]['Yes'] ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
                        	                        	<tr>
                        		<td rowspan="5"style="vertical-align: inherit;">CSAT</td>
	                        	<td>Overall Experience</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td class="text-center"><?php echo empty($overallexp[$rs])?"NA":number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td>Green%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Green'])?"NA":round($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td>Normal %</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Normal'])?"NA":round($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td>Red%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td class="text-center"><?php echo empty($alert_type[$rs]['Red'])?"NA":round($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100).'%'; ?></td>	
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td>Survey Count</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td class="text-center"><?php echo empty($alert_type[$rs])?"0":array_sum($alert_type[$rs]); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td colspan="2">Backlog</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td class="text-center"><?php echo empty($backlogweekcount[$rs])?"NA":count($backlogweekcount[$rs]); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td colspan="2">Outage %</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
	                        	$outageval=empty($nuofworkingdays[$rs])?"NA":array_sum($nuofworkingdays[$rs])/array_sum($nuofdays[$rs]);
	                        	?>
								<td class="text-center"><?php echo $outageval=='NA'?"NA":number_format((1-$outageval)*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        </tbody>
                	</table>
		       </div>
		    </div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('#drop1').change(function(){
		document.getElementById("frmsrch").action = 'tllist.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'tllist-download.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop7').change(function(){
		document.getElementById("frmsrch").action = 'tllist.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#que').change(function(){
		$('#drop1').val('Overall');
		document.getElementById("frmsrch").action = 'tllist.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
</script>
