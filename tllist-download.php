<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['tlname'] || $_POST['selectrange']){
	$tlname 	= 	$_POST['tlname'];
	$queuetype	= 	$_POST['queue'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$tlname 		=	!empty($tlname)?$tlname:"Overall";
	$queuetype 		=	!empty($queuetype)?$queuetype:"GSC";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($tlname!=""){
	$type="calendar_";
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	if($tlname!='Overall' && $tlname!=''){
		$QryCondition.=" and team='".$tlname."'";
	}
	if($queuetype!='Overall' && $queuetype!=''){
		$QryCondition.=" and que_new='".$queuetype."'";
		$getQue.=" and que_new='".$queuetype."'";
	}
	$headerQury = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	$headerQury->execute();
	$headQryArr = $headerQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	if($selecttype=='Quarterly'){	
		$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	}else{
		$tablehead=array_reverse(array_slice($arrayval, 0, 12, true));
	}
	$inflwQury = $conn->prepare("select ".$type.$selectQry.",team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$inflwQury->execute();
	$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[$weeknamearr[$type.$selectQry]][]=$weeknamearr[$type.$selectQry];
	}
	// //closure count
	$closureQury = $conn->prepare("select ".$type.$selectQry.",rma,team from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$closureQury->execute();
	$closeQuryArr = $closureQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($closeQuryArr as $closeArrval){
		$closecount[$closeArrval[$type.$selectQry]][]=$closeArrval[$type.$selectQry];
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
	}
	$rmacount=$commonobj->getarracount($rma);
	// //csat
	$csatunQury = $conn->prepare("select ".$type.$selectQry.",overall_experience,rma,alert_type from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$csatunQury->execute();
	$csatunQuryArr = $csatunQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type']=='RED'?'Red':$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//backlog
	$backlogQury = $conn->prepare("select ".$type.$selectQry." from aruba_backlog where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$backlogQury->execute();
	$backlogArr = $backlogQury->fetchAll(PDO::FETCH_ASSOC);
	foreach ($backlogArr as $bkey => $backlogvalue) {
		$backlogweekcount[$backlogvalue[$type.$selectQry]][]=$backlogvalue[$type.$selectQry];
	}
	//Avaya
	$ayaQury = $conn->prepare("select ".$type.$selectQry.",`no_of_working`,`no_of_days` from aruba_avaya where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	$ayaQury->execute();
	$ayaQuryArr = $ayaQury->fetchAll(PDO::FETCH_ASSOC);
	foreach ($ayaQuryArr as $ayakey => $ayavalue) {
		$nuofworkingdays[$ayavalue[$type.$selectQry]][]=$ayavalue['no_of_working'];
		$nuofdays[$ayavalue[$type.$selectQry]][]=$ayavalue['no_of_days'];
	}
	//print_r($nuofworkingdays);
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Tl wise Summary.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
.clr{
	background-color:#F2784B;color:white;
}
.text-center{
	text-align: center;
}
</style>
<script type="text/javascript">

	</script>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		       <table>
	                <tr class="text-center"rowspan=2>
	                    <tr>
	                        <td colspan=<?php echo count($tablehead)+2?> align="center"style="border:1px solid;background-color:#F2784B;color:white;"><H3>
	                            <i class="icon-calendar"></i>TL wise <?php echo "(".$queuetype.'-'.$tlname.")-".$selectrange; ?>
	                        </H3>
	                        </td>
	                    </tr>
	                </tr>
	            <table> 
		    </div>
		</div>
		    <div class="portlet-body">
		        <table class="table" data-height="300" border=1>
                      <thead  class="header">
                            <tr>
                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
                            	<th style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
                        		<?php	}	?>
                            </tr>
                        </thead>
                        <tbody style=" height: 120px;*display: block;">
                        	<tr >
                        		<td rowspan="1"style="vertical-align: inherit;background-color:#F2784B;color:white;">Volume</td>
	                        	<td style="background-color:#F2784B;color:white;">Open</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style='text-align: center'><?php echo empty($totopencase[$rs])?0:count($totopencase[$rs]); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td rowspan="2" style="vertical-align: inherit;background-color:#F2784B;color:white;">Closed Cases</td>
	                        	<td style="background-color:#F2784B;color:white;">Closure</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style='text-align: center'><?php echo empty($closecount[$rs])?0:count($closecount[$rs]); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
                        		<td style="vertical-align: inherit;background-color:#F2784B;color:white;">RMA Case</td>
                        		<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style='text-align: center'><?php echo empty($rmacount[$rs]['Yes'])?"0":$rmacount[$rs]['Yes'] ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
                        		<td rowspan="5"style="vertical-align: inherit;background-color:#F2784B;color:white;">CSAT</td>
	                        	<td style="vertical-align: inherit;background-color:#F2784B;color:white;">Overall Experience</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style='text-align: center'><?php echo empty($overallexp[$rs])?"NA":number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td style="vertical-align: inherit;background-color:#F2784B;color:white;">Green%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td style='text-align: center'><?php echo empty($alert_type[$rs]['Green'])?"NA":round($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="vertical-align: inherit;background-color:#F2784B;color:white;">Normal %</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style='text-align: center'><?php echo empty($alert_type[$rs]['Normal'])?"NA":round($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="vertical-align: inherit;background-color:#F2784B;color:white;">Red%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style='text-align: center'><?php echo empty($alert_type[$rs]['Red'])?"NA":round($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100).'%'; ?></td>	
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="vertical-align: inherit;background-color:#F2784B;color:white;">Survey Count</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style='text-align: center'><?php echo empty($alert_type[$rs])?"0":array_sum($alert_type[$rs]); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td colspan="2" style="vertical-align: inherit;background-color:#F2784B;color:white;">Backlog</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td style='text-align: center'><?php echo empty($backlogweekcount[$rs])?"NA":count($backlogweekcount[$rs]); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td colspan="2" style="vertical-align: inherit;background-color:#F2784B;color:white;">Outage %</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
	                        	$outageval=empty($nuofworkingdays[$rs])?"NA":array_sum($nuofworkingdays[$rs])/array_sum($nuofdays[$rs]);
	                        	?>
								<td style='text-align: center'><?php echo $outageval=='NA'?"NA":number_format((1-$outageval)*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        </tbody>
                </table>
		       </div>
		    </div>
		</div>
</form>

