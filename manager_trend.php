<?php
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['overallteam'] || $_POST['calendartype'] || $_POST['tlnamelist'] || $_POST['selectrange'] ){
	$overallteam 	= 	$_POST['overallteam'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$productgroup 	= 	$_POST['productgroup'];
}else{
	$selectrange	=	!empty($selectrange)?$selectrange:current($currentweek);
	$overallteam 	=	!empty($overallteam)?$overallteam:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
}

if($overallteam!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	if($overallteam=='Overall'){
		$QryCondition.=" and que_new!='GEC'";
		$hcount="wlan,ns";
	}else{
		$QryCondition.=" and que_new='".$overallteam."'";
		$hcount="wlan,ns";
	}

	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}

	$tlnameArrat = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct LOWER(manager_name) as manager_name from aruba_open where $type$selectQry='$selectrange' $QryCondition order by manager_name asc"),'','manager_name');

	$Qry = "where manager_name in ('".implode("','",$tlnameArrat)."') and  $type$selectQry='$selectrange'";
	//echo "select team,$type$selectQry,case_origin,LOWER(manager_name) as manager_name from aruba_open $Qry";

	$inflwQuryArr = $commonobj->getQry("select $type$selectQry,case_origin,LOWER(manager_name) as manager_name from aruba_open $Qry");
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[$weeknamearr['manager_name']][]=$weeknamearr[$type.$selectQry];
		$caseorigin[$weeknamearr['manager_name']][]=$weeknamearr['case_origin'];
	}

	$case_origin=$commonobj->getarracount($caseorigin);

	//echo "SELECT count(*),LOWER(manager_name) as manager_name from aruba_closure  $Qry group by manager_name";
	$Headcount = $commonobj->arrayColumn($commonobj->getQry("SELECT count( DISTINCT case_owner) as count,LOWER(manager_name) as manager_name from aruba_closure  $Qry group by manager_name"),'manager_name','count');

	$closeQuryArr = $commonobj->getQry("SELECT ".$type.$selectQry.",ttc,rma,sdc,case_origin,team,LOWER(manager_name) as manager_name from aruba_closure  $Qry");
	foreach($closeQuryArr as $closeArrval){
		$closecount[$closeArrval['manager_name']][]=$closeArrval[$type.$selectQry];
		if($closeArrval['ttc']<='9'){
			$ttccount[$closeArrval['manager_name']][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
			$phonettc[$closeArrval['manager_name']][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
			$webttc[$closeArrval['manager_name']][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
			$sdcphone[$closeArrval['manager_name']][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
			$sdcweb[$closeArrval['manager_name']][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval['manager_name']][]=$closeArrval['rma'];
		$sdctot[$closeArrval['manager_name']][]=$closeArrval['sdc'];
	}
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);

	//echo "select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team,LOWER(case_owner) as case_owner from aruba_csat $Qry";

	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team,LOWER(manager_name) as manager_name from aruba_csat $Qry");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval['manager_name']][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval['manager_name']][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval['manager_name']][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval['manager_name']][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['rma']=='Yes'){
			$rmacaseoe[$csatunArrval['manager_name']][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval['manager_name']][]=$csatunArrval['loyalty_index'];
		}
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//avaya


	$wkday = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",working_days from aruba_headcount where $type$selectQry='$selectrange'"),'','working_days');
	$workingdays = $wkday[0];
	//echo "select ".$type.$selectQry.",LOWER(manager_name) as manager_name ,avg(overall) as ssqa_avg from aruba_ssqa $Qry group by manager_name order by manager_name asc";

	$ssqa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name ,avg(overall) as ssqa_avg from aruba_ssqa $Qry group by manager_name order by manager_name asc"),'manager_name','ssqa_avg');

	$pa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name,avg(overall) as pa_avg from aruba_process_audit  $Qry group by manager_name order by manager_name asc"),'manager_name','pa_avg');

	$overallt1 = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name,count(*) as cnt from aruba_esc $Qry group by manager_name order by manager_name asc"),'manager_name','cnt');

	$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name,count(*) as cnt from aruba_esc $Qry AND tier_1 =  'Controllable'  group by manager_name order by manager_name asc"),'manager_name','cnt');

	$overalluncontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name,count(*) as cnt from aruba_esc $Qry AND tier_1 =  'Uncontrollable'group by manager_name order by manager_name asc"),'manager_name','cnt');

	$overallblank = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",LOWER(manager_name) as manager_name,count(*) as cnt from aruba_esc $Qry AND tier_1 =  '' group by manager_name "),'manager_name','cnt');

	$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as manager_name,count(*) as cnt from aruba_avaya_raw $Qry AND time_to_sec(total_time) >=  '28800' group by manager_name"),'manager_name','cnt');

	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(manager_name) as case_owner,count(distinct case_owner) as cnt from aruba_avaya_raw $Qry group by manager_name "),'case_owner','cnt');

	$getRca = $commonobj->arrayColumn($commonobj->getQry("select LOWER(manager_name) as manager_name,count(*) as cnt from aruba_rca $Qry  group by manager_name"),'manager_name','cnt');

	$oesplit=  $commonobj->arrayColumn($commonobj->getQry("select oe_alert,LOWER(manager_name) as manager_name from aruba_rca  $Qry "),'manager_name','oe_alert');
	$rcasplit=$commonobj->getarracount($oesplit);

	// print_r($rcasplit);
	$rcareasion = $commonobj->arrayColumn( $commonobj->getQry("select count(main_root_cause) as count,main_root_cause from aruba_rca where ".$type.$selectQry." in ('".$selectrange."') $Qry group by main_root_cause"),'main_root_cause','count');
	foreach ($rcareasion as $key => $value) {
		$rca_case[$key]=number_format($value/array_sum($rcareasion)*100,1);
	}
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}

    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		width: 16.66%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}

	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.div {
            width: 100%;
            overflow-x:scroll;
            *padding-left:5em;
            overflow-y:visible;
            padding-bottom:1px;
        }
        table,td,th{
        	border:0px solid #E7ECF1;
        }
       th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
	$(document).ready(function() {
	    var table = $('#example').DataTable( {
	        scrollY:        "1000px",
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         false,
	        fixedColumns:   {
	            leftColumns: 1,
	            //rightColumns: 1
	        }
	    } );
	} );
</script>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		        <div class="form-group col-md-3 col-sm-0 col-xs-0"></div>

		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="overallteam" onchange="reload()">
			                <option value="Overall">Overall Que</option>
			                <option value="WC">WC</option>
			                <option value="GSC">GSC</option>
		                </select>
		                <script>
		                     jQuery("#drop3").val("<?php echo $overallteam ?>");
		                </script>
		            </div>
					<div class="form-group col-md-3 col-sm-0 col-xs-0">
					<?php
					$Qery= $overallteam !='Overall' ? " and que_new = '$overallteam' " :'';
					$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where 1 $Qery");
					?>
						<select class="form-control submit" id="drop4"  name="productgroup"  onchange="reload()">
							<?php
								echo '<option value="Overall">Overall Product</option>';
			                	foreach ($productName as $key => $value) {
									echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
			                	}
			                ?>
			            </select>
			            <script>
							jQuery("#drop4").val("<?php echo $productgroup ?>");
			            </script>
			        </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype" >
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script>
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype">
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script>
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop7"  name="selectrange"  onchange="reload()">
		                	<option value="">--- Select ---</option>';
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?>
		                </select>
		                <script>
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Manager Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;">
			        	<div class="dt-buttons">
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        	</div>
			        </div>
		   		</div>
		    <div class="portlet-body">
		       <div class="">
		        	<!-- <table class="table table-striped table-bordered table-hover text-center" style='white-space: nowrap;' id='example'> -->
		        	<table id="example" class="stripe row-border table-striped order-column text-center"width="100%" style='white-space: nowrap;'>
                        <thead>
                            <tr style="*background-color:#f3d3c8;">
                            	<td rowspan="2" class="headcol style" style='vertical-align: inherit;height: 48px;border:1px solid #E7ECF1'>Metrics</td>
                            	<td colspan="3" style='border:1px solid #E7ECF1'>Open Cases</td>
                            	<td colspan='10' style='border:1px solid #E7ECF1'>Closed Cases</td>
                            	<td colspan="9" style='border:1px solid #E7ECF1'>CSAT</td>
                            	<td colspan="4" style='border:1px solid #E7ECF1'>Escalation</td>
								<td colspan="2" style='border:1px solid #E7ECF1'>Quality</td>
								<td rowspan="2" style='border:1px solid #E7ECF1'>Outage</td>
								<td colspan="4" style='border:1px solid #E7ECF1'>RCA</td>

                            </tr>
                            <tr style="*background-color:#f3d3c8;">
                            	<td style="margin-left:100px;border:1px solid #E7ECF1">Overall</td>
                            	<td style='border:1px solid #E7ECF1'>Phone</td>
                            	<td style='border:1px solid #E7ECF1'>Web</td>
                            	<td style='border:1px solid #E7ECF1'>Closure</td>
                            	<td style='border:1px solid #E7ECF1'>RMA %</td>
                            	<td style='border:1px solid #E7ECF1'>RMA Cases</td>
                            	<td style='border:1px solid #E7ECF1'>TTC &lt;9 Days</td>
                            	<td style='border:1px solid #E7ECF1'>Phone - TTC &lt;9 Days</td>
                            	<td style='border:1px solid #E7ECF1'>Web - TTC &lt;9 Days</td>
                            	<td style='border:1px solid #E7ECF1'>SDC</td>
                            	<td style='border:1px solid #E7ECF1'>SDC – Phone (45%)</td>
                            	<td style='border:1px solid #E7ECF1'>SDC – Web (35%)</td>

                            	<td style='border:1px solid #E7ECF1'>Productivity</td>

                            	<td style='border:1px solid #E7ECF1'>Overall Experience</td>
                            	<td style='border:1px solid #E7ECF1'>Green% - (80%)</td>
                            	<td style='border:1px solid #E7ECF1'>Normal %</td>
                            	<td style='border:1px solid #E7ECF1'>Red% - (3%)</td>
                            	<td style='border:1px solid #E7ECF1'>Survey Count</td>
                            	<td style='border:1px solid #E7ECF1'>Net Promoter Score - (80)</td>
                            	<td style='border:1px solid #E7ECF1'>Loyalty Index</td>
                            	<td style='border:1px solid #E7ECF1'>RMA cases OE</td>
                            	<td style='border:1px solid #E7ECF1'>RMA cases NPS</td>

								<td style='border:1px solid #E7ECF1'>Escalation</td>
								<td style='border:1px solid #E7ECF1'>ESC-Controllable</td>
								<td style='border:1px solid #E7ECF1'>ESC-Uncontrollable</td>
								<td style="">ESC-Blanks</td>

								<td style='border:1px solid #E7ECF1'>SSQA - (80%)</td>
								<td style='border:1px solid #E7ECF1'>Process Audit</td>

								<td style='border:1px solid #E7ECF1'>Count</td>
								<td style='border:1px solid #E7ECF1'>Top-2</td>
								<td style='border:1px solid #E7ECF1'>MID</td>
								<td style='border:1px solid #E7ECF1'>DSAT</td>


                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	if(!empty($tlnameArrat)){
	                        	foreach ($tlnameArrat as $enggkey => $enggname) {
	                        	?>
	                        	<tr>
		                        	<td style='border:1px solid #E7ECF1' ><?php echo ucwords($enggname);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($totopencase[$enggname])?0:count($totopencase[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($case_origin[$enggname]['Phone'])?"0":$case_origin[$enggname]['Phone']?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($case_origin[$enggname]['Web'])?"0":$case_origin[$enggname]['Web']?></td>
		                        	<!-- close case -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closecount[$enggname])?'0':count($closecount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($rmacount[$enggname])?"0.00%":round($rmacount[$enggname]['Yes']/array_sum($rmacount[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($rmacount[$enggname]['Yes'])?"0":$rmacount[$enggname]['Yes']; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttccount[$enggname])?0:count($ttccount[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($phonettc[$enggname])?"0":count($phonettc[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($webttc[$enggname])?"0":count($webttc[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($sdctotper[$enggname])?"0.00%":round($sdctotper[$enggname]['1']/array_sum($sdctotper[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($sdcphone[$enggname])?"0.00%":round(count($sdcphone[$enggname])/count($closecount[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($sdcweb[$enggname])?"0.00%":round(count($sdcweb[$enggname])/count($closecount[$enggname])*100).'%'; ?></td>
		                        	<!-- producty -->

		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closecount[$enggname])?"0":number_format(count($closecount[$enggname])/$workingdays/$Headcount[$enggname],2); ?></td>

		                        	<!-- end producty -->
		                        	<!-- csat unnormalized -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($overallexp[$enggname])?"NA":number_format(array_sum($overallexp[$enggname])/count($overallexp[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alert_type[$enggname]['Green'])?"NA":round($alert_type[$enggname]['Green']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alert_type[$enggname]['Normal'])?"NA":round($alert_type[$enggname]['Normal']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alert_type[$enggname]['Red'])?"NA":round($alert_type[$enggname]['Red']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alert_type[$enggname])?"0":array_sum($alert_type[$enggname]); ?></td>
		                        	<?php
		                        		$greenper[$enggname]=empty($netprompter[$enggname]['Promoter'])?"0.00%":round($netprompter[$enggname]['Promoter']/array_sum($netprompter[$enggname])*100).'%';
                        				$redperc[$enggname]=empty($netprompter[$enggname]['Detractor'])?"0.00%":round($netprompter[$enggname]['Detractor']/array_sum($netprompter[$enggname])*100).'%';
		                        	?>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($greenper[$enggname])?'0.00':$greenper[$enggname]-$redperc[$enggname]; ?></td>

		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($loyaltyindex[$enggname])?"NA":number_format(array_sum($loyaltyindex[$enggname])/count($loyaltyindex[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($rmacaseoe[$enggname])?'NA':number_format(array_sum($rmacaseoe[$enggname])/count($rmacaseoe[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($loyalty_index[$enggname])?'NA':number_format(array_sum($loyalty_index[$enggname])/count($loyalty_index[$enggname]),2); ?></td>
		                        	<!-- end csat unnormalized -->
									<!-- csat normalized 5% -->

									<td style='border:1px solid #E7ECF1'><?php echo empty($overallt1[$enggname])?'NA':number_format($overallt1[$enggname]); ?></td>
									<td style='border:1px solid #E7ECF1'>
										<?php
										if($overalluncontrol[$enggname] != "" && $overallcontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overallcontrol[$enggname])?'NA':number_format($overallcontrol[$enggname]/$overallt1[$enggname]*100).'%';
										}
										?>
									</td>
									<td style='border:1px solid #E7ECF1'>
										<?php
										if($overallcontrol[$enggname] != "" && $overalluncontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overalluncontrol[$enggname])?'NA':number_format($overalluncontrol[$enggname]/$overallt1[$enggname]*100).'%';
										}
										?>
									</td>
									<td style='border:1px solid #E7ECF1'>
										<?php
											echo empty($overallt1[$enggname])?'NA':number_format($overallblank[$enggname]/$overallt1[$enggname]*100).'%';
										?>
									</td>

									<td style='border:1px solid #E7ECF1'><?php echo empty($ssqa[$enggname])?'NA':number_format($ssqa[$enggname]).'%'; ?></td>

									<td style='border:1px solid #E7ECF1'><?php echo empty($pa[$enggname])?'NA':number_format($pa[$enggname]).'%'; ?></td>
									<td style='border:1px solid #E7ECF1'>
										<?php
											$outage = number_format((1-($overallout[$enggname]/($overalloutcnt[$enggname]*$workingdays)))*100,2);
											
											if($outage < 0)
											{
												$outage_new = 0;
											}else{
												$outage_new = $outage;
											}
											echo empty($overallout[$enggname])?'NA':$outage_new.'%';
										?></td>
										<?php
											$val =  empty($rcasplit[$enggname]) ?'NA':'0%';
										?>
										<td style='border:1px solid #E7ECF1'><?php echo empty($getRca[$enggname])?'NA':$getRca[$enggname]; ?></td>
										<td style='border:1px solid #E7ECF1'><?php echo empty($rcasplit[$enggname]['MID'])?$val:number_format($rcasplit[$enggname]['MID']/$getRca[$enggname]*100).'%'; ?></td>
										<td style='border:1px solid #E7ECF1'><?php echo empty($rcasplit[$enggname]['Top-2'])?$val:number_format($rcasplit[$enggname]['Top-2']/$getRca[$enggname]*100).'%'; ?></td>
										<td style='border:1px solid #E7ECF1'><?php echo empty($rcasplit[$enggname]['DSAT'])?$val:number_format($rcasplit[$enggname]['DSAT']/$getRca[$enggname]*100).'%'; ?></td>
	                        	<?php
	                        	}
	                        }else{
	                        	?>
	                        	<tr><td colspan="20" style='text-align: center;color:red'>No Data Found</td></tr>

	                        	<?php
	                        }
                        	?>
                        	</tr>
                        </tbody>
                    </table>
		        </div>
		    </div>
		</div>
</form>
<?php
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    });
	});
	$("#drop3").change(function(){
	    $("#drop4").val("");
	});

	$("#drop6").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    });
	});
	jQuery(document).ready(function($) {
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});

	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'manager_summary_export.php';
		document.getElementById("frmsrch").submit();
		return false;
	});
	function bucket() {
		document.getElementById("frmsrch").action = 'manager_trend.php';
		document.getElementById("frmsrch").submit();
		return false;
	}
	function reload() {
		document.getElementById("frmsrch").action = 'manager_trend.php';
		document.getElementById("frmsrch").submit();
		return false;
	}
</script>
