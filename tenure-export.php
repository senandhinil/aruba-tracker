<?php
error_reporting(E_ALL);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['overallteam'] || $_POST['calendartype'] || $_POST['tlnamelist'] || $_POST['selectrange'] ){
	$overallteam 	= 	$_POST['overallteam'];
	$tlnamelist 	= 	$_POST['tlnamelist'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$bucket    =	$_POST['bucket'];
}else{
	$selectrange	=	!empty($selectrange)?$selectrange:current($currentweek);
	$overallteam 	=	!empty($overallteam)?$overallteam:"Overall";
	$tlname = $commonobj->getQry("SELECT distinct team FROM  `aruba_open` where calendar_week = '$selectrange' order by id ASC LIMIT 0 , 1");
	$tlnamelist 	=	!empty($tlnamelist)?$tlnamelist:$tlname[0]['team'];
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
	$bucket  		=	!empty($bucket)?$bucket:"Overall";
}

if($overallteam!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	//echo $overallteam;
	if($overallteam=='Overall'){
		$QryCondition.=" and que_new!='GEC'";
		$hcount="wlan,ns";
	}else{
		$QryCondition.=" and que_new='".$overallteam."'";
		$hcount="wlan,ns";
		// if($overallteam=='GSC'){
		// 	$hcount=strtolower('wlan');
		// }else{
		// 	$hcount="wlan,ns";
		// }
	}
	$QryCondition.= $bucket == 'Overall' ? " " : " and bucket = '$bucket' ";

	$tlnameArrat = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct LOWER(case_owner) as case_owner from aruba_open where team='$tlnamelist' and $type$selectQry='$selectrange' $QryCondition order by case_owner asc"),'','case_owner');

	$Qry = "where case_owner in ('".implode("','",$tlnameArrat)."') and  $type$selectQry='$selectrange' and team='$tlnamelist'";

	$inflwQuryArr = $commonobj->getQry("select team,$type$selectQry,case_origin,LOWER(case_owner) as case_owner from aruba_open $Qry order by case_owner asc");
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[$weeknamearr['case_owner']][]=$weeknamearr[$type.$selectQry];
		$caseorigin[$weeknamearr['case_owner']][]=$weeknamearr['case_origin'];
	}
	
	$case_origin=$commonobj->getarracount($caseorigin);

	$closeQuryArr = $commonobj->getQry("SELECT ".$type.$selectQry.",ttc,rma,sdc,case_origin,team,LOWER(case_owner) as case_owner from aruba_closure  $Qry");
	foreach($closeQuryArr as $closeArrval){
		$closecount[$closeArrval['case_owner']][]=$closeArrval[$type.$selectQry];
		if($closeArrval['ttc']<='9'){
			$ttccount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
			$phonettc[$closeArrval['case_owner']][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
			$webttc[$closeArrval['case_owner']][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
			$sdcphone[$closeArrval['case_owner']][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
			$sdcweb[$closeArrval['case_owner']][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval['case_owner']][]=$closeArrval['rma'];
		$sdctot[$closeArrval['case_owner']][]=$closeArrval['sdc'];
	}
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);
	
	//echo "select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team,LOWER(case_owner) as case_owner from aruba_csat $Qry";

	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team,LOWER(case_owner) as case_owner from aruba_csat $Qry");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval['case_owner']][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval['case_owner']][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval['case_owner']][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['rma']=='Yes'){
			$rmacaseoe[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval['case_owner']][]=$csatunArrval['loyalty_index'];
		}
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//avaya
	$holyQuryArr = $commonobj->getQry("select ".$type.$selectQry.",working_days,$hcount from aruba_headcount where $type$selectQry='$selectrange'");
	foreach ($holyQuryArr as $holykey => $holyvalue) {
		$workingdays=$holyvalue['working_days'];
		$overallheadcount=$holyvalue['wlan']+$holyvalue['ns'];
	}
	

	$ssqaQuryArr = $commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner ,avg(overall) as ssqa_avg from aruba_ssqa $Qry group by case_owner order by case_owner asc");
	foreach($ssqaQuryArr as $ssqaArrval){
		$ssqa[$ssqaArrval['case_owner']]=$ssqaArrval['ssqa_avg'];
	}
	
	$paQuryArr = $commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,avg(overall) as pa_avg from aruba_process_audit  $Qry group by case_owner order by case_owner asc");
	foreach($paQuryArr as $paArrval){
		$pa[$paArrval['case_owner']]=$paArrval['pa_avg'];
	}
	
	$tier1QuryArr = $commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_esc $Qry group by case_owner order by case_owner asc");
	foreach ($tier1QuryArr as $t1key => $t1value) {
		$overallt1[$t1value['case_owner']]=$t1value['cnt'];
	}
	$controlQuryArr = $commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_esc $Qry AND tier_1 =  'Controllable'  group by case_owner order by case_owner asc");
	foreach ($controlQuryArr as $controlkey => $controlvalue) {  
		$overallcontrol[$controlvalue['case_owner']]=$controlvalue['cnt'];
	}
	$uncontrolQuryArr = $commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_esc $Qry AND tier_1 =  'Uncontrollable'group by case_owner order by case_owner asc");
	foreach ($uncontrolQuryArr as $uncontrolkey => $uncontrolvalue) {
		$overalluncontrol[$uncontrolvalue['case_owner']]=$uncontrolvalue['cnt'];
	}

	$overallblank = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  '' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');

	$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' AND time_to_sec(total_time) >=  '18000' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');

		
	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",LOWER(case_owner) as case_owner,count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' group by case_owner"),'case_owner','cnt');
		$engineerwise="default";

	$getRca = $commonobj->arrayColumn($commonobj->getQry("select LOWER(case_owner) as case_owner,count(*) as cnt from aruba_rca where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' group by case_owner"),'case_owner','cnt');

	$rca=  $commonobj->getQry("select LOWER(case_owner) as case_owner,oe_alert from aruba_rca where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' ");
	foreach ($rca as $rcakey => $rcavalue) {
		$oesplit[$rcavalue['case_owner']][]=$rcavalue['oe_alert'];
	}
	$rcasplit=$commonobj->getarracount($oesplit);
	//print_r($rcasplit);
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Tenure Age.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		width: 16.66%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.div { 
        width: 100%; 
        overflow-x:scroll;  
        *padding-left:5em; 
        overflow-y:visible;
        padding-bottom:1px;
    }
    
</style>
	
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="portlet box yellow-casablanca">
		    <table>
                <tr class="text-center" rowspan=2>
                    <tr>
                        <td colspan='34' align="center" style="border:1px solid;background-color:#F2784B;color:white;"><H3>
                            <i class="icon-calendar"></i>Summary -<?php echo $selectrange; ?>
                        </H3>
                        </td>
                    </tr>
                </tr>
            <table> 
		    <div class="portlet-body">
		       <div class="">
		        	<table id="example" class="stripe row-border table-striped order-column text-center" width="100%" style='white-space: nowrap;' border="1">
                        <thead>
                            <tr style="*background-color:#f3d3c8;">
                            	<td rowspan="2" class="headcol style" style="background-color:#F2784B;color:white;" '>Metrics</td>
                            	<td style="background-color:#F2784B;color:white;" colspan="3">Open Cases</td>
                            	<td style="background-color:#F2784B;color:white;" colspan='10'>Closed Cases</td>
                            	<td style="background-color:#F2784B;color:white;" colspan="9">CSAT</td>
                            	<td style="background-color:#F2784B;color:white;" colspan="4">Escalation</td>
								<td style="background-color:#F2784B;color:white;" colspan="2">Quality</td>
								<td style="background-color:#F2784B;color:white;" rowspan="2">Outage</td>
								<td style="background-color:#F2784B;color:white;" colspan="4">RCA</td>
                            </tr>
                            <tr style="*background-color:#f3d3c8;">
                            	<td style="background-color:#F2784B;color:white;" >Overall</td>
                            	<td style="background-color:#F2784B;color:white;" >Phone</td>
                            	<td style="background-color:#F2784B;color:white;" >Web</td>
                            	<td style="background-color:#F2784B;color:white;" >Closure</td>
                            	<td style="background-color:#F2784B;color:white;" >RMA %</td>
                            	<td style="background-color:#F2784B;color:white;" >RMA Cases</td>
                            	<td style="background-color:#F2784B;color:white;" >TTC &lt;9 Days</td>
                            	<td style="background-color:#F2784B;color:white;" >Phone - TTC &lt;9 Days</td>
                            	<td style="background-color:#F2784B;color:white;" >Web - TTC &lt;9 Days</td>
                            	<td style="background-color:#F2784B;color:white;" >SDC</td>
                            	<td style="background-color:#F2784B;color:white;" >SDC – Phone (45%)</td>
                            	<td style="background-color:#F2784B;color:white;" >SDC – Web (35%)</td>

                            	<td style="background-color:#F2784B;color:white;" >Productivity</td>
                            	
                            	<td style="background-color:#F2784B;color:white;" >Overall Experience</td>
                            	<td style="background-color:#F2784B;color:white;" >Green%</td>
                            	<td style="background-color:#F2784B;color:white;" >Normal %</td>
                            	<td style="background-color:#F2784B;color:white;" >Red%</td>
                            	<td style="background-color:#F2784B;color:white;" >Survey Count</td>
                            	<td style="background-color:#F2784B;color:white;" >Net Promoter Score</td>
                            	<td style="background-color:#F2784B;color:white;" >Loyalty Index</td>
                            	<td style="background-color:#F2784B;color:white;" >RMA cases OE</td>
                            	<td style="background-color:#F2784B;color:white;" >RMA cases NPS</td>
								 
								<td style="background-color:#F2784B;color:white;" >Escalation</td>
								<td style="background-color:#F2784B;color:white;" >ESC-Controllable</td>
								<td style="background-color:#F2784B;color:white;" >ESC-Uncontrollable</td>
								<td style="background-color:#F2784B;color:white;" >ESC-Blanks</td>

								<td style="background-color:#F2784B;color:white;" >SSQA</td>
								<td style="background-color:#F2784B;color:white;" >Process Audit</td>

								<td style="background-color:#F2784B;color:white;" >Count</td>
								<td style="background-color:#F2784B;color:white;" >Top-2</td>
								<td style="background-color:#F2784B;color:white;" >MID</td>
								<td style="background-color:#F2784B;color:white;" >DSAT</td>

								
								
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	if(!empty($tlnameArrat)){
	                        	foreach ($tlnameArrat as $enggkey => $enggname) {
	                        	?>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;" ><?php echo ucwords($enggname);?></td>
		                        	<td><?php echo empty($totopencase[$enggname])?0:count($totopencase[$enggname]);?></td>
		                        	<td><?php echo empty($case_origin[$enggname]['Phone'])?"0":$case_origin[$enggname]['Phone']?></td>
		                        	<td><?php echo empty($case_origin[$enggname]['Web'])?"0":$case_origin[$enggname]['Web']?></td>
		                        	<!-- close case -->
		                        	<td><?php echo empty($closecount[$enggname])?'0':count($closecount[$enggname]);?></td>
		                        	<td><?php echo empty($rmacount[$enggname])?"0.00%":round($rmacount[$enggname]['Yes']/array_sum($rmacount[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($rmacount[$enggname]['Yes'])?"0":$rmacount[$enggname]['Yes']; ?></td>
		                        	<td><?php echo empty($ttccount[$enggname])?0:count($ttccount[$enggname]); ?></td>
		                        	<td><?php echo empty($phonettc[$enggname])?"0":count($phonettc[$enggname]); ?></td>
		                        	<td><?php echo empty($webttc[$enggname])?"0":count($webttc[$enggname]); ?></td>
		                        	<td><?php echo empty($sdctotper[$enggname])?"0.00%":round($sdctotper[$enggname]['1']/array_sum($sdctotper[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($sdcphone[$enggname])?"0.00%":round(count($sdcphone[$enggname])/count($closecount[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($sdcweb[$enggname])?"0.00%":round(count($sdcweb[$enggname])/count($closecount[$enggname])*100).'%'; ?></td>
		                        	<!-- producty -->
		                        	
		                        	<td><?php echo empty($closecount[$enggname])?"0":number_format(count($closecount[$enggname])/$workingdays,2); ?></td>
		                        	
		                        	<!-- end producty -->
		                        	<!-- csat unnormalized -->
		                        	<td><?php echo empty($overallexp[$enggname])?"NA":number_format(array_sum($overallexp[$enggname])/count($overallexp[$enggname]),2); ?></td>
		                        	<td><?php echo empty($alert_type[$enggname]['Green'])?"NA":round($alert_type[$enggname]['Green']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($alert_type[$enggname]['Normal'])?"NA":round($alert_type[$enggname]['Normal']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($alert_type[$enggname]['Red'])?"NA":round($alert_type[$enggname]['Red']/array_sum($alert_type[$enggname])*100).'%'; ?></td>
		                        	<td><?php echo empty($alert_type[$enggname])?"0":array_sum($alert_type[$enggname]); ?></td>
		                        	<?php
		                        		$greenper[$enggname]=empty($netprompter[$enggname]['Green'])?"0.00%":round($netprompter[$enggname]['Green']/array_sum($netprompter[$enggname])*100).'%'; 
                        				$redperc[$enggname]=empty($netprompter[$enggname]['Red'])?"0.00%":round($netprompter[$enggname]['Red']/array_sum($netprompter[$enggname])*100).'%';
		                        	?>
		                        	<td><?php echo empty($greenper[$enggname])?"0.00%":$greenper[$enggname]-$redperc[$enggname].'%'; ?></td>
		                        	<td><?php echo empty($loyaltyindex[$enggname])?"NA":number_format(array_sum($loyaltyindex[$enggname])/count($loyaltyindex[$enggname]),2); ?></td>
		                        	<td><?php echo empty($rmacaseoe[$enggname])?'NA':number_format(array_sum($rmacaseoe[$enggname])/count($rmacaseoe[$enggname]),2); ?></td>
		                        	<td><?php echo empty($loyalty_index[$enggname])?'NA':number_format(array_sum($loyalty_index[$enggname])/count($loyalty_index[$enggname]),2); ?></td>
		                        	<!-- end csat unnormalized -->
									<!-- csat normalized 5% -->
									
									<td><?php echo empty($overallt1[$enggname])?'NA':number_format($overallt1[$enggname]); ?></td>
									<td>
										<?php 
										if($overalluncontrol[$enggname] != "" && $overallcontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overallcontrol[$enggname])?'NA':number_format($overallcontrol[$enggname]/$overallt1[$enggname]*100).'%'; 
										}
										?>
									</td>
									<td>
										<?php 
										if($overallcontrol[$enggname] != "" && $overalluncontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overalluncontrol[$enggname])?'NA':number_format($overalluncontrol[$enggname]/$overallt1[$enggname]*100).'%'; 
										}
										?>
									</td>
									<td>
										<?php 
											echo empty($overallblank[$enggname])?'NA':number_format($overallblank[$enggname]/$overallt1[$enggname]*100).'%'; 
										?>
									</td>
									<td><?php echo empty($ssqa[$enggname])?'NA':number_format($ssqa[$enggname]).'%'; ?></td>
									<td><?php echo empty($pa[$enggname])?'NA':number_format($pa[$enggname]).'%'; ?></td>
									<td>
										<?php
											if($engineerwise == "default")
											{
												$outage = round((1-($overallout[$enggname]/($overalloutcnt[$enggname])))*100);
											}else{
												$outage = round((1-($overallout[$enggname]/($overalloutcnt[$enggname]*$workingdays)))*100);
											}
											if($outage < 0)
											{
												$outage_new = 0;
											}else{
												$outage_new = $outage;
											}
											echo empty($overallout[$enggname])?'NA':$outage_new.'%';
										?></td>
										<?php $val = empty($getRca[$enggname]) ? 'NA':'0%'; ?>
										<td><?php echo empty($getRca[$enggname])?'NA':$getRca[$enggname]; ?></td>
										<td><?php echo empty($rcasplit[$enggname]['MID'])?$val:number_format($rcasplit[$enggname]['MID']/$getRca[$enggname]*100).'%'; ?></td>
										<td><?php echo empty($rcasplit[$enggname]['Top-2'])?$val:number_format($rcasplit[$enggname]['Top-2']/$getRca[$enggname]*100).'%'; ?></td>
										<td><?php echo empty($rcasplit[$enggname]['DSAT'])?$val:number_format($rcasplit[$enggname]['DSAT']/$getRca[$enggname]*100).'%'; ?></td>
	                        	<?php
	                        	}
	                        }else{
	                        	?>
	                        	<tr><td colspan="20" style='text-align: center;color:red'>No Data Found</td></tr>
	                        	
	                        	<?php
	                        }
                        	?>
                        	</tr>
                        </tbody>
                    </table>
		        </div>
		    </div>
		</div>
</form>
