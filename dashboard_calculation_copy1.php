<?php
	if($_POST['reporttype'] || $_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange'] || $_POST['team']){
	$reporttype 	= 	$_POST['reporttype'];
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$team 			=	$_POST['team'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:"Overall";
	$productwise 	=	!empty($productwise)?$productwise:"Overall";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
	$reporttype 	=	!empty($reporttype)?$reporttype:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}

if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition.=" and wlan_ns='".$projectwise."'";
		//$hcount=strtolower($projectwise);
	}else{
		//$hcount="wlan,ns";
	}
	
	if($projectwise != "Overall" && $productwise != "WC" && $productgroup == "Overall")
	{
		$hcount = $projectwise;
	}elseif($productwise == "WC" && $productgroup == "Overall" && $projectwise == "Overall")
	{
		$hcount = $productwise;
	}elseif($productwise == "Overall" && $projectwise == "Overall" && $productgroup == "Overall"){
		$hcount_type = "default";
		$hcount="wlan,ns,wc";
	}elseif($projectwise == "Overall" && $productgroup == "Overall" && ($productwise == "GSC" || $productwise == "GEC")){
		$hcount_type = "default";
		$hcount="wlan,ns";
	}elseif($productgroup != "Overall"){
		$hcount = $productgroup;
	}
	
	if($productwise!='Overall' && $productwise!=''){
		$QryCondition.=" and que_new='".$productwise."'";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}
	if($reporttype!='Overall' && $reporttype!=''){
		$QryCondition.=" and region='".$reporttype."'";
	}
	$teamQry = $QryCondition;
	// print_r($team);
	if(count($team) > 0){
		$QryCondition.= ' and team in  ("'.implode('","', $team).'")';
	}

	$headertotalArr = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct ".$type.$selectQry." from aruba_open order by id asc"),'',$type.$selectQry);
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	if($selecttype=='Quarterly'){	
		$tablehead=array_reverse(array_slice($arrayval, 0, 4, true));
	}else{
		$tablehead=array_reverse(array_slice($arrayval, 0, 12, true));
	}

	$opencasecount = $commonobj->arrayColumn($commonobj->getQry('SELECT count(*) as count,'.$type.$selectQry." from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry),$type.$selectQry,'count');

	$caseOrigin = $commonobj->getQry('SELECT count(*) as count,'.$type.$selectQry.",case_origin from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.',case_origin');
	foreach ($caseOrigin as $key => $casevalue) {
		$case_orgin[$casevalue[$type.$selectQry]][$casevalue['case_origin']]=$casevalue['count'];
	}

	$closecasecount = $commonobj->arrayColumn($commonobj->getQry("SELECT count(*) as count,".$type.$selectQry." from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry),$type.$selectQry,'count');


	$closeQuryArr = $commonobj->getQry("SELECT ".$type.$selectQry.",ttc,rma,sdc,case_origin from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition ");
	foreach($closeQuryArr as $closeArrval){
		if($closeArrval['ttc']<='9'){
			$ttccount[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['ttc']<='7'){
			$ttc7days[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<='7'){
			$phonettc7[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<='7'){
			$webttc7[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}

		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<='9'){
			$phonettc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<='9'){
			$webttc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
			$sdcphone[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
			$sdcweb[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
		$sdctot[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];

		if($closeArrval['case_origin'] == 'Phone'){
			$phonecountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin'] == 'Web'){
			$webcountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
	}
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);

	$backlogCnt = $commonobj->arrayColumn($commonobj->getQry('SELECT calendar_date,sum(count) as count,'.$type.$selectQry." from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.',calendar_date order by id asc'),$type.$selectQry,'count');

    $backlogbcktArr = $commonobj->getQry('SELECT calendar_date,sum(`0_to_5`) as bcket0to5,sum(`6_to_10`) as bcket6to10,sum(`11_to_20`) as bcket11to20,sum(`morethen20`) as morethan20,'.$type.$selectQry." from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.',calendar_date order by id asc');
    foreach ($backlogbcktArr as $key => $bcktvalue) {
        $bcket0to5[$bcktvalue[$type.$selectQry]]=$bcktvalue['bcket0to5'];
        $bcket6to10[$bcktvalue[$type.$selectQry]]=$bcktvalue['bcket6to10'];
        $bcket11to20[$bcktvalue[$type.$selectQry]]=$bcktvalue['bcket11to20'];
        $morethan20[$bcktvalue[$type.$selectQry]]=$bcktvalue['morethan20'];
    }
	//csat
    //echo "SELECT ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc";
	$csatunQuryArr = $commonobj->getQry("SELECT ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['rma']=='Yes'){
			$rmacaseoe[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		}
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//head count and working days

	$workingdays = $commonobj->arrayColumn($commonobj->getQry("SELECT working_days,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'working_days');
	
	$overallssqa = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",avg(overall) as overall_avg from aruba_ssqa where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	//print_r($overallssqa);
	
	$overallpa = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",avg(overall) as overall_avg from aruba_process_audit where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	//print_r($overallpa);

	$overallesc = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	$overallt1  = $overallesc;

	$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND mgr_tier_1 =  'Controllable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overalluncontrol =$commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND mgr_tier_1 =  'Uncontrollable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallblank = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND mgr_tier_1 =  '' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	
	$overallout = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND time_to_sec(total_time) >=  '28800' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(distinct case_owner) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	
	$csatCnt = $commonobj->arrayColumn($commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as cnt from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition and (alert_type in ('Normal','Red') or nps in ('Passive','Detractor'))  group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	//echo "SELECT ".$type.$selectQry.",count(*) AS count,mgr_tier1 from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition and alert_type !='green' and nps !='promoter' group by ".$type.$selectQry.",mgr_tier1";

	$rcaSplit=  $commonobj->getQry("SELECT ".$type.$selectQry.",count(*) AS count,mgr_tier1 from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition and (alert_type in ('Normal','Red') or nps in ('Passive','Detractor')) group by ".$type.$selectQry.",mgr_tier1");
	foreach ($rcaSplit as $key => $value) {
		$rcasplitArr[$value[$type.$selectQry]][$value['mgr_tier1'] ==''?'Blanks':$value['mgr_tier1']] = $value['count'];
	}
	
		$Qry=$type.$selectQry."_".$selectrange;

}
?>