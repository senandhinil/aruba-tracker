<?php
include 'includes/config.php';
include 'includes/session_check.php';
ini_set('max_execution_time', 120);
error_reporting(E_ALL);
if($_POST['reporttype'] || $_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange']){
	$reporttype 	= 	$_POST['reporttype'];
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:'Overall';
	$productwise 	=	!empty($productwise)?$productwise:'Overall';
	$productgroup 	=	!empty($productgroup)?$productgroup:'Overall';
	$reporttype 	=	!empty($reporttype)?$reporttype:'Overall';
	$calendartype 	=	!empty($calendartype)?$calendartype:'Normal';
	$selecttype  	=	!empty($selecttype)?$selecttype:'Weekly';
}

if($projectwise!=''){
	if($calendartype  === 'Normal'){
		$type='calendar_';
	}else{
		$type='fiscal_';
	}
	if($selecttype ==='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype ==='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype ==='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition.=" and wlan_ns='".$projectwise."'";
		//$hcount=strtolower($projectwise);
	}else{
		//$hcount="wlan,ns";
	}
	
	if($projectwise != 'Overall' && $productwise != 'WC' && $productgroup  === 'Overall')
	{
		$hcount = $projectwise;
	}elseif($productwise  === 'WC' && $productgroup  === 'Overall' && $projectwise  === 'Overall')
	{
		$hcount = $productwise;
	}elseif($productwise  === 'Overall' && $projectwise  === 'Overall' && $productgroup  === 'Overall'){
		$hcount_type = 'default';
		$hcount='wlan,ns,wc';
	}elseif($projectwise  === 'Overall' && $productgroup  === 'Overall' && ($productwise  === 'GSC' || $productwise  === 'GEC')){
		$hcount_type = 'default';
		$hcount='wlan,ns';
	}elseif($productgroup != 'Overall'){
		$hcount = $productgroup;
	}
	
	if($productwise!='Overall' && $productwise!=''){
		$QryCondition.=" and que_new='".$productwise."'";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}
	if($reporttype!='Overall' && $reporttype!=''){
		$QryCondition.=" and region='".$reporttype."'";
	}
	
	
	$headQryArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue === "$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	if($selecttype ==='Quarterly'){	
		$tablehead=array_reverse(array_slice($arrayval, 0, 4, true));
	}else{
		$tablehead=array_reverse(array_slice($arrayval, 0, 12, true));
	}
	
	$opencasecount = $commonobj->arrayColumn($commonobj->getQry('select count(*) as count,'.$type.$selectQry." from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry),$type.$selectQry,'count');

	$caseOrigin = $commonobj->getQry('select count(*) as count,'.$type.$selectQry.",case_origin from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.',case_origin');
	foreach ($caseOrigin as $key => $casevalue) {
		$case_orgin[$casevalue[$type.$selectQry]][$casevalue['case_origin']]=$casevalue['count'];
	}
	
	print_r($case_orgin);

	$closecasecount = $commonobj->arrayColumn($commonobj->getQry("select count(*) as count,".$type.$selectQry." from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry),$type.$selectQry,'count');
	
	$closeQuryArr = $commonobj->getQry("select ".$type.$selectQry.",ttc,rma,sdc,case_origin from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition ");
	foreach($closeQuryArr as $closeArrval){
		if($closeArrval['ttc']<'9'){
			$ttccount[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin'] ==='Phone' && $closeArrval['ttc']<'9'){
			$phonettc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin'] ==='Web' && $closeArrval['ttc'] < '9'){
			$webttc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc'] ==='1' && $closeArrval['case_origin'] ==='Phone'){
			$sdcphone[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc'] ==='1' && $closeArrval['case_origin'] ==='Web'){
			$sdcweb[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
		$sdctot[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];

		if($closeArrval['case_origin']  === 'Phone'){
			$phonecountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}

		if($closeArrval['case_origin']  === 'Web'){
			$webcountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
	}
	
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);
	//csat
	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['rma'] ==='Yes'){
			$rmacaseoe[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		}
	}
	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//head count and working days
	
	$workingdays = $commonobj->arrayColumn($commonobj->getQry("select working_days,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'working_days');
	//echo "select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_ssqa where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry."";
	$overallssqa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_ssqa where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	//print_r($overallssqa);
	//echo "select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_process_audit where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry."";
	$overallpa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_process_audit where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	//print_r($overallpa);
	
	// echo "select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry."";
	$overallesc = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallt1 = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  'Controllable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overalluncontrol =$commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  'Uncontrollable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallblank = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  '' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	//echo "select ".$type.$selectQry.",count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND time_to_sec(total_time) >=  '18000' group by ".$type.$selectQry."";
	$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND time_to_sec(total_time) >=  '28800' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	//print_r($overallout);
	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(distinct case_owner) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND time_to_sec(total_time) >=  '28800' $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$getRca = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_rca where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition  group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$oesplit=  $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",oe_alert from aruba_rca where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition "),$type.$selectQry,'oe_alert');
	
	$rcasplit=$commonobj->getarracount($oesplit);
	$Qry=$type.$selectQry."_".$selectrange;
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	/*.form-control {
	    border: 0px solid #c2cad8 !important;
	}*/
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
</style>
<form method='POST' id='frmsrch'>
<input type='hidden' name='_token' value="<?php echo $token; ?>">
	<div class='row top-align' >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class='portlet'>
		        <div class='portlet-body'>
		            <div class='form-group col-md-3 col-sm-3 col-xs-6'>
		                <select class='form-control submit' id='drop2'  name='projectwise'>
	                    <?php
	                    	$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
	                    	echo '<option value="Overall">Overall Project</option>';
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script>  
		                     jQuery('#drop2').val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class='form-group col-md-3 col-sm-3 col-xs-6'>
		                <select class='form-control submit' id='drop3'  name='productwise'>
		                <?php
		                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
		                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
							echo '<option value="Overall">Overall Que</option>';
							foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
					    <select class="form-control submit" id="drop4"  name="productgroup">
		                	<?php
		                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                		$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';

		                		echo "SELECT distinct product_group from aruba_open Where $Qery";
		                		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery");
								echo '<option value="Overall">Overall Product</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop1"  name="reporttype">
		                	<?php
		                	echo '<option value="Overall">Overall Region</option>';
		                	foreach($productgroupoverall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop1").val("<?php echo $reporttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype">
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop7"  name="selectrange">
		                	<option value="">--- Select ---</option>
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									$selected = $value[$type.$selectQry] ===$selectrange?"selected":"";
									echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
							jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
	<?php 

	//$productName = $productName, array('GEC'));
	$headQry = $productgroup  === 'Overall' ? " sum(`".implode("`)+sum(`",array_diff($commonobj->arrayColumn($productName,'','product_group'), array('GEC')))."`)" : " sum(`$productgroup`)";
	
	//echo "SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry;
	
	$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
	?>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        		<img src="img/exp.png" style="height: 25px;cursor: pointer;"  id='rawexport' title='Raw Download'>
			        		<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        		<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			        	
			        </div>
		    </div>
		    <div class="portlet-body">
			    <div class="table-scrollable">
			        <table class="table table-striped table-bordered table-hover text-center" id='tableId' data-height="300">
	                      <thead  class="header">
	                            <tr>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;">Target</th>
	                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
	                        		<?php	}	?>
	                            </tr>
	                        </thead>
	                       <tfoot >
	                            <tr>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
	                            	<th class='text-center' style="background-color:#F2784B;color:white;">Target</th>
	                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                            	<th class="text-center" style="background-color:#F2784B;color:white" ><?php echo $rs; ?></th>
	                        		<?php	}	?>
	                            </tr>
	                        </tfoot>
	                        <tbody class="scrollContent" style=" height: 120px;*display: block;">
	                        	<tr >
	                        		<td rowspan="3"style="vertical-align: inherit;">Volume</td>
		                        	<td>Overall</td>
		                        	<td>-</td>
		                        	<?php echo $commonobj->volOverall($tablehead,$opencasecount);
									?>
	                        	</tr>
	                        	<tr>
		                        	<td>Phone</td>
		                        	<td>-</td>
		                        	<?php  echo $commonobj->volphnweb($tablehead,$case_orgin,"Phone"); ?>
	                        	</tr>
	                        	<tr>
		                        	<td>Web</td>
		                        	<td>-</td>
		                        	<?php 
									echo $commonobj->volphnweb($tablehead,$case_orgin,"Web"); ?>
	                        	</tr>
	                        	
	                        	<tr>
	                        	<?php $cnt=$productwise !='GEC' ? '11':'9' ?>
		                        	<td rowspan=<?=$cnt?> style="vertical-align: inherit;">Closed Cases</td>
		                        	<td>Closure</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td class='text-center'><?php echo empty($closecasecount[$rs])?0:$closecasecount[$rs]; ?></td>
		                        	<?php	} ?>
	                        	</tr>
	                        	<tr>
	                        		<td>RMA %</td>
	                        		<td>-</td>
	                        		<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($rmacount[$rs])?'0.00%':number_format($rmacount[$rs]['Yes']/array_sum($rmacount[$rs])*100,2).'%' ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA Cases</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($rmacount[$rs]['Yes'])?'0':$rmacount[$rs]['Yes']; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<!-- productivity -->
	                        	<?php if($productwise !='GEC' ) {?>
	                        	<tr>
		                        	<td>Productivity</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($closecasecount[$rs])?'0':number_format($closecasecount[$rs]/$overallheadcount[$rs]/$workingdays[$rs],2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Head Count</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overallheadcount[$rs])?'NA':$overallheadcount[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<?php } ?>
	                        	<!-- end productivity -->
	                        	<tr>
		                        	<td>TTC &lt;9 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($ttccount[$rs])?'0':number_format(count($ttccount[$rs])/count($rma[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Phone - TTC &lt;9 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($phonettc[$rs])?'0':number_format(count($phonettc[$rs])/count($phonecountArr[$rs],2)*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Web - TTC &lt;9 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($webttc[$rs])?'0':number_format(count($webttc[$rs])/count($webcountArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($sdctotper[$rs])?'0.00%':number_format($sdctotper[$rs]['1']/array_sum($sdctotper[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC – Phone (45%)</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($sdcphone[$rs])?'0.00%':number_format(count($sdcphone[$rs])/$closecasecount[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td >SDC – Web (35%)</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($sdcweb[$rs])?'0.00%':number_format(count($sdcweb[$rs])/$closecasecount[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
	                        		<td rowspan='10' style='vertical-align: inherit;'>CSAT</td>
		                        	<td>Overall Experience</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td class='text-center'><?php echo empty($overallexp[$rs])?'NA':number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2); ?></td>
		                        	<?php	} ?>
	                        	</tr>
	                        	<tr>
		                        	<td>Green%</td>
		                        	<td>>=80%</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
		                        		<td class='text-center'><?php echo empty($alert_type[$rs]['Green'])?'NA':number_format($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Normal %</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($alert_type[$rs]['Normal'])?'NA':number_format($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Red%</td>
		                        	<td><=3%</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($alert_type[$rs]['Red'])?'NA':number_format($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>	
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Survey Count</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($alert_type[$rs])?'0':array_sum($alert_type[$rs]); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<?php 
	                        	foreach ($tablehead as $inflowkey=> $rs) { 
	                        		$greenper[$rs]=empty($netprompter[$rs]['Promoter'])?'0.00%':number_format($netprompter[$rs]['Promoter']/array_sum($netprompter[$rs])*100,2).'%'; 
	                        		$redperc[$rs]=empty($netprompter[$rs]['Detractor'])?'0.00%':number_format($netprompter[$rs]['Detractor']/array_sum($netprompter[$rs])*100,2).'%';
	                        	}
	                        	?>
	                        	<tr>
		                        	<td>Net Promoter Score</td>
		                        	<td>>=80</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
		                        		<td class='text-center'><?php echo empty($greenper[$rs])?'0':$greenper[$rs]-$redperc[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Loyalty Index</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($loyaltyindex[$rs])?'0':number_format(array_sum($loyaltyindex[$rs])/count($loyaltyindex[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA cases OE</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class=' text-center'><?php echo empty($rmacaseoe[$rs])?'NA':number_format(array_sum($rmacaseoe[$rs])/count($rmacaseoe[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>RMA cases NPS</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class=' text-center'><?php echo empty($loyalty_index[$rs])?'NA':number_format(array_sum($loyalty_index[$rs])/count($loyalty_index[$rs]),2); ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>No Of Working days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($workingdays[$rs])?'NA':$workingdays[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
	                        		<td rowspan='2' style='vertical-align: inherit;'>Quality</td>
		                        	<td>SSQA</td>
		                        	<td>>=80%</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td class='text-center'><?php echo empty($overallssqa[$rs])?'NA':round($overallssqa[$rs],2).'%'; ?></td>
		                        	<?php	} ?>
	                        	</tr>
								<tr>
		                        	<td>Process Audit</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overallpa[$rs])?'NA':round($overallpa[$rs],2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
								<td rowspan='4' style='vertical-align: inherit;'>Escalation</td>
		                        	<td>Escalation</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overallesc[$rs])?'NA':$overallesc[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td>ESC-Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overallcontrol[$rs])?'NA':round($overallcontrol[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td>ESC-Uncontrollable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overalluncontrol[$rs])?'NA':round($overalluncontrol[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td>ESC-Blank</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($overallblank[$rs])?'NA':round($overallblank[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
								<td colspan='2' style='vertical-align: inherit;'>Outage</td>
								<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'>
											<?php 
												//echo '(1-('.$overallout[$rs].'/('.$overalloutcnt[$rs].'*'.$workingdays[$rs].')))*100';
												$outage_cal = round((1-($overallout[$rs]/($overalloutcnt[$rs]*$workingdays[$rs])))*100,2);
												$final_outage_cal = ($outage_cal<0)?0:$outage_cal;
												echo empty($overallout[$rs])?'NA':$final_outage_cal.'%'; 
										?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
	                        		<td rowspan='4'>RCA</td>
		                        	<td>Count</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php echo empty($getRca[$rs])?'NA':$getRca[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>MID</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
		                        		$val =  empty($rcasplit[$rs]) ?'NA':'0%';
		                        		?>
		                        		<td class='text-center'><?php echo empty($rcasplit[$rs]['MID'])?$val:number_format($rcasplit[$rs]['MID']/$getRca[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>Top - 2</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
		                        		$val =  empty($rcasplit[$rs]) ?'NA':'0%';
		                        		?>
		                        		<td class='text-center'><?php echo empty($rcasplit[$rs]['Top-2'])?$val:number_format($rcasplit[$rs]['Top-2']/$getRca[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td>DSAT</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td class='text-center'><?php
		                        		$val =  empty($rcasplit[$rs]) ?'NA':'0%';
		                        		 echo empty($rcasplit[$rs]['DSAT'])?$val:number_format($rcasplit[$rs]['DSAT']/$getRca[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	
	                        </tbody>
	                    </table>
			    </div>
		    </div>
		</div>
</form>
<?php 
$time_start = microtime(true); 
echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);
include('includes/footer.php');
?>
<script type='text/javascript'>
	$('.selectweek').change(function(){
	   var selectdate= $('#drop6').val();
	   var calendertype= $('#drop5').val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $('#drop7').html('');
	        $('#drop7').html(obj);
	      }
	    }); 
	});
	// $("#drop1").change(function(){
	//     $("#drop2").val("Overall");
	//     $("#drop3").val("Overall");
	//     $("#drop4").val("Overall");
	// });
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('.submit').change(function(){
		document.getElementById("frmsrch").action = 'dashboard.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'export-summary.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});

	$('#rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
</script>
