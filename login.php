<?php 
  try {
      $server='dataengineering.csscorp.com';
      $user='ssqa';
      $pass='$!nttu@123#';
      $conn= new PDO ("mysql:host=$server;dbname=login",$user,$pass);
  }catch(PDOException $e){
     echo "Connection failed: " . $e->getMessage();
  }

if($_SERVER["REQUEST_METHOD"] == "POST") {

  $username = $_POST['username'];
  $password = $_POST['password']; 
  try {
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $qry=$conn->prepare("SELECT * FROM login WHERE email = ? and password = ? ");
    $qry->execute(array($username,$password));
    $selectQry = $qry->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($selectQry[0])) {

      $_SESSION['username'] = $selectQry[0]['email'];

      if(count($selectQry)>0){
        $id = base64_encode($_SESSION['username']);
        $token = base64_encode('ClientLogin');
        header("Location:../client_index.php?id=$id&token=$token");
		  exit;
      }
    }else {
      $error = "Your Credentials are invalid";
    }

  }
  catch(PDOException $e) 
  {
    echo "Error: " . $e->getMessage();
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
  <title>Aruba Login</title>
  <link rel="stylesheet" type="text/css" href="../assets/bootstrap/bootstrap/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/styles.css" />
    </head>
    <style type="text/css">
      body {
        background: url("../assets/illustration.png");
      }
    </style>
    <body>

      <section class="container">
        <section class="login-form">
          <form method="post" action="" role="login">

            <img src="img/css_logo_small.png" class="img-responsive" alt="Login" />

            <?php if(isset($error)) { ?>

            <div class="alert alert-danger">
              <strong>Error!</strong> <?php echo $error;?> </div>

              <?php   } ?>



              <input type="text" placeholder="E-mail or Employee-Id" name="username" required class="form-control input-lg" />
              <input type="password" name="password" placeholder="Password" name="password" type="password" required class="form-control input-lg" />
              <button type="submit" name="Login" class="btn btn-lg btn-primary btn-block">Login</button>

            </form>

          </section>
        </section>

        <script src="../assets/jquery.min.js"></script>
        <script src="../assets/bootstrap.min.js"></script>
      </body>
      </html>