<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['metrics'] || $_POST['metrics_second'] || $_POST['selectrange'] || $_POST['region']){
	$metrics 		= 	$_POST['metrics'];
	$metrics_second = 	$_POST['metrics_second'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$region 		=   $_POST['region'];
}else{
	$curntweek = $commonobj->arrayColumn($commonobj->getQry("select distinct calendar_week from aruba_csat order by id desc limit 0,1 "),'','calendar_week');
	$selectrange    =  !empty($selectrange)?$selectrange:$curntweek[0];
	$metrics 		=  !empty($metrics)?$metrics:"alert_type";
	$metrics_second =  !empty($metrics_second)?$metrics_second:"Overall";
	$calendartype 	=  !empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=  !empty($selecttype)?$selecttype:"Weekly";
	$region    		=  !empty($region)?$region:"EMEA";

} 
//if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	$QryCondition.= $region !='Overall'? " and region='$region'":'';
	
	$headertotalArr = $commonobj->arrayColumn($commonobj->getQry("select distinct ".$type.$selectQry." from aruba_csat order by id asc"),'',$type.$selectQry);
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
		   if($mastervalue==$selectrange){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	
	//Metrics and Sub metrics options
	$opt = '';
	if($metrics == "question"){
		$msArr = array("ease_of_access_score","technical_ability_score","non_technical_performance_score","kept_informed_score","solution_time_score");
	}else{
		$msArr = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct ".$metrics." from aruba_csat where ".$metrics." != '' AND $type$selectQry in ('".implode("','", $tablehead)."')"),'',$metrics);
		array_unshift($msArr,"Overall");
	}
	if(!in_array($metrics_second,$msArr)){
		$metrics_second = current($msArr);
	}
	
	foreach ($msArr as $key => $value) {
		$opt .= '<option value="'.$value.'">'.$value.'</option>';
	}
		
		if($metrics != 'question'){
			$QryCondition .= $metrics_second == 'Overall' ? '' :"AND $metrics = '$metrics_second'";
			if($metrics != 'product_easy_of_use'){
				$metricss = 'alert_type';
			}else{
				$metricss = $metrics;
			}
		}else if($metrics_second != "Overall" && $metrics == 'question'){
			//$QryCondition .= "AND $metrics = '$metrics_second'";
			$metricss = $metrics_second;
		}
		
		$overallCount = $commonobj->getQry("select account,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."') AND account != ''  $QryCondition group by $type$selectQry,account order by account");

		//echo "select account,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition group by $type$selectQry,account,$metricss order by account";

		$csatSummray = $commonobj->getQry("select account,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."') AND account != '' $QryCondition group by $type$selectQry,account,$metricss order by account");

		//$backlog_summ_manager = $commonobj->getQry("select account,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition group by $type$selectQry,account,$metricss order by account");

		foreach($csatSummray as $csatSurveyArr){
			if($metrics != 'alert_type' && trim($metrics) != 'product_easy_of_use'){
				foreach($overallCount as $overallval){
					$AccountwiseArr[$overallval['account']]['Survey Count'][$overallval[$type.$selectQry]]= $overallval['cnt'];
				}
			}else{
				if($metrics_second == 'Overall'){
					foreach($overallCount as $overallval){
						$AccountwiseArr[$overallval['account']]['Survey Count'][$overallval[$type.$selectQry]]= $overallval['cnt'];
					}
				}
			}
			
			$AccountwiseArr[$csatSurveyArr['account']][$csatSurveyArr[$metricss]][$csatSurveyArr[$type.$selectQry]]= $csatSurveyArr['cnt'];
		}
		foreach($overallCount as $overallval){
			$overallCountArr[$overallval['account']][$overallval[$type.$selectQry]]= $overallval['cnt'];
		}
		//print_r($overallCountArr);
//}
$sortorder = array('Survey Count','Green', 'Normal', 'Red','NA');
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-sm-2 {
	   		width: 16.667%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
    	margin-right: 0px;
	}
	tr, th, td{
		text-align:center;
	}
	.word-wrap {
	    width: 30em; 
	    word-wrap: break-word;
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		        <div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="region"  name="region" onchange="reload()">
		                	<option value="Overall">Overall Region</option>;
		                	<?php
		                		$regionName = $commonobj->getQry("SELECT distinct region from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by region asc");
								foreach ($regionName as $key => $value) {
									echo "<option value='$value[region]' >".$value['region']."</option>";
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#region").val("<?php echo $region ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="metric"  name="metrics" onchange="reload()">
							<option value="">--Select--</option>
		                	<option value="alert_type">Rating %</option>
							<option value="product_easy_of_use">Product EDC</option>
							<option value="customer_country">Country</option>
							<option value="question">Question</option>
							<option value="case_origin">Case Origin</option>
		                </select>
		                <script> 
		                     jQuery("#metric").val("<?php echo $metrics ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop10"  name="metrics_second" onchange="reload()">
							<option value="">--Select--</option>
		                	<?php
								echo $opt;
							?>
		                </select>
		                <script> 
		                     jQuery("#drop10").val("<?php echo $metrics_second ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype" >
							<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="drop7"  name="selectrange"  onchange="reload()" >
		                	<option value="">--- Select ---</option>';
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_csat order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
						<script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
						
					 </div>
		        </div>
		    </div>
		</div>    
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-list"></i>Account Wise Csat Survey</div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Account Name</th>
					<th scope="col">Metrics</th>
					<?php foreach($tablehead as $tblkey => $tblvalue){ ?>
						<th scope="col" ><?php echo $tblvalue; ?></th>
					<?php } ?>
				  </tr>
		        </thead>
		        <tbody>
		          	<?php 
		          	//$order = array('Survey Count','Green','Normal','Red','NA');
		          	if(count($AccountwiseArr)>0){
						foreach($AccountwiseArr as $mngkey => $mngArr){
							echo '<tr><td style="word-break:break-all;" rowspan='.count($mngArr).'>'.$mngkey.'</td>';
							foreach($sortorder as $value){
								foreach($mngArr as $reskey => $resArr){
									if($reskey == $value){
										echo '<td>'.$reskey.'</td>';
										foreach($tablehead as $tblkey => $tblvalue){
											if($overallCountArr[$mngkey][$tblvalue]){
												if($reskey != 'Survey Count'){
													$clr='';
														$val = empty($resArr[$tblvalue])?'0%':number_format($resArr[$tblvalue]/$overallCountArr[$mngkey][$tblvalue]*100,2).'%';		
													}else{
													$val = empty($resArr[$tblvalue])?0:$resArr[$tblvalue];
												}
											}else{
												$val = '-';
											}
											echo "<td style='$clr'>".$val.'</td>';
										}
										echo '</tr>';
									}
									
								} 
							}
						} 
					}else{ ?>
						<tr><td colspan="<?=count($tablehead)+2?>">No Data Found</td></tr>
					<?php }?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
</form>
<?php 
include("includes/footer.php");
?>

<script>
$("#drop9").change(function(){
	jQuery("#tlname").val("Overall");
	//jQuery("#enggname").val("Overall");
});
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	function reload(){
		document.getElementById("frmsrch").action = 'csat_account.php';
		document.getElementById("frmsrch").submit();
		return false;
	}
</script>