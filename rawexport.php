<?php
error_reporting(E_ALL);
set_time_limit(0);
include "includes/config.php";
ini_set("display_errors", "ON");
require_once '../Classes/PHPExcel.php';
require_once '../Classes/PHPExcel/IOFactory.php';
   $condi=explode("_",urldecode($_GET['query']));    
    $objPHPExcel = new PHPExcel();
    $tablename=array('aruba_open','aruba_closure','aruba_csat','aruba_ssqa','aruba_rca','aruba_process_audit','aruba_esc','aruba_avaya_raw');
    $tbl=array('Inflow','Closure','CSAT','SSQA','RCA','Process Audit','Esc','Avaya');
    $xls_filename = 'Aruba_' . time() . '.xls';
    $rowNumberH=1;
    for ($i=0; $i <count($tablename) ; $i++) { 
        $headerArr=array();
    //echo "SHOW COLUMNS FROM $tablename[$i]";
               $stm1=$conn->prepare("SHOW COLUMNS FROM $tablename[$i]");
        $stm1->execute();
        $header=$stm1->fetchAll(PDO::FETCH_ASSOC);
        foreach ($header as $key => $value) {
            $headerArr[]= $value['Field'];
        }
        $rowNumber = 2;
        //echo "SELECT * FROM $tablename[$i] where $condi[0]_$condi[1] in ('".$condi[2]."')";
    
                   $stm=$conn->prepare("SELECT * FROM $tablename[$i] where $condi[0]_$condi[1] in ('".$condi[2]."')");
                         
        $stm->execute();
        $newsheet = $objPHPExcel->createSheet();
        $newsheet->setTitle($tbl[$i]);
        $colH = 'A';
        foreach ($headerArr as $h) {
            $newsheet->setCellValue($colH . $rowNumberH, $h);
            $colH++;
        }
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            $col = 'A';
            foreach($row as $cell) {
                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Calibri');
                $newsheet->setCellValue($col.$rowNumber,$cell);
                $col++;
            }
            $rowNumber++;
        }
    }
    $objPHPExcel->removeSheetByIndex(
        $objPHPExcel->getIndex(
            $objPHPExcel->getSheetByName('Worksheet')
        )
    );
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$xls_filename");
    header("Pragma: no-cache");
    header("Expires: 0");
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
?>
