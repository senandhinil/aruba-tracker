<?php
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['overallteam'] || $_POST['calendartype'] || $_POST['tlnamelist'] || $_POST['selectrange'] ){
	$overallteam 	= 	$_POST['overallteam'];
	$tlnamelist 	= 	$_POST['tlnamelist'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$productgroup 	= 	$_POST['productgroup'];
}else{
	$selectrange	=	!empty($selectrange)?$selectrange:current($currentweek);
	$overallteam 	=	!empty($overallteam)?$overallteam:"Overall";
	$tlnamelist 	=	!empty($tlnamelist)?$tlnamelist:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
}
if(empty($tlnamelist)){
	$tlnamelist 	=	"Overall";
}
if($overallteam!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	
	if($overallteam=='Overall'){
		$QryCondition.=' and que_new!="GEC"';
		$hcount="wlan,ns";
	}else{
		$QryCondition.=" and que_new='".$overallteam."'";
		$hcount="wlan,ns";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}
	$workingday = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",working_days from aruba_headcount where $type$selectQry='$selectrange'"),'','working_days');
		$workingdays = $workingday[0];
	//echo $tlnamelist;
	if($tlnamelist=='Overall'){
		$tlnameQury = $conn->prepare("select distinct team from aruba_closure where $type$selectQry='$selectrange' $QryCondition order by team asc");
		$tlnameQury->execute();
		$tlnameArr = $tlnameQury->fetchAll(PDO::FETCH_ASSOC);
		foreach($tlnameArr as $tlnameArrval){
			$headertlname[]=$tlnameArrval['team'];
		}
		$inflwQury = $conn->prepare("select team,$type$selectQry,case_origin from aruba_open where $type$selectQry='$selectrange' order by team asc");
		$inflwQury->execute();
		$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
		foreach($inflwQuryArr as $weeknamearr){
			$totopencase[$weeknamearr['team']][]=$weeknamearr[$type.$selectQry];
			$caseorigin[$weeknamearr['team']][]=$weeknamearr['case_origin'];
		}
		$case_origin=$commonobj->getarracount($caseorigin);

		$closureQury = $conn->prepare("select ".$type.$selectQry.",ttc,rma,sdc,case_origin,team,case_owner from aruba_closure where $type$selectQry='$selectrange' order by team asc");
		$closureQury->execute();
		$closeQuryArr = $closureQury->fetchAll(PDO::FETCH_ASSOC);
		foreach($closeQuryArr as $closeArrval){
			$closecount[$closeArrval['team']][]=$closeArrval[$type.$selectQry];
			if($closeArrval['ttc']<='9'){
				$ttccount[$closeArrval['team']][]=$closeArrval['ttc'];
			}
			if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
				$phonettc[$closeArrval['team']][]=$closeArrval['ttc'];
			}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
				$webttc[$closeArrval['team']][]=$closeArrval['ttc'];
			}
			if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
				$sdcphone[$closeArrval['team']][]=$closeArrval['sdc'];
			}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
				$sdcweb[$closeArrval['team']][]=$closeArrval['sdc'];
			}
			$rma[$closeArrval['team']][]=$closeArrval['rma'];
			$sdctot[$closeArrval['team']][]=$closeArrval['sdc'];
		}
		$rmacount=$commonobj->getarracount($rma);
		$sdctotper=$commonobj->getarracount($sdctot);
		
		//echo "select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team from aruba_csat where $type$selectQry='$selectrange' order by team asc";
		$csatunQury = $conn->prepare("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team from aruba_csat where $type$selectQry='$selectrange' order by team asc");
		$csatunQury->execute();
		$csatunQuryArr = $csatunQury->fetchAll(PDO::FETCH_ASSOC);
		foreach($csatunQuryArr as $csatunArrval){
			$overallexp[$csatunArrval['team']][]=$csatunArrval['overall_experience'];
			$alerttype[$csatunArrval['team']][]=$csatunArrval['alert_type'];
			$netpromoternew[$csatunArrval['team']][]=$csatunArrval['nps'];
			$loyaltyindex[$csatunArrval['team']][]=$csatunArrval['loyalty_index'];
			if($csatunArrval['rma']=='Yes'){
				$rmacaseoe[$csatunArrval['team']][]=$csatunArrval['overall_experience'];
				$loyalty_index[$csatunArrval['team']][]=$csatunArrval['loyalty_index'];
			}
		}
		$alert_type=$commonobj->getarracount($alerttype);
		$netprompter=$commonobj->getarracount($netpromoternew);
		//head count
		
		$enggcount = $commonobj->arrayColumn($commonobj->getQry("select count(distinct case_owner) as count,team  from aruba_open where calendar_week='2017Week42' group by team"),'team','count');

		$ssqa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,avg(overall) as ssqa_avg from aruba_ssqa where $type$selectQry='$selectrange' group by team order by team asc"),'team','ssqa_avg');
		
		$pa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,avg(overall) as pa_avg from aruba_process_audit where $type$selectQry='$selectrange' group by team order by team asc"),'team','ssqa_avg');

		$overallt1 = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' group by team order by team asc"),'team','cnt');

		$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  'Controllable' group by team order by team asc"),'team','cnt');

		$overalluncontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  'Uncontrollable' group by team order by team asc"),'team','cnt');

		$overallblank = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  '' group by case_owner order by case_owner asc"),'team','cnt');
		
		
		$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",team,count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' AND time_to_sec(total_time) >=  '28800' group by team order by team asc"),'team','cnt');
		
		$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(distinct case_owner) as cnt,team from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' group by team order by team asc"),'team','cnt');
		
		$getRca = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt,team from aruba_rca where ".$type.$selectQry." = '$selectrange'  group by team"),'team','cnt');
		
		$oesplit=  $commonobj->arrayColumn($commonobj->getQry("select team,oe_alert from aruba_rca where ".$type.$selectQry." = '$selectrange'"),'team','oe_alert');
		$rcasplit=$commonobj->getarracount($oesplit);

	}else{
		if(empty($tlnamelist)){
			$tlnameArr1 = $commonobj->getQry("select distinct team from aruba_closure where $type$selectQry='$selectrange' $QryCondition order by team asc");
			foreach($tlnameArr1 as $tlnameArrval1){
				$currentlist[]=$tlnameArrval1['team'];
			}
			$tlnamelist=current($currentlist);
		}
		$tlnameArr = $commonobj->getQry("select distinct case_owner from aruba_open where $type$selectQry='$selectrange' and team='$tlnamelist' order by case_owner asc");
		foreach($tlnameArr as $tlnameArrval){
			$headertlname[]=$tlnameArrval['case_owner'];
		}
		//print_r($headertlname);
		$inflwQuryArr = $commonobj->getQry("select team,$type$selectQry,case_origin,case_owner from aruba_open where $type$selectQry='$selectrange' and team='$tlnamelist' order by case_owner asc");
		foreach($inflwQuryArr as $weeknamearr){
			$totopencase[$weeknamearr['case_owner']][]=$weeknamearr[$type.$selectQry];
			$caseorigin[$weeknamearr['case_owner']][]=$weeknamearr['case_origin'];
		}
		$case_origin=$commonobj->getarracount($caseorigin);

		$closeQuryArr = $commonobj->getQry("select ".$type.$selectQry.",ttc,rma,sdc,case_origin,team,case_owner from aruba_closure where $type$selectQry='$selectrange' and team='$tlnamelist' order by case_owner asc");
		foreach($closeQuryArr as $closeArrval){
			$closecount[$closeArrval['case_owner']][]=$closeArrval[$type.$selectQry];
			if($closeArrval['ttc']<='9'){
				$ttccount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			}
			if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
				$phonettc[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
				$webttc[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			}
			if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
				$sdcphone[$closeArrval['case_owner']][]=$closeArrval['sdc'];
			}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
				$sdcweb[$closeArrval['case_owner']][]=$closeArrval['sdc'];
			}
			$rma[$closeArrval['case_owner']][]=$closeArrval['rma'];
			$sdctot[$closeArrval['case_owner']][]=$closeArrval['sdc'];
		}
		$rmacount=$commonobj->getarracount($rma);
		$sdctotper=$commonobj->getarracount($sdctot);
		
		$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps,team,case_owner from aruba_csat where $type$selectQry='$selectrange' and team='$tlnamelist' order by case_owner asc");
		foreach($csatunQuryArr as $csatunArrval){
			$overallexp[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
			$alerttype[$csatunArrval['case_owner']][]=$csatunArrval['alert_type'];
			$netpromoternew[$csatunArrval['case_owner']][]=$csatunArrval['nps'];
			$loyaltyindex[$csatunArrval['case_owner']][]=$csatunArrval['loyalty_index'];
			if($csatunArrval['rma']=='Yes'){
				$rmacaseoe[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
				$loyalty_index[$csatunArrval['case_owner']][]=$csatunArrval['loyalty_index'];
			}
		}
		$alert_type=$commonobj->getarracount($alerttype);
		$netprompter=$commonobj->getarracount($netpromoternew);
		//avaya
		$holyQuryArr = $commonobj->getQry("select ".$type.$selectQry.",working_days,$hcount from aruba_headcount where $type$selectQry='$selectrange'");
		foreach ($holyQuryArr as $holykey => $holyvalue) {
			$workingdays=$holyvalue['working_days'];
			$overallheadcount=$holyvalue['wlan']+$holyvalue['ns'];
		}
		
		$ssqa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,avg(overall) as ssqa_avg from aruba_ssqa where $type$selectQry='$selectrange' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','ssqa_avg');
		
		$pa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,avg(overall) as pa_avg from aruba_process_audit where $type$selectQry='$selectrange' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','pa_avg');
		
		$overallt1 = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');

		$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  'Controllable' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');

		$overalluncontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  'Uncontrollable' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');
		
		$overallblank = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." = '$selectrange' AND tier_1 =  '' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');
		
		$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' AND time_to_sec(total_time) >=  '28800' and team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');
		
		$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' group by case_owner order by case_owner asc"),'case_owner','cnt');
		$engineerwise="default";
		
		$getRca = $commonobj->arrayColumn($commonobj->getQry("select case_owner,count(*) as cnt from aruba_rca where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' group by case_owner"),'case_owner','cnt');
		
		$oesplit=  $commonobj->arrayColumn($commonobj->getQry("select case_owner,oe_alert from aruba_rca where ".$type.$selectQry." = '$selectrange' AND team='$tlnamelist' "),'case_owner','oe_alert');
		$rcasplit=$commonobj->getarracount($oesplit);
	}
}

header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Engg wise report.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		width: 16.66%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.div { 
            width: 100%; 
            overflow-x:scroll;  
            *padding-left:5em; 
            overflow-y:visible;
            padding-bottom:1px;
        }
</style>

<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <table>
	                <tr class="text-center"rowspan=2>
	                    <tr>
	                        <td colspan='34' align="center" style="border:1px solid;background-color:#F2784B;color:white;"><H3>
	                            <i class="icon-calendar"></i>Engg wise <?php echo $selectrange ?>
	                        </H3>
	                        </td>
	                    </tr>
	                </tr>
	            <table>
		   		</div>
		    <div class="portlet-body">
		       <div class="">
		        	<!-- <table class="table table-striped table-bordered table-hover text-center" style='white-space: nowrap;' id='example'> -->
		        	<table id="example" class="stripe row-border table-striped order-column text-center" width="100%" style='white-space: nowrap;' border="1">
                        <thead>
                            <tr style="*background-color:#f3d3c8;">
                            	<td rowspan="2" class="headcol style" style='vertical-align: inherit;height: 48px;background-color:#F2784B;color:white;text-align: center'>Metrics</td>
                            	<td colspan="3"  style='background-color:#F2784B;color:white;text-align: center '>Open Cases</td>
                            	<td colspan='10' style='background-color:#F2784B;color:white;text-align: center '>Closed Cases</td>
                            	<td colspan="9" style='background-color:#F2784B;color:white;text-align: center '>CSAT</td>
								<td colspan="4" style='background-color:#F2784B;color:white;text-align: center '>Escalation</td>
								<td colspan="2" style='background-color:#F2784B;color:white;text-align: center '>Quality</td>
								<td rowspan="2" style='background-color:#F2784B;color:white;text-align: center '>Outage</td>
								<td colspan="4" style='background-color:#F2784B;color:white;text-align: center '>RCA</td>
                            </tr>
                            <tr style="*background-color:#f3d3c8;">
                            	<td style="background-color:#F2784B;color:white">Overall</td>
                            	<td style='background-color:#F2784B;color:white'>Phone</td>
                            	<td style='background-color:#F2784B;color:white '>Web</td>
                            	<td style='background-color:#F2784B;color:white '>Closure</td>
                            	<td style='background-color:#F2784B;color:white '>RMA %</td>
                            	<td style='background-color:#F2784B;color:white '>RMA Cases</td>
                            	<td style='background-color:#F2784B;color:white '>TTC &lt;9 Days</td>
                            	<td style='background-color:#F2784B;color:white '>Phone - TTC &lt;9 Days</td>
                            	<td style='background-color:#F2784B;color:white '>Web - TTC &lt;9 Days</td>
                            	<td style='background-color:#F2784B;color:white '>SDC</td>
                            	<td style='background-color:#F2784B;color:white '>SDC - Phone (45%)</td>
                            	<td style='background-color:#F2784B;color:white '>SDC - Web (35%)</td>

                            	<td style='background-color:#F2784B;color:white '>Productivity</td>

                            	<td style='background-color:#F2784B;color:white '>Overall Experience</td>
                            	<td style='background-color:#F2784B;color:white '>Green%</td>
                            	<td style='background-color:#F2784B;color:white '>Normal %</td>
                            	<td style='background-color:#F2784B;color:white '>Red%</td>
                            	<td style='background-color:#F2784B;color:white '>Survey Count</td>
                            	<td style='background-color:#F2784B;color:white '>Net Promoter Score</td>
                            	<td style='background-color:#F2784B;color:white '>Loyalty Index</td>
                            	<td style='background-color:#F2784B;color:white '>RMA cases OE</td>
                            	<td style='background-color:#F2784B;color:white '> RMA cases NPS</td>

								<td style='background-color:#F2784B;color:white'>Escalation</td>
								<td style='background-color:#F2784B;color:white'>ESC-Controllable</td>
								<td style='background-color:#F2784B;color:white'>ESC-Uncontrollable</td>
								<td style='background-color:#F2784B;color:white'>ESC-Blank</td>

								<td style='background-color:#F2784B;color:white'>SSQA</td>
								<td style='background-color:#F2784B;color:white'>Process Audit</td>

								<td style='background-color:#F2784B;color:white'>Count</td>
								<td style='background-color:#F2784B;color:white'>Top-2</td>
								<td style='background-color:#F2784B;color:white'>MID</td>
								<td style='background-color:#F2784B;color:white'>DSAT</td>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	if(!empty($headertlname)){
	                        	foreach ($headertlname as $enggkey => $enggname) {
	                        	?>
	                        	<tr>
		                        	<td style='background-color:#F2784B;color:white;' ><?php echo $enggname;?></td>
		                        	<!-- open case -->
		                        	<td style='text-align:center;'><?php echo empty($totopencase[$enggname])?0:count($totopencase[$enggname]);?></td>
		                        	<td style='text-align:center;'><?php echo empty($case_origin[$enggname]['Phone'])?"0":$case_origin[$enggname]['Phone']?></td>
		                        	<td style='text-align:center;'><?php echo empty($case_origin[$enggname]['Web'])?"0":$case_origin[$enggname]['Web']?></td>
		                        	<!-- close case -->
		                        	<td style='text-align:center;'><?php echo empty($closecount[$enggname])?'0':count($closecount[$enggname]);?></td>
		                        	<td style='text-align:center;'><?php echo empty($rmacount[$enggname])?"0.00%":round($rmacount[$enggname]['Yes']/array_sum($rmacount[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($rmacount[$enggname]['Yes'])?"0":$rmacount[$enggname]['Yes']; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($ttccount[$enggname])?0:count($ttccount[$enggname]); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($phonettc[$enggname])?"0":count($phonettc[$enggname]); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($webttc[$enggname])?"0":count($webttc[$enggname]); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($sdctotper[$enggname])?"0.00%":round($sdctotper[$enggname]['1']/array_sum($sdctotper[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($sdcphone[$enggname])?"0.00%":round(count($sdcphone[$enggname])/count($closecount[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($sdcweb[$enggname])?"0.00%":round(count($sdcweb[$enggname])/count($closecount[$enggname])*100,2).'%'; ?></td>
		                        	<!-- producty -->
		                        	<?php
		                        		$engCnt=count($enggcount[$enggname])>0 ? $enggcount[$enggname] : 1 ;
		                        	?>
		                        	<td style='text-align:center;'><?php echo empty($closecount[$enggname])?"0":number_format(count($closecount[$enggname])/$workingdays/$engCnt,2); ?></td>
		                        	<!-- end producty -->
		                        	<!-- csat unnormalized -->
		                        	<td style='text-align:center;'><?php echo empty($overallexp[$enggname])?"NA":number_format(array_sum($overallexp[$enggname])/count($overallexp[$enggname]),2); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($alert_type[$enggname]['Green'])?"NA":round($alert_type[$enggname]['Green']/array_sum($alert_type[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($alert_type[$enggname]['Normal'])?"NA":round($alert_type[$enggname]['Normal']/array_sum($alert_type[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($alert_type[$enggname]['Red'])?"NA":round($alert_type[$enggname]['Red']/array_sum($alert_type[$enggname])*100,2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($alert_type[$enggname])?"0":array_sum($alert_type[$enggname]); ?></td>
		                        	<?php
		                        		$greenper[$enggname]=empty($netprompter[$enggname]['Promoter'])?"0.00%":round($netprompter[$enggname]['Promoter']/array_sum($netprompter[$enggname])*100,2).'%'; 
                        				$redperc[$enggname]=empty($netprompter[$enggname]['Detractor'])?"0.00%":round($netprompter[$enggname]['Detractor']/array_sum($netprompter[$enggname])*100,2).'%';
		                        	?>
		                        	<td style='text-align:center;'><?php echo empty($greenper[$enggname])?"0.00%":round($greenper[$enggname]-$redperc[$enggname],2).'%'; ?></td>
		                        	<td style='text-align:center;'><?php echo empty($loyaltyindex[$enggname])?"NA":number_format(array_sum($loyaltyindex[$enggname])/count($loyaltyindex[$enggname]),2); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($rmacaseoe[$enggname])?'NA':number_format(array_sum($rmacaseoe[$enggname])/count($rmacaseoe[$enggname]),2); ?></td>
		                        	<td style='text-align:center;'><?php echo empty($loyalty_index[$enggname])?'NA':number_format(array_sum($loyalty_index[$enggname])/count($loyalty_index[$enggname]),2); ?></td>
		                        	<!-- end csat unnormalized -->
									<!-- csat normalized 5% -->
									
									<td style='text-align:center;'><?php echo empty($overallt1[$enggname])?'NA':number_format($overallt1[$enggname]); ?></td>
									<td style='text-align:center;'>
										<?php 
										if($overalluncontrol[$enggname] != "" && $overallcontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overallcontrol[$enggname])?'NA':round($overallcontrol[$enggname]/$overallt1[$enggname]*100,2).'%'; 
										}
										?>
									</td>
									<td style='text-align:center;'>
										<?php 
										if($overallcontrol[$enggname] != "" && $overalluncontrol[$enggname] == "")
										{
											echo "0%";
										}else{
											echo empty($overalluncontrol[$enggname])?'NA':round($overalluncontrol[$enggname]/$overallt1[$enggname]*100,2).'%'; 
										}
										?>
									</td>
									<td style='text-align:center;'>
										<?php 
											echo empty($overallt1[$enggname])?'NA':round($overallblank[$enggname]/$overallt1[$enggname]*100,2).'%'; 
										?>
									</td>
									<td style='text-align:center;'><?php echo empty($ssqa[$enggname])?'NA':round($ssqa[$enggname],2).'%'; ?></td>
									<td style='text-align:center;'><?php echo empty($pa[$enggname])?'NA':round($pa[$enggname],2).'%'; ?></td>
									<td style='text-align:center;'>
										<?php
											if($engineerwise == "default")
											{
												$outage = round((1-($overallout[$enggname]/($overalloutcnt[$enggname])))*100,2);
											}else{
												$outage = round((1-($overallout[$enggname]/($overalloutcnt[$enggname]*$workingdays)))*100,2);
											}
											if($outage < 0)
											{
												$outage_new = 0;
											}else{
												$outage_new = $outage;
											}
											echo empty($overallout[$enggname])?'NA':$outage_new.'%';
										?></td>
										<?php $val = empty($getRca[$enggname]) ? 'NA':'0%'; ?>
										<td style='text-align:center;'><?php echo empty($getRca[$enggname])?'NA':$getRca[$enggname]; ?></td>
										<td style='text-align:center;'><?php echo empty($rcasplit[$enggname]['MID'])?$val:round($rcasplit[$enggname]['MID']/$getRca[$enggname]*100,2).'%'; ?></td>
										<td style='text-align:center;'><?php echo empty($rcasplit[$enggname]['Top-2'])?$val:round($rcasplit[$enggname]['Top-2']/$getRca[$enggname]*100,2).'%'; ?></td>
										<td style='text-align:center;'><?php echo empty($rcasplit[$enggname]['DSAT'])?$val:round($rcasplit[$enggname]['DSAT']/$getRca[$enggname]*100,2).'%'; ?></td>
	                        	<?php
	                        	}
	                        }else{
	                        	?>
	                        	<tr><td colspan="20" style='text-align: center;color:red'>No Data Found</td></tr>
	                        	
	                        	<?php
	                        }
                        	?>
                        	</tr>
                        </tbody>
                    </table>
		        </div>
		    </div>
		</div>
</form>

