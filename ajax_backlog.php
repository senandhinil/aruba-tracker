<?php
include "includes/config.php";
if(isset($_POST['reporttype'])){
	if($_POST['reporttype']=="Monthly"){
		if($_POST['calendertype']=='Normal'){
			$typecalender='calendar_month';
		}else{
			$typecalender='fiscal_month';
		}
		$query = $conn->prepare("SELECT DISTINCT $typecalender FROM aruba_backlog_raw order by id desc");
		$query->execute();
		$weeklist = $query->fetchAll(PDO::FETCH_ASSOC);
		$weekdata[]='<option>-- Select --</option>';		
	    foreach($weeklist as $weeknamearr){
	       $weekdata[]='<option value="'.$weeknamearr[$typecalender].'">'.$weeknamearr[$typecalender].'</option>'; 
	    }
	    echo json_encode($weekdata);
	} else if($_POST['reporttype']=="Quarterly"){
		if($_POST['calendertype']=='Normal'){
			$typecalender='calendar_quarter';
		}else{
			$typecalender='fiscal_quarter';
		}
		$query = $conn->prepare("SELECT DISTINCT $typecalender FROM aruba_backlog_raw order by id desc");
		$query->execute();
		$weeklist = $query->fetchAll(PDO::FETCH_ASSOC);
		$weekdata[]='<option>-- Select --</option>';
	    foreach($weeklist as $weeknamearr){
	       $weekdata[]='<option value="'.$weeknamearr[$typecalender].'">'.$weeknamearr[$typecalender].'</option>'; 
	    }
	    echo json_encode($weekdata);
	}elseif($_POST['reporttype']=="Weekly"){
		if($_POST['calendertype']=='Normal'){
			$typecalender='calendar_week';
		}else{
			$typecalender='fiscal_week';
		}
		$query = $conn->prepare("SELECT DISTINCT $typecalender FROM aruba_backlog_raw order by id desc");
		$query->execute();
		$weeklist = $query->fetchAll(PDO::FETCH_ASSOC);
		$weekdata[]='<option>-- Select --</option>';		
	    foreach($weeklist as $weeknamearr){
	       $weekdata[]='<option value="'.$weeknamearr[$typecalender].'">'.$weeknamearr[$typecalender].'</option>'; 
	    }	
	    echo json_encode($weekdata);
	}else{
		$date='date';
		$query = $conn->prepare("SELECT DISTINCT $date FROM aruba_backlog_raw order by id desc");
		$query->execute();
		$weeklist = $query->fetchAll(PDO::FETCH_ASSOC);
		$weekdata[]='<option>-- Select --</option>';		
	    foreach($weeklist as $weeknamearr){
	       $weekdata[]='<option value="'.$weeknamearr['date'].'">'.$weeknamearr['date'].'</option>'; 
	    }
	    echo json_encode($weekdata);
	}	
}
?>