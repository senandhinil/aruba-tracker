	<?php 
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['overallteam'] || $_POST['calendartype'] || $_POST['tlnamelist'] || $_POST['selectrange'] ){
	$overallteam 	= 	$_POST['overallteam'];
	$tlnamelist 	= 	$_POST['tlnamelist'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange	=	!empty($selectrange)?$selectrange:current($currentweek);
	$overallteam 	=	!empty($overallteam)?$overallteam:"Overall";
	$tlnamelist 	=	!empty($tlnamelist)?$tlnamelist:"Abigail Alba";
	
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($overallteam!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	//echo $overallteam;
	if($overallteam=='Overall'){
		$QryCondition.=' and que_new!="GEC"';
	}else{
		$QryCondition.=" and que_new='".$overallteam."'";
	}
	if($tlnamelist=='Overall'){
		$engnamelist='team';
	}else{
		$engnamelist='case_owner';
	}
	// if(empty($tlnamelist)){
		$tlnameQury1 = $conn->prepare("select distinct team from aruba_closure where $type$selectQry='$selectrange' $QryCondition order by team asc");
		$tlnameQury1->execute();
		$tlnameArr1 = $tlnameQury1->fetchAll(PDO::FETCH_ASSOC);
		foreach($tlnameArr1 as $tlnameArrval1){
			$currentlist[]=$tlnameArrval1['team'];
		}
		$tlnamelist=current($currentlist);
	}
	$tlnameQury = $conn->prepare("select distinct case_owner from aruba_open where $type$selectQry='$selectrange' and team='$tlnamelist' order by case_owner asc");
	$tlnameQury->execute();
	$tlnameArr = $tlnameQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($tlnameArr as $tlnameArrval){
		$headertlname[]=$tlnameArrval['case_owner'];
	}
	// print_r($headertlname);
	$inflwQury = $conn->prepare("select case_owner,bug_posted,rma,$type$selectQry from aruba_open where team='$tlnamelist' and $type$selectQry='$selectrange' order by case_owner asc");
	$inflwQury->execute();
	$inflwQuryArr = $inflwQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[$weeknamearr['case_owner']][]=$weeknamearr[$type.$selectQry];
		$openbug_posted[$weeknamearr['case_owner']][]=$weeknamearr['bug_posted'];
		$rma[$weeknamearr['case_owner']][]=$weeknamearr['rma'];
	}
	foreach ($openbug_posted as $bugkey => $bugvalue) {
		$enggbugposted[$bugkey]=array_count_values($bugvalue);
	}

	foreach ($rma as $rmakey => $rmavalue) {
		$enggrma[$rmakey]=array_count_values($rmavalue);
	}
	$closureQury = $conn->prepare("select case_owner,$type$selectQry,ttc,bug_posted,rma,ttr,sdc,`ttc_<_9`,`ttr_<_7` from aruba_closure where team='$tlnamelist' and $type$selectQry='$selectrange' order by case_owner asc");
	$closureQury->execute();
	$closeQuryArr = $closureQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($closeQuryArr as $closeArrval){
		$closecount[$closeArrval['case_owner']][]=$closeArrval[$type.$selectQry];
		$ttccount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
		$ttc9days[$closeArrval['case_owner']][]=$closeArrval['ttc_<_9'];
		$ttr7days[$closeArrval['case_owner']][]=$closeArrval['ttr_<_7'];
		if($closeArrval['bug_posted']=='Yes'){
			$bugcount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			$ttrbug[$closeArrval['case_owner']][]=$closeArrval['ttr'];
		}else{
			$nonbugcount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			$ttrnonbug[$closeArrval['case_owner']][]=$closeArrval['ttr'];
		}
		if($closeArrval['rma']=='Yes'){
			$closermacount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			$ttrrmabug[$closeArrval['case_owner']][]=$closeArrval['ttr'];
		}else{
			$closenonrmacount[$closeArrval['case_owner']][]=$closeArrval['ttc'];
			$ttrrmanonbug[$closeArrval['case_owner']][]=$closeArrval['ttr'];
		}
		$ttrcount[$closeArrval['case_owner']][]=$closeArrval['ttr'];
		$sdctot[$closeArrval['case_owner']][]=$closeArrval['sdc'];
	}
	foreach ($sdctot as $sdckey => $sdcvalue) {
		$sdctotper[$sdckey]=array_count_values($sdcvalue);
	}
	foreach ($ttc9days as $ttc9dayskey => $ttc9daysvalue) {
		$closettc9days[$ttc9dayskey]=array_count_values($ttc9daysvalue);
	}
	foreach ($ttr7days as $ttr7dayskey => $ttr7daysvalue) {
		$closettr7days[$ttr7dayskey]=array_count_values($ttr7daysvalue);
	}
	// print_r($closecount);
	$csatunQury = $conn->prepare("select case_owner,$type$selectQry,overall_experience,tacosat,alert_typenew,net_promoter_new,loyalty_index,bug,rma from aruba_csat_unnormalized where team='$tlnamelist' and $type$selectQry='$selectrange' order by case_owner asc");
	$csatunQury->execute();
	$csatunQuryArr = $csatunQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		$tacosat[$csatunArrval['case_owner']][]=$csatunArrval['tacosat'];
		$alerttype[$csatunArrval['case_owner']][]=$csatunArrval['alert_typenew'];
		$netpromoternew[$csatunArrval['case_owner']][]=$csatunArrval['net_promoter_new'];
		$loyaltyindex[$csatunArrval['case_owner']][]=$csatunArrval['loyalty_index'];
		if($csatunArrval['bug']=='Yes'){
			 $unbugyes[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		}else{
			$unbugno[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		}
		if($csatunArrval['rma']=='Yes'){
			$unrmayes[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		}else{
			$unrmano[$csatunArrval['case_owner']][]=$csatunArrval['overall_experience'];
		}
	}
	foreach ($alerttype as $alerttypekey => $alerttypevalue) {
		$alerttypeArr[$alerttypekey]=array_count_values($alerttypevalue);
	}
	foreach ($netpromoternew as $netpromoterkey => $netpromotervalue) {
		$netpromoterArr[$netpromoterkey]=array_count_values($netpromotervalue);
	}
	$csatnQury = $conn->prepare("select case_owner,$type$selectQry,overall_experience,tacosat,alert_typenew,net_promoter_new,loyalty_index,bug,rma from aruba_csat_normalized where team='$tlnamelist' and $type$selectQry='$selectrange' order by case_owner asc");
	$csatnQury->execute();
	$csatnQuryArr = $csatnQury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatnQuryArr as $csatnArrval){
		$nor_overallexp[$csatnArrval['case_owner']][]=$csatnArrval['overall_experience'];
		$nor_tacosat[$csatnArrval['case_owner']][]=$csatnArrval['tacosat'];
		$nor_alerttype[$csatnArrval['case_owner']][]=$csatnArrval['alert_typenew'];
		$nor_netpromoternew[$csatnArrval['case_owner']][]=$csatnArrval['net_promoter_new'];
		$nor_loyaltyindex[$csatnArrval['case_owner']][]=$csatnArrval['loyalty_index'];
		if($csatnArrval['bug']=='Yes'){
			$nbugyes[$csatnArrval['case_owner']][]=$csatnArrval['overall_experience'];
		}else{
			$nbugno[$csatnArrval['case_owner']][]=$csatnArrval['overall_experience'];
		}
		if($csatnArrval['rma']=='1' || $csatnArrval['rma']=='Yes'){
			$nrmayes[$csatnArrval['case_owner']][]=$csatnArrval['overall_experience'];
		}else{
			$nrmano[$csatnArrval['case_owner']][]=$csatnArrval['overall_experience'];
		}
	}
	foreach ($nor_alerttype as $nalerttypekey => $nalerttypevalue) {
		$noralerttypeArr[$nalerttypekey]=array_count_values($nalerttypevalue);
	}
	foreach ($nor_netpromoternew as $nor_netpromoterkey => $nor_netpromotervalue) {
		$nornetpromoterArr[$nor_netpromoterkey]=array_count_values($nor_netpromotervalue);
	}

	$csatn25Qury = $conn->prepare("select case_owner,$type$selectQry,overall_experience,tacosat,alert_typenew,net_promoter_new,loyalty_index,bug,rma from aruba_csat_normalized25 where team='$tlnamelist' and $type$selectQry='$selectrange' order by case_owner asc");
	$csatn25Qury->execute();
	$csatn25QuryArr = $csatn25Qury->fetchAll(PDO::FETCH_ASSOC);
	foreach($csatn25QuryArr as $csatn25Arrval){
		$n25_overallexp[$csatn25Arrval['case_owner']][]=$csatn25Arrval['overall_experience'];
		$n25_tacosat[$csatn25Arrval['case_owner']][]=$csatn25Arrval['tacosat'];
		$n25_alerttype[$csatn25Arrval['case_owner']][]=$csatn25Arrval['alert_typenew'];
		$n25_netpromoternew[$csatn25Arrval['case_owner']][]=$csatn25Arrval['net_promoter_new'];
		$n25_loyaltyindex[$csatn25Arrval['case_owner']][]=$csatn25Arrval['loyalty_index'];
		if($csatn25Arrval['bug']=='Yes'){
			$n25bugyes[$csatn25Arrval['case_owner']][]=$csatn25Arrval['overall_experience'];
		}else{
			$n25bugno[$csatn25Arrval['case_owner']][]=$csatn25Arrval['overall_experience'];
		}
		if($csatn25Arrval['rma']=='1' || $csatn25Arrval['rma']=='Yes'){
			$n25rmayes[$csatn25Arrval['case_owner']][]=$csatn25Arrval['overall_experience'];
		}else{
			$n25rmano[$csatn25Arrval['case_owner']][]=$csatn25Arrval['overall_experience'];
		}
	}
	foreach ($n25_alerttype as $n25alerttypekey => $n25alerttypevalue) {
		$nor25alerttypeArr[$n25alerttypekey]=array_count_values($n25alerttypevalue);
	}
	foreach ($n25_netpromoternew as $n25netpromoterkey => $n25netpromotervalue) {
		$nor25netpromoterArr[$n25netpromoterkey]=array_count_values($n25netpromotervalue);
	}
	//print_r($n25_overallexp);
	$workingdayQry = $conn->prepare("select working_days from aruba_headcount where $type$selectQry='$selectrange'");
	$workingdayQry->execute();
	$workingdayArr = $workingdayQry->fetchAll(PDO::FETCH_ASSOC);
	foreach($workingdayArr as $workingdayvalue){
		$totalworkingday=$workingdayvalue['working_days'];
	}
	//echo $totalworkingday;
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		width: 16.66%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.div { 
            width: 100%; 
            overflow-x:scroll;  
            *padding-left:5em; 
            overflow-y:visible;
            padding-bottom:1px;
        }
        table,td,th{
        	border:0px solid #E7ECF1;
        }
       th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
	$(document).ready(function() {
	    var table = $('#example').DataTable( {
	        scrollY:        "1000px",
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         false,
	        fixedColumns:   {
	            leftColumns: 1,
	            //rightColumns: 1
	        }
	    } );
	} );
</script>	
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?=$token?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		            <div class="form-group col-md-3 col-sm-0 col-xs-0"></div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="overallteam">
			                <option value="Overall">Overall</option>
			                <option value="WC">WC</option>
			                <option value="GSC">GSC</option>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $overallteam ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="tlnamelist">
		                <option value='Overall'>Overall</option>
		                	<?php
		                		$tlnameQury2 = $conn->prepare("select distinct team from aruba_closure where $type$selectQry='$selectrange' $QryCondition order by team asc");
								$tlnameQury2->execute();
								$tlnameArr2 = $tlnameQury2->fetchAll(PDO::FETCH_ASSOC);
								foreach($tlnameArr2 as $tlnameArrval2){
									echo "<option value='".$tlnameArrval2['team']."'>".$tlnameArrval2['team']."</option>";
								}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $tlnamelist ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop6"  name="selecttype">
		                	<!-- <option value="Daily">Daily</option> -->
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-3 col-sm-3 col-xs-6">
		                <select class="form-control" id="drop7"  name="selectrange">
		                	<option value="">--- Select ---</option>';
		                	<?php
								$selecttypeQry = $conn->prepare("select distinct ".$type.$selectQry." from aruba_open order by open_id desc");
								$selecttypeQry->execute();
								$drowpdownArr = $selecttypeQry->fetchAll(PDO::FETCH_ASSOC);
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Engg Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	<div class="dt-buttons">
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        	</div>
			        </div>
		    </div>
		    <div class="portlet-body">
		       <div class="">
		        	<!-- <table class="table table-striped table-bordered table-hover text-center" style='white-space: nowrap;' id='example'> -->
		        	<table id="example" class="stripe row-border table-striped order-column text-center"width="100%" style='white-space: nowrap;'>
                        <thead>
                            <tr style="*background-color:#f3d3c8;">
                            	<td rowspan="2" class="headcol style" style='vertical-align: inherit;height: 48px;border:1px solid #E7ECF1'>Metrics</td>
                            	<td colspan="3" style='border:1px solid #E7ECF1'>Open Cases</td>
                            	<!-- <td rowspan="2" style='vertical-align: inherit;'>Productivity</td> -->
                            	<td colspan="11" style='border:1px solid #E7ECF1'>Closed Cases</td>
                            	<!-- <td colspan="5" style='border:1px solid #E7ECF1'>TTR</td> -->
                            	<!-- <td style='border:1px solid #E7ECF1'>SDC</td> -->
                            	<td colspan="12" style='border:1px solid #E7ECF1'>CSAT (Unnormalized)</td>
                            	<td colspan="12" style='border:1px solid #E7ECF1'>CSAT (Normalized 5%)</td>
                            	<td colspan="12" style='border:1px solid #E7ECF1'>CSAT (Normalized 2.5%)</td>
                            </tr>
                            <tr style="*background-color:#f3d3c8;">
                            	<td style="margin-left:100px;border:1px solid #E7ECF1">Overall</td>
                            	<td style='border:1px solid #E7ECF1'>RMA %</td>
                            	<td style='border:1px solid #E7ECF1'>Bug %</td>
                            	<td style='border:1px solid #E7ECF1'>Closure Count</td>
                            	<td style='border:1px solid #E7ECF1'>Productivity</td>
                            	<td style='border:1px solid #E7ECF1'>Bug Cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-Bug Cases</td>
                            	<td style='border:1px solid #E7ECF1'>RMA Cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-RMA Cases</td>
                            	<td style='border:1px solid #E7ECF1'>TTC</td>
                            	<td style='border:1px solid #E7ECF1'>TTR</td>
                            	<td style='border:1px solid #E7ECF1'>TTC &lt;9 Days</td>
                            	<td style='border:1px solid #E7ECF1'>TTR &lt;7 Days</td>
                            	<td style='border:1px solid #E7ECF1'>SDC%</td>
                            	<!-- <td style='border:1px solid #E7ECF1'>TTR</td> -->
                            	<!-- <td style='border:1px solid #E7ECF1'>Bug Cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-Bug Cases</td>
                            	<td style='border:1px solid #E7ECF1'>RMA Cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-RMA Cases</td> -->
                            	<!-- <td style='border:1px solid #E7ECF1'>SDC% - Overall</td> -->
                            	<td style='border:1px solid #E7ECF1'>Overall Experience</td>
                            	<td style='border:1px solid #E7ECF1'>TACOSAT</td>
                            	<td style='border:1px solid #E7ECF1'>Green%</td>
                            	<td style='border:1px solid #E7ECF1'>Normal%</td>
                            	<td style='border:1px solid #E7ECF1'>Red%</td>
                            	<td style='border:1px solid #E7ECF1'>Survey Count</td>
                            	<td style='border:1px solid #E7ECF1'>Net Promoter Score</td>
                            	<td style='border:1px solid #E7ECF1'>Loyalty Index</td>
                            	<td style='border:1px solid #E7ECF1'>Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>RMA cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-RMA cases</td>
                            	<td style='border:1px solid #E7ECF1'>Overall Experience</td>
                            	<td style='border:1px solid #E7ECF1'>TACOSAT</td>
                            	<td style='border:1px solid #E7ECF1'>Green%</td>
                            	<td style='border:1px solid #E7ECF1'>Normal %</td>
                            	<td style='border:1px solid #E7ECF1'>Red%</td>
                            	<td style='border:1px solid #E7ECF1'>Survey Count</td>
                            	<td style='border:1px solid #E7ECF1'>Net Promoter Score</td>
                            	<td style='border:1px solid #E7ECF1'>Loyalty Index</td>
                            	<td style='border:1px solid #E7ECF1'>Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>RMA cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-RMA cases</td>
                            	<td style='border:1px solid #E7ECF1'>Overall Experience</td>
                            	<td style='border:1px solid #E7ECF1'>TACOSAT</td>
                            	<td style='border:1px solid #E7ECF1'>Green%</td>
                            	<td style='border:1px solid #E7ECF1'>Normal %</td>
                            	<td style='border:1px solid #E7ECF1'>Red%</td>
                            	<td style='border:1px solid #E7ECF1'>Survey Count</td>
                            	<td style='border:1px solid #E7ECF1'>Net Promoter Score</td>
                            	<td style='border:1px solid #E7ECF1'>Loyalty Index</td>
                            	<td style='border:1px solid #E7ECF1'>Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-Bug cases</td>
                            	<td style='border:1px solid #E7ECF1'>RMA cases</td>
                            	<td style='border:1px solid #E7ECF1'>Non-RMA cases</td>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	if(!empty($headertlname)){
	                        	foreach ($headertlname as $enggkey => $enggname) {
	                        	?>
	                        	<tr>
		                        	<td style='border:1px solid #E7ECF1' ><?php echo $enggname;?></td>
		                        	<!-- open case -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($totopencase[$enggname])?0:count($totopencase[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($enggrma[$enggname]['Yes'])?"0.0%":number_format($enggrma[$enggname]['Yes']/array_sum($enggrma[$enggname])*100,2).'%'?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($enggbugposted[$enggname]['Yes'])?"0.0%":number_format($enggbugposted[$enggname]['Yes']/array_sum($bugpostedtot[$enggname])*100,2).'%';?></td>
		                        	<!-- productivity -->
		                        	
		                        	<!-- end productivity -->
		                        	<!-- close case -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closecount[$enggname])?'0':count($closecount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closecount[$enggname])?0:number_format(count($closecount[$enggname])/$totalworkingday,2);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($bugcount[$enggname])?'0':count($bugcount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nonbugcount[$enggname])?'0':count($nonbugcount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closermacount[$enggname])?"0":count($closermacount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closenonrmacount[$enggname])?"0":count($closenonrmacount[$enggname]);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttccount[$enggname])?'0.00':number_format(array_sum($ttccount[$enggname])/count($ttccount[$enggname]),2,'.', '');?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttrcount[$enggname])?"0.00":number_format(array_sum($ttrcount[$enggname])/count($ttrcount[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closettc9days[$enggname]['1'])?"0.00%":number_format($closettc9days[$enggname]['1']/array_sum($closettc9days[$enggname])*100,2).'%';?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($closettr7days[$enggname]['Yes'])?"0.00%":number_format($closettr7days[$enggname]['Yes']/array_sum($closettr7days[$enggname])*100,2).'%';?></td>
		                        	
		                        	<!-- SDC -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($sdctotper[$enggname])?"0.00%":number_format($sdctotper[$enggname]['1']/array_sum($sdctotper[$enggname])*100,2).'%'; ?></td>
		                        	<!-- end SDC -->
		                        	<!-- end close case -->
		                        	<!-- open TTR -->
		                        	<!-- <td style='border:1px solid #E7ECF1'><?php echo empty($ttrcount[$enggname])?"0.00":number_format(array_sum($ttrcount[$enggname])/count($ttrcount[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttrbug[$enggname])?"0.00":number_format(array_sum($ttrbug[$enggname])/count($ttrbug[$enggname]),2);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttrnonbug[$enggname])?"0.00":number_format(array_sum($ttrnonbug[$enggname])/count($ttrnonbug[$enggname]),2);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttrrmabug[$enggname])?"0.00":number_format(array_sum($ttrrmabug[$enggname])/count($ttrrmabug[$enggname]),2);?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($ttrrmanonbug[$enggname])?"0.00":number_format(array_sum($ttrrmanonbug[$enggname])/count($ttrrmanonbug[$enggname]),2);?></td> -->
		                        	<!-- end TTR -->
		                        	
		                        	<!-- csat unnormalized -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($overallexp[$enggname])?"0.00":number_format(array_sum($overallexp[$enggname])/count($overallexp[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($tacosat[$enggname])?"0.00":number_format(array_sum($tacosat[$enggname])/count($tacosat[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alerttypeArr[$enggname]['Green'])?"0.00%":number_format($alerttypeArr[$enggname]['Green']/array_sum($alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alerttypeArr[$enggname]['Normal'])?"0.00%":number_format($alerttypeArr[$enggname]['Normal']/array_sum($alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($alerttypeArr[$enggname]['Red'])?"0.00%":number_format($alerttypeArr[$enggname]['Red']/array_sum($alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($overallexp[$enggname])?'NA':count($overallexp[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($netpromoterArr[$enggname]['Green'])?"0.00%":number_format($netpromoterArr[$enggname]['Green']/array_sum($netpromoterArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($loyaltyindex[$enggname])?"0.00":number_format(array_sum($loyaltyindex[$enggname])/count($loyaltyindex[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($unbugyes[$enggname])?'NA':number_format(array_sum($unbugyes[$enggname])/count($unbugyes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($unbugno[$enggname])?'NA':number_format(array_sum($unbugno[$enggname])/count($unbugno[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($unrmayes[$enggname])?'NA':number_format(array_sum($unrmayes[$enggname])/count($unrmayes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($unrmano[$enggname])?'NA':number_format(array_sum($unrmano[$enggname])/count($unrmano[$enggname]),2); ?></td>
		                        	<!-- end csat unnormalized -->
									<!-- csat normalized 5% -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor_overallexp[$enggname])?"0.00":number_format(array_sum($nor_overallexp[$enggname])/count($nor_overallexp[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor_tacosat[$enggname])?"0.00":number_format(array_sum($nor_tacosat[$enggname])/count($nor_tacosat[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($noralerttypeArr[$enggname]['Green'])?"0.00%":number_format($noralerttypeArr[$enggname]['Green']/array_sum($noralerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($noralerttypeArr[$enggname]['Normal'])?"0.00%":number_format($noralerttypeArr[$enggname]['Normal']/array_sum($noralerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($noralerttypeArr[$enggname]['Red'])?"0.00%":number_format($noralerttypeArr[$enggname]['Red']/array_sum($noralerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor_overallexp[$enggname])?'NA':count($nor_overallexp[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nornetpromoterArr[$enggname]['Green'])?"0.00%":number_format($nornetpromoterArr[$enggname]['Green']/array_sum($nornetpromoterArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor_loyaltyindex[$enggname])?"0.00":number_format(array_sum($nor_loyaltyindex[$enggname])/count($nor_loyaltyindex[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nbugyes[$enggname])?'NA':number_format(array_sum($nbugyes[$enggname])/count($nbugyes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nbugno[$enggname])?'NA':number_format(array_sum($nbugno[$enggname])/count($nbugno[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nrmayes[$enggname])?'NA':number_format(array_sum($nrmayes[$enggname])/count($nrmayes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nrmano[$enggname])?'NA':number_format(array_sum($nrmano[$enggname])/count($nrmano[$enggname]),2); ?></td>
		                        	<!--end csat normalized 5%-->
		                        	<!-- start csat normalized 2.5% -->
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25_overallexp[$enggname])?"0.00":number_format(array_sum($n25_overallexp[$enggname])/count($n25_overallexp[$enggname]),2); ?></td>
		                        	<td><?php echo empty($n25_tacosat[$enggname])?"0.00":number_format(array_sum($n25_tacosat[$enggname])/count($n25_tacosat[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor25alerttypeArr[$enggname]['Green'])?"0.00%":number_format($nor25alerttypeArr[$enggname]['Green']/array_sum($nor25alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor25alerttypeArr[$enggname]['Normal'])?"0.00%":number_format($nor25alerttypeArr[$enggname]['Normal']/array_sum($nor25alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor25alerttypeArr[$enggname]['Red'])?"0.00%":number_format($nor25alerttypeArr[$enggname]['Red']/array_sum($nor25alerttypeArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25_overallexp[$enggname])?'NA':count($n25_overallexp[$enggname]); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($nor25netpromoterArr[$enggname]['Green'])?"0.00%":number_format($nor25netpromoterArr[$enggname]['Green']/array_sum($nor25netpromoterArr[$enggname])*100,2).'%'; ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25_loyaltyindex[$enggname])?"0.00":number_format(array_sum($n25_loyaltyindex[$enggname])/count($n25_loyaltyindex[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25bugyes[$enggname])?'NA':number_format(array_sum($n25bugyes[$enggname])/count($n25bugyes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25bugno[$enggname])?'NA':number_format(array_sum($n25bugno[$enggname])/count($n25bugno[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25rmayes[$enggname])?'NA':number_format(array_sum($n25rmayes[$enggname])/count($n25rmayes[$enggname]),2); ?></td>
		                        	<td style='border:1px solid #E7ECF1'><?php echo empty($n25rmano[$enggname])?'NA':number_format(array_sum($n25rmano[$enggname])/count($n25rmano[$enggname]),2); ?></td>
	                        	<!-- end csat normalized 2.5% -->
	                        	<?php
	                        	}
	                        }else{
	                        	?>
	                        	<tr><td colspan="20" style='text-align: center;color:red'>No Data Found</td></tr>
	                        	
	                        	<?php
	                        }
                        	?>
                        	</tr>
                        </tbody>
                    </table>
		        </div>
		    </div>
		</div>
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$("#drop6").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("#drop3").change(function(){
	    $("#drop4").val("");
	});
	
	$("#drop6").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('#drop1').change(function(){
		document.getElementById("frmsrch").action = 'engineerwise.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'engineerwise-export.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop2').change(function(){
		document.getElementById("frmsrch").action = 'engineerwise.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop3').change(function(){
		document.getElementById("frmsrch").action = 'engineerwise.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop4').change(function(){
		document.getElementById("frmsrch").action = 'engineerwise.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	$('#drop7').change(function(){
		document.getElementById("frmsrch").action = 'engineerwise.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
</script>
