<?php
include "includes/config.php";
include 'includes/session_check.php';
ini_set('max_execution_time', 120);
// require_once('dashboard_calculation_new.php');
include "includes/header.php";
// echo $QryCondition;
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	/*.form-control {
	    border: 0px solid #c2cad8 !important;
	}*/
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-2 {
			width: 12.50%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}

</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
	<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop2"  name="projectwise">
	                    <?php
	                    	$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
	                    	echo '<option value="Overall">Overall Project</option>';
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script>  
		                     jQuery("#drop2").val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop3"  name="productwise">
		                <?php
		                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
		                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
							echo '<option value="Overall">Overall Que</option>';
							foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
					    <select class="form-control submit" id="drop4"  name="productgroup">
		                	<?php
		                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                		$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
		                		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery");
								echo '<option value="Overall">Overall Product</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop1"  name="reporttype">
		                	<?php
		                	echo '<option value="Overall">Overall Region</option>';
		                	foreach($productgroupoverall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop1").val("<?php echo $reporttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control select" id="team"  name="team[]" multiple>
		                	<?php
		                	$teamName = $commonobj->getQry("SELECT DISTINCT team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $teamQry order by team asc");
		                	
		                	foreach($teamName as $value){
		                		if(count($team) == 0 ){
		                			$selected = 'selected';
		                		}else{
		                			$selected = in_array($value['team'],$team)?'selected':'';
		                		}
								echo '<option value="'.$value['team'].'"'.$selected.'>'.$value['team']."</option>";
							}
		                	?>
		                </select>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype">
		                	<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control submit" id="drop7"  name="selectrange">
		                	<option value="">--- Select ---</option>
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
									echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
		                <script> 
							jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
		            </div>
		        </div>
		    </div>
		</div>    
	</div>
	<?php 

	//$productName = $productName, array('GEC'));
	$headQry = $productgroup == 'Overall' ? " sum(`".implode("`)+sum(`",array_diff($commonobj->arrayColumn($productName,'','product_group'), array('GEC')))."`)" : " sum(`$productgroup`)";
	
	$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
	?>
		<div class="portlet box yellow-casablanca">
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-tasks"></i>Summary</div>
			        <div class="tools" style="padding-bottom: 0px;padding-top: 5px;"> 
			        	
			        		<img src="images/Xls-File-128.png" style="height: 25px;cursor: pointer;"  id='export' class='export()' title='Download Excel'>
			        		<img src="img/exp.png" style="height: 25px;cursor: pointer;"  id='rawexport' title='Raw Download'>
			        		<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			        		<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
			        	
			        </div>
		    </div>
		    <div class="portlet-body">
			    <div class="table-scrollable" id="table-view">
			        <div id="dasshboard_res">
					
					</div>
			    </div>
		    </div>
		</div>
</form>
<?php 
// $time_start = microtime(true); 
// echo 'Total execution time in seconds: ' . (microtime(true) - $time_start);
include("includes/footer.php");
?>
<script src="../assets/angular.min.js"></script>
<script src="dashboard_calculation.js"></script>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("document").ready(function(){
		$.ajax({
	      url: 'dashboard_calculation_new.php',
	      type: 'POST',
	      data: {'reporttype':'test'},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#dasshboard_res").html("");
	        $("#dasshboard_res").html(obj);
	      }
	    });
	});
	// $("#drop1").change(function(){
	//     $("#drop2").val("Overall");
	//     $("#drop3").val("Overall");
	//     $("#drop4").val("Overall");
	// });
	
	jQuery(document).ready(function($) {  
		var table = $('#tableId').DataTable( {
        	fixedHeader: true
    	} );
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	$('.submit').change(function(){
		document.getElementById("frmsrch").action = 'dashboard_old.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	$('#export').click(function(){
		document.getElementById("frmsrch").action = 'export-summary.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});

	$('#rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	function reload(){
		document.getElementById("frmsrch").action = 'dashboard_old.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}
</script>