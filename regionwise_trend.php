<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['reporttype'] || $_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange']){
	$reporttype 	= 	$_POST['reporttype'];
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:"Overall";
	$productwise 	=	!empty($productwise)?$productwise:"Overall";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
	$reporttype 	=	!empty($reporttype)?$reporttype:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($selectrange!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	/* if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition.=" and wlan_ns='".$projectwise."'";
		//$hcount=strtolower($projectwise);
	}else{
		//$hcount="wlan,ns";
	} */
	/* if($projectwise != "Overall" && $productwise != "WC" && $productgroup == "Overall")
	{
		$hcount = $projectwise;
	}elseif($productwise == "WC" && $productgroup == "Overall" && $projectwise == "Overall")
	{
		$hcount = $productwise;
	}elseif($productwise == "Overall" && $projectwise == "Overall" && $productgroup == "Overall"){
		$hcount_type = "default";
		$hcount="wlan,ns,wc";
	}elseif($projectwise == "Overall" && $productgroup == "Overall" && ($productwise == "GSC" || $productwise == "GEC")){
		$hcount_type = "default";
		$hcount="wlan,ns,wc";
	}elseif($productgroup != "Overall"){
		$hcount = $productgroup;
	} */
	if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition .= " and wlan_ns='".$projectwise."'";
	}
	if($productwise!='Overall' && $productwise!=''){
		$QryCondition .= " and que_new='".$productwise."'";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition .= " and product_group='".$productgroup."'";
	}
	if($reporttype!='Overall' && $reporttype!=''){
		$QryCondition .= " and region='".$reporttype."'";
	}
	$headQryArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	
	//Open Case Calculation
	$openQuryArr = $commonobj->getQry("select count(*) as open_count,".$type.$selectQry.",region from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($openQuryArr as $weeknamearr){
		$opencase[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['open_count'];
	}
	foreach($opencase as $openkey => $openvalArr){
		foreach($tablehead as $tblhd){
			$totopencaseArr[$openkey][] = empty($openvalArr[$tblhd])?0:$openvalArr[$tblhd];
		}
	}
	
	//Closed Case Calculation
	$closeQuryArr = $commonobj->getQry("select count(*) as close_count,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closeQuryArr as $weeknamearr){
		$closecase[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['close_count'];
	}
	foreach($closecase as $closekey => $closevalArr){
		foreach($tablehead as $tblhd){
			$closedcaseArr[$closekey][] = empty($closevalArr[$tblhd])?0:$closevalArr[$tblhd];
		}
	}
	//print_r($closedcaseArr);
	
	//Closed Case Vs TTC Calculation
	$closettcQuryArr = $commonobj->getQry("select sum(ttc) as ttc_sum,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closettcQuryArr as $weeknamearr){
		$closecasettc[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['ttc_sum'];

	}
	foreach($closecasettc as $closettckey => $closettcvalArr){
		foreach($tablehead as $tblhd){
			$a = empty($closettcvalArr[$tblhd])?0:$closettcvalArr[$tblhd];
			$closedttcArr[$closettckey][] = round($a/$closecase[$closettckey][$tblhd],2);
		}
	}
	
	//Closed Case Vs SDC Calculation
	$closesdcQuryArr = $commonobj->getQry("select sum(sdc) as sdc_sum,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closesdcQuryArr as $weeknamearr){
		$closecasesdc[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['sdc_sum'];
	}
	//print_r($closecasesdc);
	$closesdcoverallQuryArr = $commonobj->getQry("select count(sdc) as sdc_count,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closesdcoverallQuryArr as $weeknamearr){
		$closecasesdcoverall[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['sdc_count'];
	}
	//print_r($closecasesdcoverall);
	foreach($closecasesdc as $closesdckey => $closesdcvalArr){
		foreach($tablehead as $tblhd){
			$a = empty($closesdcvalArr[$tblhd])?0:$closesdcvalArr[$tblhd];
			$closedsdcArr[$closesdckey][] = round(($a/$closecasesdcoverall[$closesdckey][$tblhd])*100,2);
		}
	}
	//print_r($closedsdcArr);
	
	//Closed Cases Vs RMA
	$closermaQuryArr = $commonobj->getQry("select count(*) as rma_count,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and rma = 'Yes' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closermaQuryArr as $weeknamearr){
		$closecaserma[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['rma_count'];
	}
	foreach($closecaserma as $closermakey => $closermavalArr){
		foreach($tablehead as $tblhd){
			$closedrmaArr[$closermakey][] = empty($closermavalArr[$tblhd])?0:$closermavalArr[$tblhd];
		}
	}
	
	//Closed Cases Vs TTC < 9 Days
	
	$closenovttcQuryArr = $commonobj->getQry("select avg(ttc) as overall_ttc,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and ttc < '9' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closenovttcQuryArr as $weeknamearr){
		$closecaseovtt[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['overall_ttc'];
	}
	foreach($closecaseovtt as $closeovttkey => $closeovttvalArr){
		foreach($tablehead as $tblhd){
			$closedovttcArr[$closeovttkey][] = empty($closeovttvalArr[$tblhd])?0:$closeovttvalArr[$tblhd];
		}
	}
	
	$closenphnQuryArr = $commonobj->getQry("select avg(ttc) as phone_ttc,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and ttc < '9' AND case_origin = 'Phone' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closenphnQuryArr as $weeknamearr){
		$closecasenphn[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['phone_ttc'];
	}
	foreach($closecasenphn as $closenphnkey => $closenphnvalArr){
		foreach($tablehead as $tblhd){
			$closednphnArr[$closenphnkey][] = empty($closenphnvalArr[$tblhd])?0:$closenphnvalArr[$tblhd];
		}
	}
	
	$closenwebQuryArr = $commonobj->getQry("select avg(ttc) as phone_ttc,".$type.$selectQry.",region from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and ttc < '9' AND case_origin = 'Web' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($closenwebQuryArr as $weeknamearr){
		$closecaseweb[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(int)$a=$weeknamearr['phone_ttc'];
	}
	foreach($closecaseweb as $closewebkey => $closewebvalArr){
		foreach($tablehead as $tblhd){
			$closedwebArr[$closewebkey][] = empty($closewebvalArr[$tblhd])?0:$closewebvalArr[$tblhd];
		}
	}
	
	//CSAT Summary
	$csatoverexpQuryArr = $commonobj->getQry("select avg(overall_experience) as overall_exp_avg,count(*) as survey_count,avg(loyalty_index) as loyelty_index_avg,".$type.$selectQry.",region from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($csatoverexpQuryArr as $weeknamearr){
		$csatoverexp[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=round($weeknamearr['overall_exp_avg'],2);
		$csatsurveycnt[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=round($weeknamearr['survey_count'],2);
		$csatloyeltyindx[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=round($weeknamearr['loyelty_index_avg'],2);
	}
	foreach($csatoverexp as $csatexpkey => $csatexp){
		foreach($tablehead as $tblhd){
			$csatoverexpArr[$csatexpkey][] = empty($csatexp[$tblhd])?0:$csatexp[$tblhd];
		}
	}
	foreach($csatsurveycnt as $csatsurveykey => $csatsurvey){
		foreach($tablehead as $tblhd){
			$csatsurveyArr[$csatsurveykey][] = empty($csatsurvey[$tblhd])?0:$csatsurvey[$tblhd];
		}
	}
	foreach($csatloyeltyindx as $csatloyeltykey => $csatloyelty){
		foreach($tablehead as $tblhd){
			$csatloyeltyArr[$csatloyeltykey][] = empty($csatloyelty[$tblhd])?0:$csatloyelty[$tblhd];
		}
	}
	
	//CSAT Rating%
	//Green
	$csatratgreenQuryArr = $commonobj->getQry("select count(*) as green_count,alert_type,".$type.$selectQry.",region from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Green' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($csatratgreenQuryArr as $weeknamearr){
		$csatgreen[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['green_count'];
	}
	foreach($csatgreen as $csatgreenkey => $csatgreenval){
		foreach($tablehead as $tblhd){
			$csatgreenArr[$csatgreenkey][] = empty($csatgreenval[$tblhd])?0:round(($csatgreenval[$tblhd]/$csatsurveycnt[$csatgreenkey][$tblhd])*100,2);
		}
	}
	//Red
	$csatratredQuryArr = $commonobj->getQry("select count(*) as red_count,alert_type,".$type.$selectQry.",region from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Red' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($csatratredQuryArr as $weeknamearr){
		$csatred[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['red_count'];
	}
	foreach($csatred as $csatredkey => $csatredval){
		foreach($tablehead as $tblhd){
			$csatredArr[$csatredkey][] = empty($csatredval[$tblhd])?0:round(($csatredval[$tblhd]/$csatsurveycnt[$csatredkey][$tblhd])*100,2);
		}
	}
	//Normal
	$csatratnorQuryArr = $commonobj->getQry("select count(*) as nor_count,alert_type,".$type.$selectQry.",region from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') AND alert_type = 'Normal' $QryCondition group by region,".$type.$selectQry." order by ".$type.$selectQry." asc");
	foreach($csatratnorQuryArr as $weeknamearr){
		$csatnor[$weeknamearr['region']][$weeknamearr[$type.$selectQry]]=(float)$a=$weeknamearr['nor_count'];
	}
	//print_r($csatnor);
	foreach($csatnor as $csatnorkey => $csatnorval){
		foreach($tablehead as $tblhd){
			$csatnorArr[$csatnorkey][] = empty($csatnorval[$tblhd])?0:round(($csatnorval[$tblhd]/$csatsurveycnt[$csatnorkey][$tblhd])*100,2);
		}
	}

	//echo "select ".$type.$selectQry.",region,customer_country from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id Asc";
	$csatstock = array('Red','Normal','Green');
	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",region,customer_country,alert_type from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id Asc");
	foreach($csatunQuryArr as $csatunArrval){
		$regionArr[$csatunArrval[$type.$selectQry]][$csatunArrval['region']][]=$csatunArrval['alert_type'];
		if($csatunArrval['customer_country'] !=''){
			$regionArrName[$csatunArrval['region']]=$csatunArrval['region'];
			$countryName[$selectrange][$csatunArrval['region']][$csatunArrval['customer_country']]=$csatunArrval['customer_country'];
		}
	}
	foreach ($tablehead as $tblkey => $headvalue) {
		$regionwiseArr[$headvalue]=empty($regionArr[$headvalue])?0:$commonobj->getarracount($regionArr[$headvalue]);
	}
	
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	     border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
			<div class='col-md-12' style='margin-top:12px'>
			    <div class="portlet">
			        <div class="portlet-body">
						<div class="form-group col-md-2 col-sm-4 col-xs-6">
							<select class="form-control submit" id="drop2"  name="projectwise">
							<?php
								$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
								echo '<option value="Overall">Overall Project</option>';
								foreach($drop1project as $drop1projectval){
									echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
								}
							?>
							</select>
							<script>  
								 jQuery("#drop2").val("<?php echo $projectwise ?>");
							</script>
						</div>
						<div class="form-group col-md-2 col-sm-4 col-xs-6">
							<select class="form-control submit" id="drop3"  name="productwise">
							<?php
								$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
								$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
								echo '<option value="Overall">Overall Que</option>';
								foreach($drop1overall as $drop1overallval){
									echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
								}
							?>
							</select>
							<script> 
								 jQuery("#drop3").val("<?php echo $productwise ?>");
							</script>
						</div>
						<div class="form-group col-md-2 col-sm-4 col-xs-6">
							<select class="form-control submit" id="drop4"  name="productgroup">
								<?php
									$Qery = $projectwise !='Overall' ? " Where wlan_ns = '$projectwise' " :'';
									$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open $Qery");
									echo '<option value="Overall">Overall Product</option>';
									foreach ($productName as $key => $value) {
										echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
									} 
								?>
							</select>
							<script> 
								 jQuery("#drop4").val("<?php echo $productgroup ?>");
							</script>
						</div>
			            <div class="form-group col-md-2 col-sm-4 col-xs-6">
			                <select class="form-control selectweek" id="drop5"  name="calendartype">
			                	<option value="Normal">Calendar</option>
			                	<option value="Fiscal">Fiscal</option>
			                </select>
			                <script> 
			                     jQuery("#drop5").val("<?php echo $calendartype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-4 col-xs-6">
			                <select class="form-control selectweek" id="drop6"  name="selecttype">
			                	<option value="Weekly">Weekly</option>
			                    <option value="Monthly">Monthly</option>
			                    <option value="Quarterly">Quarterly</option>
			                </select>
			                <script> 
			                     jQuery("#drop6").val("<?php echo $selecttype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-4 col-xs-6">
			                <select class="form-control submit" id="drop7"  name="selectrange"  onchange="reload()">
			                	<option value="">--- Select ---</option>
			                	<?php
									$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
									foreach ($drowpdownArr as $key => $value) {
										$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
										echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
									}
		                		?> 
			                </select>
			                <script> 
								jQuery("#drop7").val("<?php echo $selectrange ?>");
			                </script>
			            </div>
			        </div>
			    </div>
			</div> 
		</div>
		<?php 
		foreach ($tablehead as $inflowkey=> $rs) {	
            $weekdayname[]	=$rs;
            foreach ($regionArrName as $regkey => $regvalue) {
    			foreach ($csatstock as $csatkey => $csatvalues) {
    				$regionwiseArray[$regkey][$csatvalues][]=(float)$s=empty($regionwiseArr[$rs][$regvalue][$csatvalues])?0:number_format($regionwiseArr[$rs][$regvalue][$csatvalues]/array_sum($regionwiseArr[$rs][$regvalue])*100,2);
	    		}
	    	}
		}
		?>              		
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>Inflow & Closure</div>
		            <div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container1"></div>
					</div>
					
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container2"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container4"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container5"></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div id="container6"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>CSAT</div>
		            <div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="container10"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="container11"></div>
					</div>
					<!-- <div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container12"></div>
					</div> -->
				</div>
			</div>
		</div>
		
</form>
<?php 
include("includes/footer.php");
?>
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("#drop2").change(function(){
	    $("#drop3").val("Overall");
	    $("#drop4").val("Overall");
	});
	$('.submit').change(function(){
		document.getElementById("frmsrch").action = 'regionwise_trend.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	});
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	function reload(){
		document.getElementById("frmsrch").action = 'chart.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}

$(function() {
	Highcharts.setOptions({
	    lang: {
	        decimalPoint: '.',
	        thousandsSep: ''
	    },
	    animation: {
            duration: 3000,
            easing: 'easeOutBounce'
    	}
	});
var chart = new Highcharts.Chart({
	chart: {
            zoomType: 'xy',
            height: 300,
            renderTo: 'container'
	},
    title: {
    text: 'Open Cases',
    style:{
            color: 'black',
            fontSize: '12px'
        }       
    },
    legend: {
        itemStyle: {
            fontWeight: 'bold',
            fontSize: '11px'
        }
    },
    credits: {
        enabled: false
    }, 
	colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
    xAxis: {
        categories: <?php echo json_encode($weekdayname,true);?>,
    },
    plotOptions: {
	             column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 1px black'
	                    }
	                }
	            }

	        },

    tooltip: {
        //pointFormat: "{point.y}"
        shared: true
    },

     series: [
		 <?php foreach($totopencaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:-10
                } 
			},
		 <?php } ?>]
	});

var chart = new Highcharts.Chart({
	chart: {
            zoomType: 'xy',
            height: 300,
            renderTo: 'container1'
	},
    title: {
    text: 'Closed Cases',
    style:{
            color: 'black',
            fontSize: '12px'
        }       
    },
    legend: {
        itemStyle: {
            fontWeight: 'bold',
            fontSize: '11px'
        }
    },
    credits: {
        enabled: false
    }, 
	//colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	colors: ['#FE4469', '#263249', '#0000FF', '#55FF2A', '#FF0000'],

    xAxis: {
        categories: <?php echo json_encode($weekdayname,true);?>,
    },
    plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 1px black'
	                    }
	                }
	            }
	        },

    tooltip: {
        //pointFormat: "{point.y}"
        shared: true
    },

     series: [
		 <?php foreach($closedcaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:-10
                } 
			},
		 <?php } ?>]
	});
	
	$('#container2').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Closed Cases Vs Avg. TTC ',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#FF2AAA', '#55FF2A', '#FFAA55', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                }
	            },
				column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 1px black'
	                    }
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [
			<?php foreach($closedcaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($closedttcArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-TTC',
	            type: 'spline',
				yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>]
		});
        $('#container4').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	         text: 'Close Cases Vs SDC',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#AA002B','#AA00FF', '#0000FF', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            },
				column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                    style: {
	                        textShadow: '0 0 1px black'
	                    }
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
					format: '{value} %'
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            },
					column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [
			<?php foreach($closedcaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($closedsdcArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-SDC',
	            type: 'spline',
				yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
				tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                    format:"{y}%",
	                }
	        },
			<?php } ?>
			/* {
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>,
	        },{
	            name: 'SDC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closedsdcArr,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                    format:"{y}%",
	                }
	        } */]
        });
        $('#container5').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Close Cases Vs RMA%',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#00D5AA','#2B0080', '#800055', '#800055'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        },{ // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [<?php foreach($closedcaseArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($closedrmaArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-RMA',
	            type: 'spline',
				yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			
			/* {
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>,
	        },{
	            name: 'RMA',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closedrmaArr,true);?>,
	        } */]
        });
        $('#container6').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	         text: 'TTC < 9Days - Source wise',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#0D47A1','#808000', '#A34F31', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                     //format:"{y}%"
	                }	
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        },{ // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {                
	                format:"{value}%",
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [<?php foreach($closedovttcArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-Overall',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($closednphnArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-Phone',
	            type: 'spline',
				yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
				dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        },
			<?php } ?>
			<?php foreach($closedwebArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?>-Web',
	            type: 'spline',
				yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
				dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        },
			<?php } ?>
			
			/* {
	            name: 'Overall TTC',
	            type: 'column',
	            data: <?php echo json_encode($closettcArr,true);?>,
	        },{
	            name: 'Phone TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closednphnArr,true);?>,
	             dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        },{
	            name: 'Web TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closedwebArr,true);?>,
	            dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        } */]
        });
    $('#container7').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        
	         text: 'Closed Cases Vs SDC',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        //colors: ['#263249', '#D500AA', '#2AFFAA','#F92672','#263249'],
	        colors: ['#D500AA','#2660BA', '#141414','#F92672','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format:"{value}%",
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {                
	                format:"{value}%",
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'SDC ',
	            type: 'column',
	            data: <?php echo json_encode($closesdcArr,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'SDC-Phone',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closephonesdc,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'SDC-Web',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closewebsdc,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                    format:"{y}%",
	                }
	        }]
        });
    $('#container8').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'RMA % & RMA Case',

	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#F92672', '#272822', '#2AFFAA','#F92672','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                format:"{value}%"
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'RMA Case ',
	            type: 'column',
	            data: <?php echo json_encode($closermacase,true);?>,
	        },{
	            name: 'RMA %',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closermaper,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        }]
        });
    $('#container9').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Closed Cases Vs Productivity ',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#FF2AAA', '#55FF2A', '#FFAA55', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>
	        },{
	            name: 'Productivity',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($productivity,true);?>
	        }]
        });
        $('#container10').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'CSAT - Summary',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#218C8D', '#39C1FB', '#272822','#D50055','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }
	            },
				column: {
						stacking: 'normal',
						dataLabels: {
							enabled: true,
							allowOverlap:true,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
							style: {
								textShadow: '0 0 1px black'
							}
						}
					}
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },	        
	        series: [<?php foreach($csatsurveyArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?> - Survey Count',
	            type: 'column',
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($csatoverexpArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?> - Overall Experience',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			<?php foreach($csatloyeltyArr as $region => $regionArr){ ?>
			{
	            name: '<?php echo $region; ?> - Loyelty Index',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($regionArr,true);?>,
	        },
			<?php } ?>
			/* {
	            name: 'Survey Count',
	            type: 'column',
	            data: <?php echo json_encode($surveycountArr,true);?>
	        },{
	            name: 'Overall Experience',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($csatoverexpArr,true);?>,
	        },{
	            name: 'Loyalty Index',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($loyaltyindexArr,true);?>,
	        } */]
        });

        Highcharts.chart('container11', {
		    chart: {
		        type: 'column',
		        height: 300
		    },

		    title: {
		        text: 'Region wise - CSAT %'
		    },

		    xAxis: {
		        categories: <?php echo json_encode($weekdayname,true);?>,
		    },
		    yAxis: {
		        allowDecimals: true,
		        min: 0,
		        max:100,
		        labels: {                
	                format : "{value}%"
	            },
	            title: {
	                text: '',
	            },
		    },
		    credits: {
	            enabled: false
	        },

		    tooltip: {
		        formatter: function () {
		            return '<b>' + this.x + '</b><br/>' +
		                this.series.name + ': ' + this.y +'%'+'<br/>';
		        }
		    },
		    colors: ['#FF0000','#FF7F00','#1ED570'],
		    plotOptions: {
		        column: {
		            stacking: 'normal'
		        },
		        series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                		format:"{y}%",

	                }
	            },
		    },

		    series: [
			<?php
			$i=0;
			foreach($regionwiseArray as $wkkey => $wkArr){
				foreach($wkArr as $key => $lastArr){
					?> 
					{
						name: '<?php echo $wkkey .'-'. $key; ?>',
						data: <?php echo json_encode($lastArr);?>,
						stack: '<?php echo $i; ?>'
					},
					<?php
				}
				$i++;
			}
			?>
			]
		});
	 // $('#container11').highcharts({
	 //    	chart: {
	 //            zoomType: 'xy',
	 //            height: 300
	 //        },
	 //        title: {
	 //        text: 'CSAT - Rating %',
	 //        style:{
	 //                color: 'black',
	 //                fontSize: '12px'
	 //            }       
	 //        },
	 //        legend: {
	 //            itemStyle: {
	 //                fontWeight: 'bold',
	 //                fontSize: '11px'
	 //            }
	 //        },
	 //        credits: {
	 //            enabled: false
	 //        },
	 //        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	 //        xAxis: [{
	 //            categories: <?php echo json_encode($weekdayname,true);?>,
	 //            crosshair: true
	 //        }],
	 //         plotOptions: {
	 //            series: {
	 //                dataLabels: {
	 //                    enabled: true,
	 //                     allowOverlap:true,
	 //                }	
	 //            },
		// 		column: {
		// 				stacking: 'normal',
		// 				dataLabels: {
		// 					enabled: true,
		// 					allowOverlap:true,
		// 					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
		// 					style: {
		// 						textShadow: '0 0 1px black'
		// 					}
		// 				}
		// 			}
	 //        },
	 //        yAxis: [{ // Primary yAxis
	 //            labels: {
	 //                format: '{value} %'
	 //            },
	 //            title: {
	 //                text: '',
	 //                style: {
	 //                    color: Highcharts.getOptions().colors[1]
	 //                }
	 //            }
	 //        }, { // Secondary yAxis
	 //            title: {
	 //                text: '',
	 //                style: {
	 //                    color: Highcharts.getOptions().colors[1]
	 //                }
	 //            },
	 //            labels: {
	 //                style: {
	 //                    color: Highcharts.getOptions().colors[1]
	 //                }
	 //            },
	 //            plotOptions: {
		//             series: {
		//                 dataLabels: {
		//                     enabled: true,
		//                     format:'{y}',
		//                 }
		//             }
		//         },
	 //            opposite: true
	 //        }],
	 //        tooltip: {
	 //            shared: true
	 //        },
	 //        series: [
		// 	<?php foreach($csatgreenArr as $region => $regionArr){ ?>
		// 	{
	 //            name: '<?php echo $region; ?> - Green %',
	 //            type: 'column',
	 //            data: <?php echo json_encode($regionArr,true);?>,
		// 		tooltip: {
	 //                valueSuffix: '%'
	 //            },
	 //            dataLabels: {
	 //                format:"{y}%",
	 //            }
	 //        },
		// 	<?php } ?>
			
		// 	<?php foreach($csatnorArr as $region => $regionArr){ ?>
		// 	{
	 //            name: '<?php echo $region; ?> - Normal %',
	 //            type: 'column',
	 //            data: <?php echo json_encode($regionArr,true);?>,
		// 		tooltip: {
	 //                valueSuffix: '%'
	 //            },
	 //            dataLabels: {
	 //                format:"{y}%",
	 //            }
	 //        },
		// 	<?php } ?>
			
		// 	<?php foreach($csatredArr as $region => $regionArr){ ?>
		// 	{
	 //            name: '<?php echo $region; ?> - Red %',
	 //            type: 'column',
	 //            data: <?php echo json_encode($regionArr,true);?>,
		// 		tooltip: {
	 //                valueSuffix: '%'
	 //            },
	 //            dataLabels: {
	 //                format:"{y}%",
	 //            }
	 //        },
		// 	<?php } ?>
			
		// 	<?php foreach($csatsurveyArr as $region => $regionArr){ ?>
		// 	{
	 //            name: '<?php echo $region; ?> - Survey Count',
	 //            type: 'spline',
	 //            yAxis:1,
	 //            data: <?php echo json_encode($regionArr,true);?>,
	 //        },
		// 	<?php } ?>
		// 	]
  //       }); 
});

	


'use strict';
// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(https://www.highcharts.com/samples/graphics/sand.png)';
});


Highcharts.theme = {
   colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
      '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: 'Signika, serif'
      }
   },
   title: {
      style: {
         color: 'black',
         fontSize: '16px',
         fontWeight: 'bold'
      }
   },
   subtitle: {
      style: {
         color: 'black'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   plotOptions: {
      series: {
         shadow: true
      },
      candlestick: {
         lineColor: '#404048'
      },
      map: {
         shadow: false
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#D0D0D8'
      }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#C0C0C8',
         'stroke-width': 1,
         states: {
            select: {
               fill: '#D0D0D8'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#C0C0C8'
   },

   // General
   background2: '#E0E0E8'

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
</script>
</script>
