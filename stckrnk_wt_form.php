<?php if($ltype == "l1"){
	if(in_array($productgroup,$productgroupArr1)){ ?>
		<div class="portlet">
			<div class="portlet-body">
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
					<label>Weightage</label>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					CSAT Green
					<input type="text" class="form-control textchange" required id="text1" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="loocsatgreen_weightage">
					<script>  
						jQuery("#text1").val("<?php echo $_POST['loocsatgreen_weightage'] ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Red
					<input type="text" class="form-control textchange" required id="text6" value="<?php echo $loored_weightage; ?>" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="loored_weightage">
					<script>  
						jQuery("#text6").val("<?php echo $loored_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Closure
					<input type="text" class="form-control textchange" required id="text2" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="looclosure_weightage">
					<script> 
						jQuery("#text2").val("<?php echo $looclosure_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Closure
					<input type="text" class="form-control textchange" required id="text10" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="gscclosure_weightage">
					<script> 
						jQuery("#text10").val("<?php echo $gscclosure_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					TTC
					<input type="text" class="form-control textchange" required id="text9" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="loottc_weightage">
					<script> 
						jQuery("#text9").val("<?php echo $loottc_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Soft Skills
					<input type="text" class="form-control textchange" required id="text3" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="softskill_weightage">
					<script> 
						jQuery("#text3").val("<?php echo $softskill_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Process Quality
					<input type="text" class="form-control textchange" required id="text4" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="looprocessquality_weightage">
					<script> 
						jQuery("#text4").val("<?php echo $looprocessquality_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Escalation
					<input type="text" class="form-control textchange" required id="text5" name="looescalation_weightage" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text5").val("<?php echo $looescalation_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Attendance
					<input type="text" class="form-control" id="text7" required name="atten_weightage" value="<?php echo $atten_weightage; ?>" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text7").val("<?php echo $atten_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					Backlog 
					<input type="text" class="form-control" id="text8" required name="backlog_weightage" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text8").val("<?php echo $backlog_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					Total
					<input type="text" class="form-control" id="total_weightage" name="total_weightage" value="<?php echo $total_weightage;?>" readonly>
					<script> 
						//jQuery("#text8").val("<?php echo $backlog_weightage ?>");
					</script>
				</div>
			</div>
			<div class="portlet-body">
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
					<label>Target</label>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text11" onkeypress="return isNumber(event)" value="<?php echo $loogreen_target; ?>" onchange="validate_empty(this.id)" name="loogreen_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control textchange" required id="text61" onkeypress="return isNumber(event)" value="<?php echo $loored_target; ?>" onchange="validate_empty(this.id)" name="loored_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text21" onkeypress="return isNumber(event)" value="<?php echo $looclosures_target; ?>" onchange="validate_empty(this.id)" name="looclosures_target">
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text101" onkeypress="return isNumber(event)" value="<?php echo $gscclosures_target; ?>" onchange="validate_empty(this.id)" name="gscclosures_target">
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text91" onkeypress="return isNumber(event)" value="<?php echo $loottc_target; ?>" onchange="validate_empty(this.id)" name="loottc_target">
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text31" onkeypress="return isNumber(event)" value="<?php echo $ss_target; ?>" onchange="validate_empty(this.id)" name="ss_target">
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text41" onkeypress="return isNumber(event)" value="<?php echo $loopq_target; ?>" onchange="validate_empty(this.id)" name="loopq_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text51" onchange="validate_empty(this.id)" name="looesc_target" value="<?php echo $looesc_target; ?>" >
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text71" onchange="validate_empty(this.id)" name="atten_target" value="<?php echo $atten_target; ?>" >
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					<input type="text" class="form-control" required id="text81" onchange="validate_empty(this.id)" value="<?php echo $backlog_target; ?>" name="backlog_target" >
				</div>-->
			</div>
		</div>
<?php }elseif($productgroup == "THD"){ ?>					
		<div class="portlet">
			<div class="portlet-body">
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
					<label>Weightage</label>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					CSAT Green
					<input type="text" class="form-control textchange" required id="text1" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="lotcsatgreen_weightage">
					<script>  
						jQuery("#text1").val("<?php echo $_POST['lotcsatgreen_weightage'] ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Red
					<input type="text" class="form-control textchange" required id="text6" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="lotred_weightage">
					<script>  
						jQuery("#text6").val("<?php echo $lotred_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Closure
					<input type="text" class="form-control textchange" required id="text2" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="lotclosure_weightage">
					<script> 
						jQuery("#text2").val("<?php echo $lotclosure_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Closure
					<input type="text" class="form-control textchange" required id="text10" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="gscclosure_weightage">
					<script> 
						jQuery("#text10").val("<?php echo $gscclosure_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					TTC
					<input type="text" class="form-control textchange" required id="text9" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="lotttc_weightage">
					<script> 
						jQuery("#text9").val("<?php echo $lotttc_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Soft Skills
					<input type="text" class="form-control textchange" required id="text3" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="softskill_weightage">
					<script> 
						jQuery("#text3").val("<?php echo $softskill_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Process Quality
					<input type="text" class="form-control textchange" required id="text4" onkeypress="return isNumber(event)" onchange="validate_weightage(this.id)" name="lotprocessquality_weightage">
					<script> 
						jQuery("#text4").val("<?php echo $lotprocessquality_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Escalation
					<input type="text" class="form-control textchange" required id="text5" name="lotescalation_weightage" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text5").val("<?php echo $lotescalation_weightage ?>");
					</script>
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					Attendance
					<input type="text" class="form-control" id="text7" required name="atten_weightage" value="<?php echo $atten_weightage; ?>" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text7").val("<?php echo $atten_weightage ?>");
					</script>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					Backlog 
					<input type="text" class="form-control" id="text8" required name="backlog_weightage" onchange="validate_empty(this.id)">
					<script> 
						jQuery("#text8").val("<?php echo $backlog_weightage ?>");
					</script>
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					Total
					<input type="text" class="form-control" id="total_weightage" name="total_weightage" value="<?php echo $total_weightage;?>" readonly>
					<script> 
						//jQuery("#text8").val("<?php echo $backlog_weightage ?>");
					</script>
				</div>
			</div>
			<div class="portlet-body">
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="width:5%;">
					<label>Target</label>
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text11" onkeypress="return isNumber(event)" value="<?php echo $lotgreen_target; ?>" onchange="validate_empty(this.id)" name="lotgreen_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control textchange" required id="text61" onkeypress="return isNumber(event)" value="<?php echo $lotred_target; ?>" onchange="validate_empty(this.id)" name="lotred_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text21" onkeypress="return isNumber(event)" value="<?php echo $lotclosures_target; ?>" onchange="validate_empty(this.id)" name="lotclosures_target">
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text101" onkeypress="return isNumber(event)" value="<?php echo $gscclosures_target; ?>" onchange="validate_empty(this.id)" name="gscclosures_target">
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text91" onkeypress="return isNumber(event)" value="<?php echo $lotttc_target; ?>" onchange="validate_empty(this.id)" name="lotttc_target">
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text31" onkeypress="return isNumber(event)" value="<?php echo $ss_target; ?>" onchange="validate_empty(this.id)" name="ss_target">
				</div>-->
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text41" onkeypress="return isNumber(event)" value="<?php echo $lotpq_target; ?>" onchange="validate_empty(this.id)" name="lotpq_target">
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text51" onchange="validate_empty(this.id)" name="lotesc_target" value="<?php echo $lotesc_target; ?>" >
				</div>
				<!--<div class="form-group col-md-1 col-sm-2 col-xs-6">
					<input type="text" class="form-control" required id="text71" onchange="validate_empty(this.id)" name="atten_target" value="<?php echo $atten_target; ?>" >
				</div>
				<div class="form-group col-md-1 col-sm-2 col-xs-6" style="padding-right:0px;">
					<input type="text" class="form-control" required id="text81" onchange="validate_empty(this.id)" value="<?php echo $backlog_target; ?>" name="backlog_target" >
				</div>-->
			</div>
		</div>


<?php } ?>
					