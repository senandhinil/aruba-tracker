<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['metrics'] || $_POST['metrics_second'] || $_POST['selectrange'] || $_POST['manager'] || $_POST['tlname']){
	//$projectwise 	= 	$_POST['projectwise'];
	//$productwise   	=	$_POST['productwise'];
	//$productgroup 	= 	$_POST['productgroup'];
	$metrics 	= 	$_POST['metrics'];
	$metrics_second 	= 	$_POST['metrics_second'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	//$regionwise 	= 	$_POST['regionwise'];
	if($selecttype == 'Daily'){
		$selectrange    =	date('m/j/Y',strtotime($_POST['picked_date']));
	}else{
		$selectrange    =	$_POST['selectrange'];
	}
	$manager    	=	$_POST['manager'];
	$tlname   		=	$_POST['tlname'];
	//$enggname   	=	$_POST['enggname'];
}else{
	$selectrange    =  !empty($selectrange)?$selectrange:current($currentweek);
	//$projectwise 	=  !empty($projectwise)?$projectwise:"Overall";
	//$productwise 	=  !empty($productwise)?$productwise:"Overall";
	//$productgroup 	=  !empty($productgroup)?$productgroup:"Overall";
	//$regionwise 	=  !empty($regionwise)?$regionwise:"Overall";
	$metrics 	=  !empty($metrics)?$metrics:"alert_type";
	$metrics_second 	=  !empty($metrics_second)?$metrics_second:"Overall";
	$calendartype 	=  !empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=  !empty($selecttype)?$selecttype:"Weekly";
	
	$manager    	=	empty($manager)?"Overall":$manager;
	$tlname    		=	empty($tlname)?"Overall":$tlname;
	//$enggname    	=	empty($enggname)?"Overall":$enggname;
} 
	
//if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}
	//if($metrics_second != "Overall" && $metrics == 'question'){
	//$QryCondition1.= $projectwise !='Overall' ?" and wlan_ns='".$projectwise."'":'';
	//$QryCondition1.= $productwise!='Overall'?" and que_new='".$productwise."'":'';
	//$QryCondition1.= $productgroup!='Overall'?" and product_group='".$productgroup."'":'';
	//$QryCondition1.= $regionwise!='Overall'?" and region='".$regionwise."'":'';	
	$QryCondition.= $manager!='Overall'?" and manager_name='".$manager."'":'';	
	$QryCondition.= $tlname!='Overall'?" and team='".$tlname."'":'';	
	//$QryCondition.= $enggname!='Overall'?" and case_owner='".$enggname."'":'';	
		
	$headertotalArr = $commonobj->arrayColumn($commonobj->getQry("select distinct ".$type.$selectQry." from aruba_csat order by id asc"),'',$type.$selectQry);
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
		   if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	
	//Metrics And Sub Metrics option Start
	$opt = '';
	if($metrics == "question"){
		$msArr = array("ease_of_access_score","technical_ability_score","non_technical_performance_score","kept_informed_score","solution_time_score");
	}elseif($metrics == "bucket"){
		$msArr = array("<6 Month","6 - 1 Year","1 - 2 Year","2 - 3 Year","3 - 4 Year","> 5 Year");
	}else{
		$msArr = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct ".$metrics." from aruba_csat where ".$metrics." != '' AND $type$selectQry in ('".implode("','", $tablehead)."')"),'',$metrics);
		if($metrics == 'alert_type' || $metrics == 'product_easy_of_use'){
			array_unshift($msArr,"Overall");
		}
	}
	if(!in_array($metrics_second,$msArr)){
		$metrics_second = current($msArr);
	}
	foreach ($msArr as $key => $value) {
		if($metrics == "question"){
			$opt .= '<option value="'.$value.'">'.ucfirst(str_replace("_"," ",$value)).'</option>';
		}else{
			$opt .= '<option value="'.$value.'">'.$value.'</option>';
		}
	}
	//Metrics And Sub Metrics option End
	
	$managerList = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct manager_name from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by manager_name asc"),'','manager_name');
								
	$tlQry = $manager == 'Overall' ? '' : " and manager_name ='$manager'";
	$tllist = $commonobj->arrayColumn($commonobj->getQry("SELECT distinct team from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry $tlQry  order by team asc"),'','team');
	array_unshift($tllist,"Overall");
	if(!in_array($tlname,$tllist)){
		$tlname = current($tllist);
	}
	
	if($selecttype == "Daily"){
		$avg = "";
	}else{
		$avg = "(Avg.)";
	}
	
	if($metrics != 'question'){
		$QryCondition2 .= $metrics_second == 'Overall' ? '' :"AND $metrics = '$metrics_second'";
		if($metrics != 'product_easy_of_use'){
			$metricss = 'alert_type';
		}else{
			$metricss = $metrics;
		}
	}else if($metrics_second != "Overall" && $metrics == 'question'){
		$metricss = $metrics_second;
	}
	
	if($metrics != 'alert_type' && $metrics != 'product_easy_of_use'){
		$backlog_managersurvey_count = $commonobj->getQry("select manager_name,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition $QryCondition2 group by $type$selectQry,manager_name");
		
	}else{
		$backlog_managersurvey_count = $commonobj->getQry("select manager_name,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition group by $type$selectQry,manager_name");
	}
	$backlog_summ_manager = $commonobj->getQry("select manager_name,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition $QryCondition2 group by $type$selectQry,manager_name,$metricss order by manager_name asc");
	
	if($metrics != 'alert_type' && $metrics != 'product_easy_of_use'){
		$backlog_teamsurvey_count = $commonobj->getQry("select team,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition $QryCondition2 group by $type$selectQry,team");
		
	}else{
		$backlog_teamsurvey_count = $commonobj->getQry("select team,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition group by $type$selectQry,team");
	}
	$backlog_summ_team = $commonobj->getQry("select manager_name,team,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."') $QryCondition $QryCondition2 group by $type$selectQry,team,$metricss order by team asc");
	
	if($metrics != 'alert_type' && $metrics != 'product_easy_of_use'){
		$backlog_enggsurvey_count = $commonobj->getQry("select case_owner,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition $QryCondition2 group by $type$selectQry,case_owner");
		
	}else{
		$backlog_enggsurvey_count = $commonobj->getQry("select case_owner,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."')  $QryCondition group by $type$selectQry,case_owner");
	}
	$backlog_summ_engg = $commonobj->getQry("select case_owner,manager_name,team,$metricss,count($metricss) as cnt,$type$selectQry from aruba_csat where $type$selectQry in ('".implode("','", $tablehead)."') $QryCondition $QryCondition2 group by $type$selectQry,case_owner,$metricss order by case_owner asc");
		
	foreach($backlog_managersurvey_count as $mngersurveyArr){
		$mngrsurveyresArr[$mngersurveyArr['manager_name']][$mngersurveyArr[$type.$selectQry]]= $mngersurveyArr['cnt'];
	}
	foreach($backlog_summ_manager as $mngerArr){
		if($metrics != 'alert_type' && trim($metrics) != 'product_easy_of_use'){
			foreach($backlog_managersurvey_count as $mngersurveyArr){
				$mngrresArr[$mngersurveyArr['manager_name']]['Survey Count'][$mngersurveyArr[$type.$selectQry]]= $mngersurveyArr['cnt'];
			}
		}else{
			if($metrics_second == 'Overall'){
				foreach($backlog_managersurvey_count as $mngersurveyArr){
					$mngrresArr[$mngersurveyArr['manager_name']]['Survey Count'][$mngersurveyArr[$type.$selectQry]]= $mngersurveyArr['cnt'];
				}
			}
		}
		$mngrresArr[$mngerArr['manager_name']][$mngerArr[$metricss]][$mngerArr[$type.$selectQry]]= $mngerArr['cnt'];
	}
	foreach($backlog_teamsurvey_count as $teamsurveyArrr){
		$teamsurveyArr[$teamsurveyArrr['team']][$teamsurveyArrr[$type.$selectQry]]= $teamsurveyArrr['cnt'];
	}
	foreach($backlog_summ_team as $teamArr){
		if($metrics != 'alert_type' && $metrics != 'product_easy_of_use'){
			foreach($backlog_teamsurvey_count as $teamsurveyArrr){
				$teamresArr[$teamsurveyArrr['team']]['Survey Count'][$teamsurveyArrr[$type.$selectQry]]= $teamsurveyArrr['cnt'];
			}
		}else{
			if($metrics_second == 'Overall'){
				foreach($backlog_teamsurvey_count as $teamsurveyArrr){
					$teamresArr[$teamsurveyArrr['team']]['Survey Count'][$teamsurveyArrr[$type.$selectQry]]= $teamsurveyArrr['cnt'];
				}
			}
			
		}
		$teamresArr[$teamArr['team']][$teamArr[$metricss]][$teamArr[$type.$selectQry]]= $teamArr['cnt'];
	}
	//print_r($teamresArr);
	foreach($backlog_enggsurvey_count as $enggsurveyArrr){
		$enggsurveyArr[$enggsurveyArrr['case_owner']][$enggsurveyArrr[$type.$selectQry]]= $enggsurveyArrr['cnt'];
	}
	foreach($backlog_summ_engg as $enggArr){
		if($metrics != 'alert_type' && $metrics != 'product_easy_of_use'){
			foreach($backlog_enggsurvey_count as $enggsurveyArrr){
				$enggresArr[$enggsurveyArrr['case_owner']]['Survey Count'][$enggsurveyArrr[$type.$selectQry]]= $enggsurveyArrr['cnt'];
			}
		}else{
			if($metrics_second == 'Overall'){
				foreach($backlog_enggsurvey_count as $enggsurveyArrr){
					$enggresArr[$enggsurveyArrr['case_owner']]['Survey Count'][$enggsurveyArrr[$type.$selectQry]]= $enggsurveyArrr['cnt'];
				}
			}
		}
		$enggresArr[$enggArr['case_owner']][$enggArr[$metricss]][$enggArr[$type.$selectQry]]= $enggArr['cnt'];
	}
	// }else{
		
	// }
//}
//print_r($enggsurveyArr);
$sortorder = array('Survey Count','Green','Normal','Red','NA');
include "includes/header.php";
?>
<link href="assets/jquery-ui.css" rel="stylesheet">
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-sm-2 {
	   		width: 16.667%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
	.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
    	margin-right: 0px;
	}
	tr, th, td{
		text-align:center;
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
		<div class='col-md-12' style='margin-top:12px'>
		    <div class="portlet">
		        <div class="portlet-body">
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="manager"  name="manager" onchange="reload()">
		                	<option value="Overall">Overall Manager</option>';
		                	<?php
								//$Qry.= $projectwise == 'Overall' ? '':" and wlan_ns = '$projectwise'";
		                		//$Qry.= $productwise == 'Overall' ? '':" and que_new = '$productwise'";
		                		//$Qry.= $productgroup == 'Overall' ? '':" and product_group = '$productgroup'";
		                		//$Qry.= $regionwise == 'Overall' ? '':" and region = '$regionwise'";
								//echo "SELECT distinct manager_name from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $Qry order by manager_name asc";
		                		
								foreach ($managerList as $key => $value) {
									echo'<option value="'.$value.'">'.$value.'</option>';
								}
	                		?> 
		                </select>
		                <script> 
		                     jQuery("#manager").val("<?php echo $manager ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
					    <select class="form-control" id="tlname"  name="tlname" onchange="reload()">
							
							<?php
								
								foreach ($tllist as $key => $value) {
									echo'<option value="'.$value.'">'.$value.'</option>';
								}
							?> 
		                </select>
		                <script> 
		                     jQuery("#tlname").val("<?php echo $tlname ?>");
		                </script>
		            </div>
					<!--<div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="enggname"  name="enggname" onchange="reload()">
		                	<option value="Overall">Overall Engineer</option>
							<?php
								if ( $manager != 'Overall' && $tlname == 'Overall') {
		                			$engQry = " and manager_name = '$manager' ";
		                		} else if($manager != 'Overall' && $tlname != 'Overall') {
		                			$engQry = " and team = '$tlname' ";
		                		}else if($manager == 'Overall' && $tlname != 'Overall'){
		                			$engQry = " and team = '$tlname' ";
		                		}
								$tllist = $commonobj->getQry("SELECT distinct case_owner from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."')  $engQry $Qry order by case_owner asc");
								foreach ($tllist as $key => $value) {
									echo "<option value='".$value['case_owner']."'>".$value['case_owner']."</option>";
								}
							?> 
		                </select>
		                <script> 
		                     jQuery("#enggname").val("<?php echo $enggname ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop2"  name="projectwise" onchange="reload()">
	                    <?php
							$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
	                    	echo '<option value="Overall">Overall Project</option>';
							foreach($drop1project as $drop1projectval){
							    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
							}
	                    ?>
	                    </select>
		                <script> 
		                     jQuery("#drop2").val("<?php echo $projectwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop3"  name="productwise" onchange="reload()">
		                <?php
		                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
		                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
							echo '<option value="Overall">Overall Que</option>';
							foreach($drop1overall as $drop1overallval){
								echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
							}
		                ?>
		                </select>
		                <script> 
		                     jQuery("#drop3").val("<?php echo $productwise ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop4"  name="productgroup" onchange="reload()">
		                	<?php
								
		                		//$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                		//$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
								//$Qry.= $regionwise!='Overall'?" and region='".$regionwise."'":'';
		                		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_csat Where $Qery AND product_group != 'NA'");

								echo '<option value="Overall">Overall Product</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop4").val("<?php echo $productgroup ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop8"  name="regionwise" onchange="reload()">
		                	<?php
								$productName = $commonobj->getQry("SELECT distinct region from aruba_csat Where $Qery");

								echo '<option value="Overall">Overall Region</option>';
		                		foreach ($productName as $key => $value) {
		                			echo '<option value="'.$value['region'].'">'.$value['region'].'</option>';
		                		} 
		                	?>
		                </select>
		                <script> 
		                     jQuery("#drop8").val("<?php echo $regionwise; ?>");
		                </script>
		            </div>-->	
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control" id="drop9"  name="metrics" onchange="reload()">
							<option value="">--Select--</option>
		                	<option value="alert_type">Rating %</option>
							<option value="region">Region</option>
							<option value="product_easy_of_use">Product EDC</option>
							<option value="customer_country">Country</option>
							<option value="question">Question</option>
							<option value="case_origin">Case Origin</option>
							<option value="bucket">Tenure</option>
		                </select>
		                <script> 
		                     jQuery("#drop9").val("<?php echo $metrics ?>");
		                </script>
		            </div>
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="drop10"  name="metrics_second" onchange="reload()">
							<?php
								echo $opt;
							?>
		                </select>
		                <script> 
		                     jQuery("#drop10").val("<?php echo $metrics_second ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop5"  name="calendartype">
		                	<option value="Normal">Calendar</option>
		                	<option value="Fiscal">Fiscal</option>
		                </select>
		                <script> 
		                     jQuery("#drop5").val("<?php echo $calendartype ?>");
		                </script>
		            </div>
		            
					<div class="form-group col-sm-2 col-md-3 col-xs-6">
		                <select class="form-control selectweek" id="drop6"  name="selecttype" >
							<option value="Weekly">Weekly</option>
		                    <option value="Monthly">Monthly</option>
		                    <option value="Quarterly">Quarterly</option>
		                </select>
		                <script> 
		                     jQuery("#drop6").val("<?php echo $selecttype ?>");
		                </script>
		            </div>
		            <div class="form-group col-sm-2 col-md-3 col-xs-6">
						<select class="form-control" id="drop7"  name="selectrange"  onchange="reload()" >
		                	<option value="">--- Select ---</option>';
		                	<?php
								$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_csat order by id desc");
								foreach ($drowpdownArr as $key => $value) {
									echo'<option value="'.$value[$type.$selectQry].'" $selected>'.$value[$type.$selectQry].'</option>';
								}
	                		?> 
		                </select>
						<script> 
		                     jQuery("#drop7").val("<?php echo $selectrange ?>");
		                </script>
						
					 </div>
		        </div>
		    </div>
		</div>    
	</div>
	<?php 
	foreach ($tablehead as $inflowkey=> $rs) {	
        $weekdayname[]	=$rs;	
     }
	?>  
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Manager Wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Team Manager</th>
					<th scope="col">Metrics</th>
					<?php foreach($tablehead as $tblkey => $tblvalue){ ?>
						<th scope="col"><?php echo $tblvalue; ?></th>
					<?php } ?>
				  </tr>
		        </thead>
		        <tbody>
		          	<?php 
					foreach($mngrresArr as $mngkey => $mngArr){
						echo '<tr><td rowspan='.count($mngArr).'>'.$mngkey.'</td>';
						foreach($sortorder as $value){
							foreach($mngArr as $reskey => $resArr){
								if($reskey == $value){
									echo '<td>'.$reskey.'</td>';
									foreach($tablehead as $tblkey => $tblvalue){
										if($mngrsurveyresArr[$mngkey][$tblvalue]){
											if($reskey != 'Survey Count'){
												$val = empty($resArr[$tblvalue])?'0%':round(($resArr[$tblvalue]/$mngrsurveyresArr[$mngkey][$tblvalue])*100).'%';
											}else{
												$val = empty($resArr[$tblvalue])?0:$mngrsurveyresArr[$mngkey][$tblvalue];
											}
										}else{
											$val = '-';
										}
										echo '<td>'.$val.'</td>';
									}
								echo '</tr>';
								}
							} 
						}
					} ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Team Leader Wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Team Lead</th>
					<th scope="col">Metrics</th>
					<?php foreach($tablehead as $tblkey => $tblvalue){ ?>
						<th scope="col"><?php echo $tblvalue; ?></th>
					<?php } ?>
				  </tr>
		        </thead>
		        <tbody>
		          	<?php 
					foreach($teamresArr as $mngkey => $mngArr){
						echo '<tr><td rowspan='.count($mngArr).'>'.$mngkey.'</td>';
						foreach($sortorder as $value){
							foreach($mngArr as $reskey => $resArr){
								if($reskey == $value){
									echo '<td>'.$reskey.'</td>';
									foreach($tablehead as $tblkey => $tblvalue){
										if($teamsurveyArr[$mngkey][$tblvalue]){
											if($reskey != 'Survey Count'){
												$val = empty($resArr[$tblvalue])?'0%':round(($resArr[$tblvalue]/$teamsurveyArr[$mngkey][$tblvalue])*100).'%';
											
											}else{
												$val = empty($teamsurveyArr[$mngkey][$tblvalue])?0:$teamsurveyArr[$mngkey][$tblvalue];
											}
										}else{
											$val = '-';
										}
										echo '<td>'.$val.'</td>';
									}
									echo '</tr>';
								}
								
							}
						}						
					} ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	<div class="portlet box green-jungle">
	  	<div class="portlet-title">
		    <div class="caption">
		      <i class="fa fa-user"></i>Engineer wise </div>
		    <div class="tools">
		      <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		    </div>
	 	</div>
		<div class="portlet-body">
		    <div class="table-scrollable">
		      <table class="table table-striped table-bordered table-hover text-center" >
		        <thead>
		          <tr>
					<th scope="col">Engg. Name</th>
					<th scope="col">Metrics</th>
					<?php foreach($tablehead as $tblkey => $tblvalue){ ?>
						<th scope="col"><?php echo $tblvalue; ?></th>
					<?php } ?>
				  </tr>
		        </thead>
		        <tbody>
		          	<?php 
					foreach($enggresArr as $mngkey => $mngArr){
						echo '<tr><td rowspan='.count($mngArr).'>'.$mngkey.'</td>';
						foreach($sortorder as $value){
							foreach($mngArr as $reskey => $resArr){
								if($reskey == $value){
									echo '<td>'.$reskey.'</td>';
									foreach($tablehead as $tblkey => $tblvalue){
										if($enggsurveyArr[$mngkey][$tblvalue]){
											if($reskey != 'Survey Count'){
												$val = empty($resArr[$tblvalue])?'0%':round($resArr[$tblvalue]/$enggsurveyArr[$mngkey][$tblvalue]*100).'%';
											}else{
												$val = empty($enggsurveyArr[$mngkey][$tblvalue])?0:$enggsurveyArr[$mngkey][$tblvalue];
											}
										}else{
											$val = '-';
										}
										echo '<td>'.$val.'</td>';
									}
									echo '</tr>';
								}
							} 
						}
					} ?>
		        </tbody>
		      </table>
		    </div>
		</div>
	</div>
	
</form>
<?php 
include("includes/footer.php");
?>

<script>
$("#drop9").change(function(){
	if($("#drop9").val() != 'region' || $("#drop9").val() != 'case_origin'){
		jQuery("#drop10").val("Overall");
	}
	//jQuery("#enggname").val("Overall");
});
/* $("#tlname").change(function(){
	   var tlname = $("#tlname").val();
	   var projectwise= $("#drop2").val();
	   var productwise= $("#drop3").val();
	   var productgroup= $("#drop4").val();
	   var calendartype=$("#drop5").val();
	   var selecttype=$("#drop6").val();
	   var selectrange=$("#drop7").val();
	    $.ajax({
	      url: 'ajax_backlog_summary.php',
	      type: 'POST',
	      data: {'tlname':tlname,'projectwise':projectwise,'productwise':productwise,'productgroup':productgroup,'calendartype':calendartype,'selecttype':selecttype,'selectrange':selectrange},
	      success: function(output) {
			var obj = jQuery.parseJSON( output);
	        $("#enggname").html("");
	        $("#enggname").html(obj);
		  }
	    }); 
	}); */
	function reload(){
		document.getElementById("frmsrch").action = 'csat_team.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}
	$(".selectweek").change(function(){
	    var selectdate= $("#drop6").val();
		if(selectdate == 'Daily'){
			$("#drop7").css("display","none");
		    $("#datepicker").css("display","inline-block");
	    }else{
			$("#drop7").css("display","inline-block");
			$("#datepicker").css("display","none");
			var calendertype= $("#drop5").val();
			$.ajax({
			  url: 'ajax.php',
			  type: 'POST',
			  data: {'reporttype':selectdate,'calendertype':calendertype},
			  success: function(output) {
				var obj = jQuery.parseJSON( output);
				$("#drop7").html("");
				$("#drop7").html(obj);
			  }
			}); 
		}
	});
	//$( "#datepicker" ).datepicker();
	
	
</script>