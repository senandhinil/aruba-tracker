<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
if($_POST['reporttype'] || $_POST['projectwise'] || $_POST['productwise'] || $_POST['productgroup'] || $_POST['selectrange'] || $_POST['team']){
	$reporttype 	= 	$_POST['reporttype'];
	$projectwise 	= 	$_POST['projectwise'];
	$productwise   	=	$_POST['productwise'];
	$productgroup 	= 	$_POST['productgroup'];
	$calendartype	= 	$_POST['calendartype'];
	$selecttype	    = 	$_POST['selecttype'];
	$selectrange    =	$_POST['selectrange'];
	$team = $_POST['team'];
}else{
	$selectrange    =	!empty($selectrange)?$selectrange:current($currentweek);
	$projectwise 	=	!empty($projectwise)?$projectwise:"Overall";
	$productwise 	=	!empty($productwise)?$productwise:"Overall";
	$productgroup 	=	!empty($productgroup)?$productgroup:"Overall";
	$reporttype 	=	!empty($reporttype)?$reporttype:"Overall";
	$calendartype 	=	!empty($calendartype)?$calendartype:"Normal";
	$selecttype  	=	!empty($selecttype)?$selecttype:"Weekly";
}
if($projectwise!=""){
	if($calendartype=='Normal'){
		$type="calendar_";
	}else{
		$type="fiscal_";
	}
	if($selecttype=='Quarterly'){	
		$selectQry= 'quarter';
	}else if($selecttype=='Monthly'){
		$selectQry= 'month';		
	}else if($selecttype=='Weekly'){
		$selectQry= 'week';
	}else{
		$selectQry= 'date';
	}

	if($projectwise!='Overall' && $projectwise!=''){
		$QryCondition.=" and wlan_ns='".$projectwise."'";
		//$hcount=strtolower($projectwise);
	}else{
		//$hcount="wlan,ns";
	}
	if($projectwise != "Overall" && $productwise != "WC" && $productgroup == "Overall")
	{
		$hcount = $projectwise;
	}elseif($productwise == "WC" && $productgroup == "Overall" && $projectwise == "Overall")
	{
		$hcount = $productwise;
	}elseif($productwise == "Overall" && $projectwise == "Overall" && $productgroup == "Overall"){
		$hcount_type = "default";
		$hcount="wlan,ns,wc";
	}elseif($projectwise == "Overall" && $productgroup == "Overall" && ($productwise == "GSC" || $productwise == "GEC")){
		$hcount_type = "default";
		$hcount="wlan,ns,wc";
	}elseif($productgroup != "Overall"){
		$hcount = $productgroup;
	}
	if($productwise!='Overall' && $productwise!=''){
		$QryCondition.=" and que_new='".$productwise."'";
	}
	if($productgroup!='Overall' && $productgroup!=''){
		$QryCondition.=" and product_group='".$productgroup."'";
	}
	if($reporttype!='Overall' && $reporttype!=''){
		$QryCondition.=" and region='".$reporttype."'";
	}
	$teamQry = $QryCondition;

	if(count($team) > 0){
		$QryCondition.= ' and team in  ("'.implode('","', $team).'")';
	}
	
	$headQryArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id asc");
	foreach($headQryArr as $headArrval){
		$headertotalArr[]=$headArrval[$type.$selectQry];
	}
	foreach ($headertotalArr as $masterkey => $mastervalue) {
		$totalArrval[$masterkey]=$mastervalue;
	       if($mastervalue=="$selectrange"){break;}
	}
	$arrayval=array_reverse($totalArrval);
	$tablehead=array_reverse(array_slice($arrayval, 0, 6, true));
	
	$inflwQuryArr = $commonobj->getQry("select ".$type.$selectQry.",case_origin,rma from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	foreach($inflwQuryArr as $weeknamearr){
		$totopencase[]=$weeknamearr[$type.$selectQry];
		$caseorigin[$weeknamearr[$type.$selectQry]][]=$weeknamearr['case_origin'];
	}
	$opencasecount=array_count_values($totopencase);
	$case_orgin=$commonobj->getarracount($caseorigin);
	//closure count
	$closeQuryArr = $commonobj->getQry("select ".$type.$selectQry.",ttc,rma,sdc,case_origin from aruba_closure where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	foreach($closeQuryArr as $closeArrval){
		$closecount[]=$closeArrval[$type.$selectQry];
		$ttcAvg[$closeArrval[$type.$selectQry]][] = $closeArrval['ttc'] == '-1' ? 0 : $closeArrval['ttc'];
		if($closeArrval['ttc']<='9'){
			$ttccount[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<'9'){
			$phonettc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<'9'){
			$webttc[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Phone'){
			$sdcphone[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}else if($closeArrval['sdc']=='1' && $closeArrval['case_origin']=='Web'){
			$sdcweb[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		}
		$rma[$closeArrval[$type.$selectQry]][]=$closeArrval['rma'];
		$sdctot[$closeArrval[$type.$selectQry]][]=$closeArrval['sdc'];
		if($closeArrval['case_origin']=='Phone'){
			$phonecountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Web'){
			$webcountArr[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}

		if($closeArrval['ttc']<='7'){
			$ttc7days[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
		if($closeArrval['case_origin']=='Phone' && $closeArrval['ttc']<='7'){
			$phonettc7[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}else if($closeArrval['case_origin']=='Web' && $closeArrval['ttc']<='7'){
			$webttc7[$closeArrval[$type.$selectQry]][]=$closeArrval['ttc'];
		}
	}
	//print_r($phonettc7);
	$closecasecount=array_count_values($closecount);
	$rmacount=$commonobj->getarracount($rma);
	$sdctotper=$commonobj->getarracount($sdctot);

	$backlogCnt = $commonobj->arrayColumn($commonobj->getQry('SELECT calendar_date,sum(count) as count,'.$type.$selectQry." from aruba_backlog_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.',calendar_date order by id asc'),$type.$selectQry,'count');
	
	//csat
	$csatunQuryArr = $commonobj->getQry("select ".$type.$selectQry.",overall_experience,loyalty_index,rma,alert_type,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition order by id desc");
	foreach($csatunQuryArr as $csatunArrval){
		$overallexp[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
		$alerttype[$csatunArrval[$type.$selectQry]][]=$csatunArrval['alert_type'];
		$netpromoternew[$csatunArrval[$type.$selectQry]][]=$csatunArrval['nps'];
		$loyaltyindex[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];

		if($csatunArrval['rma']=='Yes'){
			$rmacaseoe[$csatunArrval[$type.$selectQry]][]=$csatunArrval['overall_experience'];
			$loyalty_index[$csatunArrval[$type.$selectQry]][]=$csatunArrval['loyalty_index'];
		}
	}

	$alert_type=$commonobj->getarracount($alerttype);
	$netprompter=$commonobj->getarracount($netpromoternew);
	//head count and working days
	$workingdays = $commonobj->arrayColumn($commonobj->getQry("select working_days,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'working_days');
	
	$overallssqa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_ssqa where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	
	$overallpa = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",avg(overall) as overall_avg from aruba_process_audit where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'overall_avg');
	
	$overallt1 = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallcontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  'Controllable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overalluncontrol = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  'Uncontrollable' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overallblank = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",case_owner,count(*) as cnt from aruba_esc where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND tier_1 =  '' or tier_1='-' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$csat_overall = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$rcaoverallTier = $commonobj->getQry("select ".$type.$selectQry.",count(tl_tier1) as cnt,tl_tier1  from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.",tl_tier1");
	foreach ($rcaoverallTier as $key => $value) {
		$overallcsatTier[$value[$type.$selectQry]][$value['tl_tier1']]=$value['cnt'];
	}
	
	$overallout = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition AND time_to_sec(total_time) >=  '28800' group by ".$type.$selectQry.""),$type.$selectQry,'cnt');
	
	$overalloutcnt = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(distinct case_owner) as cnt from aruba_avaya_raw where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$getRca = $commonobj->arrayColumn($commonobj->getQry("select ".$type.$selectQry.",count(*) as cnt from aruba_rca where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition  group by ".$type.$selectQry.""),$type.$selectQry,'cnt');

	$rca=  $commonobj->getQry("select ".$type.$selectQry.",oe_alert from aruba_rca where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition ");
	foreach ($rca as $rcakey => $rcavalue) {
		$oesplit[$rcavalue[$type.$selectQry]][]=$rcavalue['oe_alert'];
	}
	$rcasplit=$commonobj->getarracount($oesplit);

	$rcareasion = $commonobj->arrayColumn( $commonobj->getQry("select count(main_root_cause) as count,main_root_cause from aruba_rca where ".$type.$selectQry." in ('".$selectrange."') $QryCondition group by main_root_cause"),'main_root_cause','count');
	foreach ($rcareasion as $key => $value) {
		$rca_case[$key]=(float)$a =number_format($value/array_sum($rcareasion)*100,1);
	}
	//echo "SELECT ".$type.$selectQry.",count(*) as count,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition ".$type.$selectQry.",nps";
	$npsSplit = $commonobj->getQry("SELECT ".$type.$selectQry.",count(*) as count,nps from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition group by ".$type.$selectQry.",nps");
	foreach ($npsSplit as $key => $value) {
		$npsArr[$value[$type.$selectQry]][$value['nps']] =  $value['count'];
	}
	
	$rcaSplit =  $commonobj->getQry("SELECT ".$type.$selectQry.",count(*) AS count,mgr_tier1 from aruba_csat where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $QryCondition and alert_type !='green' and  nps !='promoter' group by ".$type.$selectQry.",mgr_tier1");
	foreach ($rcaSplit as $key => $value) {
		$rcasplitArr[$value['calendar_week']][$value['mgr_tier1'] ==''?'Blanks':$value['mgr_tier1']] = $value['count'];
	}
	$Qry=$type.$selectQry."_".$selectrange;
}
include "includes/header.php";
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	     border: 1px solid #f2784b;
	}
	@media (min-width: 992px){
		.col-md-2 {
			width: 12.50%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
</style>
<form method="POST" id="frmsrch">
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="row top-align" >
			<div class='col-md-12' style='margin-top:12px'>
			    <div class="portlet">
			        <div class="portlet-body">
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop2"  name="projectwise" onchange="reload()">
		                    <?php
		                    	$drop1project = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT wlan_ns from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') order by wlan_ns asc"),'','wlan_ns');
		                    	echo '<option value="Overall">Overall Project</option>';
								foreach($drop1project as $drop1projectval){
								    echo '<option value="'.$drop1projectval.'">'.$drop1projectval.'</option>'; 
								}
		                    ?>
		                    </select>
			                <script>  
			                     jQuery("#drop2").val("<?php echo $projectwise ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop3"  name="productwise"  onchange="reload()">
			                <?php
			                	$proQry = $projectwise != 'Overall' ? " and wlan_ns ='$projectwise'" :'';
			                	$drop1overall = $commonobj->arrayColumn($commonobj->getQry("SELECT DISTINCT que_new from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $proQry order by que_new asc"),'','que_new');
								echo '<option value="Overall">Overall Que</option>';
								foreach($drop1overall as $drop1overallval){
									echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
								}
			                ?>
			                </select>
			                <script> 
			                     jQuery("#drop3").val("<?php echo $productwise ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
						    <select class="form-control submit" id="drop4"  name="productgroup"  onchange="reload()">
			                	<?php
			                		
			                		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
		                			$Qery.= $productwise !='Overall' ? " and que_new = '$productwise' " :'';
		                			$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery");
									echo '<option value="Overall">Overall Product</option>';
			                		foreach ($productName as $key => $value) {
			                			echo '<option value="'.$value['product_group'].'">'.$value['product_group'].'</option>';
			                		} 
									   
			                	?>
			                </select>
			                <script> 
			                     jQuery("#drop4").val("<?php echo $productgroup ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop1"  name="reporttype"  onchange="reload()">
			                	<?php
			                	echo '<option value="Overall">Overall Region</option>';
			                	foreach($productgroupoverall as $drop1overallval){
									echo '<option value="'.$drop1overallval.'">'.$drop1overallval.'</option>'; 
								}
			                	?>
			                </select>
			                <script> 
			                     jQuery("#drop1").val("<?php echo $reporttype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
		                <select class="form-control select" id="team"  name="team[]" multiple>
		                	<?php
		                	$teamName = $commonobj->getQry("SELECT DISTINCT team from aruba_open where ".$type.$selectQry." in ('".implode("','", $tablehead)."') $teamQry order by team asc");
		                	
		                	foreach($teamName as $value){
		                		if(count($team) == 0 ){
		                			$selected = 'selected';
		                		}else{
		                			$selected = in_array($value['team'],$team)?'selected':'';
		                		}
								echo "<option value='".$value['team']."' $selected >".$value['team']."</option>";
							}
		                	?>
		                </select>
		            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control selectweek" id="drop5"  name="calendartype">
			                	<option value="Normal">Calendar</option>
			                	<option value="Fiscal">Fiscal</option>
			                </select>
			                <script> 
			                     jQuery("#drop5").val("<?php echo $calendartype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control selectweek" id="drop6"  name="selecttype">
			                	<option value="Weekly">Weekly</option>
			                    <option value="Monthly">Monthly</option>
			                    <option value="Quarterly">Quarterly</option>
			                </select>
			                <script> 
			                     jQuery("#drop6").val("<?php echo $selecttype ?>");
			                </script>
			            </div>
			            <div class="form-group col-md-2 col-sm-3 col-xs-6">
			                <select class="form-control submit" id="drop7"  name="selectrange"  onchange="reload()">
			                	<option value="">--- Select ---</option>
			                	<?php
									$drowpdownArr = $commonobj->getQry("select distinct ".$type.$selectQry." from aruba_open order by id desc");
									foreach ($drowpdownArr as $key => $value) {
										$selected = $value[$type.$selectQry]==$selectrange?"selected":"";
										echo'<option value="'.$value[$type.$selectQry].'" '.$selected.'>'.$value[$type.$selectQry].'</option>';
									}
		                		?> 
			                </select>
			                <script> 
								jQuery("#drop7").val("<?php echo $selectrange ?>");
			                </script>
			            </div>
			        </div>
			    </div>
			</div> 
		</div>
		<?php 
			$headQry = $productgroup == 'Overall' ? " sum(`".implode("`)+sum(`",array_diff($commonobj->arrayColumn($productName,'','product_group'), array('GEC')))."`)" : " sum(`$productgroup`)";

			$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
			//print_r($overallheadcount);
			
		foreach ($tablehead as $inflowkey=> $rs) {	
            $weekdayname[]	=$rs;	
	        $opencaseArr[]	=empty($opencasecount[$rs])?0:$opencasecount[$rs];
	        $inflowphone[]	=empty($case_orgin[$rs]['Phone'])?0:$case_orgin[$rs]['Phone'];
	        $inflowweb[]		=empty($case_orgin[$rs]['Web'])?"No":$case_orgin[$rs]['Web'];
	        $closecaseArr[]=empty($closecasecount[$rs])?0:$closecasecount[$rs];
	        $closettcArr[]=$ttcval=empty($ttccount[$rs])?0:round(count($ttccount[$rs]),2);
	        $overallttc[]= (float)$a=empty($ttcAvg[$rs])?0:number_format(array_sum($ttcAvg[$rs])/$closecasecount[$rs],1);
	        $closermacase[]=empty($rmacount[$rs]['Yes'])?0:$rmacount[$rs]['Yes'];
	        $closermaper[]=(float)$v=empty($rmacount[$rs])?0:round($rmacount[$rs]['Yes']/array_sum($rmacount[$rs])*100,2);
	        $closephonettc[]=empty($phonettc[$rs])?0:count($phonettc[$rs]);
	        $closewebttc[]=empty($webttc[$rs])?0:count($webttc[$rs]);
	        $headcountArr[] = (int)$h = empty($overallheadcount[$rs])?0:$overallheadcount[$rs];
	        $closephonesdc[]=(float)$v1=empty($sdcphone[$rs])?0:round(count($sdcphone[$rs])/$closecasecount[$rs]*100,2);
	        $closewebsdc[]=(float)$v2=empty($sdcweb[$rs])?0:round(count($sdcweb[$rs])/$closecasecount[$rs]*100,2);
	        
	        $csatgreen[]=(float)$alertgreen=empty($alert_type[$rs]['Green'])?0:number_format($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100,2);
	        $csatnormal[]=(float)$alertgreen=empty($alert_type[$rs]['Normal'])?0:number_format($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100,2);
	        $csatred[]=(float)$alertgreen=empty($alert_type[$rs]['Red'])?0:number_format($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100,2);

	        $closesdcArr[]=(float)$sdcval=empty($sdctotper[$rs])?0:round($sdctotper[$rs]['1']/array_sum($sdctotper[$rs])*100,2);
	        $csatoverallexpArr[]=(float)$overallval=empty($overallexp[$rs])?0:number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2);
	        $tacosatArr[]=(float)$tacosatval=empty($tacosat[$rs])?0:number_format(array_sum($tacosat[$rs])/count($tacosat[$rs]),2);
	        $surveycountArr[]=(int)$surveyval=empty($overallexp[$rs])?0:count($overallexp[$rs]);
	        $loyaltyindexArr[]=(float)$loyaltyval=empty($loyaltyindex[$rs])?0:number_format(array_sum($loyaltyindex[$rs])/count($loyaltyindex[$rs]),2);
	        
	        $csatrmaArr[]=(float)$rmaval=empty($unrmayes[$rs])?0:number_format(array_sum($unrmayes[$rs])/count($unrmayes[$rs]),2);
	        $csatnonarmaArr[]=(float)$nonrmaval=empty($unrmano[$rs])?0:number_format(array_sum($unrmano[$rs])/count($unrmano[$rs]),2);	
	        $csatoverall5Arr[]=(float)$overall5val=empty($nor_overallexp[$rs])?0:number_format(array_sum($nor_overallexp[$rs])/count($nor_overallexp[$rs]),2);
	        $csattacosat5Arr[]=(float)$tacosat5val=empty($nor_tacosat[$rs])?0:number_format(array_sum($nor_tacosat[$rs])/count($nor_tacosat[$rs]),2);
	       
	        $surveycount5Arr[]=(int)$survey5val=empty($nor_overallexp[$rs])?'NA':count($nor_overallexp[$rs]);
	        $loyaltyindex5Arr[]=(float)$loyalty5val=empty($nor_loyaltyindex[$rs])?0:number_format(array_sum($nor_loyaltyindex[$rs])/count($nor_loyaltyindex[$rs]),2);
	        $csatbug5Arr[]=(float)$bug5val=empty($nbugyes[$rs])?0:number_format(array_sum($nbugyes[$rs])/count($nbugyes[$rs]),2);
	        $csatnonbug5Arr[]=(float)$nonbug5val=empty($nbugno[$rs])?0:number_format(array_sum($nbugno[$rs])/count($nbugno[$rs]),2);
	        $csatrma5Arr[]=(float)$rma5val=empty($nrmayes[$rs])?0:number_format(array_sum($nrmayes[$rs])/count($nrmayes[$rs]),2);
	        $csatnonrma5Arr[]=(float)$nonrma5val=empty($nrmano[$rs])?0:number_format(array_sum($nrmano[$rs])/count($nrmano[$rs]),2);
	        $csatoverall25Arr[]=(float)$overall25val=empty($n25_overallexp[$rs])?0:number_format(array_sum($n25_overallexp[$rs])/count($n25_overallexp[$rs]),2);
	        $csattacosat25Arr[]=(float)$tacosat25val=empty($n25_tacosat[$rs])?0:number_format(array_sum($n25_tacosat[$rs])/count($n25_tacosat[$rs]),2);
	        
	        $surveycount25Arr[]=(int)$survey25val=empty($n25_overallexp[$rs])?0:count($n25_overallexp[$rs]);
	        $loyaltyindex25Arr[]=(float)$loyalty25val=empty($n25_loyaltyindex[$rs])?0:number_format(array_sum($n25_loyaltyindex[$rs])/count($n25_loyaltyindex[$rs]),2);
	        $csatbug25Arr[]=(float)$bug25val=empty($n25bugyes[$rs])?0:number_format(array_sum($n25bugyes[$rs])/count($n25bugyes[$rs]),2);
	        $csatnonbug25Arr[]=(float)$nonbug25val=empty($n25bugno[$rs])?0:number_format(array_sum($n25bugno[$rs])/count($n25bugno[$rs]),2);
	        $csatrma25Arr[]=(float)$rma25val=empty($n25rmayes[$rs])?0:number_format(array_sum($n25rmayes[$rs])/count($n25rmayes[$rs]),2);
	        $csatnonrma25Arr[]=(float)$nonrma25val=empty($n25rmano[$rs])?0:number_format(array_sum($n25rmano[$rs])/count($n25rmano[$rs]),2);
	        
	        $rmacaseoes[]=(float)$v=empty($rmacaseoe[$rs])?0:number_format(array_sum($rmacaseoe[$rs])/count($rmacaseoe[$rs])*100,2);

	        $rmacasenps[]=(float)$v=empty($loyalty_index[$rs])?0:number_format(array_sum($loyalty_index[$rs])/count($loyalty_index[$rs]),2);

	        //$rmacasenps[]=(float)$v=empty($loyalty_index[$rs])?0:number_format(array_sum($loyalty_index[$rs])/count($loyalty_index[$rs]),2);

	        $productivity[]=(float)$v=empty($closecasecount[$rs])?0:number_format($closecasecount[$rs]/$workingdays[$rs]/$overallheadcount[$rs],2);
			
			$ssqa[]=(float)$v=empty($overallssqa[$rs])?0:round($overallssqa[$rs],2);
			$pa[]=(float)$v=empty($overallpa[$rs])?0:round($overallpa[$rs],2);
			
			$control[]=(float)$v=empty($overallcontrol[$rs])?0:round($overallcontrol[$rs]/$overallt1[$rs]*100,2);
			$uncontrol[]=(float)$v=empty($overalluncontrol[$rs])?0:round($overalluncontrol[$rs]/$overallt1[$rs]*100,2);
			$blank[]=(float)$v=empty($overallblank[$rs])?0:round($overallblank[$rs]/$overallt1[$rs]*100,2);
			
			$outage_cal=(float)$v=empty($overallout[$rs])?0:round((1-($overallout[$rs]/($overalloutcnt[$rs]*$workingdays[$rs])))*100,2);
			$outage[]=($outage_cal<=0)?0:$outage_cal;


		    $rca_control[]=(float)$v=empty($overallcsatTier[$rs]['Controllable'])?0:round($overallcsatTier[$rs]['Controllable']/$csat_overall[$rs]*100,2);
			$rca_uncontrol[]=(float)$v=empty($overallcsatTier[$rs]['Un-Controllable'])?0:round($overallcsatTier[$rs]['Un-Controllable']/$csat_overall[$rs]*100,2);
			$rca_blank[]=(float)$v=empty($overallcsatTier[$rs][''])?0:round($overallcsatTier[$rs]['']/$csat_overall[$rs]*100,2);

		     $phonettcArr[]=(float)$s=empty($phonettc[$rs])?0:round(count($phonettc[$rs])/count($phonecountArr[$rs])*100,2);
		     $webttcArr[]=(float)$s=empty($webttc[$rs])?"0":round(count($webttc[$rs])/count($webcountArr[$rs])*100,2);

		     $greentarget[] = (int)'80';
		     $redtarget[] = (int)'3';
		     $ssqatarget[] = (int)'80';

		    $backlogcount[] =(int)$val= empty($backlogCnt[$rs])?0:$backlogCnt[$rs];

		    $ttc7daysArr[] = (float)$val=empty($ttc7days[$rs])?0:number_format(count($ttc7days[$rs])/count($rma[$rs])*100,2);
		    $phonettc7Days[] = (float)$val = empty($phonettc7[$rs])?0:number_format(count($phonettc7[$rs])/count($phonecountArr[$rs])*100,2);
		    $webttc7days[] = (float)$v=empty($webttc7[$rs])?0:number_format(count($webttc7[$rs])/count($webcountArr[$rs])*100,2);
		    $processTarget[] = 90;

		    // nps
		    $nps_promoter[] = (float)$npspr= empty($npsArr[$rs])?0:number_format($npsArr[$rs]['Promoter']/array_sum($npsArr[$rs])*100,2);
		    $nps_passive[] = (float)$s= empty($npsArr[$rs])?0:number_format($npsArr[$rs]['Passive']/array_sum($npsArr[$rs])*100,2);
		    $nps_detractor[] = (float)$det= empty($npsArr[$rs])?0:number_format($npsArr[$rs]['Detractor']/array_sum($npsArr[$rs])*100,2);
		    $nps[] = (float)$a = $npspr - $det;

		    $Controllable = $rcasplitArr[$rs]['Aruba Controllable']+$rcasplitArr[$rs]['Controllable'];
		   $uncontrollable = $rcasplitArr[$rs]['Un-Controllable']+$rcasplitArr[$rs]['Uncontrollable'];
		    $rca_aruba_cntl[] =(float)$a= empty($Controllable)?'0%':number_format($Controllable/array_sum($rcasplitArr[$rs])*100,2);
		    $rca_tac_cntl[] = (float)$b= empty($rcasplitArr[$rs]['TAC Controllable'])?'0%':number_format($rcasplitArr[$rs]['TAC Controllable']/array_sum($rcasplitArr[$rs])*100,2);
		    $rca_wc_cntl[] =(float)$c =empty($rcasplitArr[$rs]['WC Controllable'])?'0%':number_format($rcasplitArr[$rs]['WC Controllable']/array_sum($rcasplitArr[$rs])*100,2);
		    $rca_uncntrl[] = (float)$d = empty($uncontrollable)?'0%':number_format($uncontrollable/array_sum($rcasplitArr[$rs])*100,2);
		    $rca_blanks[] = (float)$d = empty($rcasplitArr[$rs]['Blanks'])?'0%':number_format($rcasplitArr[$rs]['Blanks']/array_sum($rcasplitArr[$rs])*100,2);
		}
		//print_r($phonettc7Days);
	?>              		
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>Inflow & Closure</div>
		            <div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container1"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container2"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container4"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container5"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container6"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container12"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container7"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container8"></div>
					</div>
				</div>
				<div class="row" style="margin-top:15px">
					<div class="col-md-6 col-sm-6 col-xs-12">
					<?php if($productwise !='GEC' ) {?>
						<div id="container9"></div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box yellow-casablanca" >
		    <div class="portlet-title">
		        <div class="caption">
		            <i class="fa fa-bar-chart"></i>CSAT</div>
					<div class="tools">
		                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
		                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
		                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
		        	</div>
		    </div>
			<div class="portlet-body">
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container10"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container21"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container11"></div>
					</div>					
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container13"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container14"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container15"></div>
					</div>					
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container20"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container16"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container17"></div>
					</div>
				</div>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container18"></div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div id="container19"></div>
					</div>
				</div>
			</div>
		</div>
		
</form>
<?php include("includes/footer.php"); ?>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<!-- <script src="https://code.highcharts.com/modules/export-data.js"></script> -->
<script type="text/javascript">
	$(".selectweek").change(function(){
	   var selectdate= $("#drop6").val();
	   var calendertype= $("#drop5").val();
	    $.ajax({
	      url: 'ajax.php',
	      type: 'POST',
	      data: {'reporttype':selectdate,'calendertype':calendertype},
	      success: function(output) {
	        var obj = jQuery.parseJSON( output);
	        $("#drop7").html("");
	        $("#drop7").html(obj);
	      }
	    }); 
	});
	$("#drop1").change(function(){
	    $("#drop2").val("Overall");
	    $("#drop3").val("Overall");
	    $("#drop4").val("Overall");
	});
	
	
	jQuery(document).ready(function($) {  
		jQuery(window).load(function() {
			jQuery("#status").fadeOut();
			jQuery("#preloader").delay(1000).fadeOut("slow");
		})
	});
	function reload(){
		document.getElementById("frmsrch").action = 'chart_old.php'; 
		document.getElementById("frmsrch").submit();
		return false;
	}

$(function() {
	Highcharts.setOptions({
	    lang: {
	        decimalPoint: '.',
	        thousandsSep: ''
	    },
	    animation: {
            duration: 3000,
            easing: 'easeOutBounce'
    	}
	});
	$('#container').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Open Cases Vs Closed Cases Vs Backlog',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#FE4469', '#263249', '#0000FF', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Open Case',
	            type: 'column',
	            data: <?php echo json_encode($opencaseArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:-10
                } 
	        },{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>,
	            dataLabels: {
                    verticalAlign:'top',
                    y:5,
                } 
	        },{
	            name: 'Backlog',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($backlogcount,true);?>,
	        }]
    });

var chart = new Highcharts.Chart({
	chart: {
            zoomType: 'xy',
            height: 300,
            renderTo: 'container1'
	},
    title: {
     text: 'Case Origin - Open Cases',
    style:{
            color: 'black',
            fontSize: '12px'
        }       
    },
    legend: {
        itemStyle: {
            fontWeight: 'bold',
            fontSize: '11px'
        }
    },
    credits: {
        enabled: false
    }, 
	plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        allowOverlap:true,
                        format:"{y}",
                    },
	                animation: {
		                duration: 3000,
		                easing: 'easeOutBounce'
	            	}   
                }
            },
    colors: ['#FE4469', '#263249', '#0000FF', '#55FF2A'],
    xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	            	
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    //format:"{point.y:.0f}%"
	                    fontSize: '9px'
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}"
		                },
	                    animation: {
			                duration: 1000,
			            }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'Open Case',
	            type: 'column',
	            data: <?php echo json_encode($opencaseArr,true);?>,
	            plotOptions: {
	                series: {
	                    dataLabels: {
	                        enabled: true,
	                        format:"{y}",
	                    }   
	                }
	            },
	        },{
	            name: 'Phone',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($inflowphone,true);?>,
	            // tooltip: {
	            //     valueSuffix: '%'
	            // }
	        },{
	            name: 'Web',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($inflowweb,true);?>,
	            // tooltip: {
	            //     valueSuffix: '%'
	            // }
	        }]
	});
	$('#container2').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Closed Cases Vs Avg. TTC ',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#FF2AAA', '#55FF2A', '#FFAA55', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    allowOverlap:true,
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,

		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>
	        },{
	            name: 'TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($overallttc,true);?>
	        }]
    });
        $('#container4').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	         text: 'Close Cases Vs SDC',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#AA002B','#AA00FF', '#0000FF', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
					format: '{value} %'
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>,
	        },{
	            name: 'SDC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closesdcArr,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                    format:"{y}%",
	                }
	        }]
        });
        $('#container5').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Close Cases Vs RMA%',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#00D5AA','#2B0080', '#800055', '#800055'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        },{ // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>,
	        },{
	            name: 'RMA',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closermacase,true);?>,
	        }]
        });
        $('#container6').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	         text: 'TTC < 9 Days - Source wise',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#0D47A1','#808000', '#A34F31', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                     //format:"{y}%"
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        },{ // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {                
	                format:"{value}%",
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'Overall TTC',
	            type: 'column',
	            data: <?php echo json_encode($closettcArr,true);?>,
	        },{
	            name: 'Phone TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($phonettcArr,true);?>,
	             dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        },{
	            name: 'Web TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($webttcArr,true);?>,
	            dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        }]
        });
        $('#container12').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	         text: 'TTC < 7 Days - Source wise',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#0D47A1','#808000', '#A34F31', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                     //format:"{y}%"
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        },{ // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {                
	                format:"{value}%",
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'Overall TTC',
	            type: 'column',
	            data: <?php echo json_encode($ttc7daysArr,true);?>,
	        },{
	            name: 'Phone TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($phonettc7Days,true);?>,
	             dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        },{
	            name: 'Web TTC',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($webttc7days,true);?>,
	            dataLabels: {
	             	format:"{y}%",
	             },
	             tooltip:{
	             	valueSuffix: '%'
	             }
	        }]
        });
    $('#container7').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        
	         text: 'Closed Cases Vs SDC',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        //colors: ['#263249', '#D500AA', '#2AFFAA','#F92672','#263249'],
	        colors: ['#D500AA','#2660BA', '#141414','#F92672','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format:"{value}%",
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {                
	                format:"{value}%",
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'SDC ',
	            type: 'column',
	            data: <?php echo json_encode($closesdcArr,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'SDC-Phone',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closephonesdc,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'SDC-Web',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closewebsdc,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                    format:"{y}%",
	                }
	        }]
        });
    $('#container8').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'RMA % & RMA Case',

	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#F92672', '#272822', '#2AFFAA','#F92672','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                format:"{value}%"
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}%",
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        
	        series: [{
	            name: 'RMA Case ',
	            type: 'column',
	            data: <?php echo json_encode($closermacase,true);?>,
	        },{
	            name: 'RMA %',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($closermaper,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        }]
        });
    $('#container9').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'Closed Cases Vs Productivity ',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#FF2AAA', '#55FF2A', '#FFAA55', '#55FF2A'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}*"
		                }
		            }
	        	},
	            opposite: true
	        }, { // Tertiary yAxis
		        gridLineWidth: 0,
		        title: {
		            text: '',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        },
		        labels: {
		            format: '{value}',
		            style: {
		                color: Highcharts.getOptions().colors[1]
		            }
		        },
		        opposite: true
		    }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Close Case',
	            type: 'column',
	            data: <?php echo json_encode($closecaseArr,true);?>
	        },{
	            name: 'Productivity',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($productivity,true);?>
	        },{
	        	name:'Head Count',
	        	 yAxis: 2,
	        	 dashStyle: 'shortdot',
	        	//type:'spline',
	        	data:<?=json_encode($headcountArr,true)?>
	        }]
        });
        $('#container10').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'CSAT - Summary',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#218C8D', '#39C1FB', '#272822','#D50055','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },	        
	        series: [{
	            name: 'Survey Count',
	            type: 'column',
	            data: <?php echo json_encode($surveycountArr,true);?>
	        },{
	            name: 'Overall Experience',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($csatoverallexpArr,true);?>,
	        },{
	            name: 'Loyalty Index',
	            type: 'spline',
	            yAxis:1,
	            data: <?php echo json_encode($loyaltyindexArr,true);?>,
	        }]
        });
        $('#container21').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'NPS',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#218C8D', '#39C1FB', '#272822','#D50055','#263249'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                     format:"{y}%",
	                }
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {                
	                format:'{value}%'
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{y}"
		                }
		            }
	        	},
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	            },
	            labels: {
	                format:'{value}%'
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:"{point.y:.0f}"
		                }
		            }
	        	},
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },	        
	        tooltip: {valueSuffix: '%'},
	        series: [{
	        	name:'NPS',
	        	type:'column',
	        	yAxis:1,
	        	data:<?=json_encode($nps,true)?>,
	        	tooltip: {valueSuffix: '%'},
	        },{
	            name: 'Promoter',
	            type: 'spline',
	            data: <?php echo json_encode($nps_promoter,true);?>,
	            tooltip: {valueSuffix: '%'},
	        },{
	            name: 'Passive',
	            type: 'spline',
	            data: <?php echo json_encode($nps_passive,true);?>,
	            tooltip: {valueSuffix: '%'},
	        },{
	            name: 'Detractor',
	            type: 'spline',
	            data: <?php echo json_encode($nps_detractor,true);?>,
	            tooltip: {valueSuffix: '%'},
	        }]
        });
  //       nps_promoter
		// nps_passive
		// nps_detractor
		// nps
	    $('#container11').highcharts({
	    	chart: {
	            zoomType: 'xy',
	            height: 300
	        },
	        title: {
	        text: 'CSAT - Rating %',
	        style:{
	                color: 'black',
	                fontSize: '12px'
	            }       
	        },
	        legend: {
	            itemStyle: {
	                fontWeight: 'bold',
	                fontSize: '11px'
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        colors: ['#1ED570','#FF7F00','#FF0000',  '#FF00FF','#FF0000'],
	        xAxis: [{
	            categories: <?php echo json_encode($weekdayname,true);?>,
	            crosshair: true
	        }],
	         plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                     allowOverlap:true,
	                }	
	            }
	        },
	        yAxis: [{ // Primary yAxis
	            labels: {
	                format: '{value} %'
	            },
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            }
	        }, { // Secondary yAxis
	            title: {
	                text: '',
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            labels: {
	                style: {
	                    color: Highcharts.getOptions().colors[1]
	                }
	            },
	            plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format:'{y}',
		                }
		            }
		        },
	            opposite: true
	        }],
	        tooltip: {
	            shared: true
	        },
	        series: [{
	            name: 'Green %',
	            type:'column',	
	            data: <?php echo json_encode($csatgreen,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'Normal %',
	            type:'column',
	            data: <?php echo json_encode($csatnormal,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }
	        },{
	            name: 'Red %',
	            type:'column',
	            data: <?php echo json_encode($csatred,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        } ,{
	            name: 'Survey Count',
	            type:'spline',
	            yAxis:1,
	            data: <?php echo json_encode($surveycountArr,true);?>,

	        },{
	            name: 'Red Target',
	            type:'spline',
	            data: <?php echo json_encode($redtarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        },{
	            name: 'Green Target',
	            type:'spline',
	            data: <?php echo json_encode($greentarget,true);?>,
	            tooltip: {
	                valueSuffix: '%'
	            },
	            dataLabels: {
	                format:"{y}%",
	            }

	        }]
        });
 //    $('#container12').highcharts({
 //    	chart: {
 //            zoomType: 'xy',
 //            height: 300
 //        },
 //        title: {
 //        text: 'Survey Count & Green % ',
 //        style:{
 //                color: 'black',
 //                fontSize: '12px'
 //            }       
 //        },
 //        legend: {
 //            itemStyle: {
 //                fontWeight: 'bold',
 //                fontSize: '11px'
 //            }
 //        },
 //        credits: {
 //            enabled: false
 //        },
 //        colors: ['#F2784B', '#2660BA', '#141414','#F92672','#263249'],
 //        xAxis: [{
 //            categories: <?php echo json_encode($weekdayname,true);?>,
 //            crosshair: true
 //        }],
 //         plotOptions: {
 //            series: {
 //                dataLabels: {
 //                    enabled: true,
 //                     allowOverlap:true,
 //                }
 //            }
 //        },
 //        yAxis: [{ // Primary yAxis
 //            labels: {                
 //                style: {
 //                    color: Highcharts.getOptions().colors[1]
 //                }
 //            },
 //            title: {
 //                text: '',
 //                style: {
 //                    color: Highcharts.getOptions().colors[1]
 //                }
 //            },
 //            plotOptions: {
	//             series: {
	//                 dataLabels: {
	//                     enabled: true,
	//                     format:"{y}"
	//                 }
	//             }
 //        	},
 //        }, { // Secondary yAxis
 //            title: {
 //                text: '',
 //                style: {
 //                    color: Highcharts.getOptions().colors[1]
 //                }
 //            },
 //            labels: {
 //                style: {
 //                    color: Highcharts.getOptions().colors[1]
 //                }
 //            },
 //            plotOptions: {
	//             series: {
	//                 dataLabels: {
	//                     enabled: true,
	//                     format:"{point.y:.0f}"
	//                 }
	//             }
 //        	},
 //            opposite: true
 //        }],
 //        tooltip: {
 //            shared: true
 //        },	        
 //        series: [{
 //            name: 'Survey Count',
 //            type: 'column',
 //            data: <?php echo json_encode($surveycountArr,true);?>,
 //        },{
 //            name: 'Green %',
 //            type: 'spline',
 //            yAxis:1,
 //            data: <?php echo json_encode($csatgreen,true);?>,
 //            tooltip: {
 //                valueSuffix: '%'
 //            },
 //            dataLabels: {
 //                format:"{y}%",
 //            }
 //        }]
	// });
    $('#container13').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
         text: 'RMA Case OE Vs RMA Case NPS',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#141414','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                }
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },	        
        series: [{
            name: 'Survey Count',
            type: 'column',
            data: <?php echo json_encode($surveycountArr,true);?>,
        },{
            name: 'RMA Case OE',
            type: 'spline',
            yAxis:1,
            data: <?php echo json_encode($rmacaseoes,true);?>
        },{
            name: 'RMA Case NPS',
            type: 'spline',
            yAxis:1,
            data: <?php echo json_encode($rmacasenps,true);?>,
        }]
	});
	$('#container14').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'RMA Case OE & RMA Case NPS',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#141414','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                },
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },	        
        series: [{
            name: 'RMA Case OE',
            type: 'spline',
            yAxis:1,
            data: <?php echo json_encode($rmacaseoes,true);?>
        },{
            name: 'RMA Case NPS',
            type: 'spline',
            data: <?php echo json_encode($rmacasenps,true);?>,
        }]
	});
	$('#container15').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'Quality',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#FF557F','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                     format:"{y}%",
                },
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
               format:"{value}%",
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true,
            valueSuffix: '%'
        },	        
        series: [{
            name: 'SSQA',
            type: 'spline',
            data: <?php echo json_encode($ssqa,true);?>
        },{
        	name: 'SSQA Target',
            type: 'spline',
            data: <?php echo json_encode($ssqatarget,true);?>,
        }]
	});
	$('#container20').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'Process Audit',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#FF557F','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                     allowOverlap:true,
                     format:"{y}%",
                },
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
               format:"{value}%",
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true,
            valueSuffix: '%'
        },	        
        series: [{
            name: 'Process Audit',
            type: 'spline',
            data: <?php echo json_encode($pa,true);?>,
        },{
        	name: 'Process Target',
            type: 'spline',
            data: <?php echo json_encode($processTarget,true);?>,
        }]
	});
	$('#container16').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'Escalation',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#141414','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
					format: '{y}%',
					 allowOverlap:true,
                },
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },	        
        series: [{
            name: 'Esc - Controllable',
            type: 'spline',
            data: <?php echo json_encode($control,true);?>
        },{
            name: 'Esc - Uncontrollable',
            type: 'spline',
            data: <?php echo json_encode($uncontrol,true);?>,
        },{
            name: 'Blank',
            type: 'spline',
            data: <?php echo json_encode($blank,true);?>,
        }]
	});
	$('#container17').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'Outage',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#141414','#F92672','#263249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
					format: '{y}%',
					allowOverlap:true,
				},
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                format:"{value}%"
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true
        },	        
        series: [{
            name: 'Outage',
            type: 'column',
            data: <?php echo json_encode($outage,true);?>
        }]
	});

	// $('#container18').highcharts({
 //        chart: {
 //            type: 'column',
 //            height: 300

 //        },
 //        title: {
 //            text: 'Backlog Buketwise',
 //            style:{
 //                color: 'black',
 //                fontSize: '12px'
 //        	},
 //        },
 //        legend: {
 //        itemStyle: {
 //            fontWeight: 'bold',
 //            fontSize: '10px'
 //        }
 //    	},
 //        credits: {
 //            enabled: false
 //        },
 //        xAxis: [{
 //            categories: <?php echo json_encode($weekdayname,true);?>,
 //            crosshair: true
 //        }],
 //        colors: ['#FF0000', '#FE9955', '#05BC57','#F92672','#263249'],
 //        yAxis: {
 //            stackLabels: {
 //            	format:'{value}%',
 //                enabled: false,
 //                allowOverlap:true,
 //                style: {
 //                    fontWeight: 'small',
 //                    fontSize:10,
 //                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
 //                }
 //            }
 //        },
 //        tooltip: {
 //            //headerFormat: '<b>{point.x}</b><br/>',
 //            pointFormat: '{series.name}: {point.y}%<br/>'
 //        },
 //        plotOptions: {
 //            column: {
 //                stacking: 'normal',
 //                dataLabels: {
 //                    enabled: true,
 //                    allowOverlap:true,
 //                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
 //                    style: {
 //                        textShadow: '0 0 1px black'
 //                    }
 //                }
 //            }
 //        },
 //        tooltip: {
 //                valueSuffix: '%'
 //            },
 //        series: [{
 //            name: 'DSAT',
 //            data: <?php echo json_encode($dsat,true);?>,
 //            dataLabels: {
 //                    format:"{y}%",
 //            },
 //        },{
 //            name: 'MID', 
 //            data: <?php echo json_encode($midvalue,true);?>,
 //            dataLabels: {
 //                    format:"{y}%",
 //            },
 //        }, {
 //            name: 'Top-2', 
 //            data: <?php echo json_encode($topvalue,true);?>,
 //            dataLabels: {
 //                    format:"{y}%",
 //            },
 //        },{
 //            name: 'RCA', 
 //            type:'spline',

 //            data: <?php echo json_encode($rcacount,true);?>,
 //        }]
 //    });
    $('#container18').highcharts({
    	chart: {
            zoomType: 'xy',
            height: 300
        },
        title: {
        text: 'RCA',
        style:{
                color: 'black',
                fontSize: '12px'
            }       
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '11px'
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#FF557F', '#2660BA', '#141414','#F92672','#263249','#565249'],
        xAxis: [{
            categories: <?php echo json_encode($weekdayname,true);?>,
            crosshair: true
        }],
         plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
					format: '{y}%',
					 allowOverlap:true,
                },
                animation: {
	                duration: 3000,
	                easing: 'easeOutBounce'
            	}
            }
        },
        yAxis: [{ // Primary yAxis
            labels: {                
                format:"{value}%"
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{y}"
	                }
	            }
        	},
        }, { // Secondary yAxis
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    format:"{point.y:.0f}"
	                }
	            }
        	},
            opposite: true
        }],
        tooltip: {
            shared: true,
            valueSuffix: '%'
        },	        
        series: [{
            name: 'Aruba Controllable',
            type: 'spline',
            data: <?php echo json_encode($rca_aruba_cntl,true);?>
        },{
            name: 'TAC Controllable',
            type: 'spline',
            data: <?php echo json_encode($rca_tac_cntl,true);?>,
        },{
            name: 'WC Controllable',
            type: 'spline',
            data: <?php echo json_encode($rca_wc_cntl,true);?>,
        },{
            name: 'Un-Controllable',
            type: 'spline',
            data: <?php echo json_encode($rca_uncntrl,true);?>,
        },{
            name: 'Blanks',
            type: 'spline',
            data: <?php echo json_encode($rca_blanks,true);?>,
        }]
	});
  
});
Math.easeOutBounce = function (pos) {
    if ((pos) < (1 / 2.75)) {
        return (7.5625 * pos * pos);
    }
    if (pos < (2 / 2.75)) {
        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
    }
    if (pos < (2.5 / 2.75)) {
        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
    }
    return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
};

	


'use strict';
// import Highcharts from '../parts/Globals.js';
/* global document */
// Load the fonts
// Highcharts.createElement('link', {
//    href: 'https://fonts.googleapis.com/css?family=Signika:400,700',
//    rel: 'stylesheet',
//    type: 'text/css'
// }, null, document.getElementsByTagName('head')[0]);

// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(https://www.highcharts.com/samples/graphics/sand.png)';
});


Highcharts.theme = {
   colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
      '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: null,
      style: {
         fontFamily: 'Signika, serif'
      }
   },
   title: {
      style: {
         color: 'black',
         fontSize: '16px',
         fontWeight: 'bold'
      }
   },
   subtitle: {
      style: {
         color: 'black'
      }
   },
   tooltip: {
      borderWidth: 0
   },
   legend: {
      itemStyle: {
         fontWeight: 'bold',
         fontSize: '13px'
      }
   },
   xAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   yAxis: {
      labels: {
         style: {
            color: '#272822'
         }
      }
   },
   plotOptions: {
      series: {
         shadow: true
      },
      candlestick: {
         lineColor: '#404048'
      },
      map: {
         shadow: false
      }
   },

   // Highstock specific
   navigator: {
      xAxis: {
         gridLineColor: '#D0D0D8'
      }
   },
   rangeSelector: {
      buttonTheme: {
         fill: 'white',
         stroke: '#C0C0C8',
         'stroke-width': 1,
         states: {
            select: {
               fill: '#D0D0D8'
            }
         }
      }
   },
   scrollbar: {
      trackBorderColor: '#C0C0C8'
   },

   // General
   background2: '#E0E0E8'

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
$('#csat-rawexport').click(function(){
		var query=<?php echo json_encode($Qry); ?>;
		res = encodeURI(query);
		document.getElementById("frmsrch").action = 'csat_rawexport.php?query='+res; 
		document.getElementById("frmsrch").submit();
		return false;
	});
</script>

