<?php
set_time_limit(0);
include "includes/config.php";
include 'includes/session_check.php';
ini_set('max_execution_time', 120);
require_once('dashboard_calculation.php');
//include "includes/header.php";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Summary.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<style type="text/css">
	.filter-postion{
	    position: fixed;
	    top: 0px !important;
	    z-index: 1 !important;
	}
	@font-face {
	    font-family: myFirstFont;
	    src: url(fonts/Calibri.ttf);
	}
	body{
	    font-family: myFirstFont;
	    *font-size: 10px;
	}
	.form-control {
	    border: 0px solid #c2cad8 !important;
	}
    .td-style{
        *font-weight: 700;
        font-size: 8px;
        text-align:center;
    }
    th{
        font-size: 12px;
    }
    .tr-color{
    	background-color: #DAACCA;
    }
    .bold-font{
    	    font-weight: 900 !important;
    }
    .form-control {
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    background-color: #f3d3c8;
	}
	@media (min-width: 992px){
		.col-md-3 {
	   		 width: 14.28%;
		}
		.page-content-wrapper .page-content {
		    margin-left: 235px;
		    margin-top: 0;
		    min-height: 600px;
		    padding: 0px 20px 10px;
		}
		
	}
	@media (max-width: 992px){
		.top-align{
			margin-top:-40px;
		}
	}
tbody { 
  height: 150px; 
  overflow-y: auto;
  overflow-x: hidden;
}
td{
 	text-align:center !important;
 	font-size:9px !important;
}
</style>

<form method="POST" >
<input type="hidden" name="_token" value="<?php echo $token; ?>">
		<div class="portlet box yellow-casablanca">
		    <table>
                <tr class="text-center" rowspan=2>
                    <tr>
                        <td colspan=<?php echo count($tablehead)+3?> align="center" style="border:1px solid;background-color:#F2784B;color:white;"><H3>
                            <i class="icon-calendar"></i>Summary -<?php echo $selectrange; ?>
                        </H3>
                        </td>
                    </tr>
                </tr>
            <table> 
            <?php 
        		$Qery = $projectwise !='Overall' ? " wlan_ns = '$projectwise' " :" id!=''";
        		$Qery.= $productwise !='Overall' ? " and que_new in  ('".implode("','",$productwise)."')" :'';
        		$productName = $commonobj->getQry("SELECT distinct product_group from aruba_open Where $Qery");
            	
				//$headQry = $productgroup == 'Overall' ? " sum(`".implode("`)+sum(`",array_diff($commonobj->arrayColumn($productName,'','product_group'), array('GEC')))."`)" : " sum(`$productgroup`)";
				if(count($productwise) == '3' || !in_array('WC' , $productwise) ){
					$headQry = $productgroup == 'Overall' ? " sum(`".implode("`)+sum(`",array_diff($commonobj->arrayColumn($productName,'','product_group'), array('GEC')))."`)" : " sum(`$productgroup`)";
				}else{
					$headQry = 'WC';
				}

				$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');

				//echo "SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry;

				//$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from  aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
				
				/* head count sheet*/
				// if($location == "Overall"){
				// 	//echo "SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry;
				// 	$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
				// }else{
				// 	//echo "SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount_location_based where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and location = '$location' group by ".$type.$selectQry;
				// 	$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount_location_based where ".$type.$selectQry." in ('".implode("','", $tablehead)."') and location = '$location' group by ".$type.$selectQry),$type.$selectQry,'headcount');
				// }
				/* head count sheet*/

				//$overallheadcount=$commonobj->arrayColumn($commonobj->getQry("SELECT $headQry as headcount,".$type.$selectQry." from aruba_headcount where ".$type.$selectQry." in ('".implode("','", $tablehead)."') group by ".$type.$selectQry),$type.$selectQry,'headcount');
				?>
		    <div class="portlet-body">
		        <table class="table table-fixed-header table-striped table-bordered table-hover text-center scroll" data-height="300" border=1>
                      <thead  class="header">
                            <tr>
                            	<th class='text-center' style="background-color:#F2784B;color:white;" colspan="2">Metrics</th>
                            	<th class='text-center' style="background-color:#F2784B;color:white;">Target</th>
                            	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
                            	<th style="background-color:#F2784B;color:white;text-align:center;"><?php echo $rs; ?></th>
                        		<?php	}	?>
                            </tr>
                        </thead>
                        <tbody style=" height: 120px;*display: block;">
                        	<tr>
                        		<td rowspan="3" style="vertical-align: inherit;" style="background-color:#F2784B;color:white;">Volume</td>
	                        	<td style="vertical-align: inherit;" style="background-color:#F2784B;color:white;">Overall</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($opencasecount[$rs])?0:$opencasecount[$rs]; ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Phone</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($case_orgin[$rs]['Phone'])?"No":$case_orgin[$rs]['Phone']; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Web</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($case_orgin[$rs]['Web'])?"No":$case_orgin[$rs]['Web']; ?></td></td>
	                        	<?php	}?>
                        	</tr>
                        	
                        	<tr>
                        	<?php
	                        		$GEC = count($productwise) == '3' || !in_array('GEC',$productwise) ? "Yes":"No";
	                        	?>
                        	<?php $cnt = ($GEC == 'Yes')? '14':'12' ?>

	                        	<td rowspan=<?=$cnt?> style="vertical-align: inherit;background-color:#F2784B;color:white;">Closed Cases</td>
	                        	<td style="background-color:#F2784B;color:white;">Closure</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($closecasecount[$rs])?0:$closecasecount[$rs]; ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
                        		<td style="background-color:#F2784B;color:white;">RMA %</td>
                        		<td>-</td>
                        		<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($rmacount[$rs])?"0.00%":round($rmacount[$rs]['Yes']/array_sum($rmacount[$rs])*100,2).'%' ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">RMA Cases</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($rmacount[$rs]['Yes'])?"0":$rmacount[$rs]['Yes']; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<!-- productivity -->
                        	<?php if( $GEC == 'Yes') {?>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Productivity</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($closecasecount[$rs])?"0":number_format($closecasecount[$rs]/$workingdays[$rs]/$overallheadcount[$rs],2); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Head Count</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallheadcount[$rs])?"NA":$overallheadcount[$rs]; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<?php } ?>
                        	<!-- end productivity -->
                        	<tr>
		                        <td style="background-color:#F2784B;color:white;">TTC &lt;7 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($ttc7days[$rs])?"0":number_format(count($ttc7days[$rs])/count($rma[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">Phone - TTC &lt;7 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($phonettc7[$rs])?"0":number_format(count($phonettc7[$rs])/count($phonecountArr[$rs],2)*100).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">Web - TTC &lt;7 Days</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($webttc7[$rs])?"0":number_format(count($webttc7[$rs])/count($webcountArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">TTC &lt;9 Days</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($ttccount[$rs])?"0":round(count($ttccount[$rs])/count($rma[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Phone - TTC &lt;9 Days</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($phonettc[$rs])?"0":round(count($phonettc[$rs])/count($phonecountArr[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Web - TTC &lt;9 Days</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($webttc[$rs])?"0":round(count($webttc[$rs])/count($webcountArr[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;" >SDC</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($sdctotper[$rs])?"0.00%":round($sdctotper[$rs]['1']/array_sum($sdctotper[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;" >SDC(Phone 45%)</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($sdcphone[$rs])?"0.00%":round(count($sdcphone[$rs])/$closecasecount[$rs]*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;" >SDC(Web 35%)</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($sdcweb[$rs])?"0.00%":round(count($sdcweb[$rs])/$closecasecount[$rs]*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
		                        <td style="background-color:#F2784B;color:white;" rowspan="5">Backlog</td>
								<td style="background-color:#F2784B;color:white;">Backlog Count</td>
		                        <td>-</td>
		                        	<!-- <td>-</td> -->
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($backlogCnt[$rs])?0:$backlogCnt[$rs]; ?></td>
		                        	<?php	} ?>
	                        </tr>
							<tr>
                                <td style="background-color:#F2784B;color:white;">0 to 5 days</td>
                                <td>-</td>
                                <?php foreach ($tablehead as $inflowkey=> $rs) {    ?>
									<td style="text-align:center;font-size:12px"><?php echo empty($bcket0to5[$rs])?0:$bcket0to5[$rs]; ?></td>
                                <?php   } ?>
                            </tr>
                            <tr>
								<td style="background-color:#F2784B;color:white;">06 to 10 days</td>
                                <td>-</td>
                                <?php foreach ($tablehead as $inflowkey=> $rs) {    ?>
									<td style="text-align:center;font-size:12px"><?php echo empty($bcket6to10[$rs])?0:$bcket6to10[$rs]; ?></td>
                                <?php   } ?>
                            </tr>
                            <tr>
								<td style="background-color:#F2784B;color:white;">11 to 20 days</td>
                                <td>-</td>
                                <?php foreach ($tablehead as $inflowkey=> $rs) {    ?>
									<td style="text-align:center;font-size:12px"><?php echo empty($bcket11to20[$rs])?0:$bcket11to20[$rs]; ?></td>
                                <?php   } ?>
                            </tr>
                            <tr>
								<td style="background-color:#F2784B;color:white;">More than 20 days</td>
                                <td>-</td>
                                <?php foreach ($tablehead as $inflowkey=> $rs) {    ?>
									<td style="text-align:center;font-size:12px"><?php echo empty($morethan20[$rs])?0:$morethan20[$rs]; ?></td>
                                <?php   } ?>
                            </tr>
							<tr>
                        		<td  style="background-color:#F2784B;color:white;" rowspan="10" style="vertical-align: inherit;">CSAT</td>
	                        	<td style="background-color:#F2784B;color:white;">Overall Experience</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallexp[$rs])?"NA":number_format(array_sum($overallexp[$rs])/count($overallexp[$rs]),2); ?></td>
	                        	<?php	} ?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Green%</td>
	                        	<td>80%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($alert_type[$rs]['Green'])?"NA":number_format($alert_type[$rs]['Green']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Normal %</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($alert_type[$rs]['Normal'])?"NA":number_format($alert_type[$rs]['Normal']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Red%</td>
	                        	<td>3%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($alert_type[$rs]['Red'])?"NA":number_format($alert_type[$rs]['Red']/array_sum($alert_type[$rs])*100,2).'%'; ?></td>	
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Survey Count</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($alert_type[$rs])?"0":array_sum($alert_type[$rs]); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<?php 
                        	foreach ($tablehead as $inflowkey=> $rs) { 
                        		$greenper[$rs]=empty($netprompter[$rs]['Promoter'])?"0.00%":number_format($netprompter[$rs]['Promoter']/array_sum($netprompter[$rs])*100,2).'%'; 
	                        	$redperc[$rs]=empty($netprompter[$rs]['Detractor'])?"0.00%":number_format($netprompter[$rs]['Detractor']/array_sum($netprompter[$rs])*100,2).'%';
                        	}
                        	?>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Net Promoter Score</td>
	                        	<td>80%</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 	?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($greenper[$rs])?"0.00%":$greenper[$rs]-$redperc[$rs]; ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">Loyalty Index</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($loyaltyindex[$rs])?"0":number_format(array_sum($loyaltyindex[$rs])/count($loyaltyindex[$rs]),2); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">RMA cases OE</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($rmacaseoe[$rs])?'NA':number_format(array_sum($rmacaseoe[$rs])/count($rmacaseoe[$rs]),2); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">RMA cases NPS</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($loyalty_index[$rs])?'NA':number_format(array_sum($loyalty_index[$rs])/count($loyalty_index[$rs]),2); ?></td>
	                        	<?php	}?>
                        	</tr>
                        	<tr>
	                        	<td style="background-color:#F2784B;color:white;">No Of Working days</td>
	                        	<td>-</td>
	                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
	                        		<td style="text-align:center;font-size:12px"><?php echo empty($workingdays[$rs])?"NA":$workingdays[$rs]; ?></td>
	                        	<?php	}?>
                        	</tr>
							<tr>
	                        	<td rowspan="2" style="vertical-align: inherit;background-color:#F2784B;color:white;">Quality</td>
		                        	<td style="background-color:#F2784B;color:white;">SSQA</td>
		                        	<td>80%</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) {	?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallssqa[$rs])?"NA":round($overallssqa[$rs],2)."%"; ?></td>
		                        	<?php	} ?>
	                        	</tr>
								<tr>
		                        	<td style="background-color:#F2784B;color:white;">Process Audit</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallpa[$rs])?"NA":round($overallpa[$rs],2)."%"; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
								<td rowspan="4" style="vertical-align: inherit;background-color:#F2784B;color:white;">Escalation</td>
		                        	<td style="background-color:#F2784B;color:white;">Escalation</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallesc[$rs])?"NA":$overallesc[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td style="background-color:#F2784B;color:white;">ESC-Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallcontrol[$rs])?"NA":round($overallcontrol[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td style="background-color:#F2784B;color:white;">ESC-Uncontrollable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overalluncontrol[$rs])?"NA":round($overalluncontrol[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
		                        	<td style="background-color:#F2784B;color:white;">ESC-Blank</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallblank[$rs])?"NA":round($overallblank[$rs]/$overallt1[$rs]*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
								<tr>
								<td colspan="2"  style="background-color:#F2784B;color:white;">Outage</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($overallout[$rs])?"NA":round((1-($overallout[$rs]/($overalloutcnt[$rs]*$workingdays[$rs])))*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        		<tr>
	                        		<td rowspan="6" style="background-color:#F2784B;color:white;">RCA</td>
		                        	<td  style="background-color:#F2784B;color:white;">Count</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($csatCnt[$rs])?"NA":$csatCnt[$rs]; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">Aruba Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
		                        		$Controllable = $rcasplitArr[$rs]['Aruba Controllable']+$rcasplitArr[$rs]['Controllable'];
		                        	?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($Controllable)?'0%':number_format($Controllable/array_sum($rcasplitArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">TAC Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($rcasplitArr[$rs]['TAC Controllable'])?'0%':number_format($rcasplitArr[$rs]['TAC Controllable']/array_sum($rcasplitArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">WC Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($rcasplitArr[$rs]['WC Controllable'])?'0%':number_format($rcasplitArr[$rs]['WC Controllable']/array_sum($rcasplitArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">Un-Controllable</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { 
		                        		$uncontrollable = $rcasplitArr[$rs]['Un-Controllable']+$rcasplitArr[$rs]['Uncontrollable'];
		                        	?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($uncontrollable)?'0%':number_format($uncontrollable/array_sum($rcasplitArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
	                        	<tr>
		                        	<td style="background-color:#F2784B;color:white;">Blanks</td>
		                        	<td>-</td>
		                        	<?php foreach ($tablehead as $inflowkey=> $rs) { ?>
		                        		<td style="text-align:center;font-size:12px"><?php echo empty($rcasplitArr[$rs]['Blanks'])?'0%':number_format($rcasplitArr[$rs]['Blanks']/array_sum($rcasplitArr[$rs])*100,2).'%'; ?></td>
		                        	<?php	}?>
	                        	</tr>
                        </tbody>
                    </table>
		    </div>
		</div>
</form>

