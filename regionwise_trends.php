<?php
echo "<pre>";
$resArr =Array
(
    'Americas' => Array
        (
            'Green' => Array
                (
                    '0' => 64.15,
                    '1' => 75.51,
                    '2' => 64.81,
                    '3' => 75.68,
                    '4' => 83.33,
                    '5' => 77.55,
                ),

            'Normal' => Array
                (
                    '0' => 26.42,
                    '1' => 16.33,
                    '2' => 31.48,
                    '3' => 18.92,
                    '4' => 13.33,
                    '5' => 16.33,
                ),

            'Red' => Array
                (
                    '0' => 9.43,
                    '1' => 8.16,
                    '2' => 3.70,
                    '3' => 5.41,
                    '4' => 3.33,
                    '5' => 6.12,
                ),

            'NA' => Array
                (
                    '0' => 0,
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                    '5' => 0,
                ),

        ),

    'APAC' => Array
        (
            'Green' => Array
                (
                    '0' => 66.67,
                    '1' => 75.00,
                    '2' => 75.00,
                    '3' => 92.31,
                    '4' => 70.83,
                    '5' => 71.43,
                ),

            'Normal' => Array
                (
                    '0' => 25.00,
                    '1' => 15.00,
                    '2' => 16.67,
                    '3' => 7.69,
                    '4' => 20.83,
                    '5' => 21.43,
                ),

            'Red' => Array
                (
                    '0' => 8.33,
                    '1' => 10.00,
                    '2' => 8.33,
                    '3' => 0,
                    '4' => 8.33,
                    '5' => 7.14,
                ),

            'NA' => Array
                (
                    '0' => 0,
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                    '5' => 0,
                ),

        ),

    'EMEA' => Array
        (
            'Green' => Array
                (
                    '0' => 35.71,
                    '1' => 66.67,
                    '2' => 55.56,
                    '3' => 68.42,
                    '4' => 72.22,
                    '5' => 58.82,
                ),

            'Normal' => Array
                (
                    '0' => 57.14,
                    '1' => 22.22,
                    '2' => 40.74,
                    '3' => 31.58,
                    '4' => 27.78,
                    '5' => 35.29,
                ),

            'Red' => Array
                (
                    '0' => 7.14,
                    '1' => 11.11,
                    '2' => 3.70,
                    '3' => 0,
                    '4' => 0,
                    '5' => 5.88,
                ),

            'NA' => Array
                (
                    '0' => 0,
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                    '5' => 0
                ),

        )

);

print_r($resArr);
?>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
Highcharts.chart('container', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Total fruit consumtion, grouped by gender'
    },

    xAxis: {
        categories: ['week1','week2','week3','week4','week5','week6']
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Number of fruits'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },
    colors: ['#1ED570','#FF7F00','#FF0000'],
    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [
	<?php
	$i=0;
	foreach($resArr as $wkkey => $wkArr){
		foreach($wkArr as $key => $lastArr){
			?> 
			{
				name: '<?php echo $wkkey .'-'. $key; ?>',
				data: <?php echo json_encode($lastArr);?>,
				stack: '<?php echo $i; ?>'
			},
			<?php
		}
		$i++;
	}
	?>
	]
});
</script>

<?php
// series: [{
//         name: 'Red',
//         data: [2, 5, 6, 2, 1],
//         stack: '1'
//     }, {
//         name: 'Normal',
//         data: [2, 5, 6, 2, 1],
//         stack: '1'
//     },{
//         name: 'Green',
//         data: [5, 3, 4, 7, 2],
//         stack: '1'
//     },{
//         name: 'Green',
//         data: [5, 3, 4, 7, 2],
//         stack: '2'
//     }, {
//         name: 'Normal',
//         data: [3, 3, 4, 4, 3],
//         stack: '2'
//     }, {
//         name: 'Red',
//         data: [3, 3, 4, 4, 3],
//         stack: '2'
//     }, {
//         name: 'Green',
//         data: [3, 4, 4, 2, 5],
//         stack: '3'
//     }, {
//         name: 'Normal',
//         data: [3, 2, 4, 4, 3],
//         stack: '3'
//     }, {
//        name: 'Red',
//         data: [3, 2, 4, 4, 3],
//         stack: '3'
//     }]
?>